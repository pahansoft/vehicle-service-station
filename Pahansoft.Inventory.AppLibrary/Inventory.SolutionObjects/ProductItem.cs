﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class ProductItem : SolutionObjectBase
    {
        public ProductItem() { }
        public ProductItem(int? id) { this.ID = id; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string OtherDescription { get; set; }
        public string ReferenceNumber { get; set; }
        public SolutionObjects.BrandName BrandName { get; set; }
        public SolutionObjects.Manufacturer Manufacturer { get; set; }
        public SolutionObjects.ProductType ProductType { get; set; }
        public int? ROL { get; set; }
        public int? ROQuantity { get; set; }
        public SolutionObjects.Country Country{ get; set; }
        public SolutionObjects.SalesRef SalesRef { get; set; }
        public string Model { get; set; }
        public Decimal CostPrice { get; set; }
        public Decimal SellingPrice { get; set; }
        public string Colour { get; set; }
        public bool IsBrandNewItem { get; set; }

        public int AutoNum { get; set; }
    }
}
