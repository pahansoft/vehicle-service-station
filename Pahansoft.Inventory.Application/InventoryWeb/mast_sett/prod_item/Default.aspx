﻿<%@ Page Language="C#" MasterPageFile="~/shrd/mast.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="mast_sett_prod_item_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../../inc_fils/js/jquery.js"></script>
    <script type="text/javascript" src="../../../inc_fils/js/main_menu.js"></script>
    <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">Product Item -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonSearchOperation_Click">Search Product Item</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New Product Item</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Product Item Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%">Code <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxCode" runat="server" Text="- AUTO GENERATED -" 
                                                                        CssClass="textbox" MaxLength="20" Width="154px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription0" runat="server"
                                                                        ControlToValidate="TextBoxCode" Display="Dynamic" ErrorMessage="Required" ForeColor="Red"
                                                                        ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Description <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxDescription" runat="server" CssClass="textbox" MaxLength="50"
                                                                        Width="250px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                                                                        ControlToValidate="TextBoxDescription" Display="Dynamic" ErrorMessage="Required"
                                                                        ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">
                                                                    Other Description</td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxOtherDescription" runat="server" CssClass="textbox" 
                                                                        MaxLength="50" Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Part Number
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxReferenceNumber" runat="server" 
                                                                        CssClass="textbox" MaxLength="50"
                                                                        Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Origin</td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListCountry" runat="server"
                                                                        CssClass="dropdownlist" Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Brand Name
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListBrandName" runat="server"
                                                                        CssClass="dropdownlist" Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Manufacturer
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListManufacturer" runat="server" CssClass="dropdownlist"
                                                                        Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Importer</td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListImporter" runat="server"
                                                                        CssClass="dropdownlist" Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Product Type
                <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListProductType" runat="server" CssClass="dropdownlist"
                                                                        Width="250px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription1"
                                                                        runat="server" ControlToValidate="DropDownListProductType" Display="Dynamic"
                                                                        ErrorMessage="Required" ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Model</td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxModel" runat="server" CssClass="textbox"
                                                                        MaxLength="50" Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Colour</td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxColour" runat="server" CssClass="textbox"
                                                                        MaxLength="50" Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                          <%--  \\----------------\\--%>
                                                             <tr>
                                                                <td style="width: 20%">Cost Price</td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxCostPrice" runat="server" CssClass="textbox"
                                                                        MaxLength="20" Width="100px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                               <tr>
                                                                <td style="width: 20%">Selling Price</td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxSellingPrice" runat="server" CssClass="textbox"
                                                                        MaxLength="20" Width="100px"></asp:TextBox>
                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <td style="width: 20%">ROL <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxROL" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                                        ControlToValidate="TextBoxROL" Display="Dynamic" ErrorMessage="Required"
                                                                        ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator2" runat="server"
                                                                        ControlToValidate="TextBoxROL" Display="Dynamic" ErrorMessage="Invalid"
                                                                        MaximumValue="100000000" MinimumValue="1" Type="Integer"
                                                                        ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">RO Quantity <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxROQuantity" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxROQuantity"
                                                                        Display="Dynamic" ErrorMessage="Required" ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="TextBoxROQuantity"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="100000000" MinimumValue="1"
                                                                        Type="Integer" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Brand New Item</td>
                                                                <td>
                                                                    <asp:CheckBox ID="CheckBoxBrandNewItem" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td class="pageButtonArea">
                                                            <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                                                                OnClick="ButtonSubmit_Click" Width="70px" />
                                                            &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                   OnClick="ButtonReset_Click" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Search Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%">Code
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxSrchCode" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Description
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxSrchDescription" runat="server" CssClass="textbox" MaxLength="50"
                                                                        Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Serial Number
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:TextBox ID="TextBoxSrchReferenceNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Product Type
                                                                </td>
                                                                <td>
                                                                    &nbsp;<asp:DropDownList ID="DropDownListSrchProductType" runat="server" CssClass="dropdownlist"
                                                                        Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Brand New Items Only</td>
                                                                <td>
                                                                    <asp:CheckBox ID="CheckBoxSrchBrandNewItemsOnly" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="pageButtonArea">
                                                                    <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                                                                        Width="70px" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
                                                    OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
                                                    OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                    <Columns>
                                                        <asp:ButtonField CommandName="EditRecord" Text="Edit">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="ViewRecord" Text="View">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField HeaderText="Code">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Description">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Serial Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Origin">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Brand Name">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Product Type">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Brand New">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="grid-pager" />
                                                    <EmptyDataTemplate>
                                                        <div id="norec" class="no-record-msg">
                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-left: 15px">No Record(s) Found!
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="grid-header" />
                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Product Item Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 20%;">Code
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 79%;">
                                                                    <asp:Label ID="LabelViewCode" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Description
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewDescription" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Part Number
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewReferenceNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Origin</td>
                                                                <td style="text-align: center;">:</td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewOrigin" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Brand Name
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewBrandName" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Manufacturer
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewManufacturer" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Importer</td>
                                                                <td style="text-align: center;">:</td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewImporter" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Product Type
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewProductType" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Model</td>
                                                                <td style="text-align: center;">:</td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewModel" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Colour
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewColour" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>ROL
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewROL" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>RO Quantity
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewROQuantity" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Brand New Item
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewBrandNewItem" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                           
                                                            <tr>
                                                                <td>Cost Price
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewCostPrice" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                                <tr class="view-back">
                                                                <td>Selling Price
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewSellingPrice" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%" 
        style="margin-top: 60px">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
