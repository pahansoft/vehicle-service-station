﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Location : SolutionObjectBase
    {
        public Location() { }
        public Location(int? id) { this.ID = id; }

        public SolutionObjects.Store Store { get; set; }
        public string Description { get; set; }
    }
}
