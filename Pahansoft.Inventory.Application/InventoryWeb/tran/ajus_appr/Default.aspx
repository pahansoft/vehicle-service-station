﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
 CodeFile="Default.aspx.cs" Inherits="tran_ajus_appr_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
 <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
 <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground">
     <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
       <td class="pageHedder">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
         <tr>
          <td style="height: 17px">
           Stock Adjustment Approval -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
          </td>
          <td align="right" style="height: 17px">
           <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonSearchOperation_Click">Search Stock Adjustment</asp:LinkButton>
          </td>
         </tr>
        </table>
       </td>
      </tr>
      <tr>
       <td>
        <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke;
             border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid;
             border-bottom: gainsboro 1px solid;" cellpadding="0" cellspacing="0">
             <tr>
              <td style="height: 50px">
               <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px; width: 35px">
                   <img src="../../images/warning.png" />
                  </td>
                  <td>
                   <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                  </td>
                 </tr>
                </table>
               </asp:Panel>
               <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px;">
                   <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                   &nbsp;
                  </td>
                  <td align="right" style="padding-right: 20px">
                   <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                    Style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                    Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                  </td>
                 </tr>
                </table>
               </asp:Panel>
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelSearchDetails" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Search Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Adjustment Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchAdjustmentNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Date Adjustment
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchDateAdjustment" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateAdjustment"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="search"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Status
                </td>
                <td>
                 <asp:DropDownList ID="DropDownListSrchStatus" runat="server" CssClass="dropdownlist"
                  Width="100px">
                 </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td>
                </td>
                <td class="pageButtonArea">
                 <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                  Width="70px" ValidationGroup="search" />
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
             CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
             OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
             OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
             <PagerSettings Mode="NumericFirstLast" />
             <RowStyle CssClass="grid-row-normal-stc" />
             <Columns>
              <asp:ButtonField CommandName="ViewRecord" Text="View">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:BoundField HeaderText="Adjustment Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Date Adjustment">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Remarks">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Status">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
             </Columns>
             <PagerStyle CssClass="grid-pager" />
             <EmptyDataTemplate>
              <div id="norec" class="no-record-msg">
               <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                <tbody>
                 <tr>
                  <td style="padding-left: 15px">
                   No Record(s) Found!
                  </td>
                 </tr>
                </tbody>
               </table>
              </div>
             </EmptyDataTemplate>
             <HeaderStyle CssClass="grid-header" />
             <AlternatingRowStyle CssClass="grid-row-alt-stc" />
            </asp:GridView>
            <table border="0" style="border-collapse: collapse" width="100%">
             <tr>
              <td style="text-align: right">
               <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                 <td style="text-align: right">
                  &nbsp;
                 </td>
                </tr>
                <tr>
                 <td style="text-align: right">
                  <strong>
                   <label id="entries">
                    Entries:</label>&nbsp;<asp:DropDownList ID="DropDownListShowEntries" runat="server"
                     AutoPostBack="True" CssClass="dropdownlist" onChange="document.getElementById('wait').style.display=''; document.getElementById('entries').style.display='none'; this.style.display='none';"
                     OnSelectedIndexChanged="DropDownListShowEntries_SelectedIndexChanged">
                     <asp:ListItem Value="10">10</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>50</asp:ListItem>
                     <asp:ListItem>75</asp:ListItem>
                     <asp:ListItem>100</asp:ListItem>
                     <asp:ListItem>125</asp:ListItem>
                     <asp:ListItem>150</asp:ListItem>
                    </asp:DropDownList>
                   <label id="wait" onclick="document.getElementById('<%=DropDownListShowEntries.ClientID%>').style.display=''; document.getElementById('entries').style.display=''; this.style.display='none';"
                    style="display: none; color: red">
                    Wait..</label>
                  </strong>
                 </td>
                </tr>
               </table>
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Stock Adjustment Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr class="view-back">
                <td style="width: 20%;">
                 Adjustment Number
                </td>
                <td style="width: 1%; text-align: center;">
                 :
                </td>
                <td style="width: 79%;">
                 <asp:Label ID="LabelViewAdjustmentNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Date Adjustment
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewDateAdjustment" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Remarks
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <fieldset>
             <legend style="font-weight: bold">Adjustment Item(s)</legend>
             <div style="padding: 10px;">
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td>
                  <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                   OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                   <PagerSettings Mode="NumericFirstLast" />
                   <RowStyle CssClass="grid-row-normal-stc" />
                   <Columns>
                    <asp:BoundField HeaderText="Product Item">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Type">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Location">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Adjustment Quantity">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Return Quantity">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                   </Columns>
                   <PagerStyle CssClass="grid-pager" />
                   <EmptyDataTemplate>
                    <div id="norec" class="no-record-msg">
                     <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                      <tbody>
                       <tr>
                        <td style="padding-left: 15px">
                         No Items(s) Found!
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                   </EmptyDataTemplate>
                   <HeaderStyle CssClass="grid-header" />
                   <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                  </asp:GridView>
                 </td>
                </tr>
               </tbody>
              </table>
             </div>
            </fieldset>
            <br />
            <fieldset>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td>
                 Status <span style="color: red">*</span>
                </td>
                <td>
                 <asp:DropDownList ID="DropDownListStatus" runat="server" CssClass="dropdownlist"
                  Width="100px">
                 </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td style="vertical-align: text-top; padding-top: 3px; width: 20%;">
                 Reason
                </td>
                <td>
                 <asp:TextBox ID="TextBoxReason" runat="server" CssClass="textbox" MaxLength="20"
                  Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <table border="0" style="width: 100%">
             <tr>
              <td class="pageButtonArea">
               <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                OnClick="ButtonSubmit_Click" Width="70px" />
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </asp:Panel>
 <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px;
     padding-top: 15px" align="right">
     <img src="../../../images/AccessDenied.jpg" />
    </td>
    <caption>
     <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold;
      color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute;
       top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
    </caption>
   </tr>
  </table>
 </asp:Panel>
 <script language="javascript">
  $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateAdjustment").mask("99/99/9999");
 </script>
</asp:Content>
