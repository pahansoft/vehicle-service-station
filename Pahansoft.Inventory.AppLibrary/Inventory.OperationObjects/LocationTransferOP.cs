﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class LocationTransferOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveLocationTransfer(Inventory.SolutionObjects.LocationTransfer locationTransfer)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (locationTransfer.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.LocationTransferDO().Create(locationTransfer);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new LocationTransferDetail List
                    foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in locationTransfer.LocationTransferDetailList)
                    {
                        innerDTO = new DataAccessObjects.LocationTransferDetailDO().Create(locationTransferDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(locationTransfer);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in locationTransfer.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.LocationTransferDO().Edit(locationTransfer);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit LocationTransferDetail List
                    foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in locationTransfer.LocationTransferDetailList)
                    {
                        if (locationTransferDetail.ID == null)
                        {
                            innerDTO = new DataAccessObjects.LocationTransferDetailDO().Create(locationTransferDetail);
                        }
                        else
                            innerDTO = new DataAccessObjects.LocationTransferDetailDO().Edit(locationTransferDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete LocationTransferDetail List
                    foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in locationTransfer.LocationTransferDetailList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.LocationTransferDetailDO().Delete(locationTransferDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(locationTransfer);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in locationTransfer.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteLocationTransfer(Inventory.SolutionObjects.LocationTransfer locationTransfer)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.LocationTransferDO().Delete(locationTransfer);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete LocationTransferDetail List
                foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in locationTransfer.LocationTransferDetailList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.LocationTransferDetailDO().Delete(locationTransferDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                CreateItemStock(locationTransfer);

                // Add, Edit ItemStock List
                foreach (SolutionObjects.ItemStock itemStock in locationTransfer.ItemStockList)
                {
                    innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.LocationTransfer GetLocationTransfer(int id)
        {
            return new Inventory.DataAccessObjects.LocationTransferDO().Find(id);
        }

        public Inventory.SolutionObjects.LocationTransfer GetLocationTransferProxy(int id)
        {
            return new Inventory.DataAccessObjects.LocationTransferDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> GetAllLocationTransfers()
        {
            return new Inventory.DataAccessObjects.LocationTransferDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> SearchLocationTransfers(string transferNumber, DateTime? dateTransfer, string referenceNumber)
        {
            return new Inventory.DataAccessObjects.LocationTransferDO().Search(transferNumber, dateTransfer, referenceNumber);
        }

        private SolutionObjects.LocationTransfer CreateItemStock(SolutionObjects.LocationTransfer _locationTransfer)
        {
            _locationTransfer.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            SolutionObjects.ItemStock _fromItemStock = null;
            SolutionObjects.ItemStock _toItemStock = null;
            SolutionObjects.ItemStockTransaction _itemStockTransaction;

            foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in _locationTransfer.LocationTransferDetailList)
            {
                _fromItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(locationTransferDetail.ItemStock.ID.Value);
                _fromItemStock.CurrentQuantity -= locationTransferDetail.Quantity.Value;
                _fromItemStock.User = locationTransferDetail.User;
                _fromItemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _fromItemStock;
                _itemStockTransaction.ProductItem = _fromItemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.LocationTransfer;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _locationTransfer.ID;
                _itemStockTransaction.TransactionDetailID = locationTransferDetail.ID;
                _itemStockTransaction.ReferenceNumber = _locationTransfer.TransferNumber;
                _itemStockTransaction.Store = _fromItemStock.Store;
                _itemStockTransaction.Location = _fromItemStock.Location;
                _itemStockTransaction.Rack = _fromItemStock.Rack;
                _itemStockTransaction.Quantity = locationTransferDetail.Quantity;
                _itemStockTransaction.UnitPrice = _fromItemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _fromItemStock.SellingPrice;
                _itemStockTransaction.User = _locationTransfer.User;
                _fromItemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _locationTransfer.ItemStockList.Add(_fromItemStock);

                _toItemStock = null;
                IList<SolutionObjects.ItemStock> _itemStockList = new OperationObjects.ItemStockOP().Search(_fromItemStock.ProductItem, locationTransferDetail.ToStore, locationTransferDetail.ToLocation, locationTransferDetail.ToRack, SolutionObjects.StockTypes.Trading, _fromItemStock.LotNumber, _fromItemStock.UnitPrice, _fromItemStock.SellingPrice);
                if (_itemStockList.Count > 0)
                {
                    _toItemStock = _itemStockList[0];
                    _toItemStock.CurrentQuantity += locationTransferDetail.Quantity.Value;
                    _toItemStock.User = locationTransferDetail.User;
                }
                else
                {
                    _toItemStock = new SolutionObjects.ItemStock();
                    _toItemStock.CurrentQuantity = locationTransferDetail.Quantity.Value;
                    _toItemStock.ProductItem = _fromItemStock.ProductItem;
                    _toItemStock.StockTypes = SolutionObjects.StockTypes.Trading;
                    _toItemStock.LotNumber = _fromItemStock.LotNumber;
                    _toItemStock.Store = locationTransferDetail.ToStore;
                    _toItemStock.Location = locationTransferDetail.ToLocation;
                    _toItemStock.Rack = locationTransferDetail.ToRack;
                    _toItemStock.UnitPrice = _fromItemStock.UnitPrice;
                    _toItemStock.SellingPrice = _fromItemStock.SellingPrice;
                    _toItemStock.DateManufactured = _fromItemStock.DateManufactured;
                    _toItemStock.DateExpiry = _fromItemStock.DateExpiry;
                    _toItemStock.User = _locationTransfer.User;
                }

                _toItemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _toItemStock;
                _itemStockTransaction.ProductItem = _toItemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.LocationTransfer;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _locationTransfer.ID;
                _itemStockTransaction.TransactionDetailID = locationTransferDetail.ID;
                _itemStockTransaction.ReferenceNumber = _locationTransfer.TransferNumber;
                _itemStockTransaction.Store = _toItemStock.Store;
                _itemStockTransaction.Location = _toItemStock.Location;
                _itemStockTransaction.Rack = _toItemStock.Rack;
                _itemStockTransaction.Quantity = locationTransferDetail.Quantity;
                _itemStockTransaction.UnitPrice = _toItemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _toItemStock.SellingPrice;
                _itemStockTransaction.User = _locationTransfer.User;
                _toItemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _locationTransfer.ItemStockList.Add(_toItemStock);
            }

            foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in _locationTransfer.LocationTransferDetailList.RemovedList)
            {
                if (locationTransferDetail.ID == null)
                    continue;

                _fromItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(locationTransferDetail.ItemStock.ID.Value);
                _fromItemStock.CurrentQuantity += locationTransferDetail.Quantity.Value;
                _fromItemStock.User = locationTransferDetail.User;
                _fromItemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _fromItemStock;
                _itemStockTransaction.ProductItem = _fromItemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.LocationTransfer;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _locationTransfer.ID;
                _itemStockTransaction.TransactionDetailID = locationTransferDetail.ID;
                _itemStockTransaction.ReferenceNumber = _locationTransfer.TransferNumber;
                _itemStockTransaction.Store = _fromItemStock.Store;
                _itemStockTransaction.Location = _fromItemStock.Location;
                _itemStockTransaction.Rack = _fromItemStock.Rack;
                _itemStockTransaction.Quantity = locationTransferDetail.Quantity;
                _itemStockTransaction.UnitPrice = _fromItemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _fromItemStock.SellingPrice;
                _itemStockTransaction.Remarks = "Delete Location Transfer Detail";
                _itemStockTransaction.User = _locationTransfer.User;
                _fromItemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _locationTransfer.ItemStockList.Add(_fromItemStock);

                IList<SolutionObjects.ItemStock> _itemStockList = new OperationObjects.ItemStockOP().Search(_fromItemStock.ProductItem, locationTransferDetail.ToStore, locationTransferDetail.ToLocation, locationTransferDetail.ToRack, SolutionObjects.StockTypes.Trading, _fromItemStock.LotNumber, _fromItemStock.UnitPrice, _fromItemStock.SellingPrice);
                _toItemStock = _itemStockList[0];
                _toItemStock.CurrentQuantity -= locationTransferDetail.Quantity.Value;
                _toItemStock.User = locationTransferDetail.User;
                _toItemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _toItemStock;
                _itemStockTransaction.ProductItem = _toItemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.LocationTransfer;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _locationTransfer.ID;
                _itemStockTransaction.TransactionDetailID = locationTransferDetail.ID;
                _itemStockTransaction.ReferenceNumber = _locationTransfer.TransferNumber;
                _itemStockTransaction.Store = _toItemStock.Store;
                _itemStockTransaction.Location = _toItemStock.Location;
                _itemStockTransaction.Rack = _toItemStock.Rack;
                _itemStockTransaction.Quantity = locationTransferDetail.Quantity;
                _itemStockTransaction.UnitPrice = _toItemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _toItemStock.SellingPrice;
                _itemStockTransaction.Remarks = "Delete Location Transfer Detail";
                _itemStockTransaction.User = _locationTransfer.User;
                _toItemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _locationTransfer.ItemStockList.Add(_toItemStock);
            }

            return _locationTransfer;
        }
    }
}
