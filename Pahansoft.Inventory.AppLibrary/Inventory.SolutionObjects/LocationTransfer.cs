﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class LocationTransfer : SolutionObjectBase
    {
        public LocationTransfer() { }
        public LocationTransfer(int? id) { this.ID = id; }

        public string TransferNumber { get; set; }
        public DateTime? DateTransfer { get; set; }
        public string ReferenceNumber { get; set; }
        public string Remarks { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail> LocationTransferDetailList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
