﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Text;

public partial class repo_item_Default : System.Web.UI.Page
{
    public const string targetProducts = "TargetProducts";

    private void fillDropdownList()
    {
        Util.ClearDropdown(DropDownListItemType);
        foreach (SolutionObjects.ProductType productType in new OperationObjects.ProductTypeOP().GetAllProductTypes())
        {

            DropDownListItemType.Items.Add(new ListItem(productType.Description, productType.ID.Value.ToString()));
        }

    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetProductItems(string Text)
    {
        var ProductItems = HttpContext.Current.Session[targetProducts] as IList<SolutionObjects.ItemStock>;

        var returnList = new List<string>();
        foreach (var item in ProductItems.Where(t => t.ProductItem.Code.ToLower().Contains(Text.ToLower()) || t.ProductItem.ReferenceNumber.ToLower().Contains(Text.ToLower()) || t.ProductItem.Description.ToLower().Contains(Text.ToLower())).ToList())
        {
            var sbDescription = new StringBuilder();
            //sbDescription.Append(item.ProductItem.Code);
            //sbDescription.Append(item.ProductItem.ReferenceNumber != string.Empty ? ("~" + item.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append(item.ProductItem.Description);
            //sbDescription.Append("-").Append(item.ProductItem.Description);
            //sbDescription.Append("|").Append(item.SellingPrice.Value.ToString("F"));
            returnList.Add(sbDescription.ToString());
        }

        return returnList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            fillDropdownList();

            IList<SolutionObjects.ItemStock> itemStocks = new List<SolutionObjects.ItemStock>();
            foreach (var item in new OperationObjects.ItemStockOP().GetAllItemStocks())
            {
                if (item.CurrentQuantity > 0)
                {
                    item.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(item.ProductItem.ID.Value);
                    itemStocks.Add(item);
                }
            }
            HttpContext.Current.Session[targetProducts] = itemStocks;

        }
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        int? ProductItem = null;
        string code = null;
        string Description = null;
        string PartNumber = null;
        bool? BrandNewOnly = null;

        if (TextBoxItemCode.Text.Trim() != string.Empty)
            code = TextBoxItemCode.Text;
        if (TextBoxDescription.Text.Trim() != string.Empty)
            Description = TextBoxDescription.Text;
        if (TextBoxPartNumber.Text.Trim() != string.Empty)
            PartNumber = TextBoxPartNumber.Text;
        if (CheckBoxBrandNewOnly.Checked)
            BrandNewOnly = true;


        if (DropDownListItemType.SelectedIndex > 0)
            ProductItem = int.Parse(DropDownListItemType.SelectedValue);

        HtmlGenericControl dispval = new HtmlGenericControl();
        dispval.InnerHtml = "<script language='javascript'> print('" + ProductItem + "','" + code + "','" + Description + "','" + PartNumber + "','" + BrandNewOnly + "');</script>";
        Page.Controls.Add(dispval);
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {

    }
    protected void DropDownListItemType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
   
}