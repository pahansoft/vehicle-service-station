﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class ProductType : SolutionObjectBase
    {
        public ProductType() { }
        public ProductType(int? id) { this.ID = id; }

        public string Description { get; set; }
    }
}
