﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CompanyOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Savecompany(Inventory.SolutionObjects.Company company)
        {
            if (company.ID == null)
                return new Inventory.DataAccessObjects.CompanyDO().Create(company);
            else
                return new Inventory.DataAccessObjects.CompanyDO().Edit(company);
        }

        public Pahansoft.CommonObjects.DataTransferObject Deletecompany(Inventory.SolutionObjects.Company company)
        {
            if (company.ID != null)
                return new Inventory.DataAccessObjects.CompanyDO().Delete(company);
            else
            {
                company.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = company;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Company GetCompany(int id)
        {
            return new Inventory.DataAccessObjects.CompanyDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Company> GetAllCompanys()
        {
            return new Inventory.DataAccessObjects.CompanyDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Company> SearchCompanys(string description)
        {
            return new Inventory.DataAccessObjects.CompanyDO().Search(description);
        }
    }
}
