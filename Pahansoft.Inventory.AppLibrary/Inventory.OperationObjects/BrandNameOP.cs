﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class BrandNameOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveBrandName(Inventory.SolutionObjects.BrandName brandName)
        {
            if (brandName.ID == null)
                return new Inventory.DataAccessObjects.BrandNameDO().Create(brandName);
            else
                return new Inventory.DataAccessObjects.BrandNameDO().Edit(brandName);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteBrandName(Inventory.SolutionObjects.BrandName brandName)
        {
            if (brandName.ID != null)
                return new Inventory.DataAccessObjects.BrandNameDO().Delete(brandName);
            else
            {
                brandName.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = brandName;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.BrandName GetBrandName(int id)
        {
            return new Inventory.DataAccessObjects.BrandNameDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> GetAllBrandNames()
        {
            return new Inventory.DataAccessObjects.BrandNameDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> SearchBrandNames(string description, SolutionObjects.Manufacturer manufacturer)
        {
            return new Inventory.DataAccessObjects.BrandNameDO().Search(description, manufacturer);
        }
    }
}
