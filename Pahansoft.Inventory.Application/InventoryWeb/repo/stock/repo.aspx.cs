﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion


public partial class repo_stock_repo_repo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int? Store = null;
        int? Location = null;
        int? Rack = null;


        if (Request.Params["Store"].ToString() != string.Empty)
            Store = int.Parse(Request.Params["Store"].ToString());
        if (Request.Params["Location"].ToString() != string.Empty)
            Location = int.Parse(Request.Params["Location"].ToString());
        if (Request.Params["Rack"].ToString() != string.Empty)
            Rack = int.Parse(Request.Params["Rack"].ToString());


        Generate(Store, Location, Rack);
    }
    private int GetPageCount(int count, int linesPerPage)
    {
        int _pageCount = 0;
        if (count > linesPerPage)
        {
            if (count % linesPerPage == 0)
                _pageCount = count / linesPerPage;
            else
                _pageCount = count / linesPerPage + 1;
        }
        else
            _pageCount = 1;

        return _pageCount;
    }
    private void Generate(int? StoreID, int? LocaionID, int? RackID)
    {


        SolutionObjects.Store store = null;
        if (StoreID != null) store = new SolutionObjects.Store(StoreID);

        SolutionObjects.Location location = null;
        if (LocaionID != null) location = new SolutionObjects.Location(LocaionID);

        SolutionObjects.Rack rack = null;
        if (RackID != null) rack = new SolutionObjects.Rack(RackID);


        var list = new OperationObjects.ItemStockOP().Search(null, store, location, rack, null, null, null, null);
        if (list.Count == 0)
            return;

        int _linesPerPage = 1000;
        int _pageCount = GetPageCount(list.Count, _linesPerPage);
        int _innerCount = 0;
        int _listIndex = 0;
        StringBuilder _sb = new StringBuilder();
        StringBuilder inneSb = new StringBuilder();

        for (int i = 0; i < _pageCount; i++)
        {
            if (_listIndex == list.Count)
                break;
            _sb.Append("<div style=\"page-break-after: always;\">");
            _sb.Append("<table align=\"center\" style=\"width: 90%;  font-family:Courier New;\">");

            #region - Page Header -

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"text-align: center; font-size:large; font-weight:bold;\"> SERVICE STATION STOCK REPORT</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"text-align: center; font-size:small; font-weight:bold;\">&nbsp;</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #endregion
            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">");
            _sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\" cellpadding=\"3\">");

            _sb.Append("<tr style=\"text-align: center; font-size:small; font-weight:bold; height:30px; background-color: #F5F5F5;\">");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Item No</td>");
            _sb.Append("<td style=\"width : 20%; text-align: center;\">Item Name</td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Part Number</td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Store</td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Location </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Rack </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Current Quantity</td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Unit Price</td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Seling Price</td>");
            _sb.Append("</tr>");


            _innerCount = 0;
            for (int j = _listIndex; j < list.Count; j++)
            {
                _sb.Append("<tr style=\"font-size:small;\">");
                
                    SolutionObjects.ProductItem itemnoValue = new OperationObjects.ProductItemOP().GetProductItem(int.Parse(list[_listIndex].ProductItem.ID.ToString()));
                    if (itemnoValue != null)
                { 
                       _sb.Append("<td>" + itemnoValue.Code + "</td>");
                       _sb.Append("<td>" + itemnoValue.Description + " " + itemnoValue.OtherDescription + "</td>");
                       _sb.Append("<td>" + itemnoValue.ReferenceNumber + "</td>");

                }
                else
                {
                    _sb.Append("<td>-</td>");
                    _sb.Append("<td>-</td>");
                    _sb.Append("<td>-</td>");
                }


                SolutionObjects.Store storeValue = new OperationObjects.StoreOP().GetStore(int.Parse(list[_listIndex].Store.ID.ToString()));
                _sb.Append("<td>" + storeValue.Description + "</td>");

                SolutionObjects.Location locationValue = new OperationObjects.LocationOP().GetLocation(int.Parse(list[_listIndex].Location.ID.ToString()));
                _sb.Append("<td>" + locationValue.Description + "</td>");

                SolutionObjects.Rack rackValue = new OperationObjects.RackOP().GetRack(int.Parse(list[_listIndex].Rack.ID.ToString()));
                _sb.Append("<td>" + rackValue.Description + "</td>");


                _sb.Append("<td>" + list[_listIndex].CurrentQuantity.ToString() + "</td>");
                _sb.Append("<td style=\"width : 15%; text-align: right;\">" + list[_listIndex].UnitPrice.ToString() + "</td>");
                _sb.Append("<td style=\"width : 15%; text-align: right;\">" + list[_listIndex].SellingPrice.ToString() + "</td>");

                //_sb.Append("<td>" + list[_listIndex].StockType + list[_listIndex].StockType + "</td>");
                // _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].Store + "</td>");
                // _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].Location + "</td>");
                //_sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].Rack + "</td>");


                if (_innerCount == _linesPerPage - 1)
                {
                    _listIndex += 1;
                    break;
                }
                else
                {
                    if (_listIndex == list.Count - 1)
                        break;
                    else
                    {
                        _listIndex += 1;
                        _innerCount += 1;

                    }
                }
            }

            _sb.Append("</table>");
            _sb.Append("</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">&nbsp;</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #region - Footer -

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">");
            _sb.Append("<table style=\"width: 100%;\">");

            _sb.Append("<tr style=\"font-size:small;\">");
            _sb.Append("<td style=\"width : 50%\">" + ("Page [" + (i + 1) + "] of [" + _pageCount + "]") + "</td>");
            _sb.Append("<td style=\"width : 50%; text-align: right;\">" + (DateTime.Today.ToShortDateString() + "&nbsp;&nbsp;" + DateTime.Now.ToShortTimeString()) + "</td>");
            _sb.Append("</tr>");

            _sb.Append("</table>");
            _sb.Append("</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #endregion

            _sb.Append("</table>");
            _sb.Append("</div>");
        }
        LabelReportBody1.Text = _sb.ToString();

    }

}