﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class CashCollectionSummary : SolutionObjectBase
    {
        public CashCollectionSummary() { }
        public CashCollectionSummary(int? id) { this.ID = id; }

        public string CashCollectionNumber { get; set; }
        public SolutionObjects.CashCollector CashCollector { get; set; }
        public DateTime? DateCollected { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? CommissionAmount { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail> CashCollectionDetailList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.CashCollectorCommission> CashCollectorCommissionList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.CustomerPayment> CustomerPaymentList { get; set; }
    }
}
