﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class PurchaseOrderDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail)
        {
            if (PurchaseOrderDetail.ID == null)
                return new DataAccessObjects.PurchaseOrderDetailDO().Create(PurchaseOrderDetail);
            else
                return new DataAccessObjects.PurchaseOrderDetailDO().Edit(PurchaseOrderDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail)
        {
            if (PurchaseOrderDetail.ID != null)
                return new DataAccessObjects.PurchaseOrderDetailDO().Delete(PurchaseOrderDetail);
            else
            {
                PurchaseOrderDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = PurchaseOrderDetail;
                return _dts;
            }
        }

        public SolutionObjects.PurchaseOrderDetail Get(int id)
        {
            return new DataAccessObjects.PurchaseOrderDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrderDetail> GetAll()
        {
            return new DataAccessObjects.PurchaseOrderDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrderDetail> GetAllFor(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            return new DataAccessObjects.PurchaseOrderDetailDO().GetAllFor(PurchaseOrder);
        }
    }
}
