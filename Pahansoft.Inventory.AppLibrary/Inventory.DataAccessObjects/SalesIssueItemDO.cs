﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SalesIssueItemDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.SalesIssueItem salesIssueItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateSalesIssueItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssueItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.SalesIssue != null ? salesIssueItem.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.ItemStock != null ? salesIssueItem.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IssuedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.IssuedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Discount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Discount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Tax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Tax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Gross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Gross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Net", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Net);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.ReturnedQuantity);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssueItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesIssueItem.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.SalesIssueItem salesIssueItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditSalesIssueItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesIssueItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssueItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.SalesIssue != null ? salesIssueItem.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.ItemStock != null ? salesIssueItem.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IssuedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.IssuedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Discount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Discount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Tax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Tax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Gross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Gross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Net", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.Net);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssueItem.ReturnedQuantity);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssueItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.SalesIssueItem salesIssueItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteSalesIssueItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssueItem.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesIssueItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssueItem.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssueItem;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesIssueItem.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.SalesIssueItem Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetSalesIssueItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.SalesIssueItem salesIssueItem = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                        int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                        int issuedQuantityOrdinal = dr.GetOrdinal("IssuedQuantity");
                        int discountOrdinal = dr.GetOrdinal("Discount");
                        int taxOrdinal = dr.GetOrdinal("Tax");
                        int grossOrdinal = dr.GetOrdinal("Gross");
                        int netOrdinal = dr.GetOrdinal("Net");
                        int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");

                        salesIssueItem = new Inventory.SolutionObjects.SalesIssueItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssueItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesIssueItem.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) salesIssueItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(issuedQuantityOrdinal)) salesIssueItem.IssuedQuantity = dr.GetDecimal(issuedQuantityOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) salesIssueItem.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) salesIssueItem.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossOrdinal)) salesIssueItem.Gross = dr.GetDecimal(grossOrdinal);
                        if (!dr.IsDBNull(netOrdinal)) salesIssueItem.Net = dr.GetDecimal(netOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) salesIssueItem.ReturnedQuantity = dr.GetDecimal(returnedQuantityOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssueItem;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllSalesIssueItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> salesIssueItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesIssueItem>();
            Inventory.SolutionObjects.SalesIssueItem salesIssueItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int issuedQuantityOrdinal = dr.GetOrdinal("IssuedQuantity");
                    int discountOrdinal = dr.GetOrdinal("Discount");
                    int taxOrdinal = dr.GetOrdinal("Tax");
                    int grossOrdinal = dr.GetOrdinal("Gross");
                    int netOrdinal = dr.GetOrdinal("Net");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");

                    while (dr.Read())
                    {
                        salesIssueItem = new Inventory.SolutionObjects.SalesIssueItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssueItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesIssueItem.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) salesIssueItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(issuedQuantityOrdinal)) salesIssueItem.IssuedQuantity = dr.GetDecimal(issuedQuantityOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) salesIssueItem.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) salesIssueItem.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossOrdinal)) salesIssueItem.Gross = dr.GetDecimal(grossOrdinal);
                        if (!dr.IsDBNull(netOrdinal)) salesIssueItem.Net = dr.GetDecimal(netOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) salesIssueItem.ReturnedQuantity = dr.GetDecimal(returnedQuantityOrdinal);

                        salesIssueItemList.Add(salesIssueItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssueItemList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> GetAllFor(SolutionObjects.SalesIssue salesIssue)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllSalesIssueItemsForSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> salesIssueItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesIssueItem>();
            Inventory.SolutionObjects.SalesIssueItem salesIssueItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int issuedQuantityOrdinal = dr.GetOrdinal("IssuedQuantity");
                    int discountOrdinal = dr.GetOrdinal("Discount");
                    int taxOrdinal = dr.GetOrdinal("Tax");
                    int grossOrdinal = dr.GetOrdinal("Gross");
                    int netOrdinal = dr.GetOrdinal("Net");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");

                    while (dr.Read())
                    {
                        salesIssueItem = new Inventory.SolutionObjects.SalesIssueItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssueItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesIssueItem.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) salesIssueItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(issuedQuantityOrdinal)) salesIssueItem.IssuedQuantity = dr.GetDecimal(issuedQuantityOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) salesIssueItem.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) salesIssueItem.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossOrdinal)) salesIssueItem.Gross = dr.GetDecimal(grossOrdinal);
                        if (!dr.IsDBNull(netOrdinal)) salesIssueItem.Net = dr.GetDecimal(netOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) salesIssueItem.ReturnedQuantity = dr.GetDecimal(returnedQuantityOrdinal);

                        salesIssueItemList.Add(salesIssueItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssueItemList;
        }

        #endregion
    }
}
