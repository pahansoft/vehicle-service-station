﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Manufacturer : SolutionObjectBase
    {
        public Manufacturer() { }
        public Manufacturer(int? id) { this.ID = id; }

        public string Description { get; set; }
        public SolutionObjects.Country Country { get; set; }
    }
}
