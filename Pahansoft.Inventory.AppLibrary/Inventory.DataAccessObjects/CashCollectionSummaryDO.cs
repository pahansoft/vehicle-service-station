﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CashCollectionSummaryDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionSummary.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CashCollectionNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CashCollector != null ? cashCollectionSummary.CashCollector.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCollected", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, cashCollectionSummary.DateCollected);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.TotalAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CommissionAmount);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionSummary;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectionSummary.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectionSummary.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionSummary.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CashCollectionNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CashCollector != null ? cashCollectionSummary.CashCollector.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCollected", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, cashCollectionSummary.DateCollected);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.TotalAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionSummary.CommissionAmount);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionSummary;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectionSummary.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionSummary.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionSummary;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectionSummary.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.CashCollectionSummary Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int cashCollectionNumberOrdinal = dr.GetOrdinal("CashCollectionNumber");
                        int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                        int dateCollectedOrdinal = dr.GetOrdinal("DateCollected");
                        int totalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                        cashCollectionSummary = new Inventory.SolutionObjects.CashCollectionSummary(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionSummary.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionNumberOrdinal)) cashCollectionSummary.CashCollectionNumber = dr.GetString(cashCollectionNumberOrdinal);
                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectionSummary.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(dateCollectedOrdinal)) cashCollectionSummary.DateCollected = dr.GetDateTime(dateCollectedOrdinal);
                        if (!dr.IsDBNull(totalAmountOrdinal)) cashCollectionSummary.TotalAmount = dr.GetDecimal(totalAmountOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectionSummary.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectionSummary.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        cashCollectionSummary.CashCollectionDetailList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail>();
                        foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in new DataAccessObjects.CashCollectionDetailDO().GetAllFor(cashCollectionSummary))
                            cashCollectionSummary.CashCollectionDetailList.Add(cashCollectionDetail);

                        cashCollectionSummary.CashCollectorCommissionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.CashCollectorCommission>();
                        foreach (SolutionObjects.CashCollectorCommission cashCollectorCommission in new DataAccessObjects.CashCollectorCommissionDO().GetAllFor(cashCollectionSummary))
                            cashCollectionSummary.CashCollectorCommissionList.Add(cashCollectorCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionSummary;
        }

        public Inventory.SolutionObjects.CashCollectionSummary FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int cashCollectionNumberOrdinal = dr.GetOrdinal("CashCollectionNumber");
                        int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                        int dateCollectedOrdinal = dr.GetOrdinal("DateCollected");
                        int totalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                        cashCollectionSummary = new Inventory.SolutionObjects.CashCollectionSummary(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionSummary.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionNumberOrdinal)) cashCollectionSummary.CashCollectionNumber = dr.GetString(cashCollectionNumberOrdinal);
                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectionSummary.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(dateCollectedOrdinal)) cashCollectionSummary.DateCollected = dr.GetDateTime(dateCollectedOrdinal);
                        if (!dr.IsDBNull(totalAmountOrdinal)) cashCollectionSummary.TotalAmount = dr.GetDecimal(totalAmountOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectionSummary.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectionSummary.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionSummary;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCashCollectionSummaries";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> cashCollectionSummaryList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectionSummary>();
            Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectionNumberOrdinal = dr.GetOrdinal("CashCollectionNumber");
                    int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                    int dateCollectedOrdinal = dr.GetOrdinal("DateCollected");
                    int totalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        cashCollectionSummary = new Inventory.SolutionObjects.CashCollectionSummary(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionSummary.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionNumberOrdinal)) cashCollectionSummary.CashCollectionNumber = dr.GetString(cashCollectionNumberOrdinal);
                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectionSummary.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(dateCollectedOrdinal)) cashCollectionSummary.DateCollected = dr.GetDateTime(dateCollectedOrdinal);
                        if (!dr.IsDBNull(totalAmountOrdinal)) cashCollectionSummary.TotalAmount = dr.GetDecimal(totalAmountOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectionSummary.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectionSummary.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        cashCollectionSummaryList.Add(cashCollectionSummary);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionSummaryList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> Search(string cashCollectionNumber, DateTime? dateCollected, SolutionObjects.CashCollector cashCollector)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllCashCollectionSummaries";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionNumber != string.Empty ? cashCollectionNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCollected", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, dateCollected != null ? dateCollected : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollector != null ? cashCollector.ID : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> cashCollectionSummaryList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectionSummary>();
            Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectionNumberOrdinal = dr.GetOrdinal("CashCollectionNumber");
                    int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                    int dateCollectedOrdinal = dr.GetOrdinal("DateCollected");
                    int totalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        cashCollectionSummary = new Inventory.SolutionObjects.CashCollectionSummary(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionSummary.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionNumberOrdinal)) cashCollectionSummary.CashCollectionNumber = dr.GetString(cashCollectionNumberOrdinal);
                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectionSummary.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(dateCollectedOrdinal)) cashCollectionSummary.DateCollected = dr.GetDateTime(dateCollectedOrdinal);
                        if (!dr.IsDBNull(totalAmountOrdinal)) cashCollectionSummary.TotalAmount = dr.GetDecimal(totalAmountOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectionSummary.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectionSummary.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        cashCollectionSummaryList.Add(cashCollectionSummary);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionSummaryList;
        }

        #endregion
    }
}
