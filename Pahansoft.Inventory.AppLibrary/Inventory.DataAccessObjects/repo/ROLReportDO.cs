﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace Inventory.DataAccessObjects.repo
{
  public class ROLReportDO

       {
           #region --- Search ---
      public System.Collections.Generic.IList<Inventory.SolutionObjects.repo.ROLReport> Search( int? store, int? location, int? rack, int? ProductType)
         
           {
               System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
               command.CommandText = "T_SP_GetAllROLStocks";
               command.CommandType = CommandType.StoredProcedure;
               Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, store != null ? store : null);
               Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, location != null ? location : null);
               Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack != null ? rack : null);
               Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductType", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, ProductType != null ? ProductType : null);
              
               Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
               System.Collections.Generic.IList<Inventory.SolutionObjects.repo.ROLReport> itemStockList = new System.Collections.Generic.List<Inventory.SolutionObjects.repo.ROLReport>();
               Inventory.SolutionObjects.repo.ROLReport rolreport;

               if (command.Connection.State == ConnectionState.Open)
               {
                   System.Data.IDataReader dr = command.ExecuteReader();

                   if (dr.RecordsAffected != 0)
                   {

                       int CodeOrdinal = dr.GetOrdinal("Code");
                       int DescriptionOrdinal = dr.GetOrdinal("Description");
                       int ReferenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                       int OtherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");
                       int ROLOrdinal = dr.GetOrdinal("ROL");
                       int QuantityOrdinal = dr.GetOrdinal("Quantity");

                       while (dr.Read())
                       {
                           rolreport = new Inventory.SolutionObjects.repo.ROLReport();


                           if (!dr.IsDBNull(CodeOrdinal)) rolreport.Code = dr.GetString(CodeOrdinal);
                           if (!dr.IsDBNull(DescriptionOrdinal)) rolreport.Description = dr.GetString(DescriptionOrdinal);
                           if (!dr.IsDBNull(ReferenceNumberOrdinal)) rolreport.ReferenceNumber = dr.GetString(ReferenceNumberOrdinal);
                           if (!dr.IsDBNull(OtherDescriptionOrdinal)) rolreport.OtherDescription = dr.GetString(OtherDescriptionOrdinal);
                           if (!dr.IsDBNull(ROLOrdinal)) rolreport.ROL = dr.GetInt32(ROLOrdinal);
                           if (!dr.IsDBNull(QuantityOrdinal)) rolreport.Quantity = dr.GetDecimal(QuantityOrdinal);
                          
                           itemStockList.Add(rolreport);
                       }
                   }
               }

               Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
               return itemStockList;
           }

         
           #endregion


       }
}
