using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for Util
/// </summary>
public class Util
{
    public Util()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string RenderControl(Control ctrl)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        ctrl.RenderControl(hw);
        return sb.ToString();
    }

    public static DataSet GetData(string cmdstring)
    {
        string result = string.Empty;
        string strConnection = string.Empty;
        DataSet ds = new DataSet();
        try
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["PahansoftConnectionString"].ToString());
            con.Open();

            SqlDataAdapter objadapter = new SqlDataAdapter(cmdstring, con);
            objadapter.Fill(ds);
            con.Close();
        }
        catch (Exception)
        {
            throw;
        }
        return ds;
    }

    public static string ExecuteStatement(string commandSQL)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PahansoftConnectionString"].ToString());
        System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(commandSQL);
        command.Connection = con;
        string _message = string.Empty;

        try
        {
            command.Connection.Open();
            command.ExecuteNonQuery();
            _message = "OK";
        }
        catch (Exception ex)
        {
            _message = ex.Message;
        }
        finally
        {
            if (con != null)
            {
                if (con.State != ConnectionState.Closed)
                    con.Close();

                con.Dispose();
                con = null;
            }
        }

        return _message;
    }

    public static void EncodeString(TableCell cell, string text, int maxLength)
    {
        string writeText = "";
        cell.Wrap = false;
        if (text != null)
        {
            text = cell.Page.Server.HtmlEncode(text) + "";
            if (text.Length > maxLength)
            {
                writeText = text.Substring(0, maxLength - 1) + " ...";
                //writeText = "<ins datetime=\"null\" nicetitle=\"" + text + "\" style=\"text-decoration: none\">" + writeText + "</ins>";
                cell.ToolTip = text;
            }
            else
            {
                writeText = text;
            }
        }
        if (string.IsNullOrEmpty(writeText))
            writeText = "<center> - </center>";

        cell.Text = writeText;

    }

    public static void setCultureInfo(AjaxControlToolkit.MaskedEditExtender extender)
    {
        extender.CultureName = ConfigurationManager.AppSettings["CultureInfo"];
    }

    public static string FormatString(string text, int maxLength)
    {
        string writeText = "";
        if (text != null)
        {
            if (text.Length > maxLength)
            {
                writeText = text.Substring(0, maxLength - 1) + " ...";
            }
            else
            {
                writeText = text;
            }
        }
        return writeText;
    }

    public static string FormatEmptyString(string text)
    {
        string writeText = "";
        if (text == null)
            writeText = "-";
        else
        {
            if (text.Trim().Equals(string.Empty))
                writeText = "-";
            else
                writeText = text.Trim();
        }
        return writeText;
    }

    public static void ClearDropdown(DropDownList dropdownList)
    {
        dropdownList.Items.Clear();
        dropdownList.Items.Add(new ListItem("-- Select --", " "));
    }

    #region - Type Conversions-

    public static DateTime? ParseDate(string dateText)
    {
        dateText = dateText.Trim();
        DateTime dt;

        if (DateTime.TryParse(dateText, out dt))
        {
            if (dt.Year < 1901)
                return null;
            else
                return dt;
        }
        else
            return null;
    }

    public static int? ParseInt(string number)
    {
        number = number.Trim();
        int i;
        if (int.TryParse(number, out i))
            return i;
        else
            return null;
    }

    public static decimal? ParseDecimal(string number)
    {
        number = number.Trim();
        decimal i;
        if (decimal.TryParse(number, out i))
            return i;
        else
            return null;

    }

    #endregion

    #region --- Encrytion & Dycrypt ---

    public static string GetMd5Hash(MD5 md5Hash, string input)
    {

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

    // Verify a hash against a string.
    public static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
    {
        // Hash the input.
        string hashOfInput = GetMd5Hash(md5Hash, input);

        // Create a StringComparer an compare the hashes.
        StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        if (0 == comparer.Compare(hashOfInput, hash))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    #endregion

    public static string GenereteSequenceNumber(string prefix, int year, int month)
    {
        StringBuilder _sb = new StringBuilder();
        _sb.Append("DECLARE	@return_value INT, @DocumentNumber VARCHAR(50)");
        _sb.Append("EXEC [dbo].[R_SP_GenerateDocumentNumber]");
        _sb.Append("@Prefix = " + prefix + ",");
        _sb.Append("@TransactionYear = " + year.ToString() + ",");
        _sb.Append("@TransactionMonth = " + month.ToString() + ",");
        _sb.Append("@DocumentNumber = @DocumentNumber OUTPUT");

        DataTable _dt = GetData(_sb.ToString()).Tables[0];
        return _dt.Rows[0][0].ToString();
    }

}
