﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SalesRefCommissionOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSalesRefCommission(Inventory.SolutionObjects.SalesRefCommission salesRefCommission)
        {
            if (salesRefCommission.ID == null)
                return new Inventory.DataAccessObjects.SalesRefCommissionDO().Create(salesRefCommission);
            else
                return new Inventory.DataAccessObjects.SalesRefCommissionDO().Edit(salesRefCommission);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSalesRefCommission(Inventory.SolutionObjects.SalesRefCommission salesRefCommission)
        {
            if (salesRefCommission.ID != null)
                return new Inventory.DataAccessObjects.SalesRefCommissionDO().Delete(salesRefCommission);
            else
            {
                salesRefCommission.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = salesRefCommission;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.SalesRefCommission GetSalesRefCommission(int id)
        {
            return new Inventory.DataAccessObjects.SalesRefCommissionDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> GetAllCustomerPaymentEntries()
        {
            return new Inventory.DataAccessObjects.SalesRefCommissionDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> GetAllFor(SolutionObjects.SalesIssue salesIssue)
        {
            return new Inventory.DataAccessObjects.SalesRefCommissionDO().GetAllFor(salesIssue);
        }
    }
}
