﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography;

public partial class shrd_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonLogin_Click(object sender, EventArgs e)
    {
        //LabelMessage.Text = string.Empty;
        using (MD5 _md5Hash = MD5.Create())
            if (Admin.SignOnComponents.UserSecurity.ValidLoginAuthentication(TextBoxUserName.Text, Util.GetMd5Hash(_md5Hash, TextBoxPassword.Text.Trim())) == true)
            {
                FormsAuthentication.RedirectFromLoginPage(TextBoxUserName.Text, false);
                Session["UserName"] = TextBoxUserName.Text;

                if (Admin.SignOnComponents.UserSecurity.IsSuperUser(TextBoxUserName.Text))
                {

                    Session["UserType"] = "Super User";
                }
                else
                {
                    Session["UserType"] = "User";

                }


                Session["CompanyID"] = Admin.SignOnComponents.UserSecurity.GetUserPrimaryCompanyID(TextBoxUserName.Text);
                // save user login
                Admin.SignOnComponents.UserLoginInfo.UserLogin(TextBoxUserName.Text);
            }
            else
            {
                Session["CompanyID"] = null;

                //LabelMessage.Text = "Invalid User Name Or Password.";
                return;
            }
    }
}