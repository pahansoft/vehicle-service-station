﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SalesRefOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSalesRef(Inventory.SolutionObjects.SalesRef salesRef)
        {
            if (salesRef.ID == null)
                return new Inventory.DataAccessObjects.SalesRefDO().Create(salesRef);
            else
                return new Inventory.DataAccessObjects.SalesRefDO().Edit(salesRef);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSalesRef(Inventory.SolutionObjects.SalesRef salesRef)
        {
            if (salesRef.ID != null)
                return new Inventory.DataAccessObjects.SalesRefDO().Delete(salesRef);
            else
            {
                salesRef.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = salesRef;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.SalesRef GetSalesRef(int id)
        {
            return new Inventory.DataAccessObjects.SalesRefDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> GetAllSalesRefs()
        {
            return new Inventory.DataAccessObjects.SalesRefDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> SearchSalesRefs(string code, string name)
        {
            return new Inventory.DataAccessObjects.SalesRefDO().Search(code, name);
        }
    }
}
