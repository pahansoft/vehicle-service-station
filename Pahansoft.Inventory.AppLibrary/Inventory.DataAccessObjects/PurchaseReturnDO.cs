﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class PurchaseReturnDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.PurchaseReturn purchaseReturn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreatePurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.ReturnNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateReturn", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, purchaseReturn.DateReturn);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, purchaseReturn.GRN != null ? purchaseReturn.GRN.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, purchaseReturn.TotalNetAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.Remarks);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = purchaseReturn;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                purchaseReturn.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.PurchaseReturn purchaseReturn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditPurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, purchaseReturn.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, purchaseReturn.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.ReturnNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateReturn", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, purchaseReturn.DateReturn);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, purchaseReturn.GRN != null ? purchaseReturn.GRN.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, purchaseReturn.TotalNetAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.Remarks);
          
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = purchaseReturn;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.PurchaseReturn purchaseReturn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeletePurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, purchaseReturn.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, purchaseReturn.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, purchaseReturn.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = purchaseReturn;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                purchaseReturn.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.PurchaseReturn Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.PurchaseReturn purchaseReturn = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int returnNumberOrdinal = dr.GetOrdinal("ReturnNumber");
                        int dateReturnOrdinal = dr.GetOrdinal("DateReturn");
                        int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                        int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        purchaseReturn = new Inventory.SolutionObjects.PurchaseReturn(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) purchaseReturn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(returnNumberOrdinal)) purchaseReturn.ReturnNumber = dr.GetString(returnNumberOrdinal);
                        if (!dr.IsDBNull(dateReturnOrdinal)) purchaseReturn.DateReturn = dr.GetDateTime(dateReturnOrdinal);
                        if (!dr.IsDBNull(gRNIDOrdinal)) purchaseReturn.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) purchaseReturn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) purchaseReturn.Remarks = dr.GetString(remarksOrdinal);

                        purchaseReturn.PurchaseReturnItemList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.PurchaseReturnItem>();
                        foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in new DataAccessObjects.PurchaseReturnItemDO().GetAllFor(purchaseReturn))
                            purchaseReturn.PurchaseReturnItemList.Add(purchaseReturnItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return purchaseReturn;
        }

        public Inventory.SolutionObjects.PurchaseReturn FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.PurchaseReturn purchaseReturn = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int returnNumberOrdinal = dr.GetOrdinal("ReturnNumber");
                        int dateReturnOrdinal = dr.GetOrdinal("DateReturn");
                        int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                        int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        purchaseReturn = new Inventory.SolutionObjects.PurchaseReturn(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) purchaseReturn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(returnNumberOrdinal)) purchaseReturn.ReturnNumber = dr.GetString(returnNumberOrdinal);
                        if (!dr.IsDBNull(dateReturnOrdinal)) purchaseReturn.DateReturn = dr.GetDateTime(dateReturnOrdinal);
                        if (!dr.IsDBNull(gRNIDOrdinal)) purchaseReturn.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) purchaseReturn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) purchaseReturn.Remarks = dr.GetString(remarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return purchaseReturn;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturn> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseReturns";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturn> purchaseReturnList = new System.Collections.Generic.List<Inventory.SolutionObjects.PurchaseReturn>();
            Inventory.SolutionObjects.PurchaseReturn purchaseReturn;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int returnNumberOrdinal = dr.GetOrdinal("ReturnNumber");
                    int dateReturnOrdinal = dr.GetOrdinal("DateReturn");
                    int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                    int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        purchaseReturn = new Inventory.SolutionObjects.PurchaseReturn(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) purchaseReturn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(returnNumberOrdinal)) purchaseReturn.ReturnNumber = dr.GetString(returnNumberOrdinal);
                        if (!dr.IsDBNull(dateReturnOrdinal)) purchaseReturn.DateReturn = dr.GetDateTime(dateReturnOrdinal);
                        if (!dr.IsDBNull(gRNIDOrdinal)) purchaseReturn.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) purchaseReturn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) purchaseReturn.Remarks = dr.GetString(remarksOrdinal);

                        purchaseReturnList.Add(purchaseReturn);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return purchaseReturnList;
        }

        #endregion
    }
}
