﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class JobCardDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.JobCard JobCard)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.JobNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.VehicleNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.Customer != null ? JobCard.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Address", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.Address);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.TelephoneNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StartDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.StartDate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@EndDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.EndDate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupervisorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.Supervisor != null ? JobCard.Supervisor.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalGross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNet", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalNet);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsApproved", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, JobCard.IsApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedBy", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.ApprovedBy);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CompanyID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.Company !=null?JobCard.Company.ID:null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCard;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCard.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.JobCard JobCard)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCard.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.JobNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.VehicleNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.Customer != null ? JobCard.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Address", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.Address);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.TelephoneNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StartDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.StartDate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@EndDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.EndDate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupervisorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.Supervisor != null ? JobCard.Supervisor.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalGross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNet", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCard.TotalNet);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsApproved", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, JobCard.IsApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, JobCard.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedBy", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.ApprovedBy);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CompanyID", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.Company != null ? JobCard.Company.ID : null);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCard;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.JobCard JobCard)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCard.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCard.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCard;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCard.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.JobCard Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.JobCard JobCard = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int JobNumberOrdinal = dr.GetOrdinal("JobNumber");
                        int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                        int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                        int AddressOrdinal = dr.GetOrdinal("Address");
                        int TelephoneNumberOrdinal = dr.GetOrdinal("TelephoneNumber");
                        int StartDateOrdinal = dr.GetOrdinal("StartDate");
                        int EndDateOrdinal = dr.GetOrdinal("EndDate");
                        int SupervisorIDOrdinal = dr.GetOrdinal("SupervisorID");
                        int TotalGrossOrdinal = dr.GetOrdinal("TotalGross");
                        int TotalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int TotalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int TotalNetOrdinal = dr.GetOrdinal("TotalNet");
                        int RemarksOrdinal = dr.GetOrdinal("Remarks");
                        int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                        int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");
                        int CompanyOrdinal = dr.GetOrdinal("CompanyID");


                        JobCard = new Inventory.SolutionObjects.JobCard(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCard.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobNumberOrdinal)) JobCard.JobNumber = dr.GetString(JobNumberOrdinal);
                        if (!dr.IsDBNull(VehicleNumberOrdinal)) JobCard.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) JobCard.Customer =new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(AddressOrdinal)) JobCard.Address = dr.GetString(AddressOrdinal);
                        if (!dr.IsDBNull(TelephoneNumberOrdinal)) JobCard.TelephoneNumber = dr.GetString(TelephoneNumberOrdinal);
                        if (!dr.IsDBNull(StartDateOrdinal)) JobCard.StartDate = dr.GetDateTime(StartDateOrdinal);
                        if (!dr.IsDBNull(EndDateOrdinal)) JobCard.EndDate = dr.GetDateTime(EndDateOrdinal);
                        if (!dr.IsDBNull(SupervisorIDOrdinal)) JobCard.Supervisor  =new SolutionObjects.Supervisor(dr.GetInt32(SupervisorIDOrdinal));
                        if (!dr.IsDBNull(TotalGrossOrdinal)) JobCard.TotalGross = dr.GetDecimal(TotalGrossOrdinal);
                        if (!dr.IsDBNull(TotalDiscountOrdinal)) JobCard.TotalDiscount = dr.GetDecimal(TotalDiscountOrdinal);
                        if (!dr.IsDBNull(TotalTaxOrdinal)) JobCard.TotalTax = dr.GetDecimal(TotalTaxOrdinal);
                        if (!dr.IsDBNull(TotalNetOrdinal)) JobCard.TotalNet = dr.GetDecimal(TotalNetOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) JobCard.Remarks = dr.GetString(RemarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) JobCard.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) JobCard.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) JobCard.ApprovedBy = dr.GetString(ApprovedByOrdinal);
                        if (!dr.IsDBNull(CompanyOrdinal)) JobCard.Company =new SolutionObjects.Company( dr.GetInt32(CompanyOrdinal));

                        JobCard.ItemList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.JobCardItem>();
                        foreach (var item in new DataAccessObjects.JobCardItemDO().GetAllFor(JobCard))
                            JobCard.ItemList.Add(item);

                        JobCard.ServiceList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.JobCardService>();
                        foreach (var item in new DataAccessObjects.JobCardServiceDO().GetAllFor(JobCard))
                            JobCard.ServiceList.Add(item);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCard;
        }

        public Inventory.SolutionObjects.JobCard FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.JobCard JobCard = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int JobNumberOrdinal = dr.GetOrdinal("JobNumber");
                        int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                        int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                        int AddressOrdinal = dr.GetOrdinal("Address");
                        int TelephoneNumberOrdinal = dr.GetOrdinal("TelephoneNumber");
                        int StartDateOrdinal = dr.GetOrdinal("StartDate");
                        int EndDateOrdinal = dr.GetOrdinal("EndDate");
                        int SupervisorIDOrdinal = dr.GetOrdinal("SupervisorID");
                        int TotalGrossOrdinal = dr.GetOrdinal("TotalGross");
                        int TotalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int TotalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int TotalNetOrdinal = dr.GetOrdinal("TotalNet");
                        int RemarksOrdinal = dr.GetOrdinal("Remarks");
                        int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                        int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");
                        int CompanyOrdinal = dr.GetOrdinal("CompanyID");

                        JobCard = new Inventory.SolutionObjects.JobCard(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCard.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobNumberOrdinal)) JobCard.JobNumber = dr.GetString(JobNumberOrdinal);
                        if (!dr.IsDBNull(VehicleNumberOrdinal)) JobCard.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) JobCard.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(AddressOrdinal)) JobCard.Address = dr.GetString(AddressOrdinal);
                        if (!dr.IsDBNull(TelephoneNumberOrdinal)) JobCard.TelephoneNumber = dr.GetString(TelephoneNumberOrdinal);
                        if (!dr.IsDBNull(StartDateOrdinal)) JobCard.StartDate = dr.GetDateTime(StartDateOrdinal);
                        if (!dr.IsDBNull(EndDateOrdinal)) JobCard.EndDate = dr.GetDateTime(EndDateOrdinal);
                        if (!dr.IsDBNull(SupervisorIDOrdinal)) JobCard.Supervisor = new SolutionObjects.Supervisor(dr.GetInt32(SupervisorIDOrdinal));
                        if (!dr.IsDBNull(TotalGrossOrdinal)) JobCard.TotalGross = dr.GetDecimal(TotalGrossOrdinal);
                        if (!dr.IsDBNull(TotalDiscountOrdinal)) JobCard.TotalDiscount = dr.GetDecimal(TotalDiscountOrdinal);
                        if (!dr.IsDBNull(TotalTaxOrdinal)) JobCard.TotalTax = dr.GetDecimal(TotalTaxOrdinal);
                        if (!dr.IsDBNull(TotalNetOrdinal)) JobCard.TotalNet = dr.GetDecimal(TotalNetOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) JobCard.Remarks = dr.GetString(RemarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) JobCard.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) JobCard.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) JobCard.ApprovedBy = dr.GetString(ApprovedByOrdinal);
                        if (!dr.IsDBNull(CompanyOrdinal)) JobCard.Company = new SolutionObjects.Company(dr.GetInt32(CompanyOrdinal));


                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCard;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCard> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllJobCards";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCard> JobCardList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCard>();
            Inventory.SolutionObjects.JobCard JobCard;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobNumberOrdinal = dr.GetOrdinal("JobNumber");
                    int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int AddressOrdinal = dr.GetOrdinal("Address");
                    int TelephoneNumberOrdinal = dr.GetOrdinal("TelephoneNumber");
                    int StartDateOrdinal = dr.GetOrdinal("StartDate");
                    int EndDateOrdinal = dr.GetOrdinal("EndDate");
                    int SupervisorIDOrdinal = dr.GetOrdinal("SupervisorID");
                    int TotalGrossOrdinal = dr.GetOrdinal("TotalGross");
                    int TotalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int TotalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int TotalNetOrdinal = dr.GetOrdinal("TotalNet");
                    int RemarksOrdinal = dr.GetOrdinal("Remarks");
                    int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                    int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");
                    int CompanyOrdinal = dr.GetOrdinal("CompanyID");
                    while (dr.Read())
                    {
                        JobCard = new Inventory.SolutionObjects.JobCard(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCard.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobNumberOrdinal)) JobCard.JobNumber = dr.GetString(JobNumberOrdinal);
                        if (!dr.IsDBNull(VehicleNumberOrdinal)) JobCard.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) JobCard.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(AddressOrdinal)) JobCard.Address = dr.GetString(AddressOrdinal);
                        if (!dr.IsDBNull(TelephoneNumberOrdinal)) JobCard.TelephoneNumber = dr.GetString(TelephoneNumberOrdinal);
                        if (!dr.IsDBNull(StartDateOrdinal)) JobCard.StartDate = dr.GetDateTime(StartDateOrdinal);
                        if (!dr.IsDBNull(EndDateOrdinal)) JobCard.EndDate = dr.GetDateTime(EndDateOrdinal);
                        if (!dr.IsDBNull(SupervisorIDOrdinal)) JobCard.Supervisor = new SolutionObjects.Supervisor(dr.GetInt32(SupervisorIDOrdinal));
                        if (!dr.IsDBNull(TotalGrossOrdinal)) JobCard.TotalGross = dr.GetDecimal(TotalGrossOrdinal);
                        if (!dr.IsDBNull(TotalDiscountOrdinal)) JobCard.TotalDiscount = dr.GetDecimal(TotalDiscountOrdinal);
                        if (!dr.IsDBNull(TotalTaxOrdinal)) JobCard.TotalTax = dr.GetDecimal(TotalTaxOrdinal);
                        if (!dr.IsDBNull(TotalNetOrdinal)) JobCard.TotalNet = dr.GetDecimal(TotalNetOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) JobCard.Remarks = dr.GetString(RemarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) JobCard.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) JobCard.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) JobCard.ApprovedBy = dr.GetString(ApprovedByOrdinal);
                        if (!dr.IsDBNull(CompanyOrdinal)) JobCard.Company = new SolutionObjects.Company(dr.GetInt32(CompanyOrdinal));


                        JobCardList.Add(JobCard);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCard> Search(string JobNumber, string VehicleNumber, int? CustomerID, bool? ApproveOnly)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllJobCards";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobNumber != string.Empty ? JobNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleNumber != string.Empty ? VehicleNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, CustomerID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedOnly", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, ApproveOnly);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCard> JobCardList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCard>();
            Inventory.SolutionObjects.JobCard JobCard;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobNumberOrdinal = dr.GetOrdinal("JobNumber");
                    int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int AddressOrdinal = dr.GetOrdinal("Address");
                    int TelephoneNumberOrdinal = dr.GetOrdinal("TelephoneNumber");
                    int StartDateOrdinal = dr.GetOrdinal("StartDate");
                    int EndDateOrdinal = dr.GetOrdinal("EndDate");
                    int SupervisorIDOrdinal = dr.GetOrdinal("SupervisorID");
                    int TotalGrossOrdinal = dr.GetOrdinal("TotalGross");
                    int TotalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int TotalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int TotalNetOrdinal = dr.GetOrdinal("TotalNet");
                    int RemarksOrdinal = dr.GetOrdinal("Remarks");
                    int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                    int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");
                    int CompanyOrdinal = dr.GetOrdinal("CompanyID");

                    while (dr.Read())
                    {
                        JobCard = new Inventory.SolutionObjects.JobCard(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCard.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobNumberOrdinal)) JobCard.JobNumber = dr.GetString(JobNumberOrdinal);
                        if (!dr.IsDBNull(VehicleNumberOrdinal)) JobCard.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) JobCard.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(AddressOrdinal)) JobCard.Address = dr.GetString(AddressOrdinal);
                        if (!dr.IsDBNull(TelephoneNumberOrdinal)) JobCard.TelephoneNumber = dr.GetString(TelephoneNumberOrdinal);
                        if (!dr.IsDBNull(StartDateOrdinal)) JobCard.StartDate = dr.GetDateTime(StartDateOrdinal);
                        if (!dr.IsDBNull(EndDateOrdinal)) JobCard.EndDate = dr.GetDateTime(EndDateOrdinal);
                        if (!dr.IsDBNull(SupervisorIDOrdinal)) JobCard.Supervisor = new SolutionObjects.Supervisor(dr.GetInt32(SupervisorIDOrdinal));
                        if (!dr.IsDBNull(TotalGrossOrdinal)) JobCard.TotalGross = dr.GetDecimal(TotalGrossOrdinal);
                        if (!dr.IsDBNull(TotalDiscountOrdinal)) JobCard.TotalDiscount = dr.GetDecimal(TotalDiscountOrdinal);
                        if (!dr.IsDBNull(TotalTaxOrdinal)) JobCard.TotalTax = dr.GetDecimal(TotalTaxOrdinal);
                        if (!dr.IsDBNull(TotalNetOrdinal)) JobCard.TotalNet = dr.GetDecimal(TotalNetOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) JobCard.Remarks = dr.GetString(RemarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) JobCard.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) JobCard.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) JobCard.ApprovedBy = dr.GetString(ApprovedByOrdinal);
                        if (!dr.IsDBNull(CompanyOrdinal)) JobCard.Company = new SolutionObjects.Company(dr.GetInt32(CompanyOrdinal));

                        JobCardList.Add(JobCard);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardList;
        }

        #endregion
    }
}
