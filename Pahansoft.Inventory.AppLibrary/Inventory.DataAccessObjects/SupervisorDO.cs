﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SupervisorDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.Supervisor Supervisor)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateSupervisor";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = Supervisor;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                Supervisor.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.Supervisor Supervisor)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditSupervisor";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, Supervisor.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, Supervisor.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = Supervisor;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.Supervisor Supervisor)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteSupervisor";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, Supervisor.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, Supervisor.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, Supervisor.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = Supervisor;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                Supervisor.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.Supervisor Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetSupervisor";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.Supervisor Supervisor = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int nameOrdinal = dr.GetOrdinal("Name");
                        int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                        int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                        int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                        int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                        int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                        int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                        int faxOrdinal = dr.GetOrdinal("Fax");
                        int emailOrdinal = dr.GetOrdinal("Email");

                        Supervisor = new Inventory.SolutionObjects.Supervisor(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) Supervisor.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) Supervisor.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) Supervisor.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) Supervisor.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) Supervisor.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) Supervisor.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) Supervisor.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) Supervisor.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) Supervisor.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) Supervisor.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) Supervisor.Email = dr.GetString(emailOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return Supervisor;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllSupervisors";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> SupervisorList = new System.Collections.Generic.List<Inventory.SolutionObjects.Supervisor>();
            Inventory.SolutionObjects.Supervisor Supervisor;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        Supervisor = new Inventory.SolutionObjects.Supervisor(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) Supervisor.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) Supervisor.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) Supervisor.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) Supervisor.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) Supervisor.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) Supervisor.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) Supervisor.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) Supervisor.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) Supervisor.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) Supervisor.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) Supervisor.Email = dr.GetString(emailOrdinal);

                        SupervisorList.Add(Supervisor);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return SupervisorList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> Search(string code, string name)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllSupervisors";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, name != string.Empty ? name : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> SupervisorList = new System.Collections.Generic.List<Inventory.SolutionObjects.Supervisor>();
            Inventory.SolutionObjects.Supervisor Supervisor;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        Supervisor = new Inventory.SolutionObjects.Supervisor(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) Supervisor.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) Supervisor.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) Supervisor.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) Supervisor.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) Supervisor.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) Supervisor.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) Supervisor.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) Supervisor.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) Supervisor.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) Supervisor.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) Supervisor.Email = dr.GetString(emailOrdinal);

                        SupervisorList.Add(Supervisor);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return SupervisorList;
        }

        #endregion
    }
}