﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CashCollectorCommissionOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCashCollectorCommission(Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission)
        {
            if (cashCollectorCommission.ID == null)
                return new Inventory.DataAccessObjects.CashCollectorCommissionDO().Create(cashCollectorCommission);
            else
                return new Inventory.DataAccessObjects.CashCollectorCommissionDO().Edit(cashCollectorCommission);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCashCollectorCommission(Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission)
        {
            if (cashCollectorCommission.ID != null)
                return new Inventory.DataAccessObjects.CashCollectorCommissionDO().Delete(cashCollectorCommission);
            else
            {
                cashCollectorCommission.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = cashCollectorCommission;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.CashCollectorCommission GetCashCollectorCommission(int id)
        {
            return new Inventory.DataAccessObjects.CashCollectorCommissionDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> GetAllCashCollectorCommissions()
        {
            return new Inventory.DataAccessObjects.CashCollectorCommissionDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> GetAllFor(SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            return new Inventory.DataAccessObjects.CashCollectorCommissionDO().GetAllFor(cashCollectionSummary);
        }
    }
}
