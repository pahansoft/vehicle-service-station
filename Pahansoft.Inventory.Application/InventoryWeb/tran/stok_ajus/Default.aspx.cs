﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_stok_ajus_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.StockAdjustment _stockAdjustment;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxAdjustmentNumber.Text = string.Empty;
        TextBoxDateAdjustment.Text = DateTime.Today.ToShortDateString();
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
        ClearFilterSearch();
    }

    private void ClearItem()
    {
        Util.ClearDropdown(DropDownListProductItem);
        DropDownListStockType.SelectedIndex = 0;
        DropDownListAdjustmentType.SelectedIndex = 0;
        DropDownListStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);
        LabelAvailableQuantity.Text = "-";
        TextBoxAdjustQuantity.Text = string.Empty;
        TextBoxReturnQuantity.Text = string.Empty;

        TRStore.Visible = false;
        TRLocation.Visible = false;
        TRRack.Visible = false;
        TRReturnQuantity.Visible = false;
        DropDownListAdjustmentType.Enabled = true;

        LabelValidateQuantity.Visible = false;
        LabelValidateReturnQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
    }

    private void ClearSearch()
    {
        TextBoxSrchAdjustmentNumber.Text = string.Empty;
        TextBoxSrchDateAdjustment.Text = string.Empty;
        DropDownListSrchStatus.SelectedIndex = 0;
    }

    private void ClearFilterSearch()
    {
        TextBoxFilterCode.Text = string.Empty;
        TextBoxFilterDescription.Text = string.Empty;
        TextBoxFilterLotNumber.Text = string.Empty;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
            DropDownListStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));

        Util.ClearDropdown(DropDownListStockType);
        DropDownListStockType.Items.Add(new ListItem(SolutionObjects.StockTypes.Trading.ToString(), SolutionObjects.StockTypes.Trading.ToString()));
        DropDownListStockType.Items.Add(new ListItem(SolutionObjects.StockTypes.Damaged.ToString(), SolutionObjects.StockTypes.Damaged.ToString()));
        //DropDownListStockType.Items.Add(new ListItem(SolutionObjects.StockTypes.Expired.ToString(), SolutionObjects.StockTypes.Expired.ToString()));

        Util.ClearDropdown(DropDownListAdjustmentType);
        DropDownListAdjustmentType.Items.Add(new ListItem(SolutionObjects.AdjustmentTypes.Plus.ToString(), SolutionObjects.AdjustmentTypes.Plus.ToString()));
        DropDownListAdjustmentType.Items.Add(new ListItem(SolutionObjects.AdjustmentTypes.Minus.ToString(), SolutionObjects.AdjustmentTypes.Minus.ToString()));

        Util.ClearDropdown(DropDownListSrchStatus);
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Pending.ToString(), SolutionObjects.ApprovalStatusses.Pending.ToString()));
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Approved.ToString(), SolutionObjects.ApprovalStatusses.Approved.ToString()));
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Rejected.ToString(), SolutionObjects.ApprovalStatusses.Rejected.ToString()));
    }

    private void Search()
    {
        DateTime? _dateAdjustment = Util.ParseDate(TextBoxSrchDateAdjustment.Text.Trim());
        SolutionObjects.ApprovalStatusses? _approvalStatus = null;
        if (DropDownListSrchStatus.SelectedIndex != 0)
            _approvalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), DropDownListSrchStatus.SelectedValue);

        GridViewSearchList.DataSource = new OperationObjects.StockAdjustmentOP().SearchStockAdjustments(TextBoxSrchAdjustmentNumber.Text.Trim(), _dateAdjustment, _approvalStatus);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _stockAdjustment = new SolutionObjects.StockAdjustment();
            _stockAdjustment.StockAdjustmentDetailList = new CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail>();
            ViewState[_target] = _stockAdjustment;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _stockAdjustment = new SolutionObjects.StockAdjustment();
        _stockAdjustment.StockAdjustmentDetailList = new CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail>();
        ViewState[_target] = _stockAdjustment;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _stockAdjustment = ViewState[_target] as SolutionObjects.StockAdjustment;
        _stockAdjustment.StockAdjustmentDetailList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _stockAdjustment;
        GridViewItemAdd.DataSource = _stockAdjustment.StockAdjustmentDetailList;
        GridViewItemAdd.DataBind();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.StockAdjustmentDetail _stockAdjustmentDetail = e.Row.DataItem as SolutionObjects.StockAdjustmentDetail;
            if (_stockAdjustmentDetail.ItemStock != null)
            {
                _stockAdjustmentDetail.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_stockAdjustmentDetail.ItemStock.ID.Value);
                _stockAdjustmentDetail.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_stockAdjustmentDetail.ItemStock.ProductItem.ID.Value);
            }
            if (_stockAdjustmentDetail.Store != null)
                _stockAdjustmentDetail.Store = new OperationObjects.StoreOP().GetStore(_stockAdjustmentDetail.Store.ID.Value);
            if (_stockAdjustmentDetail.Location != null)
                _stockAdjustmentDetail.Location = new OperationObjects.LocationOP().GetLocation(_stockAdjustmentDetail.Location.ID.Value);
            if (_stockAdjustmentDetail.Rack != null)
                _stockAdjustmentDetail.Rack = new OperationObjects.RackOP().GetRack(_stockAdjustmentDetail.Rack.ID.Value);

            StringBuilder _sbProductItem = new StringBuilder();
            _sbProductItem.Append(_stockAdjustmentDetail.ItemStock.ProductItem != null ? _stockAdjustmentDetail.ItemStock.ProductItem.Code + "-" + _stockAdjustmentDetail.ItemStock.ProductItem.Description : "-").Append("<br>");
            _sbProductItem.Append("[" + _stockAdjustmentDetail.ItemStock.UnitPrice.Value.ToString("F") + " / " + _stockAdjustmentDetail.ItemStock.SellingPrice.Value.ToString("F") + "]");

            StringBuilder _sbType = new StringBuilder();
            _sbType.Append(_stockAdjustmentDetail.StockType != null ? _stockAdjustmentDetail.StockType.Value.ToString() : "-").Append("<br>");
            _sbType.Append(_stockAdjustmentDetail.AdjustmentType != null ? _stockAdjustmentDetail.AdjustmentType.Value.ToString() : "-");

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_stockAdjustmentDetail.Store != null ? _stockAdjustmentDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Location != null ? _stockAdjustmentDetail.Location.Description : string.Empty).Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Rack != null ? _stockAdjustmentDetail.Rack.Description : string.Empty);

            e.Row.Cells[1].Text = _sbProductItem.ToString();
            e.Row.Cells[2].Text = _sbType.ToString();
            e.Row.Cells[3].Text = _sbLocation.ToString();
            e.Row.Cells[4].Text = _stockAdjustmentDetail.Quantity != null ? _stockAdjustmentDetail.Quantity.Value.ToString() : "-";
            e.Row.Cells[5].Text = _stockAdjustmentDetail.ReturnQuantity != null ? _stockAdjustmentDetail.ReturnQuantity.Value.ToString() : "-";
        }
    }
    protected void ButtonSearchFilter_Click(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListProductItem);
        foreach (SolutionObjects.ItemStock itemStock in new OperationObjects.ItemStockOP().FilterSearch(TextBoxFilterCode.Text.Trim(), TextBoxFilterDescription.Text.Trim(), TextBoxFilterLotNumber.Text.Trim(), SolutionObjects.StockTypes.Trading))
        {
            itemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(itemStock.ProductItem.ID.Value);
            DropDownListProductItem.Items.Add(new ListItem(itemStock.ProductItem.Code + " - " + itemStock.ProductItem.Description + " [" + itemStock.UnitPrice.Value.ToString("F") + " / " + itemStock.SellingPrice.Value.ToString("F") + "]", itemStock.ID.Value.ToString()));
        }
    }
    protected void DropDownListProductItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        LabelAvailableQuantity.Text = "-";
        if (DropDownListProductItem.SelectedIndex != 0)
        {
            SolutionObjects.ItemStock _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(int.Parse(DropDownListProductItem.SelectedValue));
            LabelAvailableQuantity.Text = _itemStock.CurrentQuantity.Value.ToString();
        }
    }
    protected void DropDownListStockType_SelectedIndexChanged(object sender, EventArgs e)
    {
        TRStore.Visible = false;
        TRLocation.Visible = false;
        TRRack.Visible = false;
        TRReturnQuantity.Visible = false;
        DropDownListAdjustmentType.SelectedIndex = 0;
        DropDownListAdjustmentType.Enabled = true;

        if (DropDownListStockType.SelectedIndex == 2 || DropDownListStockType.SelectedIndex == 3)
        {
            DropDownListAdjustmentType.SelectedIndex = 2;
            DropDownListAdjustmentType.Enabled = false;

            TRStore.Visible = true;
            TRLocation.Visible = true;
            TRRack.Visible = true;
            TRReturnQuantity.Visible = true;
        }
    }
    protected void DropDownListStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);

        if (DropDownListStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
                DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListRack);

        if (DropDownListLocation.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Rack rack in new OperationObjects.RackOP().SearchRacks(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue)), new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue))))
                DropDownListRack.Items.Add(new ListItem(rack.Description, rack.ID.Value.ToString()));
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        _stockAdjustment = ViewState[_target] as SolutionObjects.StockAdjustment;
        LabelValidateMultiple.Visible = false;
        LabelValidateQuantity.Visible = false;
        LabelValidateReturnQuantity.Visible = false;
        LabelValidateNonTrading.Visible = false;

        bool _hasFound = false;
        foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in _stockAdjustment.StockAdjustmentDetailList)
        {
            if (DropDownListProductItem.SelectedValue == stockAdjustmentDetail.ItemStock.ID.Value.ToString())
            {
                _hasFound = true;
                break;
            }
        }
        if (_hasFound)
        {
            LabelValidateMultiple.Visible = true;
            return;
        }

        if (DropDownListStockType.SelectedIndex != 1 && Util.ParseInt(TextBoxAdjustQuantity.Text.Trim()) > Util.ParseInt(LabelAvailableQuantity.Text))
        {
            LabelValidateQuantity.Visible = true;
            return;
        }

        if (DropDownListStockType.SelectedIndex == 2 || DropDownListStockType.SelectedIndex == 3)
        {
            if (TextBoxReturnQuantity.Text.Trim() != string.Empty)
            {
                if (Util.ParseInt(TextBoxReturnQuantity.Text.Trim()) > Util.ParseInt(TextBoxAdjustQuantity.Text.Trim()))
                {
                    LabelValidateReturnQuantity.Visible = true;
                    return;
                }
            }

            if (DropDownListStore.SelectedIndex == 0 || DropDownListLocation.SelectedIndex == 0 || DropDownListRack.SelectedIndex == 0)
            {
                LabelValidateNonTrading.Visible = true;
                return;
            }
        }

        SolutionObjects.StockAdjustmentDetail _stockAdjustmentDetail = new SolutionObjects.StockAdjustmentDetail();
        _stockAdjustmentDetail.StockAdjustment = _stockAdjustment;
        _stockAdjustmentDetail.ItemStock = new SolutionObjects.ItemStock(int.Parse(DropDownListProductItem.SelectedValue));
        _stockAdjustmentDetail.AdjustmentType = (SolutionObjects.AdjustmentTypes)Enum.Parse(typeof(SolutionObjects.AdjustmentTypes), DropDownListAdjustmentType.SelectedValue);
        _stockAdjustmentDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), DropDownListStockType.SelectedValue);
        _stockAdjustmentDetail.Quantity = Util.ParseInt(TextBoxAdjustQuantity.Text.Trim());
        if (DropDownListStockType.SelectedIndex == 2 || DropDownListStockType.SelectedIndex == 3)
        {
            _stockAdjustmentDetail.Store = new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue));
            _stockAdjustmentDetail.Location = new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue));
            _stockAdjustmentDetail.Rack = new SolutionObjects.Rack(int.Parse(DropDownListRack.SelectedValue));
            _stockAdjustmentDetail.ReturnQuantity = Util.ParseInt(TextBoxReturnQuantity.Text.Trim());
        }
        _stockAdjustmentDetail.User = User.Identity.Name;
        _stockAdjustment.StockAdjustmentDetailList.Add(_stockAdjustmentDetail);

        ViewState[_target] = _stockAdjustment;
        GridViewItemAdd.DataSource = _stockAdjustment.StockAdjustmentDetailList;
        GridViewItemAdd.DataBind();
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _stockAdjustment = ViewState[_target] as SolutionObjects.StockAdjustment;

        if (_stockAdjustment.ID == null)
            _stockAdjustment.AdjustmentNumber = Util.GenereteSequenceNumber("SAJ", DateTime.Today.Year, DateTime.Today.Month);
        _stockAdjustment.DateAdjustment = Util.ParseDate(TextBoxDateAdjustment.Text.Trim());
        _stockAdjustment.Remarks = TextBoxRemarks.Text.Trim();
        _stockAdjustment.ApprovalStatus = SolutionObjects.ApprovalStatusses.Pending;
        _stockAdjustment.User = User.Identity.Name;

        try
        {
            bool _isNew = false;
            if (_stockAdjustment.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.StockAdjustmentOP().SaveStockAdjustment(_stockAdjustment);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>Stock Adjustment Number : " + _stockAdjustment.AdjustmentNumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                _stockAdjustment = new SolutionObjects.StockAdjustment();
                _stockAdjustment.StockAdjustmentDetailList = new CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail>();
                ViewState[_target] = _stockAdjustment;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _stockAdjustment = ViewState[_target] as SolutionObjects.StockAdjustment;

        if (_stockAdjustment.ID != null)
        {
            _stockAdjustment = new OperationObjects.StockAdjustmentOP().GetStockAdjustment(_stockAdjustment.ID.Value);
            ViewState[_target] = _stockAdjustment;

            TextBoxAdjustmentNumber.Text = _stockAdjustment.AdjustmentNumber;
            TextBoxDateAdjustment.Text = _stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty;
            TextBoxRemarks.Text = _stockAdjustment.Remarks;

            GridViewItemAdd.DataSource = _stockAdjustment.StockAdjustmentDetailList;
            GridViewItemAdd.DataBind();
        }
        else
        {
            _stockAdjustment = new SolutionObjects.StockAdjustment();
            _stockAdjustment.StockAdjustmentDetailList = new CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail>();
            ViewState[_target] = _stockAdjustment;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _stockAdjustment = new OperationObjects.StockAdjustmentOP().GetStockAdjustment(_targetid);

            ClearAll();
            ViewState[_target] = _stockAdjustment;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxAdjustmentNumber.Text = _stockAdjustment.AdjustmentNumber;
            TextBoxDateAdjustment.Text = _stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty;
            TextBoxRemarks.Text = _stockAdjustment.Remarks;

            GridViewItemAdd.DataSource = _stockAdjustment.StockAdjustmentDetailList;
            GridViewItemAdd.DataBind();
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _stockAdjustment = new OperationObjects.StockAdjustmentOP().GetStockAdjustment(_targetid);
            _stockAdjustment.User = User.Identity.Name;

            int _listCount = _stockAdjustment.StockAdjustmentDetailList.Count - 1;
            while (_listCount >= 0)
            {
                _stockAdjustment.StockAdjustmentDetailList.RemoveAt(_listCount);
                _listCount--;
            }

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.StockAdjustmentOP().DeleteStockAdjustment(_stockAdjustment);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _stockAdjustment = new OperationObjects.StockAdjustmentOP().GetStockAdjustment(_targetid);

            ViewState[_target] = _stockAdjustment;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewAdjustmentNumber.Text = Util.FormatEmptyString(_stockAdjustment.AdjustmentNumber);
            LabelViewDateAdjustment.Text = Util.FormatEmptyString(_stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty);
            LabelViewRemarks.Text = Util.FormatEmptyString(_stockAdjustment.Remarks);

            GridViewItemView.DataSource = _stockAdjustment.StockAdjustmentDetailList;
            GridViewItemView.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _stockAdjustment = e.Row.DataItem as SolutionObjects.StockAdjustment;

            Util.EncodeString(e.Row.Cells[3], _stockAdjustment.AdjustmentNumber, 20);
            Util.EncodeString(e.Row.Cells[4], _stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], _stockAdjustment.Remarks, 50);
            Util.EncodeString(e.Row.Cells[6], _stockAdjustment.ApprovalStatus.Value.ToString(), 20);

            if (_stockAdjustment.ApprovalStatus != SolutionObjects.ApprovalStatusses.Pending)
            {
                e.Row.Cells[0].Enabled = false;
                e.Row.Cells[1].Enabled = false;
            }

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _stockAdjustment.AdjustmentNumber + "');");
                }
            }
        }
    }
    protected void DropDownListShowEntries_SelectedIndexChanged(object sender, EventArgs e)
    {
        int size = 10;
        size = size > 150 ? 150 : size;
        if (int.TryParse(DropDownListShowEntries.SelectedValue, out size))
            GridViewSearchList.PageSize = size;
        Search();
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.StockAdjustmentDetail _stockAdjustmentDetail = e.Row.DataItem as SolutionObjects.StockAdjustmentDetail;
            if (_stockAdjustmentDetail.ItemStock != null)
            {
                _stockAdjustmentDetail.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_stockAdjustmentDetail.ItemStock.ID.Value);
                _stockAdjustmentDetail.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_stockAdjustmentDetail.ItemStock.ProductItem.ID.Value);
            }
            if (_stockAdjustmentDetail.Store != null)
                _stockAdjustmentDetail.Store = new OperationObjects.StoreOP().GetStore(_stockAdjustmentDetail.Store.ID.Value);
            if (_stockAdjustmentDetail.Location != null)
                _stockAdjustmentDetail.Location = new OperationObjects.LocationOP().GetLocation(_stockAdjustmentDetail.Location.ID.Value);
            if (_stockAdjustmentDetail.Rack != null)
                _stockAdjustmentDetail.Rack = new OperationObjects.RackOP().GetRack(_stockAdjustmentDetail.Rack.ID.Value);

            StringBuilder _sbProductItem = new StringBuilder();
            _sbProductItem.Append(_stockAdjustmentDetail.ItemStock.ProductItem != null ? _stockAdjustmentDetail.ItemStock.ProductItem.Code + "-" + _stockAdjustmentDetail.ItemStock.ProductItem.Description : "-").Append("<br>");
            _sbProductItem.Append("[" + _stockAdjustmentDetail.ItemStock.UnitPrice.Value.ToString("F") + " / " + _stockAdjustmentDetail.ItemStock.SellingPrice.Value.ToString("F") + "]");

            StringBuilder _sbType = new StringBuilder();
            _sbType.Append(_stockAdjustmentDetail.StockType != null ? _stockAdjustmentDetail.StockType.Value.ToString() : "-").Append("<br>");
            _sbType.Append(_stockAdjustmentDetail.AdjustmentType != null ? _stockAdjustmentDetail.AdjustmentType.Value.ToString() : "-");

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_stockAdjustmentDetail.Store != null ? _stockAdjustmentDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Location != null ? _stockAdjustmentDetail.Location.Description : string.Empty).Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Rack != null ? _stockAdjustmentDetail.Rack.Description : string.Empty);

            e.Row.Cells[0].Text = _sbProductItem.ToString();
            e.Row.Cells[1].Text = _sbType.ToString();
            e.Row.Cells[2].Text = _sbLocation.ToString();
            e.Row.Cells[3].Text = _stockAdjustmentDetail.Quantity != null ? _stockAdjustmentDetail.Quantity.Value.ToString() : "-";
            e.Row.Cells[4].Text = _stockAdjustmentDetail.ReturnQuantity != null ? _stockAdjustmentDetail.ReturnQuantity.Value.ToString() : "-";
        }
    }

}