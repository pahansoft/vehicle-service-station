﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class StockAdjustment : SolutionObjectBase
    {
        public StockAdjustment() { }
        public StockAdjustment(int? id) { this.ID = id; }

        public string AdjustmentNumber { get; set; }
        public DateTime? DateAdjustment { get; set; }
        public string Remarks { get; set; }
        public SolutionObjects.ApprovalStatusses? ApprovalStatus { get; set; }
        public DateTime? DateApproved { get; set; }
        public string UserApproved { get; set; }
        public string ApprovedReason { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail> StockAdjustmentDetailList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
