﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class StockAdjustmentDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateStockAdjustmentDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockAdjustmentID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.StockAdjustment != null ? stockAdjustmentDetail.StockAdjustment.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ItemStock != null ? stockAdjustmentDetail.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AdjustmentType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.AdjustmentType != null ? stockAdjustmentDetail.AdjustmentType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.StockType != null ? stockAdjustmentDetail.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Store != null ? stockAdjustmentDetail.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Location != null ? stockAdjustmentDetail.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Rack != null ? stockAdjustmentDetail.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ReturnQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Remarks);
         
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustmentDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                stockAdjustmentDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditStockAdjustmentDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockAdjustmentID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.StockAdjustment != null ? stockAdjustmentDetail.StockAdjustment.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ItemStock != null ? stockAdjustmentDetail.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AdjustmentType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.AdjustmentType != null ? stockAdjustmentDetail.AdjustmentType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.StockType != null ? stockAdjustmentDetail.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Store != null ? stockAdjustmentDetail.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Location != null ? stockAdjustmentDetail.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Rack != null ? stockAdjustmentDetail.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ReturnQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.Remarks);
         
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustmentDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteStockAdjustmentDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustmentDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustmentDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                stockAdjustmentDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.StockAdjustmentDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetStockAdjustmentDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int stockAdjustmentIDOrdinal = dr.GetOrdinal("StockAdjustmentID");
                        int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                        int adjustmentTypeOrdinal = dr.GetOrdinal("AdjustmentType");
                        int stockTypeOrdinal = dr.GetOrdinal("StockType");
                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int rackIDOrdinal = dr.GetOrdinal("RackID");
                        int quantityOrdinal = dr.GetOrdinal("Quantity");
                        int returnQuantityOrdinal = dr.GetOrdinal("ReturnQuantity");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        stockAdjustmentDetail = new Inventory.SolutionObjects.StockAdjustmentDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustmentDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(stockAdjustmentIDOrdinal)) stockAdjustmentDetail.StockAdjustment = new SolutionObjects.StockAdjustment(dr.GetInt32(stockAdjustmentIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) stockAdjustmentDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(adjustmentTypeOrdinal)) stockAdjustmentDetail.AdjustmentType = (SolutionObjects.AdjustmentTypes)Enum.Parse(typeof(SolutionObjects.AdjustmentTypes), dr.GetString(adjustmentTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) stockAdjustmentDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(storeIDOrdinal)) stockAdjustmentDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) stockAdjustmentDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) stockAdjustmentDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) stockAdjustmentDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(returnQuantityOrdinal)) stockAdjustmentDetail.ReturnQuantity = dr.GetDecimal(returnQuantityOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustmentDetail.Remarks = dr.GetString(remarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustmentDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllStockAdjustmentDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> stockAdjustmentDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.StockAdjustmentDetail>();
            Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int stockAdjustmentIDOrdinal = dr.GetOrdinal("StockAdjustmentID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int adjustmentTypeOrdinal = dr.GetOrdinal("AdjustmentType");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int returnQuantityOrdinal = dr.GetOrdinal("ReturnQuantity");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        stockAdjustmentDetail = new Inventory.SolutionObjects.StockAdjustmentDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustmentDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(stockAdjustmentIDOrdinal)) stockAdjustmentDetail.StockAdjustment = new SolutionObjects.StockAdjustment(dr.GetInt32(stockAdjustmentIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) stockAdjustmentDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(adjustmentTypeOrdinal)) stockAdjustmentDetail.AdjustmentType = (SolutionObjects.AdjustmentTypes)Enum.Parse(typeof(SolutionObjects.AdjustmentTypes), dr.GetString(adjustmentTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) stockAdjustmentDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(storeIDOrdinal)) stockAdjustmentDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) stockAdjustmentDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) stockAdjustmentDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) stockAdjustmentDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(returnQuantityOrdinal)) stockAdjustmentDetail.ReturnQuantity = dr.GetDecimal(returnQuantityOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustmentDetail.Remarks = dr.GetString(remarksOrdinal);

                        stockAdjustmentDetailList.Add(stockAdjustmentDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustmentDetailList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> GetAllFor(SolutionObjects.StockAdjustment stockAdjustment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllStockAdjustmentDetailsForStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockAdjustmentID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustment.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> stockAdjustmentDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.StockAdjustmentDetail>();
            Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int stockAdjustmentIDOrdinal = dr.GetOrdinal("StockAdjustmentID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int adjustmentTypeOrdinal = dr.GetOrdinal("AdjustmentType");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int returnQuantityOrdinal = dr.GetOrdinal("ReturnQuantity");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        stockAdjustmentDetail = new Inventory.SolutionObjects.StockAdjustmentDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustmentDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(stockAdjustmentIDOrdinal)) stockAdjustmentDetail.StockAdjustment = new SolutionObjects.StockAdjustment(dr.GetInt32(stockAdjustmentIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) stockAdjustmentDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(adjustmentTypeOrdinal)) stockAdjustmentDetail.AdjustmentType = (SolutionObjects.AdjustmentTypes)Enum.Parse(typeof(SolutionObjects.AdjustmentTypes), dr.GetString(adjustmentTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) stockAdjustmentDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(storeIDOrdinal)) stockAdjustmentDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) stockAdjustmentDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) stockAdjustmentDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) stockAdjustmentDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(returnQuantityOrdinal)) stockAdjustmentDetail.ReturnQuantity = dr.GetDecimal(returnQuantityOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustmentDetail.Remarks = dr.GetString(remarksOrdinal);

                        stockAdjustmentDetailList.Add(stockAdjustmentDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustmentDetailList;
        }

        #endregion
    }
}
