﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Service : SolutionObjectBase
    {
        public Service() { }
        public Service(int? id) { this.ID = id; }

        public string Description { get; set; }
    }
}
