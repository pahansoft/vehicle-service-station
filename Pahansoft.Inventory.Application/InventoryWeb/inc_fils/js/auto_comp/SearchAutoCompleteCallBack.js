﻿// JScript File

var _finalLookupKey = null;

function initLoadCDSAccountObjectCall(txt, hidenFieldID) {
    var _innerSerachQuery = txt.value;
    var _checkMinLength = 2;

    if (_innerSerachQuery.length >= _checkMinLength) {
        if (_finalLookupKey == null || _finalLookupKey.toUpperCase() != _innerSerachQuery.toUpperCase().substr(0, _checkMinLength)) {
            _finalLookupKey = _innerSerachQuery.substr(0, _checkMinLength);
            
            var _mainSearchBox = txt;
            _mainSearchBox.className = "autocomplete_text_busy";
            _mainSearchBox.blur();
            var xhr;
            try {
                xhr = new XMLHttpRequest();
            }
            catch (error) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
                catch (error) {
                    xhr = null;
                }
            }

            if (xhr != null) {
                xhr.open('POST', '?callType=ajaxGetCDSAccountList&LookupKey=' + _finalLookupKey, true);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200 || xhr.status == 304) {
                            var _responseObjectListString = xhr.responseText;
                            if (_responseObjectListString != "") {
                                
                                if(_responseObjectListString == "0")
                                {
                                    _mainSearchBox.className = "autocomplete_text";
                                    _mainSearchBox.focus();
                                    var _objTextArray = new Array();
                                    var _objIDArray = new Array();                                    
                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '200px', 'LookupList',true , false);
                                }
                                else
                                {
                                    var _returnedXMLobject = ConverttoXMLObject(_responseObjectListString);
                                    var _returnedObjectsList = _returnedXMLobject.documentElement.getElementsByTagName("MainObject");
                            
                                    var _objTextArray = new Array(_returnedObjectsList.length);
                                    var _objIDArray = new Array(_returnedObjectsList.length);
                                    for (var i = 0; i < _returnedObjectsList.length; i++) {
                                        _objTextArray[i] = _returnedObjectsList[i].getAttribute("TextNode");
                                        _objIDArray[i] = _returnedObjectsList[i].getAttribute("IDNode").replace(/#@1@#/g, "&");
                                    }

                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '200px', 'LookupList',true, false);
                                }
                            }
                            _mainSearchBox.className = "autocomplete_text";
                            _mainSearchBox.focus();
                        }
                        else {
                            alert('error');
                        }
                    }
                };

                xhr.send(null);

                return false;
            }
            else {
                return true;
            }
        }
    }
}

function initLoadShareCodeObjectCall(txt, hidenFieldID) {
    var _innerSerachQuery = txt.value;
    var _checkMinLength = 1;

    if (_innerSerachQuery.length >= _checkMinLength) {
        if (_finalLookupKey == null || _finalLookupKey.toUpperCase() != _innerSerachQuery.toUpperCase().substr(0, _checkMinLength)) {
            _finalLookupKey = _innerSerachQuery.substr(0, _checkMinLength);

            var _mainSearchBox = txt;
            _mainSearchBox.className = "autocomplete_text_busy";
            _mainSearchBox.blur();
            var xhr;
            try {
                xhr = new XMLHttpRequest();
            }
            catch (error) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
                catch (error) {
                    xhr = null;
                }
            }

            if (xhr != null) {
                xhr.open('POST', '?callType=ajaxGetShareCodeList&LookupKey=' + _finalLookupKey, true);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200 || xhr.status == 304) {
                            var _responseObjectListString = xhr.responseText;
                            if (_responseObjectListString != "") {

                                if(_responseObjectListString == "0")
                                {
                                    _mainSearchBox.className = "autocomplete_text";
                                    _mainSearchBox.focus();
                                    var _objTextArray = new Array();
                                    var _objIDArray = new Array();                                    
                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '200px', 'LookupList',true, false);
                                }
                                else
                                {
                                    var _returnedXMLobject = ConverttoXMLObject(_responseObjectListString);
                                    var _returnedObjectsList = _returnedXMLobject.documentElement.getElementsByTagName("MainObject");
                                    
                                    var _objTextArray = new Array(_returnedObjectsList.length);
                                    var _objIDArray = new Array(_returnedObjectsList.length);
                                    for (var i = 0; i < _returnedObjectsList.length; i++) {
                                        _objTextArray[i] = _returnedObjectsList[i].getAttribute("TextNode");
                                        _objIDArray[i] = _returnedObjectsList[i].getAttribute("IDNode").replace(/#@1@#/g, "&");
                                    }

                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '175px', 'LookupList',true , false);
                                }
                            }
                            _mainSearchBox.className = "autocomplete_text";
                            _mainSearchBox.focus();
                        }
                        else {
                            alert('error');
                        }
                    }
                };

                xhr.send(null);

                return false;
            }
            else {
                return true;
            }
        }
    }
}


function initLoadAdvisorObjectCall(txt, hidenFieldID) {
    var _innerSerachQuery = txt.value;
    var _checkMinLength = 1;

    if (_innerSerachQuery.length >= _checkMinLength) {
        if (_finalLookupKey == null || _finalLookupKey.toUpperCase() != _innerSerachQuery.toUpperCase().substr(0, _checkMinLength)) {
            _finalLookupKey = _innerSerachQuery.substr(0, _checkMinLength);

            var _mainSearchBox = txt;
            _mainSearchBox.className = "autocomplete_text_busy";
            _mainSearchBox.blur();
            var xhr;
            try {
                xhr = new XMLHttpRequest();
            }
            catch (error) {
                try {
                    xhr = new ActiveXObject('Microsoft.XMLHTTP');
                }
                catch (error) {
                    xhr = null;
                }
            }

            if (xhr != null) {
                xhr.open('POST', '?callType=ajaxGetAdvisorCodeList&LookupKey=' + _finalLookupKey, true);

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200 || xhr.status == 304) {
                            var _responseObjectListString = xhr.responseText;
                            if (_responseObjectListString != "") {

                                if(_responseObjectListString == "0")
                                {
                                    _mainSearchBox.className = "autocomplete_text";
                                    _mainSearchBox.focus();
                                    var _objTextArray = new Array();
                                    var _objIDArray = new Array();                                    
                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '200px', 'LookupList',true , false);
                                }
                                else
                                {
                                    var _returnedXMLobject = ConverttoXMLObject(_responseObjectListString);
                                    var _returnedObjectsList = _returnedXMLobject.documentElement.getElementsByTagName("MainObject");
                                    
                                    var _objTextArray = new Array(_returnedObjectsList.length);
                                    var _objIDArray = new Array(_returnedObjectsList.length);
                                    for (var i = 0; i < _returnedObjectsList.length; i++) {
                                        _objTextArray[i] = _returnedObjectsList[i].getAttribute("TextNode");
                                        _objIDArray[i] = _returnedObjectsList[i].getAttribute("IDNode").replace(/#@1@#/g, "&");
                                    }

                                    var obj = new actb(_mainSearchBox, _objTextArray, _objIDArray, hidenFieldID, '175px', 'LookupList',true, false);
                                }
                            }
                            _mainSearchBox.className = "autocomplete_text";
                            _mainSearchBox.focus();
                        }
                        else {
                            alert('error');
                        }
                    }
                };

                xhr.send(null);

                return false;
            }
            else {
                return true;
            }
        }
    }
}



function ConverttoXMLObject(text) {
    if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.async = 'false';
        doc.loadXML(text);
    } else {
        var parser = new DOMParser();
        var doc = parser.parseFromString(text, 'text/xml');
    }
    return doc;
}

