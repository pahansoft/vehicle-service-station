﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class LocationTransferDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.LocationTransfer locationTransfer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransferNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.TransferNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateTransfer", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, locationTransfer.DateTransfer);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransfer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                locationTransfer.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.LocationTransfer locationTransfer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransfer.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, locationTransfer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransferNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.TransferNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateTransfer", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, locationTransfer.DateTransfer);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransfer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.LocationTransfer locationTransfer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransfer.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, locationTransfer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransfer.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransfer;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                locationTransfer.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.LocationTransfer Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.LocationTransfer locationTransfer = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int transferNumberOrdinal = dr.GetOrdinal("TransferNumber");
                        int dateTransferOrdinal = dr.GetOrdinal("DateTransfer");
                        int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        locationTransfer = new Inventory.SolutionObjects.LocationTransfer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransfer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(transferNumberOrdinal)) locationTransfer.TransferNumber = dr.GetString(transferNumberOrdinal);
                        if (!dr.IsDBNull(dateTransferOrdinal)) locationTransfer.DateTransfer = dr.GetDateTime(dateTransferOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) locationTransfer.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) locationTransfer.Remarks = dr.GetString(remarksOrdinal);

                        locationTransfer.LocationTransferDetailList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail>();
                        foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in new DataAccessObjects.LocationTransferDetailDO().GetAllFor(locationTransfer))
                            locationTransfer.LocationTransferDetailList.Add(locationTransferDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransfer;
        }

        public Inventory.SolutionObjects.LocationTransfer FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.LocationTransfer locationTransfer = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int transferNumberOrdinal = dr.GetOrdinal("TransferNumber");
                        int dateTransferOrdinal = dr.GetOrdinal("DateTransfer");
                        int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        locationTransfer = new Inventory.SolutionObjects.LocationTransfer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransfer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(transferNumberOrdinal)) locationTransfer.TransferNumber = dr.GetString(transferNumberOrdinal);
                        if (!dr.IsDBNull(dateTransferOrdinal)) locationTransfer.DateTransfer = dr.GetDateTime(dateTransferOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) locationTransfer.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) locationTransfer.Remarks = dr.GetString(remarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransfer;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllLocationTransfers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> locationTransferList = new System.Collections.Generic.List<Inventory.SolutionObjects.LocationTransfer>();
            Inventory.SolutionObjects.LocationTransfer locationTransfer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int transferNumberOrdinal = dr.GetOrdinal("TransferNumber");
                    int dateTransferOrdinal = dr.GetOrdinal("DateTransfer");
                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        locationTransfer = new Inventory.SolutionObjects.LocationTransfer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransfer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(transferNumberOrdinal)) locationTransfer.TransferNumber = dr.GetString(transferNumberOrdinal);
                        if (!dr.IsDBNull(dateTransferOrdinal)) locationTransfer.DateTransfer = dr.GetDateTime(dateTransferOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) locationTransfer.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) locationTransfer.Remarks = dr.GetString(remarksOrdinal);

                        locationTransferList.Add(locationTransfer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransferList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> Search(string transferNumber, DateTime? dateTransfer, string referenceNumber)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllLocationTransfers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransferNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, transferNumber != string.Empty ? transferNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateTransfer", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, dateTransfer != null ? dateTransfer : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, referenceNumber != string.Empty ? referenceNumber : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransfer> locationTransferList = new System.Collections.Generic.List<Inventory.SolutionObjects.LocationTransfer>();
            Inventory.SolutionObjects.LocationTransfer locationTransfer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int transferNumberOrdinal = dr.GetOrdinal("TransferNumber");
                    int dateTransferOrdinal = dr.GetOrdinal("DateTransfer");
                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        locationTransfer = new Inventory.SolutionObjects.LocationTransfer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransfer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(transferNumberOrdinal)) locationTransfer.TransferNumber = dr.GetString(transferNumberOrdinal);
                        if (!dr.IsDBNull(dateTransferOrdinal)) locationTransfer.DateTransfer = dr.GetDateTime(dateTransferOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) locationTransfer.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) locationTransfer.Remarks = dr.GetString(remarksOrdinal);

                        locationTransferList.Add(locationTransfer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransferList;
        }

        #endregion
    }
}
