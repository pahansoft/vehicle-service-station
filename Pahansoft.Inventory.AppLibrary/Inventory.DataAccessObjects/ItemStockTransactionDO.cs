﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class ItemStockTransactionDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateItemStockTransaction";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ItemStock != null ? itemStockTransaction.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ProductItem != null ? itemStockTransaction.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionType != null ? itemStockTransaction.TransactionType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.StockType != null ? itemStockTransaction.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionDetailID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Store != null ? itemStockTransaction.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Location != null ? itemStockTransaction.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Rack != null ? itemStockTransaction.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.Remarks);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStockTransaction;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                itemStockTransaction.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditItemStockTransaction";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, itemStockTransaction.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ItemStock != null ? itemStockTransaction.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ProductItem != null ? itemStockTransaction.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionType != null ? itemStockTransaction.TransactionType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.StockType != null ? itemStockTransaction.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TransactionDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.TransactionDetailID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Store != null ? itemStockTransaction.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Location != null ? itemStockTransaction.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.Rack != null ? itemStockTransaction.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStockTransaction.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.Remarks);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStockTransaction;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteItemStockTransaction";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStockTransaction.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, itemStockTransaction.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStockTransaction.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStockTransaction;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                itemStockTransaction.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.ItemStockTransaction Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetItemStockTransaction";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                        int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                        int transactionTypeOrdinal = dr.GetOrdinal("TransactionType");
                        int stockTypeOrdinal = dr.GetOrdinal("StockType");
                        int transactionIDOrdinal = dr.GetOrdinal("TransactionID");
                        int transactionDetailIDOrdinal = dr.GetOrdinal("TransactionDetailID");
                        int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int rackIDOrdinal = dr.GetOrdinal("RackID");
                        int quantityOrdinal = dr.GetOrdinal("Quantity");
                        int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        itemStockTransaction = new Inventory.SolutionObjects.ItemStockTransaction(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStockTransaction.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(itemStockIDOrdinal)) itemStockTransaction.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStockTransaction.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(transactionTypeOrdinal)) itemStockTransaction.TransactionType = (SolutionObjects.TransactionTypes)Enum.Parse(typeof(SolutionObjects.TransactionTypes), dr.GetString(transactionTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStockTransaction.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(transactionIDOrdinal)) itemStockTransaction.TransactionID = dr.GetInt32(transactionIDOrdinal);
                        if (!dr.IsDBNull(transactionDetailIDOrdinal)) itemStockTransaction.TransactionDetailID = dr.GetInt32(transactionDetailIDOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) itemStockTransaction.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStockTransaction.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStockTransaction.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStockTransaction.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) itemStockTransaction.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStockTransaction.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStockTransaction.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStockTransaction.Remarks = dr.GetString(remarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockTransaction;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllItemStockTransactions";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> itemStockTransactionList = new System.Collections.Generic.List<Inventory.SolutionObjects.ItemStockTransaction>();
            Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int transactionTypeOrdinal = dr.GetOrdinal("TransactionType");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int transactionIDOrdinal = dr.GetOrdinal("TransactionID");
                    int transactionDetailIDOrdinal = dr.GetOrdinal("TransactionDetailID");
                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        itemStockTransaction = new Inventory.SolutionObjects.ItemStockTransaction(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStockTransaction.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(itemStockIDOrdinal)) itemStockTransaction.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStockTransaction.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(transactionTypeOrdinal)) itemStockTransaction.TransactionType = (SolutionObjects.TransactionTypes)Enum.Parse(typeof(SolutionObjects.TransactionTypes), dr.GetString(transactionTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStockTransaction.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(transactionIDOrdinal)) itemStockTransaction.TransactionID = dr.GetInt32(transactionIDOrdinal);
                        if (!dr.IsDBNull(transactionDetailIDOrdinal)) itemStockTransaction.TransactionDetailID = dr.GetInt32(transactionDetailIDOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) itemStockTransaction.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStockTransaction.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStockTransaction.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStockTransaction.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) itemStockTransaction.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStockTransaction.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStockTransaction.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStockTransaction.Remarks = dr.GetString(remarksOrdinal);

                        itemStockTransactionList.Add(itemStockTransaction);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockTransactionList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> GetAllFor(SolutionObjects.ItemStock itemStock)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllItemStockTransactionsForItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> itemStockTransactionList = new System.Collections.Generic.List<Inventory.SolutionObjects.ItemStockTransaction>();
            Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int transactionTypeOrdinal = dr.GetOrdinal("TransactionType");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int transactionIDOrdinal = dr.GetOrdinal("TransactionID");
                    int transactionDetailIDOrdinal = dr.GetOrdinal("TransactionDetailID");
                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        itemStockTransaction = new Inventory.SolutionObjects.ItemStockTransaction(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStockTransaction.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(itemStockIDOrdinal)) itemStockTransaction.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStockTransaction.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(transactionTypeOrdinal)) itemStockTransaction.TransactionType = (SolutionObjects.TransactionTypes)Enum.Parse(typeof(SolutionObjects.TransactionTypes), dr.GetString(transactionTypeOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStockTransaction.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(transactionIDOrdinal)) itemStockTransaction.TransactionID = dr.GetInt32(transactionIDOrdinal);
                        if (!dr.IsDBNull(transactionDetailIDOrdinal)) itemStockTransaction.TransactionDetailID = dr.GetInt32(transactionDetailIDOrdinal);
                        if (!dr.IsDBNull(referenceNumberOrdinal)) itemStockTransaction.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStockTransaction.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStockTransaction.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStockTransaction.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) itemStockTransaction.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStockTransaction.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStockTransaction.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStockTransaction.Remarks = dr.GetString(remarksOrdinal);

                        itemStockTransactionList.Add(itemStockTransaction);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockTransactionList;
        }

        #endregion
    }
}
