﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_ajus_appr_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.StockAdjustment _stockAdjustment;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        DropDownListStatus.SelectedIndex = 0;
        TextBoxReason.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchAdjustmentNumber.Text = string.Empty;
        TextBoxSrchDateAdjustment.Text = string.Empty;
        DropDownListSrchStatus.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListSrchStatus);
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Pending.ToString(), SolutionObjects.ApprovalStatusses.Pending.ToString()));
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Approved.ToString(), SolutionObjects.ApprovalStatusses.Approved.ToString()));
        DropDownListSrchStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Rejected.ToString(), SolutionObjects.ApprovalStatusses.Rejected.ToString()));

        DropDownListStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Approved.ToString(), SolutionObjects.ApprovalStatusses.Approved.ToString()));
        DropDownListStatus.Items.Add(new ListItem(SolutionObjects.ApprovalStatusses.Rejected.ToString(), SolutionObjects.ApprovalStatusses.Rejected.ToString()));
    }

    private void Search()
    {
        DateTime? _dateAdjustment = Util.ParseDate(TextBoxSrchDateAdjustment.Text.Trim());
        SolutionObjects.ApprovalStatusses? _approvalStatus = null;
        if (DropDownListSrchStatus.SelectedIndex != 0)
            _approvalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), DropDownListSrchStatus.SelectedValue);

        GridViewSearchList.DataSource = new OperationObjects.StockAdjustmentOP().SearchStockAdjustments(TextBoxSrchAdjustmentNumber.Text.Trim(), _dateAdjustment, _approvalStatus);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _stockAdjustment = new SolutionObjects.StockAdjustment();
            ViewState[_target] = _stockAdjustment;
            LabelPageOperation.Text = "Search";
            LinkButtonSearchOperation.Visible = false;

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonSearchOperation.Visible = false;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _stockAdjustment = new OperationObjects.StockAdjustmentOP().GetStockAdjustment(_targetid);

            ClearAll();
            ViewState[_target] = _stockAdjustment;

            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonSearchOperation.Visible = true;

            LabelViewAdjustmentNumber.Text = Util.FormatEmptyString(_stockAdjustment.AdjustmentNumber);
            LabelViewDateAdjustment.Text = Util.FormatEmptyString(_stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty);
            LabelViewRemarks.Text = Util.FormatEmptyString(_stockAdjustment.Remarks);

            GridViewItemView.DataSource = _stockAdjustment.StockAdjustmentDetailList;
            GridViewItemView.DataBind();

            ButtonSubmit.Enabled = true;
            DropDownListStatus.Enabled = true;
            DropDownListStatus.SelectedIndex = 0;
            TextBoxReason.ReadOnly = false;
            if (_stockAdjustment.ApprovalStatus != SolutionObjects.ApprovalStatusses.Pending)
            {
                ButtonSubmit.Enabled = false;
                DropDownListStatus.Enabled = false;
                TextBoxReason.ReadOnly = true;
                DropDownListStatus.SelectedValue = _stockAdjustment.ApprovalStatus.Value.ToString();
                TextBoxReason.Text = _stockAdjustment.ApprovedReason;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _stockAdjustment = e.Row.DataItem as SolutionObjects.StockAdjustment;

            Util.EncodeString(e.Row.Cells[1], _stockAdjustment.AdjustmentNumber, 20);
            Util.EncodeString(e.Row.Cells[2], _stockAdjustment.DateAdjustment != null ? _stockAdjustment.DateAdjustment.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[3], _stockAdjustment.Remarks, 50);
            Util.EncodeString(e.Row.Cells[4], _stockAdjustment.ApprovalStatus.Value.ToString(), 20);
        }
    }
    protected void DropDownListShowEntries_SelectedIndexChanged(object sender, EventArgs e)
    {
        int size = 10;
        size = size > 150 ? 150 : size;
        if (int.TryParse(DropDownListShowEntries.SelectedValue, out size))
            GridViewSearchList.PageSize = size;
        Search();
    }
    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.StockAdjustmentDetail _stockAdjustmentDetail = e.Row.DataItem as SolutionObjects.StockAdjustmentDetail;
            if (_stockAdjustmentDetail.ItemStock != null)
            {
                _stockAdjustmentDetail.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_stockAdjustmentDetail.ItemStock.ID.Value);
                _stockAdjustmentDetail.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_stockAdjustmentDetail.ItemStock.ProductItem.ID.Value);
            }
            if (_stockAdjustmentDetail.Store != null)
                _stockAdjustmentDetail.Store = new OperationObjects.StoreOP().GetStore(_stockAdjustmentDetail.Store.ID.Value);
            if (_stockAdjustmentDetail.Location != null)
                _stockAdjustmentDetail.Location = new OperationObjects.LocationOP().GetLocation(_stockAdjustmentDetail.Location.ID.Value);
            if (_stockAdjustmentDetail.Rack != null)
                _stockAdjustmentDetail.Rack = new OperationObjects.RackOP().GetRack(_stockAdjustmentDetail.Rack.ID.Value);

            StringBuilder _sbProductItem = new StringBuilder();
            _sbProductItem.Append(_stockAdjustmentDetail.ItemStock.ProductItem != null ? _stockAdjustmentDetail.ItemStock.ProductItem.Code + "-" + _stockAdjustmentDetail.ItemStock.ProductItem.Description : "-").Append("<br>");
            _sbProductItem.Append("[" + _stockAdjustmentDetail.ItemStock.UnitPrice.Value.ToString("F") + " / " + _stockAdjustmentDetail.ItemStock.SellingPrice.Value.ToString("F") + "]");

            StringBuilder _sbType = new StringBuilder();
            _sbType.Append(_stockAdjustmentDetail.StockType != null ? _stockAdjustmentDetail.StockType.Value.ToString() : "-").Append("<br>");
            _sbType.Append(_stockAdjustmentDetail.AdjustmentType != null ? _stockAdjustmentDetail.AdjustmentType.Value.ToString() : "-");

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_stockAdjustmentDetail.Store != null ? _stockAdjustmentDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Location != null ? _stockAdjustmentDetail.Location.Description : string.Empty).Append("<br>");
            _sbLocation.Append(_stockAdjustmentDetail.Rack != null ? _stockAdjustmentDetail.Rack.Description : string.Empty);

            e.Row.Cells[0].Text = _sbProductItem.ToString();
            e.Row.Cells[1].Text = _sbType.ToString();
            e.Row.Cells[2].Text = _sbLocation.ToString();
            e.Row.Cells[3].Text = _stockAdjustmentDetail.Quantity != null ? _stockAdjustmentDetail.Quantity.Value.ToString() : "-";
            e.Row.Cells[4].Text = _stockAdjustmentDetail.ReturnQuantity != null ? _stockAdjustmentDetail.ReturnQuantity.Value.ToString() : "-";
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _stockAdjustment = ViewState[_target] as SolutionObjects.StockAdjustment;

        _stockAdjustment.ApprovalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), DropDownListStatus.SelectedValue);
        _stockAdjustment.UserApproved = User.Identity.Name;
        _stockAdjustment.DateApproved = DateTime.Now;
        _stockAdjustment.ApprovedReason = TextBoxReason.Text.Trim();
        _stockAdjustment.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.StockAdjustmentOP().ApproveStockAdjustment(_stockAdjustment);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                _stockAdjustment = new SolutionObjects.StockAdjustment();
                ViewState[_target] = _stockAdjustment;
                PanelSearchDetails.Visible = true;
                PanelView.Visible = false;
                LabelPageOperation.Text = "Search";
                LinkButtonSearchOperation.Visible = false;
                Search();
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
}