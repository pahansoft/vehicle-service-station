﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class StockAdjustmentOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveStockAdjustment(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (stockAdjustment.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.StockAdjustmentDO().Create(stockAdjustment);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new StockAdjustmentDetail List
                    foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in stockAdjustment.StockAdjustmentDetailList)
                    {
                        innerDTO = new DataAccessObjects.StockAdjustmentDetailDO().Create(stockAdjustmentDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.StockAdjustmentDO().Edit(stockAdjustment);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit StockAdjustmentDetail List
                    foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in stockAdjustment.StockAdjustmentDetailList)
                    {
                        if (stockAdjustmentDetail.ID == null)
                            innerDTO = new DataAccessObjects.StockAdjustmentDetailDO().Create(stockAdjustmentDetail);
                        else
                            innerDTO = new DataAccessObjects.StockAdjustmentDetailDO().Edit(stockAdjustmentDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete StockAdjustmentDetail List
                    foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in stockAdjustment.StockAdjustmentDetailList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.StockAdjustmentDetailDO().Delete(stockAdjustmentDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject ApproveStockAdjustment(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.StockAdjustmentDO().Edit(stockAdjustment);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                if (stockAdjustment.ApprovalStatus == SolutionObjects.ApprovalStatusses.Approved)
                {
                    CreateItemStock(stockAdjustment);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in stockAdjustment.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteStockAdjustment(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.StockAdjustmentDO().Delete(stockAdjustment);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete StockAdjustmentDetail List
                foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in stockAdjustment.StockAdjustmentDetailList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.StockAdjustmentDetailDO().Delete(stockAdjustmentDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.StockAdjustment GetStockAdjustment(int id)
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDO().Find(id);
        }

        public Inventory.SolutionObjects.StockAdjustment GetStockAdjustmentProxy(int id)
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> GetAllStockAdjustments()
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> SearchStockAdjustments(string adjustmentNumber, DateTime? dateAdjustment, SolutionObjects.ApprovalStatusses? approvalStatus)
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDO().Search(adjustmentNumber, dateAdjustment, approvalStatus);
        }

        private SolutionObjects.StockAdjustment CreateItemStock(SolutionObjects.StockAdjustment _stockAdjustment)
        {
            _stockAdjustment.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            SolutionObjects.ItemStock _itemStock = null;
            SolutionObjects.ItemStockTransaction _itemStockTransaction;
            bool _hasFound;
            foreach (SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail in _stockAdjustment.StockAdjustmentDetailList)
            {
                _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(stockAdjustmentDetail.ItemStock.ID.Value);
                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                if (stockAdjustmentDetail.AdjustmentType.Value == SolutionObjects.AdjustmentTypes.Plus)
                    _itemStock.CurrentQuantity += stockAdjustmentDetail.Quantity;
                else
                    _itemStock.CurrentQuantity -= stockAdjustmentDetail.Quantity;
                _itemStock.User = stockAdjustmentDetail.User;

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _itemStock;
                _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.StockAdjustment;
                _itemStockTransaction.StockType = stockAdjustmentDetail.StockType;
                _itemStockTransaction.TransactionID = _stockAdjustment.ID;
                _itemStockTransaction.TransactionDetailID = stockAdjustmentDetail.ID;
                _itemStockTransaction.ReferenceNumber = _stockAdjustment.AdjustmentNumber;
                _itemStockTransaction.Store = _itemStock.Store;
                _itemStockTransaction.Location = _itemStock.Location;
                _itemStockTransaction.Rack = _itemStock.Rack;
                _itemStockTransaction.Quantity = stockAdjustmentDetail.Quantity;
                _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                if (stockAdjustmentDetail.StockType.Value == SolutionObjects.StockTypes.Trading)
                    _itemStockTransaction.Remarks = "Stock Adjustment - Trading";
                else if (stockAdjustmentDetail.StockType.Value == SolutionObjects.StockTypes.Damaged)
                    _itemStockTransaction.Remarks = "Stock Adjustment - Damaged";
                else
                    _itemStockTransaction.Remarks = "Stock Adjustment - Expired";
                _itemStockTransaction.User = _stockAdjustment.User;
                _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _stockAdjustment.ItemStockList.Add(_itemStock);

                _hasFound = false;
                if (stockAdjustmentDetail.StockType.Value != SolutionObjects.StockTypes.Trading)
                {
                    IList<SolutionObjects.ItemStock> _existItemStockList = new OperationObjects.ItemStockOP().Search(_itemStock.ProductItem, stockAdjustmentDetail.Store, stockAdjustmentDetail.Location, stockAdjustmentDetail.Rack, stockAdjustmentDetail.StockType.Value, _itemStock.LotNumber, _itemStock.UnitPrice, _itemStock.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _itemStock.CurrentQuantity += stockAdjustmentDetail.Quantity;
                        _hasFound = true;
                    }

                    if (!_hasFound)
                    {
                        _itemStock = new SolutionObjects.ItemStock();
                        _itemStock.CurrentQuantity = stockAdjustmentDetail.Quantity;
                        _itemStock.ProductItem = stockAdjustmentDetail.ItemStock.ProductItem;
                        _itemStock.StockTypes = stockAdjustmentDetail.StockType.Value;
                        _itemStock.LotNumber = stockAdjustmentDetail.ItemStock.LotNumber;
                        _itemStock.Store = stockAdjustmentDetail.Store;
                        _itemStock.Location = stockAdjustmentDetail.Location;
                        _itemStock.Rack = stockAdjustmentDetail.Rack;
                        _itemStock.UnitPrice = stockAdjustmentDetail.ItemStock.UnitPrice;
                        _itemStock.SellingPrice = stockAdjustmentDetail.ItemStock.SellingPrice;
                        _itemStock.DateManufactured = stockAdjustmentDetail.ItemStock.DateManufactured;
                        _itemStock.DateExpiry = stockAdjustmentDetail.ItemStock.DateExpiry;
                    }

                    _itemStock.User = _stockAdjustment.User;
                    _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                    _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                    _itemStockTransaction.ItemStock = _itemStock;
                    _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                    _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.StockAdjustment;
                    _itemStockTransaction.StockType = stockAdjustmentDetail.StockType;
                    _itemStockTransaction.TransactionID = _stockAdjustment.ID;
                    _itemStockTransaction.TransactionDetailID = stockAdjustmentDetail.ID;
                    _itemStockTransaction.ReferenceNumber = _stockAdjustment.AdjustmentNumber;
                    _itemStockTransaction.Store = _itemStock.Store;
                    _itemStockTransaction.Location = _itemStock.Location;
                    _itemStockTransaction.Rack = _itemStock.Rack;
                    _itemStockTransaction.Quantity = stockAdjustmentDetail.Quantity;
                    _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                    _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                    if (stockAdjustmentDetail.StockType.Value == SolutionObjects.StockTypes.Damaged)
                        _itemStockTransaction.Remarks = "Stock Adjustment - Damaged";
                    else
                        _itemStockTransaction.Remarks = "Stock Adjustment - Expired";
                    _itemStockTransaction.User = _stockAdjustment.User;
                    _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                    _stockAdjustment.ItemStockList.Add(_itemStock);
                }
            }

            return _stockAdjustment;
        }
    }
}
