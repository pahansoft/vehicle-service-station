﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class GRN : SolutionObjectBase
    {
        public GRN() { }
        public GRN(int? id) { this.ID = id; }

        public string GRNNumber { get; set; }
        public DateTime? DateGRN { get; set; }
        //public string PONumber { get; set; }
        //public DateTime? DatePO { get; set; }
        public SolutionObjects.Supplier Supplier { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal? TotalTax { get; set; }
        public decimal? TotalGrossAmount { get; set; }
        public decimal? TotalNetAmount { get; set; }
        public string Remarks { get; set; }

        public bool IsDirectGRN { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal? BalanceAmount { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string ApprovedBy { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.GRNDetail> GRNDetailList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
