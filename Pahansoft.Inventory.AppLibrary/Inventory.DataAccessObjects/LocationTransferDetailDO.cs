﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class LocationTransferDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateLocationTransferDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransferDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationTransferID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.LocationTransfer != null ? locationTransferDetail.LocationTransfer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromStoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromStore != null ? locationTransferDetail.FromStore.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromLocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromLocation != null ? locationTransferDetail.FromLocation.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromRackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromRack != null ? locationTransferDetail.FromRack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToStoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToStore != null ? locationTransferDetail.ToStore.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToLocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToLocation != null ? locationTransferDetail.ToLocation.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToRackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToRack != null ? locationTransferDetail.ToRack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ItemStock != null ? locationTransferDetail.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, locationTransferDetail.Quantity);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransferDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                locationTransferDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditLocationTransferDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, locationTransferDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransferDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationTransferID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.LocationTransfer != null ? locationTransferDetail.LocationTransfer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromStoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromStore != null ? locationTransferDetail.FromStore.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromLocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromLocation != null ? locationTransferDetail.FromLocation.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FromRackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.FromRack != null ? locationTransferDetail.FromRack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToStoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToStore != null ? locationTransferDetail.ToStore.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToLocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToLocation != null ? locationTransferDetail.ToLocation.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ToRackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ToRack != null ? locationTransferDetail.ToRack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ItemStock != null ? locationTransferDetail.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, locationTransferDetail.Quantity);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransferDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteLocationTransferDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransferDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, locationTransferDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, locationTransferDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = locationTransferDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                locationTransferDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.LocationTransferDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetLocationTransferDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int locationTransferIDOrdinal = dr.GetOrdinal("LocationTransferID");
                        int fromStoreIDOrdinal = dr.GetOrdinal("FromStoreID");
                        int fromLocationIDOrdinal = dr.GetOrdinal("FromLocationID");
                        int fromRackIDOrdinal = dr.GetOrdinal("FromRackID");
                        int toStoreIDOrdinal = dr.GetOrdinal("ToStoreID");
                        int toLocationIDOrdinal = dr.GetOrdinal("ToLocationID");
                        int toRackIDOrdinal = dr.GetOrdinal("ToRackID");
                        int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                        int quantityOrdinal = dr.GetOrdinal("Quantity");

                        locationTransferDetail = new Inventory.SolutionObjects.LocationTransferDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransferDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(locationTransferIDOrdinal)) locationTransferDetail.LocationTransfer = new SolutionObjects.LocationTransfer(dr.GetInt32(locationTransferIDOrdinal));
                        if (!dr.IsDBNull(fromStoreIDOrdinal)) locationTransferDetail.FromStore = new SolutionObjects.Store(dr.GetInt32(fromStoreIDOrdinal));
                        if (!dr.IsDBNull(fromLocationIDOrdinal)) locationTransferDetail.FromLocation = new SolutionObjects.Location(dr.GetInt32(fromLocationIDOrdinal));
                        if (!dr.IsDBNull(fromRackIDOrdinal)) locationTransferDetail.FromRack = new SolutionObjects.Rack(dr.GetInt32(fromRackIDOrdinal));
                        if (!dr.IsDBNull(toStoreIDOrdinal)) locationTransferDetail.ToStore = new SolutionObjects.Store(dr.GetInt32(toStoreIDOrdinal));
                        if (!dr.IsDBNull(toLocationIDOrdinal)) locationTransferDetail.ToLocation = new SolutionObjects.Location(dr.GetInt32(toLocationIDOrdinal));
                        if (!dr.IsDBNull(toRackIDOrdinal)) locationTransferDetail.ToRack = new SolutionObjects.Rack(dr.GetInt32(toRackIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) locationTransferDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) locationTransferDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransferDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllLocationTransferDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> locationTransferDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.LocationTransferDetail>();
            Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int locationTransferIDOrdinal = dr.GetOrdinal("LocationTransferID");
                    int fromStoreIDOrdinal = dr.GetOrdinal("FromStoreID");
                    int fromLocationIDOrdinal = dr.GetOrdinal("FromLocationID");
                    int fromRackIDOrdinal = dr.GetOrdinal("FromRackID");
                    int toStoreIDOrdinal = dr.GetOrdinal("ToStoreID");
                    int toLocationIDOrdinal = dr.GetOrdinal("ToLocationID");
                    int toRackIDOrdinal = dr.GetOrdinal("ToRackID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");

                    while (dr.Read())
                    {
                        locationTransferDetail = new Inventory.SolutionObjects.LocationTransferDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransferDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(locationTransferIDOrdinal)) locationTransferDetail.LocationTransfer = new SolutionObjects.LocationTransfer(dr.GetInt32(locationTransferIDOrdinal));
                        if (!dr.IsDBNull(fromStoreIDOrdinal)) locationTransferDetail.FromStore = new SolutionObjects.Store(dr.GetInt32(fromStoreIDOrdinal));
                        if (!dr.IsDBNull(fromLocationIDOrdinal)) locationTransferDetail.FromLocation = new SolutionObjects.Location(dr.GetInt32(fromLocationIDOrdinal));
                        if (!dr.IsDBNull(fromRackIDOrdinal)) locationTransferDetail.FromRack = new SolutionObjects.Rack(dr.GetInt32(fromRackIDOrdinal));
                        if (!dr.IsDBNull(toStoreIDOrdinal)) locationTransferDetail.ToStore = new SolutionObjects.Store(dr.GetInt32(toStoreIDOrdinal));
                        if (!dr.IsDBNull(toLocationIDOrdinal)) locationTransferDetail.ToLocation = new SolutionObjects.Location(dr.GetInt32(toLocationIDOrdinal));
                        if (!dr.IsDBNull(toRackIDOrdinal)) locationTransferDetail.ToRack = new SolutionObjects.Rack(dr.GetInt32(toRackIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) locationTransferDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) locationTransferDetail.Quantity = dr.GetDecimal(quantityOrdinal);

                        locationTransferDetailList.Add(locationTransferDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransferDetailList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> GetAllFor(SolutionObjects.LocationTransfer locationTransfer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllLocationTransferDetailsForLocationTransfer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationTransferID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, locationTransfer.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> locationTransferDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.LocationTransferDetail>();
            Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int locationTransferIDOrdinal = dr.GetOrdinal("LocationTransferID");
                    int fromStoreIDOrdinal = dr.GetOrdinal("FromStoreID");
                    int fromLocationIDOrdinal = dr.GetOrdinal("FromLocationID");
                    int fromRackIDOrdinal = dr.GetOrdinal("FromRackID");
                    int toStoreIDOrdinal = dr.GetOrdinal("ToStoreID");
                    int toLocationIDOrdinal = dr.GetOrdinal("ToLocationID");
                    int toRackIDOrdinal = dr.GetOrdinal("ToRackID");
                    int itemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");

                    while (dr.Read())
                    {
                        locationTransferDetail = new Inventory.SolutionObjects.LocationTransferDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) locationTransferDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(locationTransferIDOrdinal)) locationTransferDetail.LocationTransfer = new SolutionObjects.LocationTransfer(dr.GetInt32(locationTransferIDOrdinal));
                        if (!dr.IsDBNull(fromStoreIDOrdinal)) locationTransferDetail.FromStore = new SolutionObjects.Store(dr.GetInt32(fromStoreIDOrdinal));
                        if (!dr.IsDBNull(fromLocationIDOrdinal)) locationTransferDetail.FromLocation = new SolutionObjects.Location(dr.GetInt32(fromLocationIDOrdinal));
                        if (!dr.IsDBNull(fromRackIDOrdinal)) locationTransferDetail.FromRack = new SolutionObjects.Rack(dr.GetInt32(fromRackIDOrdinal));
                        if (!dr.IsDBNull(toStoreIDOrdinal)) locationTransferDetail.ToStore = new SolutionObjects.Store(dr.GetInt32(toStoreIDOrdinal));
                        if (!dr.IsDBNull(toLocationIDOrdinal)) locationTransferDetail.ToLocation = new SolutionObjects.Location(dr.GetInt32(toLocationIDOrdinal));
                        if (!dr.IsDBNull(toRackIDOrdinal)) locationTransferDetail.ToRack = new SolutionObjects.Rack(dr.GetInt32(toRackIDOrdinal));
                        if (!dr.IsDBNull(itemStockIDOrdinal)) locationTransferDetail.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(itemStockIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) locationTransferDetail.Quantity = dr.GetDecimal(quantityOrdinal);

                        locationTransferDetailList.Add(locationTransferDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return locationTransferDetailList;
        }

        #endregion
    }
}
