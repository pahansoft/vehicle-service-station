﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class CashCollectorCommission : SolutionObjectBase
    {
        public CashCollectorCommission() { }
        public CashCollectorCommission(int? id) { this.ID = id; }

        public SolutionObjects.CashCollector CashCollector { get; set; }
        public SolutionObjects.CashCollectionSummary CashCollectionSummary { get; set; }
        public DateTime? DateCommission { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? CommissionAmount { get; set; }
    }
}
