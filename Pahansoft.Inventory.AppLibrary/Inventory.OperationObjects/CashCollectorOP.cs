﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CashCollectorOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCashCollector(Inventory.SolutionObjects.CashCollector cashCollector)
        {
            if (cashCollector.ID == null)
                return new Inventory.DataAccessObjects.CashCollectorDO().Create(cashCollector);
            else
                return new Inventory.DataAccessObjects.CashCollectorDO().Edit(cashCollector);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCashCollector(Inventory.SolutionObjects.CashCollector cashCollector)
        {
            if (cashCollector.ID != null)
                return new Inventory.DataAccessObjects.CashCollectorDO().Delete(cashCollector);
            else
            {
                cashCollector.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = cashCollector;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.CashCollector GetCashCollector(int id)
        {
            return new Inventory.DataAccessObjects.CashCollectorDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> GetAllCashCollectors()
        {
            return new Inventory.DataAccessObjects.CashCollectorDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> SearchCashCollectors(string code, string name)
        {
            return new Inventory.DataAccessObjects.CashCollectorDO().Search(code, name);
        }
    }
}
