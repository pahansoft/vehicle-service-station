﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CashCollectorDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.CashCollector cashCollector)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateCashCollector";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollector;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollector.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.CashCollector cashCollector)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditCashCollector";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollector.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollector.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollector;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CashCollector cashCollector)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteCashCollector";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollector.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollector.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollector.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollector;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollector.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.CashCollector Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetCashCollector";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CashCollector cashCollector = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int nameOrdinal = dr.GetOrdinal("Name");
                        int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                        int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                        int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                        int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                        int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                        int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                        int faxOrdinal = dr.GetOrdinal("Fax");
                        int emailOrdinal = dr.GetOrdinal("Email");

                        cashCollector = new Inventory.SolutionObjects.CashCollector(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollector.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) cashCollector.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) cashCollector.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) cashCollector.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) cashCollector.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) cashCollector.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) cashCollector.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) cashCollector.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) cashCollector.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) cashCollector.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) cashCollector.Email = dr.GetString(emailOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollector;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllCashCollectors";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> cashCollectorList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollector>();
            Inventory.SolutionObjects.CashCollector cashCollector;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        cashCollector = new Inventory.SolutionObjects.CashCollector(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollector.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) cashCollector.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) cashCollector.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) cashCollector.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) cashCollector.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) cashCollector.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) cashCollector.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) cashCollector.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) cashCollector.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) cashCollector.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) cashCollector.Email = dr.GetString(emailOrdinal);

                        cashCollectorList.Add(cashCollector);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectorList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> Search(string code, string name)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllCashCollectors";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, name != string.Empty ? name : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollector> cashCollectorList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollector>();
            Inventory.SolutionObjects.CashCollector cashCollector;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        cashCollector = new Inventory.SolutionObjects.CashCollector(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollector.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) cashCollector.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) cashCollector.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) cashCollector.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) cashCollector.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) cashCollector.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) cashCollector.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) cashCollector.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) cashCollector.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) cashCollector.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) cashCollector.Email = dr.GetString(emailOrdinal);

                        cashCollectorList.Add(cashCollector);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectorList;
        }

        #endregion
    }
}
