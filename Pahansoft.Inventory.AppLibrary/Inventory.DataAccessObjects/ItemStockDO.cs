﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class ItemStockDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.ItemStock itemStock)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.ProductItem != null ? itemStock.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.StockTypes != null ? itemStock.StockTypes.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.LotNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Store != null ? itemStock.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Location != null ? itemStock.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Rack != null ? itemStock.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@OpeningQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.OpeningQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CurrentQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.CurrentQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateManufactured", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, itemStock.DateManufactured);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateExpiry", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, itemStock.DateExpiry);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStock;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                itemStock.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.ItemStock itemStock)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, itemStock.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.ProductItem != null ? itemStock.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.StockTypes != null ? itemStock.StockTypes.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.LotNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Store != null ? itemStock.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Location != null ? itemStock.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.Rack != null ? itemStock.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@OpeningQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.OpeningQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CurrentQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.CurrentQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, itemStock.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateManufactured", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, itemStock.DateManufactured);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateExpiry", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, itemStock.DateExpiry);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStock;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.ItemStock itemStock)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, itemStock.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, itemStock.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, itemStock.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = itemStock;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                itemStock.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.ItemStock Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ItemStock itemStock = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                        int stockTypeOrdinal = dr.GetOrdinal("StockType");
                        int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int rackIDOrdinal = dr.GetOrdinal("RackID");
                        int openingQuantityOrdinal = dr.GetOrdinal("OpeningQuantity");
                        int currentQuantityOrdinal = dr.GetOrdinal("CurrentQuantity");
                        int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                        int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        itemStock = new Inventory.SolutionObjects.ItemStock(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStock.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStock.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStock.StockTypes = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) itemStock.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStock.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStock.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStock.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(openingQuantityOrdinal)) itemStock.OpeningQuantity = dr.GetDecimal(openingQuantityOrdinal);
                        if (!dr.IsDBNull(currentQuantityOrdinal)) itemStock.CurrentQuantity = dr.GetDecimal(currentQuantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStock.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStock.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) itemStock.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) itemStock.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStock.Remarks = dr.GetString(remarksOrdinal);

                        itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();
                        foreach (SolutionObjects.ItemStockTransaction itemStockTransaction in new DataAccessObjects.ItemStockTransactionDO().GetAllFor(itemStock))
                            itemStock.ItemStockTransactionList.Add(itemStockTransaction);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStock;
        }

        public Inventory.SolutionObjects.ItemStock FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetItemStock";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ItemStock itemStock = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                        int stockTypeOrdinal = dr.GetOrdinal("StockType");
                        int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int rackIDOrdinal = dr.GetOrdinal("RackID");
                        int openingQuantityOrdinal = dr.GetOrdinal("OpeningQuantity");
                        int currentQuantityOrdinal = dr.GetOrdinal("CurrentQuantity");
                        int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                        int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");

                        itemStock = new Inventory.SolutionObjects.ItemStock(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStock.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStock.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStock.StockTypes = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) itemStock.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStock.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStock.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStock.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(openingQuantityOrdinal)) itemStock.OpeningQuantity = dr.GetDecimal(openingQuantityOrdinal);
                        if (!dr.IsDBNull(currentQuantityOrdinal)) itemStock.CurrentQuantity = dr.GetDecimal(currentQuantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStock.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStock.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) itemStock.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) itemStock.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStock.Remarks = dr.GetString(remarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStock;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllItemStocks";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> itemStockList = new System.Collections.Generic.List<Inventory.SolutionObjects.ItemStock>();
            Inventory.SolutionObjects.ItemStock itemStock;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int openingQuantityOrdinal = dr.GetOrdinal("OpeningQuantity");
                    int currentQuantityOrdinal = dr.GetOrdinal("CurrentQuantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                    int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        itemStock = new Inventory.SolutionObjects.ItemStock(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStock.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStock.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStock.StockTypes = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) itemStock.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStock.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStock.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStock.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(openingQuantityOrdinal)) itemStock.OpeningQuantity = dr.GetDecimal(openingQuantityOrdinal);
                        if (!dr.IsDBNull(currentQuantityOrdinal)) itemStock.CurrentQuantity = dr.GetDecimal(currentQuantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStock.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStock.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) itemStock.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) itemStock.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStock.Remarks = dr.GetString(remarksOrdinal);

                        itemStockList.Add(itemStock);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> Search(SolutionObjects.ProductItem productItem, SolutionObjects.Store store, SolutionObjects.Location location, SolutionObjects.Rack rack, SolutionObjects.StockTypes? stockType, string lotNumber, decimal? unitPrice, decimal? sellingPrice)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllItemStocks";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem != null ? productItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, store != null ? store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, location != null ? location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack != null ? rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockType != null ? stockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, lotNumber != string.Empty ? lotNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, unitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, sellingPrice);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> itemStockList = new System.Collections.Generic.List<Inventory.SolutionObjects.ItemStock>();
            Inventory.SolutionObjects.ItemStock itemStock;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int openingQuantityOrdinal = dr.GetOrdinal("OpeningQuantity");
                    int currentQuantityOrdinal = dr.GetOrdinal("CurrentQuantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                    int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        itemStock = new Inventory.SolutionObjects.ItemStock(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStock.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStock.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStock.StockTypes = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) itemStock.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStock.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStock.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStock.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(openingQuantityOrdinal)) itemStock.OpeningQuantity = dr.GetDecimal(openingQuantityOrdinal);
                        if (!dr.IsDBNull(currentQuantityOrdinal)) itemStock.CurrentQuantity = dr.GetDecimal(currentQuantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStock.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStock.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) itemStock.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) itemStock.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStock.Remarks = dr.GetString(remarksOrdinal);

                        itemStockList.Add(itemStock);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> FilterSearch(string productItemCode, string productItemDescription, string lotNumber, SolutionObjects.StockTypes? stockType)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllItemStocksForFilterSearch";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemCode", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItemCode != string.Empty ? productItemCode : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemDescription", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItemDescription != string.Empty ? productItemDescription : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, lotNumber != string.Empty ? lotNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockType != null ? stockType.Value.ToString() : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> itemStockList = new System.Collections.Generic.List<Inventory.SolutionObjects.ItemStock>();
            Inventory.SolutionObjects.ItemStock itemStock;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int openingQuantityOrdinal = dr.GetOrdinal("OpeningQuantity");
                    int currentQuantityOrdinal = dr.GetOrdinal("CurrentQuantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                    int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        itemStock = new Inventory.SolutionObjects.ItemStock(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) itemStock.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(productItemIDOrdinal)) itemStock.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) itemStock.StockTypes = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) itemStock.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) itemStock.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) itemStock.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) itemStock.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(openingQuantityOrdinal)) itemStock.OpeningQuantity = dr.GetDecimal(openingQuantityOrdinal);
                        if (!dr.IsDBNull(currentQuantityOrdinal)) itemStock.CurrentQuantity = dr.GetDecimal(currentQuantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) itemStock.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) itemStock.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) itemStock.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) itemStock.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) itemStock.Remarks = dr.GetString(remarksOrdinal);

                        itemStockList.Add(itemStock);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return itemStockList;
        }

        #endregion
    }
}
