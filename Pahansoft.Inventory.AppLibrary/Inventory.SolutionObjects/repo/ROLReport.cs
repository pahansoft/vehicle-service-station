﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects.repo
{
      [Serializable]


public class ROLReport : SolutionObjectBase
 
    {
 public ROLReport() { }
 public ROLReport(int? id) { this.ID = id; }

 public string Code { get; set; }
 public string Description { get; set; }
 public string ReferenceNumber { get; set; }
 public string OtherDescription { get; set; }
 public int? ROL { get; set; }
 public decimal? Quantity { get; set; }


    }
}
