﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SalesRefDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.SalesRef salesRef)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateSalesRef";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRef;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesRef.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.SalesRef salesRef)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditSalesRef";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRef.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesRef.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRef;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.SalesRef salesRef)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteSalesRef";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRef.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesRef.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRef.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRef;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesRef.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.SalesRef Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetSalesRef";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.SalesRef salesRef = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int nameOrdinal = dr.GetOrdinal("Name");
                        int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                        int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                        int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                        int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                        int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                        int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                        int faxOrdinal = dr.GetOrdinal("Fax");
                        int emailOrdinal = dr.GetOrdinal("Email");

                        salesRef = new Inventory.SolutionObjects.SalesRef(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRef.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) salesRef.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) salesRef.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) salesRef.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) salesRef.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) salesRef.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) salesRef.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) salesRef.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) salesRef.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) salesRef.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) salesRef.Email = dr.GetString(emailOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRef;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllSalesRefs";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> salesRefList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesRef>();
            Inventory.SolutionObjects.SalesRef salesRef;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        salesRef = new Inventory.SolutionObjects.SalesRef(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRef.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) salesRef.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) salesRef.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) salesRef.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) salesRef.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) salesRef.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) salesRef.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) salesRef.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) salesRef.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) salesRef.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) salesRef.Email = dr.GetString(emailOrdinal);

                        salesRefList.Add(salesRef);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRefList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> Search(string code, string name)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllSalesRefs";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, name != string.Empty ? name : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRef> salesRefList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesRef>();
            Inventory.SolutionObjects.SalesRef salesRef;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        salesRef = new Inventory.SolutionObjects.SalesRef(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRef.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) salesRef.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) salesRef.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) salesRef.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) salesRef.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) salesRef.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) salesRef.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) salesRef.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) salesRef.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) salesRef.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) salesRef.Email = dr.GetString(emailOrdinal);

                        salesRefList.Add(salesRef);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRefList;
        }

        #endregion
    }
}
