﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_prod_item_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.ProductItem _productItem;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        //TextBoxCode.Text = string.Empty;
        TextBoxDescription.Text = string.Empty;
        TextBoxOtherDescription.Text = string.Empty;

        TextBoxReferenceNumber.Text = string.Empty;
        DropDownListBrandName.SelectedIndex = 0;
        DropDownListProductType.SelectedIndex = 0;
        DropDownListManufacturer.SelectedIndex = 0;
        TextBoxROL.Text = string.Empty;
        TextBoxROQuantity.Text = string.Empty;
        DropDownListCountry.SelectedIndex = 0;
        DropDownListImporter.SelectedIndex = 0;
        TextBoxModel.Text = string.Empty;
        TextBoxColour.Text = string.Empty;
        TextBoxCostPrice.Text = string.Empty;
        TextBoxSellingPrice.Text = string.Empty;

        CheckBoxBrandNewItem.Checked = false;
    }
    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListBrandName);
        foreach (SolutionObjects.BrandName brandName in new OperationObjects.BrandNameOP().GetAllBrandNames())
            DropDownListBrandName.Items.Add(new ListItem(brandName.Description, brandName.ID.Value.ToString()));

        Util.ClearDropdown(DropDownListManufacturer);
        foreach (SolutionObjects.Manufacturer manufacturer in new OperationObjects.ManufacturerOP().GetAllManufacturers())
            DropDownListManufacturer.Items.Add(new ListItem(manufacturer.Description, manufacturer.ID.Value.ToString()));

        Util.ClearDropdown(DropDownListProductType);
        Util.ClearDropdown(DropDownListSrchProductType);
        foreach (var productType in new OperationObjects.ProductTypeOP().GetAllProductTypes())
        {
            DropDownListProductType.Items.Add(new ListItem(productType.Description, productType.ID.Value.ToString()));
            DropDownListSrchProductType.Items.Add(new ListItem(productType.Description, productType.ID.Value.ToString()));
        }

        Util.ClearDropdown(DropDownListCountry);
        foreach (SolutionObjects.Country country in new OperationObjects.ContryOP().GetAllCountries())
            DropDownListCountry.Items.Add(new ListItem(country.Description, country.ID.Value.ToString()));

        Util.ClearDropdown(DropDownListImporter);
        foreach (SolutionObjects.SalesRef salesRef in new OperationObjects.SalesRefOP().GetAllSalesRefs())
            DropDownListImporter.Items.Add(new ListItem(salesRef.Name, salesRef.ID.Value.ToString()));
    }

    private void ClearSearch()
    {
        TextBoxSrchCode.Text = string.Empty;
        TextBoxSrchDescription.Text = string.Empty;
        TextBoxSrchReferenceNumber.Text = string.Empty;
        DropDownListSrchProductType.SelectedIndex = 0;
        CheckBoxSrchBrandNewItemsOnly.Checked = false;
    }

    private void Search()
    {
        SolutionObjects.ProductType _productType = null;
        if (DropDownListSrchProductType.SelectedIndex != 0)
            _productType = new SolutionObjects.ProductType(int.Parse(DropDownListSrchProductType.SelectedValue));

        bool? BrandNewItemsOnly = null;
        if (CheckBoxSrchBrandNewItemsOnly.Checked)
            BrandNewItemsOnly = true;

        GridViewSearchList.DataSource = new OperationObjects.ProductItemOP().SearchProductItems(TextBoxSrchCode.Text.Trim(), TextBoxSrchDescription.Text.Trim(), TextBoxSrchReferenceNumber.Text.Trim(), _productType, BrandNewItemsOnly);
        GridViewSearchList.DataBind();
    }
    private int AutoNum()
    {
       // _productItem = new OperationObjects.ProductItemOP().GetproductCode();
       SolutionObjects.ProductItem productItem = new OperationObjects.ProductItemOP().GetproductCode();
       //TextBoxCode.Text = productItem.AutoNum.ToString();
       return productItem.AutoNum;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();

            _productItem = new SolutionObjects.ProductItem();
            ViewState[_target] = _productItem;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
            AutoNum();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _productItem = new SolutionObjects.ProductItem();
        ViewState[_target] = _productItem;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _productItem = ViewState[_target] as SolutionObjects.ProductItem;

        if (_productItem.Code == null)
            _productItem.Code = AutoNum().ToString();
        else
            _productItem.Code = TextBoxCode.Text;
        _productItem.Description = TextBoxDescription.Text.Trim();
        _productItem.OtherDescription = TextBoxOtherDescription.Text.Trim();
        _productItem.ReferenceNumber = TextBoxReferenceNumber.Text.Trim();
        if (DropDownListBrandName.SelectedIndex != 0)
            _productItem.BrandName = new SolutionObjects.BrandName(int.Parse(DropDownListBrandName.SelectedValue));
        else
            _productItem.BrandName = null;
        if (DropDownListManufacturer.SelectedIndex != 0)
            _productItem.Manufacturer = new SolutionObjects.Manufacturer(int.Parse(DropDownListManufacturer.SelectedValue));
        else
            _productItem.Manufacturer = null;
        if (DropDownListProductType.SelectedIndex != 0)
            _productItem.ProductType = new SolutionObjects.ProductType(int.Parse(DropDownListProductType.SelectedValue));
        else
            _productItem.ProductType = null;
        _productItem.ROL = Util.ParseInt(TextBoxROL.Text.Trim());
        _productItem.ROQuantity = Util.ParseInt(TextBoxROQuantity.Text.Trim());
        if (DropDownListCountry.SelectedIndex != 0)
            _productItem.Country = new SolutionObjects.Country(int.Parse(DropDownListCountry.SelectedValue));
        else
            _productItem.Country = null;
        if (DropDownListImporter.SelectedIndex != 0)
            _productItem.SalesRef = new SolutionObjects.SalesRef(int.Parse(DropDownListImporter.SelectedValue));
        else
            _productItem.SalesRef = null;
        _productItem.Model = TextBoxModel.Text.Trim();
        _productItem.Colour = TextBoxColour.Text.Trim();
        _productItem.CostPrice =Decimal.Parse( TextBoxCostPrice.Text.Trim());
        _productItem.SellingPrice = Decimal.Parse (TextBoxSellingPrice.Text.Trim());
        _productItem.IsBrandNewItem = CheckBoxBrandNewItem.Checked;
        _productItem.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.ProductItemOP().SaveProductItem(_productItem);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Itemcode "+(AutoNum()-1).ToString()+" Record saved successfully";

                ClearAll();
                _productItem = new SolutionObjects.ProductItem();
                ViewState[_target] = _productItem;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Code cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Code cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _productItem = ViewState[_target] as SolutionObjects.ProductItem;

        if (_productItem.ID != null)
        {
            _productItem = new OperationObjects.ProductItemOP().GetProductItem(_productItem.ID.Value);
            ViewState[_target] = _productItem;

            TextBoxCode.Text = _productItem.Code;
            TextBoxDescription.Text = _productItem.Description;
            TextBoxReferenceNumber.Text = _productItem.ReferenceNumber;
            if (_productItem.BrandName != null)
                DropDownListBrandName.SelectedValue = _productItem.BrandName.ID.Value.ToString();
            if (_productItem.Manufacturer != null)
                DropDownListManufacturer.SelectedValue = _productItem.Manufacturer.ID.Value.ToString();
            if (_productItem.ProductType != null)
                DropDownListProductType.SelectedValue = _productItem.ProductType.ID.Value.ToString();
            TextBoxROL.Text = _productItem.ROL != null ? _productItem.ROL.Value.ToString() : string.Empty;
            TextBoxROQuantity.Text = _productItem.ROQuantity != null ? _productItem.ROQuantity.Value.ToString() : string.Empty;
            if (_productItem.Country != null)
                DropDownListCountry.SelectedValue = _productItem.Country.ID.Value.ToString();
            if (_productItem.SalesRef != null)
                DropDownListImporter.SelectedValue = _productItem.SalesRef.ID.Value.ToString();
            TextBoxModel.Text = _productItem.Model;
            TextBoxColour.Text = _productItem.Colour;
            TextBoxCostPrice.Text = _productItem.CostPrice.ToString() ;
            TextBoxSellingPrice.Text = _productItem.SellingPrice.ToString();
            CheckBoxBrandNewItem.Checked = _productItem.IsBrandNewItem;
        }
        else
        {
            _productItem = new SolutionObjects.ProductItem();
            ViewState[_target] = _productItem;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _productItem = new OperationObjects.ProductItemOP().GetProductItem(_targetid);

            ClearAll();
            ViewState[_target] = _productItem;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxCode.Text = _productItem.Code;
            TextBoxDescription.Text = _productItem.Description;
            TextBoxOtherDescription.Text = _productItem.OtherDescription;

            TextBoxReferenceNumber.Text = _productItem.ReferenceNumber;
            if (_productItem.BrandName != null)
                DropDownListBrandName.SelectedValue = _productItem.BrandName.ID.Value.ToString();
            if (_productItem.Manufacturer != null)
                DropDownListManufacturer.SelectedValue = _productItem.Manufacturer.ID.Value.ToString();
            if (_productItem.ProductType != null)
                DropDownListProductType.SelectedValue = _productItem.ProductType.ID.Value.ToString();
            TextBoxROL.Text = _productItem.ROL != null ? _productItem.ROL.Value.ToString() : string.Empty;
            TextBoxROQuantity.Text = _productItem.ROQuantity != null ? _productItem.ROQuantity.Value.ToString() : string.Empty;
            if (_productItem.Country != null)
                DropDownListCountry.SelectedValue = _productItem.Country.ID.Value.ToString();
            if (_productItem.SalesRef != null)
                DropDownListImporter.SelectedValue = _productItem.SalesRef.ID.Value.ToString();
            TextBoxModel.Text = _productItem.Model;
            TextBoxColour.Text = _productItem.Colour;
            TextBoxCostPrice.Text = _productItem.CostPrice.ToString();
            TextBoxSellingPrice.Text = _productItem.SellingPrice.ToString();
            CheckBoxBrandNewItem.Checked = _productItem.IsBrandNewItem;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _productItem = new OperationObjects.ProductItemOP().GetProductItem(_targetid);
            _productItem.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.ProductItemOP().DeleteProductItem(_productItem);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _productItem = new OperationObjects.ProductItemOP().GetProductItem(_targetid);

            ViewState[_target] = _productItem;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewCode.Text = Util.FormatEmptyString(_productItem.Code);
            LabelViewDescription.Text = Util.FormatEmptyString(_productItem.Description);
            LabelViewReferenceNumber.Text = Util.FormatEmptyString(_productItem.ReferenceNumber);
            LabelViewBrandName.Text = Util.FormatEmptyString(_productItem.BrandName != null ? new OperationObjects.BrandNameOP().GetBrandName(_productItem.BrandName.ID.Value).Description : string.Empty);
            LabelViewManufacturer.Text = Util.FormatEmptyString(_productItem.Manufacturer != null ? new OperationObjects.ManufacturerOP().GetManufacturer(_productItem.Manufacturer.ID.Value).Description : string.Empty);
            LabelViewProductType.Text = Util.FormatEmptyString(_productItem.ProductType != null ? new OperationObjects.ProductTypeOP().GetProductType(_productItem.ProductType.ID.Value).Description : string.Empty);
            LabelViewROL.Text = Util.FormatEmptyString(_productItem.ROL != null ? _productItem.ROL.Value.ToString() : string.Empty);
            LabelViewROQuantity.Text = Util.FormatEmptyString(_productItem.ROQuantity != null ? _productItem.ROQuantity.Value.ToString() : string.Empty);
            LabelViewOrigin.Text = Util.FormatEmptyString(_productItem.Country != null ? new OperationObjects.ContryOP().GetCountry(_productItem.Country.ID.Value).Description : string.Empty);
            LabelViewImporter.Text = Util.FormatEmptyString(_productItem.SalesRef != null ? new OperationObjects.SalesRefOP().GetSalesRef(_productItem.SalesRef.ID.Value).Name : string.Empty);
            LabelViewModel.Text = Util.FormatEmptyString(_productItem.Model);
            LabelViewColour.Text = Util.FormatEmptyString(_productItem.Colour);
            LabelViewCostPrice.Text =_productItem.CostPrice.ToString();
            LabelViewSellingPrice.Text =_productItem.SellingPrice.ToString();

            LabelViewBrandNewItem.Text = Util.FormatEmptyString(_productItem.IsBrandNewItem ? "Yes" : "No");
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _productItem = e.Row.DataItem as SolutionObjects.ProductItem;
            if (_productItem.BrandName != null)
                _productItem.BrandName = new OperationObjects.BrandNameOP().GetBrandName(_productItem.BrandName.ID.Value);
            if (_productItem.ProductType != null)
                _productItem.ProductType = new OperationObjects.ProductTypeOP().GetProductType(_productItem.ProductType.ID.Value);
            if (_productItem.Country != null)
                _productItem.Country = new OperationObjects.ContryOP().GetCountry(_productItem.Country.ID.Value);
            if (_productItem.SalesRef != null)
                _productItem.SalesRef = new OperationObjects.SalesRefOP().GetSalesRef(_productItem.SalesRef.ID.Value);

            Util.EncodeString(e.Row.Cells[3], _productItem.Code, 50);
            Util.EncodeString(e.Row.Cells[4], _productItem.Description, 50);
            Util.EncodeString(e.Row.Cells[5], _productItem.ReferenceNumber, 50);
            Util.EncodeString(e.Row.Cells[6], _productItem.Country != null ? _productItem.Country.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[7], _productItem.BrandName != null ? _productItem.BrandName.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[8], _productItem.ProductType != null ? _productItem.ProductType.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[9], _productItem.IsBrandNewItem ? "Yes" : "No", 5);
            
            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _productItem.Description + "');");
                }
            }
        }
    }
}