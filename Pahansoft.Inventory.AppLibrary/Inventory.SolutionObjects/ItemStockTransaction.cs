﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class ItemStockTransaction : SolutionObjectBase
    {
        public ItemStockTransaction() { }
        public ItemStockTransaction(int? id) { this.ID = id; }

        public SolutionObjects.ItemStock ItemStock { get; set; }
        public SolutionObjects.ProductItem ProductItem { get; set; }
        public SolutionObjects.TransactionTypes? TransactionType { get; set; }
        public SolutionObjects.StockTypes? StockType { get; set; }
        public int? TransactionID { get; set; }
        public int? TransactionDetailID { get; set; }
        public string ReferenceNumber { get; set; }
        public SolutionObjects.Store Store { get; set; }
        public SolutionObjects.Location Location { get; set; }
        public SolutionObjects.Rack Rack { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? SellingPrice { get; set; }
        public string Remarks { get; set; }
    }
}
