﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class BrandNameDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.BrandName brandName)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateBrandName";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, brandName.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, brandName.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ManufacturerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, brandName.Manufacturer != null ? brandName.Manufacturer.ID : null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = brandName;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                brandName.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.BrandName brandName)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditBrandName";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, brandName.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, brandName.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, brandName.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, brandName.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ManufacturerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, brandName.Manufacturer != null ? brandName.Manufacturer.ID : null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = brandName;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.BrandName brandName)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteBrandName";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, brandName.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, brandName.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, brandName.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = brandName;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                brandName.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.BrandName Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetBrandName";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.BrandName brandName = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int descriptionOrdinal = dr.GetOrdinal("Description");
                        int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");

                        brandName = new Inventory.SolutionObjects.BrandName(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) brandName.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) brandName.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) brandName.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return brandName;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllBrandNames";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> brandNameList = new System.Collections.Generic.List<Inventory.SolutionObjects.BrandName>();
            Inventory.SolutionObjects.BrandName brandName;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");

                    while (dr.Read())
                    {
                        brandName = new Inventory.SolutionObjects.BrandName(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) brandName.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) brandName.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) brandName.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));

                        brandNameList.Add(brandName);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return brandNameList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> Search(string description, SolutionObjects.Manufacturer manufacturer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllBrandNames";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, description != string.Empty ? description : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ManufacturerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, manufacturer != null ? manufacturer.ID : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.BrandName> brandNameList = new System.Collections.Generic.List<Inventory.SolutionObjects.BrandName>();
            Inventory.SolutionObjects.BrandName brandName;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");

                    while (dr.Read())
                    {
                        brandName = new Inventory.SolutionObjects.BrandName(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) brandName.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) brandName.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) brandName.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));

                        brandNameList.Add(brandName);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return brandNameList;
        }

        #endregion
    }
}
