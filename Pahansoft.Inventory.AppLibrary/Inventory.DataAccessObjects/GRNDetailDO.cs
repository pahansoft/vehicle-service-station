﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class GRNDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.GRNDetail gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateGRNDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.GRN != null ? gRNDetail.GRN.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ProductItem != null ? gRNDetail.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.StockType != null ? gRNDetail.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.LotNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Store != null ? gRNDetail.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Location != null ? gRNDetail.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Rack != null ? gRNDetail.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateManufactured", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, gRNDetail.DateManufactured);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateExpiry", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, gRNDetail.DateExpiry);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Discount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Discount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Tax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Tax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.NetAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FreeQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.FreeQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseOrderDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.PurchaseOrderDetail!=null? gRNDetail.PurchaseOrderDetail.ID:null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                gRNDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.GRNDetail gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditGRNDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, gRNDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.GRN != null ? gRNDetail.GRN.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ProductItem != null ? gRNDetail.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StockType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.StockType != null ? gRNDetail.StockType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LotNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.LotNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Store != null ? gRNDetail.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Location != null ? gRNDetail.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@RackID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.Rack != null ? gRNDetail.Rack.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.SellingPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateManufactured", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, gRNDetail.DateManufactured);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateExpiry", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, gRNDetail.DateExpiry);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Discount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Discount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Tax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.Tax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.NetAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@FreeQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.FreeQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseOrderDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.PurchaseOrderDetail != null ? gRNDetail.PurchaseOrderDetail.ID : null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.GRNDetail gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteGRNDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, gRNDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                gRNDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.GRNDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetGRNDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.GRNDetail gRNDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                        int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                        int stockTypeOrdinal = dr.GetOrdinal("StockType");
                        int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int rackIDOrdinal = dr.GetOrdinal("RackID");
                        int quantityOrdinal = dr.GetOrdinal("Quantity");
                        int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                        int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                        int discountOrdinal = dr.GetOrdinal("Discount");
                        int taxOrdinal = dr.GetOrdinal("Tax");
                        int grossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                        int netAmountOrdinal = dr.GetOrdinal("NetAmount");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                        int freeQuantityOrdinal = dr.GetOrdinal("FreeQuantity");
                        int purchaseOrderDetailIDOrdinal = dr.GetOrdinal("PurchaseOrderDetailID");

                        gRNDetail = new Inventory.SolutionObjects.GRNDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNIDOrdinal)) gRNDetail.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) gRNDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) gRNDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) gRNDetail.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) gRNDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) gRNDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) gRNDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) gRNDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) gRNDetail.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) gRNDetail.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) gRNDetail.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) gRNDetail.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) gRNDetail.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) gRNDetail.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossAmountOrdinal)) gRNDetail.GrossAmount = dr.GetDecimal(grossAmountOrdinal);
                        if (!dr.IsDBNull(netAmountOrdinal)) gRNDetail.NetAmount = dr.GetDecimal(netAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) gRNDetail.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetDecimal(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(freeQuantityOrdinal)) gRNDetail.FreeQuantity = dr.GetDecimal(freeQuantityOrdinal);
                        if (!dr.IsDBNull(purchaseOrderDetailIDOrdinal)) gRNDetail.PurchaseOrderDetail =new SolutionObjects.PurchaseOrderDetail( dr.GetInt32(purchaseOrderDetailIDOrdinal));

                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllGRNDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> gRNDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.GRNDetail>();
            Inventory.SolutionObjects.GRNDetail gRNDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                    int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                    int discountOrdinal = dr.GetOrdinal("Discount");
                    int taxOrdinal = dr.GetOrdinal("Tax");
                    int grossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int netAmountOrdinal = dr.GetOrdinal("NetAmount");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                    int freeQuantityOrdinal = dr.GetOrdinal("FreeQuantity");
                    int purchaseOrderDetailIDOrdinal = dr.GetOrdinal("PurchaseOrderDetailID");

                    while (dr.Read())
                    {
                        gRNDetail = new Inventory.SolutionObjects.GRNDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNIDOrdinal)) gRNDetail.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) gRNDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) gRNDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) gRNDetail.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) gRNDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) gRNDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) gRNDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) gRNDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) gRNDetail.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) gRNDetail.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) gRNDetail.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) gRNDetail.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) gRNDetail.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) gRNDetail.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossAmountOrdinal)) gRNDetail.GrossAmount = dr.GetDecimal(grossAmountOrdinal);
                        if (!dr.IsDBNull(netAmountOrdinal)) gRNDetail.NetAmount = dr.GetDecimal(netAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) gRNDetail.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetInt32(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(freeQuantityOrdinal)) gRNDetail.FreeQuantity = dr.GetDecimal(freeQuantityOrdinal);
                        if (!dr.IsDBNull(purchaseOrderDetailIDOrdinal)) gRNDetail.PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(dr.GetInt32(purchaseOrderDetailIDOrdinal));
                        gRNDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetailList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> GetAllFor(SolutionObjects.GRN grn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllGRNDetailsForGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, grn.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> gRNDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.GRNDetail>();
            Inventory.SolutionObjects.GRNDetail gRNDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int gRNIDOrdinal = dr.GetOrdinal("GRNID");
                    int productItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int stockTypeOrdinal = dr.GetOrdinal("StockType");
                    int lotNumberOrdinal = dr.GetOrdinal("LotNumber");
                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int rackIDOrdinal = dr.GetOrdinal("RackID");
                    int quantityOrdinal = dr.GetOrdinal("Quantity");
                    int unitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int sellingPriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int dateManufacturedOrdinal = dr.GetOrdinal("DateManufactured");
                    int dateExpiryOrdinal = dr.GetOrdinal("DateExpiry");
                    int discountOrdinal = dr.GetOrdinal("Discount");
                    int taxOrdinal = dr.GetOrdinal("Tax");
                    int grossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int netAmountOrdinal = dr.GetOrdinal("NetAmount");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                    int freeQuantityOrdinal = dr.GetOrdinal("FreeQuantity");
                    int purchaseOrderDetailIDOrdinal = dr.GetOrdinal("PurchaseOrderDetailID");

                    while (dr.Read())
                    {
                        gRNDetail = new Inventory.SolutionObjects.GRNDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNIDOrdinal)) gRNDetail.GRN = new SolutionObjects.GRN(dr.GetInt32(gRNIDOrdinal));
                        if (!dr.IsDBNull(productItemIDOrdinal)) gRNDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(productItemIDOrdinal));
                        if (!dr.IsDBNull(stockTypeOrdinal)) gRNDetail.StockType = (SolutionObjects.StockTypes)Enum.Parse(typeof(SolutionObjects.StockTypes), dr.GetString(stockTypeOrdinal));
                        if (!dr.IsDBNull(lotNumberOrdinal)) gRNDetail.LotNumber = dr.GetString(lotNumberOrdinal);
                        if (!dr.IsDBNull(storeIDOrdinal)) gRNDetail.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) gRNDetail.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(rackIDOrdinal)) gRNDetail.Rack = new SolutionObjects.Rack(dr.GetInt32(rackIDOrdinal));
                        if (!dr.IsDBNull(quantityOrdinal)) gRNDetail.Quantity = dr.GetDecimal(quantityOrdinal);
                        if (!dr.IsDBNull(unitPriceOrdinal)) gRNDetail.UnitPrice = dr.GetDecimal(unitPriceOrdinal);
                        if (!dr.IsDBNull(sellingPriceOrdinal)) gRNDetail.SellingPrice = dr.GetDecimal(sellingPriceOrdinal);
                        if (!dr.IsDBNull(dateManufacturedOrdinal)) gRNDetail.DateManufactured = dr.GetDateTime(dateManufacturedOrdinal);
                        if (!dr.IsDBNull(dateExpiryOrdinal)) gRNDetail.DateExpiry = dr.GetDateTime(dateExpiryOrdinal);
                        if (!dr.IsDBNull(discountOrdinal)) gRNDetail.Discount = dr.GetDecimal(discountOrdinal);
                        if (!dr.IsDBNull(taxOrdinal)) gRNDetail.Tax = dr.GetDecimal(taxOrdinal);
                        if (!dr.IsDBNull(grossAmountOrdinal)) gRNDetail.GrossAmount = dr.GetDecimal(grossAmountOrdinal);
                        if (!dr.IsDBNull(netAmountOrdinal)) gRNDetail.NetAmount = dr.GetDecimal(netAmountOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) gRNDetail.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetInt32(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(freeQuantityOrdinal)) gRNDetail.FreeQuantity = dr.GetDecimal(freeQuantityOrdinal);
                        if (!dr.IsDBNull(purchaseOrderDetailIDOrdinal)) gRNDetail.PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(dr.GetInt32(purchaseOrderDetailIDOrdinal));
                    
                        gRNDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetailList;
        }

        #endregion
    }
}
