﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects.repo
{
   public class ROLReportOP
    {
       public System.Collections.Generic.IList<Inventory.SolutionObjects.repo.ROLReport> Search( int? store, int? location, int? rack, int? ProductType)
        {
            return new Inventory.DataAccessObjects.repo.ROLReportDO().Search( store, location, rack, ProductType);
        }

    }

}


