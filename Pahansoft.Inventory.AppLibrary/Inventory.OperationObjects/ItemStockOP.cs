﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ItemStockOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveItemStock(Inventory.SolutionObjects.ItemStock itemStock)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            if (itemStock.ID == null)
            {
                dto = new Inventory.DataAccessObjects.ItemStockDO().Create(itemStock);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Add new ItemStockTransaction List
                foreach (SolutionObjects.ItemStockTransaction itemStockTransaction in itemStock.ItemStockTransactionList)
                {
                    innerDTO = new DataAccessObjects.ItemStockTransactionDO().Create(itemStockTransaction);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }
            }
            else
            {
                dto = new Inventory.DataAccessObjects.ItemStockDO().Edit(itemStock);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Add, Edit ItemStockTransaction List
                foreach (SolutionObjects.ItemStockTransaction itemStockTransaction in itemStock.ItemStockTransactionList)
                {
                    if (itemStockTransaction.ID == null)
                        innerDTO = new DataAccessObjects.ItemStockTransactionDO().Create(itemStockTransaction);
                    else
                        innerDTO = new DataAccessObjects.ItemStockTransactionDO().Edit(itemStockTransaction);

                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Delete ItemStockTransaction List
                foreach (SolutionObjects.ItemStockTransaction itemStockTransaction in itemStock.ItemStockTransactionList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.ItemStockTransactionDO().Delete(itemStockTransaction);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }
            }

            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteItemStock(Inventory.SolutionObjects.ItemStock itemStock)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.ItemStockDO().Delete(itemStock);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete ItemStockTransaction List
                foreach (SolutionObjects.ItemStockTransaction itemStockTransaction in itemStock.ItemStockTransactionList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.ItemStockTransactionDO().Delete(itemStockTransaction);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.ItemStock GetItemStock(int id)
        {
            return new Inventory.DataAccessObjects.ItemStockDO().Find(id);
        }

        public Inventory.SolutionObjects.ItemStock GetItemStockProxy(int id)
        {
            return new Inventory.DataAccessObjects.ItemStockDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> GetAllItemStocks()
        {
            return new Inventory.DataAccessObjects.ItemStockDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> Search(SolutionObjects.ProductItem productItem, SolutionObjects.Store store, SolutionObjects.Location location, SolutionObjects.Rack rack, SolutionObjects.StockTypes? stockType, string lotNumber, decimal? unitPrice, decimal? sellingPrice)
        {
            return new Inventory.DataAccessObjects.ItemStockDO().Search(productItem, store, location, rack, stockType, lotNumber, unitPrice, sellingPrice);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStock> FilterSearch(string productItemCode, string productItemDescription, string lotNumber, SolutionObjects.StockTypes? stockType)
        {
            return new Inventory.DataAccessObjects.ItemStockDO().FilterSearch(productItemCode, productItemDescription, lotNumber, stockType);
        }
    }
}
