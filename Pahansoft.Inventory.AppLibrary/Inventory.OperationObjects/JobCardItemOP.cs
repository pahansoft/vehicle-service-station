﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class JobCardItemOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(SolutionObjects.JobCardItem JobCardItem)
        {
            if (JobCardItem.ID == null)
                return new DataAccessObjects.JobCardItemDO().Create(JobCardItem);
            else
                return new DataAccessObjects.JobCardItemDO().Edit(JobCardItem);
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.JobCardItem JobCardItem)
        {
            if (JobCardItem.ID != null)
                return new DataAccessObjects.JobCardItemDO().Delete(JobCardItem);
            else
            {
                JobCardItem.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = JobCardItem;
                return _dts;
            }
        }

        public SolutionObjects.JobCardItem Get(int id)
        {
            return new DataAccessObjects.JobCardItemDO().Find(id);
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCardItem> GetAll()
        {
            return new DataAccessObjects.JobCardItemDO().GetAll();
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCardItem> GetAllFor(SolutionObjects.JobCard JobCard)
        {
            return new DataAccessObjects.JobCardItemDO().GetAllFor(JobCard);
        }
    }
}
