﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class VehicleDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(Inventory.SolutionObjects.VehicleDetail VehicleDetail)
        {
            if (VehicleDetail.ID == null)
                return new Inventory.DataAccessObjects.VehicleDetailDO().Create(VehicleDetail);
            else
                return new Inventory.DataAccessObjects.VehicleDetailDO().Edit(VehicleDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.VehicleDetail VehicleDetail)
        {
            if (VehicleDetail.ID != null)
                return new Inventory.DataAccessObjects.VehicleDetailDO().Delete(VehicleDetail);
            else
            {
                VehicleDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = VehicleDetail;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.VehicleDetail Get(int id)
        {
            return new Inventory.DataAccessObjects.VehicleDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.VehicleDetail> GetAll()
        {
            return new Inventory.DataAccessObjects.VehicleDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.VehicleDetail> Search(string VehicleNumber, int? CustomerID)
        {
            return new Inventory.DataAccessObjects.VehicleDetailDO().Search(VehicleNumber, CustomerID);
        }
    }
}
