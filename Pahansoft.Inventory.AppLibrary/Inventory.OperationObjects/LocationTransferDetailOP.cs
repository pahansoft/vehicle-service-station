﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class LocationTransferDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveLocationTransferDetail(Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail)
        {
            if (locationTransferDetail.ID == null)
                return new Inventory.DataAccessObjects.LocationTransferDetailDO().Create(locationTransferDetail);
            else
                return new Inventory.DataAccessObjects.LocationTransferDetailDO().Edit(locationTransferDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteLocationTransferDetail(Inventory.SolutionObjects.LocationTransferDetail locationTransferDetail)
        {
            if (locationTransferDetail.ID != null)
                return new Inventory.DataAccessObjects.LocationTransferDetailDO().Delete(locationTransferDetail);
            else
            {
                locationTransferDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = locationTransferDetail;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.LocationTransferDetail GetLocationTransferDetail(int id)
        {
            return new Inventory.DataAccessObjects.LocationTransferDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> GetAllLocationTransferDetails()
        {
            return new Inventory.DataAccessObjects.LocationTransferDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.LocationTransferDetail> GetAllFor(SolutionObjects.LocationTransfer locationTransfer)
        {
            return new Inventory.DataAccessObjects.LocationTransferDetailDO().GetAllFor(locationTransfer);
        }
    }
}
