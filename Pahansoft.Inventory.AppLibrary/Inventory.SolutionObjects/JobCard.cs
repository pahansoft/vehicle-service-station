﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class JobCard : SolutionObjectBase
    {
        public JobCard() { }
        public JobCard(int? id) { this.ID = id; }

        public string JobNumber { get; set; }
        public string VehicleNumber { get; set; }
        public SolutionObjects.Customer Customer { get; set; }
        public string Address { get; set; }
        public string TelephoneNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SolutionObjects.Supervisor Supervisor { get; set; }
        public decimal? TotalGross { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal? TotalTax { get; set; }
        public decimal? TotalNet { get; set; }
        public string Remarks { get; set; }
        public bool IsApproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string ApprovedBy { get; set; }
        public SolutionObjects.Company Company { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.JobCardItem> ItemList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.JobCardService> ServiceList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
