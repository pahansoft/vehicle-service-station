﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_manu_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.Manufacturer _manufacturer;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxDescription.Text = string.Empty;
        DropDownListCountry.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListCountry);
        Util.ClearDropdown(DropDownListSrchCountry);
        foreach (SolutionObjects.Country country in new OperationObjects.ContryOP().GetAllCountries())
        {
            DropDownListCountry.Items.Add(new ListItem(country.Description, country.ID.Value.ToString()));
            DropDownListSrchCountry.Items.Add(new ListItem(country.Description, country.ID.Value.ToString()));
        }

    }

    private void ClearSearch()
    {
        TextBoxSrchDescription.Text = string.Empty;
        DropDownListSrchCountry.SelectedIndex = 0;
    }

    private void Search()
    {
        SolutionObjects.Country _country = null;
        if (DropDownListSrchCountry.SelectedIndex != 0)
            _country = new SolutionObjects.Country(int.Parse(DropDownListSrchCountry.SelectedValue));

        GridViewSearchList.DataSource = new OperationObjects.ManufacturerOP().SearchManufacturers(TextBoxSrchDescription.Text.Trim(), _country);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            ViewState[_target] = new SolutionObjects.Manufacturer();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.Manufacturer();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _manufacturer = ViewState[_target] as SolutionObjects.Manufacturer;
        _manufacturer.Description = TextBoxDescription.Text.Trim();
        if (DropDownListCountry.SelectedIndex != 0)
            _manufacturer.Country = new SolutionObjects.Country(int.Parse(DropDownListCountry.SelectedValue));
        else
            _manufacturer.Country = null;

        _manufacturer.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.ManufacturerOP().SaveManufacturer(_manufacturer);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.Manufacturer();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _manufacturer = ViewState[_target] as SolutionObjects.Manufacturer;

        if (_manufacturer.ID != null)
        {
            _manufacturer = new OperationObjects.ManufacturerOP().GetManufacturer(_manufacturer.ID.Value);
            ViewState[_target] = _manufacturer;

            TextBoxDescription.Text = _manufacturer.Description;
            if (_manufacturer.Country != null)
                DropDownListCountry.SelectedValue = _manufacturer.Country.ID.Value.ToString();
        }
        else
        {
            ViewState[_target] = new SolutionObjects.Manufacturer();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _manufacturer = new OperationObjects.ManufacturerOP().GetManufacturer(_targetid);

            ClearAll();
            ViewState[_target] = _manufacturer;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxDescription.Text = _manufacturer.Description;
            if (_manufacturer.Country != null)
                DropDownListCountry.SelectedValue = _manufacturer.Country.ID.Value.ToString();
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _manufacturer = new OperationObjects.ManufacturerOP().GetManufacturer(_targetid);
            _manufacturer.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.ManufacturerOP().DeleteManufacturer(_manufacturer);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _manufacturer = e.Row.DataItem as SolutionObjects.Manufacturer;
            if (_manufacturer.Country != null)
                _manufacturer.Country = new OperationObjects.ContryOP().GetCountry(_manufacturer.Country.ID.Value);
            Util.EncodeString(e.Row.Cells[2], _manufacturer.Description, 50);
            Util.EncodeString(e.Row.Cells[3], _manufacturer.Country != null ? _manufacturer.Country.Description : string.Empty, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _manufacturer.Description + "');");
                }
            }
        }
    }
}