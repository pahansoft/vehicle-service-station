﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
 CodeFile="Default.aspx.cs" Inherits="tran_loca_tran_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
 <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
 <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground">
     <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
       <td class="pageHedder">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
         <tr>
          <td style="height: 17px">
           Location Transfer -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
          </td>
          <td align="right" style="height: 17px">
           <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonSearchOperation_Click">Search Location Transfer</asp:LinkButton>
           <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New Location Transfer</asp:LinkButton>
          </td>
         </tr>
        </table>
       </td>
      </tr>
      <tr>
       <td>
        <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke;
             border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid;
             border-bottom: gainsboro 1px solid;" cellpadding="0" cellspacing="0">
             <tr>
              <td style="height: 50px">
               <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px; width: 35px">
                   <img src="../../images/warning.png" />
                  </td>
                  <td>
                   <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                  </td>
                 </tr>
                </table>
               </asp:Panel>
               <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px;">
                   <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                   &nbsp;
                  </td>
                  <td align="right" style="padding-right: 20px">
                   <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                    Style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                    Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                  </td>
                 </tr>
                </table>
               </asp:Panel>
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Location Transfer Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Transfer Number <span style="color: Red;">*</span>
                </td>
                <td>
                 <asp:TextBox ID="TextBoxTransferNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td>
                 Date Transfer <span style="color: Red;">*</span>
                </td>
                <td>
                 <asp:TextBox ID="TextBoxDateTransfer" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                  ControlToValidate="TextBoxDateTransfer" Display="Dynamic" ErrorMessage="Required"
                  ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxDateTransfer"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Reference Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxReferenceNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <Triggers>
              <asp:PostBackTrigger ControlID="GridViewItemAdd" />
              <asp:PostBackTrigger ControlID="ButtonItemAdd" />
              <asp:PostBackTrigger ControlID="DropDownListFromStore" />
              <asp:PostBackTrigger ControlID="DropDownListFromLocation" />
              <asp:PostBackTrigger ControlID="DropDownListToStore" />
              <asp:PostBackTrigger ControlID="DropDownListToLocation" />
              <asp:PostBackTrigger ControlID="DropDownListProductItem" />
             </Triggers>
             <ContentTemplate>
              <fieldset>
               <legend style="font-weight: bold">Transfer Item(s)</legend>
               <div style="padding: 10px;">
                <table style="width: 100%" border="0">
                 <tbody>
                  <tr>
                   <td colspan="2">
                    <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                     OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                     DataKeyNames="ID" AutoGenerateColumns="False">
                     <PagerSettings Mode="NumericFirstLast" />
                     <RowStyle CssClass="grid-row-normal-stc" />
                     <Columns>
                      <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                       <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                      </asp:ButtonField>
                      <asp:BoundField HeaderText="From Location">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="To Location">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Product Item">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Quantity">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                     </Columns>
                     <PagerStyle CssClass="grid-pager" />
                     <EmptyDataTemplate>
                      <div id="norec" class="no-record-msg">
                       <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                        <tbody>
                         <tr>
                          <td style="padding-left: 15px">
                           No Items(s) Found!
                          </td>
                         </tr>
                        </tbody>
                       </table>
                      </div>
                     </EmptyDataTemplate>
                     <HeaderStyle CssClass="grid-header" />
                     <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                    </asp:GridView>
                   </td>
                  </tr>
                 </tbody>
                </table>
                <br />
                <table style="width: 100%" border="0">
                 <tbody>
                  <tr>
                   <td colspan="2">
                    <table border="0" style="width: 100%">
                     <tr>
                      <td style="width: 49%">
                       <fieldset>
                        <legend>From</legend>
                        <div style="padding-right: 10px; padding-left: 10px; padding-bottom: 10px; padding-top: 10px">
                         <table border="0" style="width: 100%">
                          <tr>
                           <td style="width: 20%">
                            Store <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListFromStore" runat="server" AutoPostBack="True" CssClass="dropdownlist"
                             Width="250px" OnSelectedIndexChanged="DropDownListFromStore_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorFromStore" runat="server"
                             ControlToValidate="DropDownListFromStore" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                          <tr>
                           <td>
                            Location <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListFromLocation" runat="server" AutoPostBack="True"
                             CssClass="dropdownlist" Width="250px" OnSelectedIndexChanged="DropDownListFromLocation_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorFromLocation" runat="server"
                             ControlToValidate="DropDownListFromLocation" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                          <tr>
                           <td>
                            Sub Location <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListFromRack" runat="server" CssClass="dropdownlist"
                             Width="250px" AutoPostBack="True" OnSelectedIndexChanged="DropDownListFromRack_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorFromRack" runat="server"
                             ControlToValidate="DropDownListFromRack" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                         </table>
                        </div>
                       </fieldset>
                      </td>
                      <td style="width: 2%">
                      </td>
                      <td style="width: 49%">
                       <fieldset>
                        <legend>To</legend>
                        <div style="padding-right: 10px; padding-left: 10px; padding-bottom: 10px; padding-top: 10px">
                         <table border="0" style="width: 100%">
                          <tr>
                           <td style="width: 20%">
                            Store <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListToStore" runat="server" AutoPostBack="True" CssClass="dropdownlist"
                             Width="250px" OnSelectedIndexChanged="DropDownListToStore_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorToStore" runat="server"
                             ControlToValidate="DropDownListToStore" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                          <tr>
                           <td>
                            Location <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListToLocation" runat="server" AutoPostBack="True"
                             CssClass="dropdownlist" Width="250px" OnSelectedIndexChanged="DropDownListToLocation_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorToLocation" runat="server"
                             ControlToValidate="DropDownListToLocation" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                          <tr>
                           <td>
                            Sub Location <span style="color: red">*</span>
                           </td>
                           <td>
                            <asp:DropDownList ID="DropDownListToRack" runat="server" CssClass="dropdownlist"
                             Width="250px">
                            </asp:DropDownList>
                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidatorToRack" runat="server"
                             ControlToValidate="DropDownListToRack" Display="Dynamic" ErrorMessage="Required"
                             ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                           </td>
                          </tr>
                         </table>
                        </div>
                       </fieldset>
                      </td>
                     </tr>
                    </table>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    &nbsp;
                   </td>
                   <td>
                   </td>
                  </tr>
                  <tr>
                   <td style="width: 20%">
                    Product Item <span style="color: red">*</span>
                   </td>
                   <td>
                    <asp:DropDownList ID="DropDownListProductItem" runat="server" Width="450px" AutoPostBack="True"
                     OnSelectedIndexChanged="DropDownListProductItem_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListProductItem"
                     Display="Dynamic" ErrorMessage="Required" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                   </td>
                  </tr>
                  <tr style="height: 25px">
                   <td style="vertical-align: text-top; border-top-style: 3px">
                    Available Quantity
                   </td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelAvailableQuantity" runat="server" Text="-"></asp:Label></strong>
                   </td>
                  </tr>
                  <tr>
                   <td style="width: 20%">
                    Transfer Quantity <span style="color: red">*</span>
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxTransferQuantity" runat="server" CssClass="textbox" MaxLength="20"
                     Width="100px"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxTransferQuantity"
                     Display="Dynamic" ErrorMessage="Required" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="TextBoxTransferQuantity"
                     Display="Dynamic" ErrorMessage="Invalid" MaximumValue="1000000" MinimumValue="1"
                     Type="Integer" ValidationGroup="AddItem"></asp:RangeValidator>
                    <strong>
                     <asp:Label ID="LabelValidateQuantity" runat="server" Style="color: #FF0000" Text="Cannot be greater than the Available Quantity"
                      Visible="False"></asp:Label>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    &nbsp;
                   </td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelValidateMultiple" runat="server" Style="color: #FF0000" Text="Product Item already entered"
                      Visible="False"></asp:Label>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td>
                   </td>
                   <td class="pageButtonArea">
                    <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                     Text="Add" ValidationGroup="AddItem" Width="70px" />
                   </td>
                  </tr>
                 </tbody>
                </table>
               </div>
              </fieldset>
             </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <fieldset>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="vertical-align: text-top; padding-top: 3px; width: 20%;">
                 Remarks
                </td>
                <td>
                 <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                  Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <table border="0" style="width: 100%">
             <tr>
              <td class="pageButtonArea">
               <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                OnClick="ButtonSubmit_Click" Width="70px" />
               &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                OnClick="ButtonReset_Click" Width="70px" />
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Search Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Transfer Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchTransferNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Date Transfer
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchDateTransfer" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateTransfer"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="search"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Reference Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchReferenceNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td>
                </td>
                <td class="pageButtonArea">
                 <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                  Width="70px" ValidationGroup="search" />
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
             CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
             OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
             OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
             <PagerSettings Mode="NumericFirstLast" />
             <RowStyle CssClass="grid-row-normal-stc" />
             <Columns>
              <asp:ButtonField CommandName="EditRecord" Text="Edit">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="ViewRecord" Text="View">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:BoundField HeaderText="Transfer Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Date Transfer">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Reference Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Remarks">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
             </Columns>
             <PagerStyle CssClass="grid-pager" />
             <EmptyDataTemplate>
              <div id="norec" class="no-record-msg">
               <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                <tbody>
                 <tr>
                  <td style="padding-left: 15px">
                   No Record(s) Found!
                  </td>
                 </tr>
                </tbody>
               </table>
              </div>
             </EmptyDataTemplate>
             <HeaderStyle CssClass="grid-header" />
             <AlternatingRowStyle CssClass="grid-row-alt-stc" />
            </asp:GridView>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Location Transfer Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr class="view-back">
                <td style="width: 20%;">
                 Transfer Number
                </td>
                <td style="width: 1%; text-align: center;">
                 :
                </td>
                <td style="width: 79%;">
                 <asp:Label ID="LabelViewTransferNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Date Transfer
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewDateTransfer" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Reference Number
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewReferenceNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Remarks
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <fieldset>
             <legend style="font-weight: bold">GRN Item(s)</legend>
             <div style="padding: 10px;">
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td>
                  <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                   OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                   <PagerSettings Mode="NumericFirstLast" />
                   <RowStyle CssClass="grid-row-normal-stc" />
                   <Columns>
                    <asp:BoundField HeaderText="From Location">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="To Location">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Product Item">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Quantity">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                   </Columns>
                   <PagerStyle CssClass="grid-pager" />
                   <EmptyDataTemplate>
                    <div id="norec" class="no-record-msg">
                     <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                      <tbody>
                       <tr>
                        <td style="padding-left: 15px">
                         No Items(s) Found!
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                   </EmptyDataTemplate>
                   <HeaderStyle CssClass="grid-header" />
                   <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                  </asp:GridView>
                 </td>
                </tr>
               </tbody>
              </table>
             </div>
            </fieldset>
           </td>
          </tr>
         </table>
        </asp:Panel>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </asp:Panel>
 <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px;
     padding-top: 15px" align="right">
     <img src="../../../images/AccessDenied.jpg" />
    </td>
    <caption>
     <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold;
      color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute;
       top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
    </caption>
   </tr>
  </table>
 </asp:Panel>
 <script language="javascript">
  $("#ctl00_ContentPlaceHolder1_TextBoxDateTransfer").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateTransfer").mask("99/99/9999");
 </script>
</asp:Content>
