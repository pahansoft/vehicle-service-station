﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class PurchaseReturnOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SavePurchaseReturn(Inventory.SolutionObjects.PurchaseReturn purchaseReturn)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();
            List<SolutionObjects.GRNDetail> _gRNDetailList = new List<SolutionObjects.GRNDetail>();
            SolutionObjects.GRNDetail _gRNDetail;

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (purchaseReturn.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.PurchaseReturnDO().Create(purchaseReturn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new PurchaseReturnItem List
                    foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in purchaseReturn.PurchaseReturnItemList)
                    {
                        innerDTO = new DataAccessObjects.PurchaseReturnItemDO().Create(purchaseReturnItem);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);

                        _gRNDetail = new OperationObjects.GRNDetailOP().GetGRNDetail(purchaseReturnItem.GRNDetail.ID.Value);
                        if (_gRNDetail.ReturnedQuantity != null)
                            _gRNDetail.ReturnedQuantity += purchaseReturnItem.ReturnedQuantity;
                        else
                            _gRNDetail.ReturnedQuantity = purchaseReturnItem.ReturnedQuantity;
                        _gRNDetail.User = purchaseReturnItem.User;

                        innerDTO = new DataAccessObjects.GRNDetailDO().Edit(_gRNDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(purchaseReturn);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in purchaseReturn.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.PurchaseReturnDO().Edit(purchaseReturn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit PurchaseReturnItem List
                    foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in purchaseReturn.PurchaseReturnItemList)
                    {
                        if (purchaseReturnItem.ID == null)
                            innerDTO = new DataAccessObjects.PurchaseReturnItemDO().Create(purchaseReturnItem);
                        else
                            innerDTO = new DataAccessObjects.PurchaseReturnItemDO().Edit(purchaseReturnItem);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete PurchaseReturnItem List
                    foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in purchaseReturn.PurchaseReturnItemList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.PurchaseReturnItemDO().Delete(purchaseReturnItem);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }                    
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeletePurchaseReturn(Inventory.SolutionObjects.PurchaseReturn purchaseReturn)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.PurchaseReturnDO().Delete(purchaseReturn);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete PurchaseReturnItem List
                foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in purchaseReturn.PurchaseReturnItemList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.PurchaseReturnItemDO().Delete(purchaseReturnItem);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.PurchaseReturn GetPurchaseReturn(int id)
        {
            return new Inventory.DataAccessObjects.PurchaseReturnDO().Find(id);
        }

        public Inventory.SolutionObjects.PurchaseReturn GetPurchaseReturnProxy(int id)
        {
            return new Inventory.DataAccessObjects.PurchaseReturnDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturn> GetAllPurchaseReturns()
        {
            return new Inventory.DataAccessObjects.PurchaseReturnDO().GetAll();
        }

        private SolutionObjects.PurchaseReturn CreateItemStock(SolutionObjects.PurchaseReturn _purchaseReturn)
        {
            _purchaseReturn.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            SolutionObjects.ItemStock _itemStock = null;
            SolutionObjects.ItemStockTransaction _itemStockTransaction;
            IList<SolutionObjects.ItemStock> _existItemStockList;
            bool _hasFound;
            foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in _purchaseReturn.PurchaseReturnItemList)
            {
                _hasFound = false;
                purchaseReturnItem.GRNDetail = new OperationObjects.GRNDetailOP().GetGRNDetail(purchaseReturnItem.GRNDetail.ID.Value);

                _existItemStockList = new OperationObjects.ItemStockOP().Search(purchaseReturnItem.GRNDetail.ProductItem, purchaseReturnItem.GRNDetail.Store, purchaseReturnItem.GRNDetail.Location, purchaseReturnItem.GRNDetail.Rack, SolutionObjects.StockTypes.Trading, purchaseReturnItem.GRNDetail.LotNumber, purchaseReturnItem.GRNDetail.UnitPrice, purchaseReturnItem.GRNDetail.SellingPrice);
                if (_existItemStockList.Count > 0)
                {
                    _itemStock = _existItemStockList[0];
                    _hasFound = true;
                }
                if (!_hasFound)
                {
                    _existItemStockList = new OperationObjects.ItemStockOP().Search(purchaseReturnItem.GRNDetail.ProductItem, purchaseReturnItem.GRNDetail.Store, purchaseReturnItem.GRNDetail.Location, null, SolutionObjects.StockTypes.Trading, purchaseReturnItem.GRNDetail.LotNumber, purchaseReturnItem.GRNDetail.UnitPrice, purchaseReturnItem.GRNDetail.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _hasFound = true;
                    }
                }
                if (!_hasFound)
                {
                    _existItemStockList = new OperationObjects.ItemStockOP().Search(purchaseReturnItem.GRNDetail.ProductItem, purchaseReturnItem.GRNDetail.Store, null, null, SolutionObjects.StockTypes.Trading, purchaseReturnItem.GRNDetail.LotNumber, purchaseReturnItem.GRNDetail.UnitPrice, purchaseReturnItem.GRNDetail.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _hasFound = true;
                    }
                }
                if (!_hasFound)
                {
                    _existItemStockList = new OperationObjects.ItemStockOP().Search(purchaseReturnItem.GRNDetail.ProductItem, null, null, null, SolutionObjects.StockTypes.Trading, purchaseReturnItem.GRNDetail.LotNumber, purchaseReturnItem.GRNDetail.UnitPrice, purchaseReturnItem.GRNDetail.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _hasFound = true;
                    }
                }

                if (!_hasFound)
                    _itemStock = new SolutionObjects.ItemStock();

                if (_itemStock.ID != null)
                    _itemStock.CurrentQuantity -= purchaseReturnItem.ReturnedQuantity.Value;
                else
                {
                    _itemStock.ProductItem = purchaseReturnItem.GRNDetail.ProductItem;
                    _itemStock.StockTypes = SolutionObjects.StockTypes.Trading;
                    _itemStock.LotNumber = purchaseReturnItem.GRNDetail.LotNumber;
                    _itemStock.Store = purchaseReturnItem.GRNDetail.Store;
                    _itemStock.Location = purchaseReturnItem.GRNDetail.Location;
                    _itemStock.Rack = purchaseReturnItem.GRNDetail.Rack;
                    _itemStock.UnitPrice = purchaseReturnItem.GRNDetail.UnitPrice;
                    _itemStock.SellingPrice = purchaseReturnItem.GRNDetail.SellingPrice;
                    _itemStock.DateManufactured = purchaseReturnItem.GRNDetail.DateManufactured;
                    _itemStock.DateExpiry = purchaseReturnItem.GRNDetail.DateExpiry;
                }

                _itemStock.User = _purchaseReturn.User;
                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();
                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _itemStock;
                _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.PurchaseReturn;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _purchaseReturn.ID;
                _itemStockTransaction.TransactionDetailID = purchaseReturnItem.ID;
                _itemStockTransaction.ReferenceNumber = _purchaseReturn.ReturnNumber;
                _itemStockTransaction.Store = _itemStock.Store;
                _itemStockTransaction.Location = _itemStock.Location;
                _itemStockTransaction.Rack = _itemStock.Rack;
                _itemStockTransaction.Quantity = purchaseReturnItem.ReturnedQuantity;
                _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                _itemStockTransaction.User = _purchaseReturn.User;
                _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _purchaseReturn.ItemStockList.Add(_itemStock);
            }

            return _purchaseReturn;
        }
    }
}
