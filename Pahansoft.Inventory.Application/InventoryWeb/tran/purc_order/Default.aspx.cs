﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.Services;
using System.Web.UI.HtmlControls;

#endregion

public partial class tran_purc_order_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    public const string _targetProducts = "TargetProducts";
    public const string _targetSuppliers = "TargetSuppliers";
    SolutionObjects.PurchaseOrder PurchaseOrder;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxPONumber.Text = "- AUTO GENERATED -";
        TextBoxPODate.Text = DateTime.Today.ToShortDateString();
        TextBoxSupplier.Text = string.Empty;
        TextBoxTotalAmount.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        LabelItemError.Visible = false;
        TextBoxProductItem.Text = string.Empty;
        TextBoxUnitPrice.Text = string.Empty;
        TextBoxQuantity.Text = string.Empty;
        TextBoxLineTotal.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchPODate.Text = string.Empty;
        TextBoxSrchPONumber.Text = string.Empty;
        TextBoxSrchSupplier.Text = string.Empty;
    }

    private void Search()
    {
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        int? SupplierID = null;
        if (TextBoxSrchSupplier.Text.Trim() != string.Empty)
            SupplierID = Suppliers.FirstOrDefault(t => t.Name == TextBoxSrchSupplier.Text).ID;

        GridViewSearchList.DataSource = new OperationObjects.PurchaseOrderOP().Search(TextBoxSrchPONumber.Text.Trim(), Util.ParseDate(TextBoxSrchPODate.Text.Trim()), SupplierID);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotal()
    {
        PurchaseOrder = HttpContext.Current.Session[_target] as SolutionObjects.PurchaseOrder;

        Decimal TotalAmount = 0;

        foreach (var item in PurchaseOrder.PurchaseOrderDetails)
            TotalAmount += item.LineTotal.Value;

        TextBoxTotalAmount.Text = TotalAmount.ToString("F");
    }

    private void FillForEdit(int PurchaseOrderID)
    {
        PurchaseOrder = new OperationObjects.PurchaseOrderOP().Get(PurchaseOrderID);
        if (PurchaseOrder.Supplier != null)
            PurchaseOrder.Supplier = new OperationObjects.SupplierOP().GetSupplier(PurchaseOrder.Supplier.ID.Value);

        HttpContext.Current.Session[_target] = PurchaseOrder;

        TextBoxPONumber.Text = PurchaseOrder.PONumber;
        TextBoxPODate.Text = PurchaseOrder.DatePO != null ? PurchaseOrder.DatePO.Value.ToShortDateString() : DateTime.Today.ToShortDateString();
        if (PurchaseOrder.Supplier != null)
            TextBoxSupplier.Text = PurchaseOrder.Supplier.Name;
        TextBoxTotalAmount.Text = PurchaseOrder.TotalAmount != null ? PurchaseOrder.TotalAmount.Value.ToString() : string.Empty;
        TextBoxRemarks.Text = PurchaseOrder.Remarks;

        GridViewItemAdd.DataSource = PurchaseOrder.PurchaseOrderDetails;
        GridViewItemAdd.DataBind();
    }

    private void FillForView(int PurchaseOrderID)
    {
        PurchaseOrder = new OperationObjects.PurchaseOrderOP().Get(PurchaseOrderID);
        if (PurchaseOrder.Supplier != null)
            PurchaseOrder.Supplier = new OperationObjects.SupplierOP().GetSupplier(PurchaseOrder.Supplier.ID.Value);

        HttpContext.Current.Session[_target] = PurchaseOrder;

        LabelViewPONumber.Text = Util.FormatEmptyString(PurchaseOrder.PONumber);
        LabelViewPODate.Text = Util.FormatEmptyString(PurchaseOrder.DatePO != null ? PurchaseOrder.DatePO.Value.ToShortDateString() : DateTime.Today.ToShortDateString());
        LabelViewSupplier.Text = Util.FormatEmptyString(PurchaseOrder.Supplier != null ? PurchaseOrder.Supplier.Name : string.Empty);
        LabelViewTotalAmount.Text = Util.FormatEmptyString(PurchaseOrder.TotalAmount != null ? PurchaseOrder.TotalAmount.Value.ToString() : string.Empty);
        LabelViewRemarks.Text = Util.FormatEmptyString(PurchaseOrder.Remarks);

        GridViewItemView.DataSource = PurchaseOrder.PurchaseOrderDetails;
        GridViewItemView.DataBind();
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetProductItems(string Text)
    {
        var ProductItems = HttpContext.Current.Session[_targetProducts] as IList<SolutionObjects.ProductItem>;

        var returnList = new List<string>();
        foreach (var item in ProductItems.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.ReferenceNumber.ToLower().Contains(Text.ToLower()) || t.Description.ToLower().Contains(Text.ToLower())).ToList() )
        {
            var sbDescription = new StringBuilder();
            sbDescription.Append(item.Code);
            sbDescription.Append(item.ReferenceNumber != string.Empty ? ("-" + item.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(item.Description);
            returnList.Add(sbDescription.ToString());
        }

        return returnList;
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetSuppliers(string Text)
    {
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        var returnList = new List<string>();
        foreach (var item in Suppliers.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.Name.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Name);

        return returnList;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ClearAll();
            PurchaseOrder = new SolutionObjects.PurchaseOrder();
            PurchaseOrder.PurchaseOrderDetails = new CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail>();
            HttpContext.Current.Session[_target] = PurchaseOrder;
            LabelPageOperation.Text = "Add New";

            var ProductItems = new OperationObjects.ProductItemOP().GetAllProductItems();
            HttpContext.Current.Session[_targetProducts] = ProductItems;

            var Suppliers = new OperationObjects.SupplierOP().GetAllSuppliers();
            HttpContext.Current.Session[_targetSuppliers] = Suppliers;

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        PurchaseOrder = new SolutionObjects.PurchaseOrder();
        PurchaseOrder.PurchaseOrderDetails = new CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail>();
        HttpContext.Current.Session[_target] = PurchaseOrder;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }

    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        PurchaseOrder = HttpContext.Current.Session[_target] as SolutionObjects.PurchaseOrder;
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        int? SupplierID = null;
        if (TextBoxSupplier.Text.Trim() != string.Empty)
        {
            var Supplier = Suppliers.FirstOrDefault(t => t.Name == TextBoxSupplier.Text);
            if (Supplier == null)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = "Invalid Supplier";
                return;
            }
            else
                SupplierID = Supplier.ID;
        }

        if (PurchaseOrder.ID == null)
            PurchaseOrder.PONumber = Util.GenereteSequenceNumber("PO", DateTime.Today.Year, DateTime.Today.Month);
        PurchaseOrder.DatePO = Util.ParseDate(TextBoxPODate.Text.Trim());
        PurchaseOrder.Supplier = new SolutionObjects.Supplier(SupplierID);
        PurchaseOrder.TotalAmount = Util.ParseDecimal(TextBoxTotalAmount.Text.Trim());
        PurchaseOrder.Remarks = TextBoxRemarks.Text.Trim();
        PurchaseOrder.User = User.Identity.Name;

        try
        {
            bool _isNew = false;
            if (PurchaseOrder.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.PurchaseOrderOP().Save(PurchaseOrder);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>PO Number : " + PurchaseOrder.PONumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                HtmlGenericControl dispval = new HtmlGenericControl();
                dispval.InnerHtml = "<script language='javascript'> print('" + PurchaseOrder.ID + "');</script>";
                Page.Controls.Add(dispval);

                ClearAll();
                PurchaseOrder = new SolutionObjects.PurchaseOrder();
                PurchaseOrder.PurchaseOrderDetails = new CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail>();
                HttpContext.Current.Session[_target] = PurchaseOrder;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        PurchaseOrder = HttpContext.Current.Session[_target] as SolutionObjects.PurchaseOrder;

        if (PurchaseOrder.ID != null)
        {
            FillForEdit(PurchaseOrder.ID.Value);
        }
        else
        {
            PurchaseOrder = new SolutionObjects.PurchaseOrder();
            PurchaseOrder.PurchaseOrderDetails = new CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail>();
            HttpContext.Current.Session[_target] = PurchaseOrder;
        }
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        PurchaseOrder = HttpContext.Current.Session[_target] as SolutionObjects.PurchaseOrder;
        PurchaseOrder.PurchaseOrderDetails.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        HttpContext.Current.Session[_target] = PurchaseOrder;
        GridViewItemAdd.DataSource = PurchaseOrder.PurchaseOrderDetails;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        UpdatePanelSummary.Update();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var PurchaseOrderDetail = e.Row.DataItem as SolutionObjects.PurchaseOrderDetail;
            if (PurchaseOrderDetail.ProductItem != null)
                PurchaseOrderDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItem(PurchaseOrderDetail.ProductItem.ID.Value);

            var sbDescription = new StringBuilder();
            sbDescription.Append(PurchaseOrderDetail.ProductItem.Code);
            sbDescription.Append(PurchaseOrderDetail.ProductItem.ReferenceNumber != string.Empty ? ("-" + PurchaseOrderDetail.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(PurchaseOrderDetail.ProductItem.Description);

            Util.EncodeString(e.Row.Cells[1], PurchaseOrderDetail.ProductItem != null ? sbDescription.ToString() : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[2], PurchaseOrderDetail.UnitPrice != null ? PurchaseOrderDetail.UnitPrice.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[3], PurchaseOrderDetail.Quantity != null ? PurchaseOrderDetail.Quantity.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[4], PurchaseOrderDetail.LineTotal != null ? PurchaseOrderDetail.LineTotal.Value.ToString() : string.Empty, 20);
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        var ProductItems = HttpContext.Current.Session[_targetProducts] as IList<SolutionObjects.ProductItem>;
        LabelItemError.Visible = false;

        int? ProductItemID = null;
        if (TextBoxProductItem.Text.Trim() != string.Empty)
        {
            List<string> itemCode = TextBoxProductItem.Text.Trim().Split('-').ToList<string>();
            var ProductItem = ProductItems.FirstOrDefault(t => t.Code == itemCode[0]);
            if (ProductItem == null)
            {
                LabelItemError.Text = "Invalid Product Item";
                LabelItemError.Visible = true;
                return;
            }
            else
                ProductItemID = ProductItem.ID;
        }

        PurchaseOrder = HttpContext.Current.Session[_target] as SolutionObjects.PurchaseOrder;
        foreach (var item in PurchaseOrder.PurchaseOrderDetails)
        {
            if (item.ProductItem.ID == ProductItemID)
            {
                LabelItemError.Text = "Product Item already entered";
                LabelItemError.Visible = true;
                return;
            }
        }

        var PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail();
        PurchaseOrderDetail.PurchaseOrder = PurchaseOrder;
        PurchaseOrderDetail.ProductItem = new SolutionObjects.ProductItem(ProductItemID.Value);
        PurchaseOrderDetail.UnitPrice = Util.ParseDecimal(TextBoxUnitPrice.Text.Trim());
        PurchaseOrderDetail.Quantity = Util.ParseDecimal(TextBoxQuantity.Text.Trim());
        PurchaseOrderDetail.LineTotal = Util.ParseDecimal(TextBoxLineTotal.Text.Trim());
        PurchaseOrderDetail.User = User.Identity.Name;
        PurchaseOrder.PurchaseOrderDetails.Add(PurchaseOrderDetail);

        HttpContext.Current.Session[_target] = PurchaseOrder;
        GridViewItemAdd.DataSource = PurchaseOrder.PurchaseOrderDetails;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        ClearItem();
        UpdatePanelSummary.Update();
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            ClearAll();
            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            FillForEdit(_targetid);
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            PurchaseOrder = new OperationObjects.PurchaseOrderOP().Get(_targetid);
            PurchaseOrder.User = User.Identity.Name;

            int _listCount = PurchaseOrder.PurchaseOrderDetails.Count - 1;
            while (_listCount >= 0)
            {
                PurchaseOrder.PurchaseOrderDetails.RemoveAt(_listCount);
                _listCount--;
            }

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.PurchaseOrderOP().Delete(PurchaseOrder);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            FillForView(_targetid);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PurchaseOrder = e.Row.DataItem as SolutionObjects.PurchaseOrder;
            if (PurchaseOrder.Supplier != null)
                PurchaseOrder.Supplier = new OperationObjects.SupplierOP().GetSupplier(PurchaseOrder.Supplier.ID.Value);

            Util.EncodeString(e.Row.Cells[3], PurchaseOrder.PONumber, 20);
            Util.EncodeString(e.Row.Cells[4], PurchaseOrder.DatePO != null ? PurchaseOrder.DatePO.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], PurchaseOrder.Supplier != null ? PurchaseOrder.Supplier.Name : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[6], PurchaseOrder.TotalAmount != null ? PurchaseOrder.TotalAmount.Value.ToString() : string.Empty, 20);
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var PurchaseOrderDetail = e.Row.DataItem as SolutionObjects.PurchaseOrderDetail;
            if (PurchaseOrderDetail.ProductItem != null)
                PurchaseOrderDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItem(PurchaseOrderDetail.ProductItem.ID.Value);

            var sbDescription = new StringBuilder();
            sbDescription.Append(PurchaseOrderDetail.ProductItem.Code);
            sbDescription.Append(PurchaseOrderDetail.ProductItem.ReferenceNumber != string.Empty ? ("-" + PurchaseOrderDetail.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(PurchaseOrderDetail.ProductItem.Description);

            Util.EncodeString(e.Row.Cells[0], PurchaseOrderDetail.ProductItem != null ? sbDescription.ToString() : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[1], PurchaseOrderDetail.UnitPrice != null ? PurchaseOrderDetail.UnitPrice.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[2], PurchaseOrderDetail.Quantity != null ? PurchaseOrderDetail.Quantity.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[3], PurchaseOrderDetail.LineTotal != null ? PurchaseOrderDetail.LineTotal.Value.ToString() : string.Empty, 20);
        }
    }
}