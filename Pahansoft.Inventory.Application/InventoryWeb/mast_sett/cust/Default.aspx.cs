﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_cust_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.Customer _customer;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxCode.Text = string.Empty;
        TextBoxName.Text = string.Empty;
        TextBoxAddressLine1.Text = string.Empty;
        TextBoxAddressLine2.Text = string.Empty;
        TextBoxAddressLine3.Text = string.Empty;
        TextBoxTelephoneHome.Text = string.Empty;
        TextBoxTelephoneMobile.Text = string.Empty;
        TextBoxTelephoneOffice.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxEmail.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchCode.Text = string.Empty;
        TextBoxSrchName.Text = string.Empty;
    }

    private void Search()
    {
        GridViewSearchList.DataSource = new OperationObjects.CustomerOP().SearchCustomers(TextBoxSrchCode.Text.Trim(), TextBoxSrchName.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            ViewState[_target] = new SolutionObjects.Customer();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.Customer();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _customer = ViewState[_target] as SolutionObjects.Customer;
        _customer.Code = TextBoxCode.Text.Trim();
        _customer.Name = TextBoxName.Text.Trim();
        _customer.AddressLine1 = TextBoxAddressLine1.Text.Trim();
        _customer.AddressLine2 = TextBoxAddressLine2.Text.Trim();
        _customer.AddressLine3 = TextBoxAddressLine3.Text.Trim();
        _customer.TelephoneHome = TextBoxTelephoneHome.Text.Trim();
        _customer.TelephoneMobile = TextBoxTelephoneMobile.Text.Trim();
        _customer.TelephoneOffice = TextBoxTelephoneOffice.Text.Trim();
        _customer.Fax = TextBoxFax.Text.Trim();
        _customer.Email = TextBoxEmail.Text.Trim();
        _customer.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.CustomerOP().SaveCustomer(_customer);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.Customer();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Code cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Code cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _customer = ViewState[_target] as SolutionObjects.Customer;

        if (_customer.ID != null)
        {
            _customer = new OperationObjects.CustomerOP().GetCustomer(_customer.ID.Value);
            ViewState[_target] = _customer;

            TextBoxCode.Text = _customer.Code;
            TextBoxName.Text = _customer.Name;
            TextBoxAddressLine1.Text = _customer.AddressLine1;
            TextBoxAddressLine2.Text = _customer.AddressLine2;
            TextBoxAddressLine3.Text = _customer.AddressLine3;
            TextBoxTelephoneHome.Text = _customer.TelephoneHome;
            TextBoxTelephoneMobile.Text = _customer.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _customer.TelephoneOffice;
            TextBoxFax.Text = _customer.Fax;
            TextBoxEmail.Text = _customer.Email;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.Customer();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _customer = new OperationObjects.CustomerOP().GetCustomer(_targetid);

            ClearAll();
            ViewState[_target] = _customer;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxCode.Text = _customer.Code;
            TextBoxName.Text = _customer.Name;
            TextBoxAddressLine1.Text = _customer.AddressLine1;
            TextBoxAddressLine2.Text = _customer.AddressLine2;
            TextBoxAddressLine3.Text = _customer.AddressLine3;
            TextBoxTelephoneHome.Text = _customer.TelephoneHome;
            TextBoxTelephoneMobile.Text = _customer.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _customer.TelephoneOffice;
            TextBoxFax.Text = _customer.Fax;
            TextBoxEmail.Text = _customer.Email;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _customer = new OperationObjects.CustomerOP().GetCustomer(_targetid);
            _customer.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.CustomerOP().DeleteCustomer(_customer);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _customer = new OperationObjects.CustomerOP().GetCustomer(_targetid);

            ViewState[_target] = _customer;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewCode.Text = Util.FormatEmptyString(_customer.Code);
            LabelViewName.Text = Util.FormatEmptyString(_customer.Name);
            LabelViewAddressLine1.Text = Util.FormatEmptyString(_customer.AddressLine1);
            LabelViewAddressLine2.Text = Util.FormatEmptyString(_customer.AddressLine2);
            LabelViewAddressLine3.Text = Util.FormatEmptyString(_customer.AddressLine3);
            LabelViewTelephoneHome.Text = Util.FormatEmptyString(_customer.TelephoneHome);
            LabelViewTelephoneMobile.Text = Util.FormatEmptyString(_customer.TelephoneMobile);
            LabelViewTelephoneOffice.Text = Util.FormatEmptyString(_customer.TelephoneOffice);
            LabelViewFax.Text = Util.FormatEmptyString(_customer.Fax);
            LabelViewEmail.Text = Util.FormatEmptyString(_customer.Email);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _customer = e.Row.DataItem as SolutionObjects.Customer;
            Util.EncodeString(e.Row.Cells[3], _customer.Code, 50);
            Util.EncodeString(e.Row.Cells[4], _customer.Name, 50);
            Util.EncodeString(e.Row.Cells[5], _customer.AddressLine1, 50);
            Util.EncodeString(e.Row.Cells[6], _customer.TelephoneMobile, 50);
            Util.EncodeString(e.Row.Cells[7], _customer.Email, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _customer.Name + "');");
                }
            }
        }
    }
}