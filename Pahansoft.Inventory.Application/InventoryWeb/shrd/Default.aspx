﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="shrd_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>Inventory Control System</title>
 <link href="../inc_fils/css/login.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
  .style1
  {
  }
  .style2
  {
   width: 121px;
  }
  .style3
  {
   height: 296px;
   width: 700px;
  }
  .style4
  {
   height: 60px;
   width: 298px;
   text-align: center;
  }
  .style6
  {
   height: 60px;
   width: 700px;
  }
  .style20
  {
   color: #000099;
   font-size: 22px;
  }
  .style21
  {
   height: 296px;
   width: 298px;
  }
  .style22
  {
   width: 15px;
  }
  .style23
  {
   width: 106px;
  }
 </style>
</head>
<body>
 <form id="form1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" />
 <div>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
   <tr>
    <td valign="middle" align="center">
     <table border="0" cellpadding="0" cellspacing="0" class="backgroundImage">
      <tr>
       <td class="backgroundImage" align="left" valign="top">
        <table align="right">
         <tr>
          <td class="style21">
           &nbsp;
          </td>
          <td class="style3">
         </td>
         </tr>
         <tr>
          <td style="padding-left: 30px; color: #FFFFFF; font-size: 18px; font-family: 'Segoe UI';"
           class="style4">
           &nbsp;
           <br/>
           &nbsp;
          </td>
          <td class="style6">
           &nbsp;
           <table border="0" >
            <tr>
             <td style="padding-top: 12px;" valign="middle" class="style1" colspan="7">
              <br />
              &nbsp;
             </td>
            </tr>
               
            <tr>
             <td style="padding-bottom: 5px; color: #00FFFF; font-weight: bold; font-style: normal; font-family: Arial, Helvetica, sans-serif;" valign="middle" class="style2">
              </td>
             <td style="width: 50px">
              
             </td>
             <td class="style22">
             </td>
             <td>              
                   <table>
                        <tr>
                        <td>
                            <span style="font-family: 'Segoe UI';" class="style20"> <strong style="color: #0000FF; font-size: 15pt; font-family: 'AvantGarde Bk BT'; font-weight: bold;">User Name </strong></span>
                        </td>
                        <td>
                            <strong><asp:TextBox ID="TextBoxUserName" runat="server" Style="border: 1px solid darkgray;
                            font-size: 16px; font-family: 'Segoe UI'; color: #FFFFFF; background-color: #6666FF;
                            font-weight: 700;" Width="150px"></asp:TextBox></strong>
                        </td>

                        </tr>

                        <tr> 
                            <td> 
                            <span style="font-family: 'Segoe UI';" class="style20"><strong style="color: #0000FF; font-size: 15pt; font-family: 'AvantGarde Bk BT'; font-weight: bold;">Password</strong></span> <strong>
                            
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxPassword" runat="server" Style="border: 1px solid darkgray;
                            font-size: 16px; font-family: 'Segoe UI'; color: #FFFFFF; background-color: #6666FF;
                            font-weight: 700;" Width="150px" TextMode="Password"></asp:TextBox></strong>
                        </td>

                        </tr>

                        <tr>
                            <td></td>
                            <td> 
                            <asp:Button ID="ButtonLogin" runat="server" Style="border: 1px solid darkgray; font-size: 18px;
                            color: #FFFFFF; font-family: 'Segoe UI'; background-color: #6666FF; font-weight: 700;"
                            Text="LOGIN" Width="68px" OnClick="ButtonLogin_Click" />
                        </td></tr>

                   </table>   
                 
                
             </td>
             <td style="width: 100px">
              
             </td>
             <td style="width: 15px">
             </td>
             <td style="width: 50px">
             
             </td>
            </tr>
           </table>
           <span style="font-family: 'Segoe UI';">
            <asp:Label ID="LabelMessage" runat="server" ForeColor="Red"></asp:Label></span>
          </td>
         </tr>
        </table>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </div>
 </form>
</body>
</html>
