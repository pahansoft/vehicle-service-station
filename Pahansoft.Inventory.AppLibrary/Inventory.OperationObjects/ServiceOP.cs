﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ServiceOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveService(Inventory.SolutionObjects.Service Service)
        {
            if (Service.ID == null)
                return new Inventory.DataAccessObjects.ServiceDO().Create(Service);
            else
                return new Inventory.DataAccessObjects.ServiceDO().Edit(Service);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteService(Inventory.SolutionObjects.Service Service)
        {
            if (Service.ID != null)
                return new Inventory.DataAccessObjects.ServiceDO().Delete(Service);
            else
            {
                Service.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = Service;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Service GetService(int id)
        {
            return new Inventory.DataAccessObjects.ServiceDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Service> GetAllService()
        {
            return new Inventory.DataAccessObjects.ServiceDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Service> SearchServices(string description)
        {
            return new Inventory.DataAccessObjects.ServiceDO().Search(description);
        }
    }
}

