﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
 CodeFile="Default.aspx.cs" Inherits="tran_cash_coll_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
 <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
 <script src="../../inc_fils/js/auto_comp/common.js" type="text/javascript"></script>
 <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground">
     <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
       <td class="pageHedder">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
         <tr>
          <td style="height: 17px">
           Cash Collection -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
          </td>
          <td align="right" style="height: 17px">
           <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonSearchOperation_Click">Search Cash Collection</asp:LinkButton>
           <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New Cash Collection</asp:LinkButton>
          </td>
         </tr>
        </table>
       </td>
      </tr>
      <tr>
       <td>
        <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke;
             border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid;
             border-bottom: gainsboro 1px solid;" cellpadding="0" cellspacing="0">
             <tr>
              <td style="height: 50px">
               <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px; width: 35px">
                   <img src="../../images/warning.png" />
                  </td>
                  <td>
                   <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                  </td>
                 </tr>
                </table>
               </asp:Panel>
               <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px;">
                   <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                   &nbsp;
                  </td>
                  <td align="right" style="padding-right: 20px">
                   <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                    Style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                    Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                  </td>
                 </tr>
                </table>
               </asp:Panel>
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Cash Collection Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Collection Number <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxCollectionNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                </td>
                <td style="width: 20%">
                </td>
                <td style="width: 30%">
                </td>
               </tr>
               <tr>
                <td>
                 Date Collection <span style="color: Red;">*</span>
                </td>
                <td>
                 <asp:TextBox ID="TextBoxDateCollection" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                  ControlToValidate="TextBoxDateCollection" Display="Dynamic" ErrorMessage="Required"
                  ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxDateCollection"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                </td>
                <td>
                </td>
                <td>
                </td>
               </tr>
               <tr>
                <td>
                 Cash Collector <span style="color: Red;">*</span>
                </td>
                <td colspan="3">
                 <asp:DropDownList ID="DropDownListCashCollector" runat="server" Width="250px">
                 </asp:DropDownList>
                 &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListCashCollector"
                  Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <Triggers>
              <asp:PostBackTrigger ControlID="GridViewItemAdd" />
              <asp:PostBackTrigger ControlID="ButtonItemAdd" />
              <asp:PostBackTrigger ControlID="DropDownListStore" />
              <asp:PostBackTrigger ControlID="DropDownListLocation" />
             </Triggers>
             <ContentTemplate>--%>
            <fieldset>
             <legend style="font-weight: bold">Customer Payment(s)</legend>
             <div style="padding: 10px;">
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td colspan="2">
                  <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                   OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                   DataKeyNames="ID" AutoGenerateColumns="False">
                   <PagerSettings Mode="NumericFirstLast" />
                   <RowStyle CssClass="grid-row-normal-stc" />
                   <Columns>
                    <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                     <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                    </asp:ButtonField>
                    <asp:BoundField HeaderText="Card Number">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Customer">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Paid Amount">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                   </Columns>
                   <PagerStyle CssClass="grid-pager" />
                   <EmptyDataTemplate>
                    <div id="norec" class="no-record-msg">
                     <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                      <tbody>
                       <tr>
                        <td style="padding-left: 15px">
                         No Items(s) Found!
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                   </EmptyDataTemplate>
                   <HeaderStyle CssClass="grid-header" />
                   <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                  </asp:GridView>
                 </td>
                </tr>
               </tbody>
              </table>
              <asp:UpdatePanel ID="UpdatePanel1" runat="server">
               <Triggers>
                <asp:PostBackTrigger ControlID="DropDownListSalesIssue" />
               </Triggers>
               <ContentTemplate>
                <table style="width: 100%" border="0">
                 <tbody>
                  <tr>
                   <td style="width: 20%">
                    &nbsp;
                   </td>
                   <td>
                   </td>
                  </tr>
                  <tr>
                   <td style="width: 20%; vertical-align: text-top; border-top-style: 3px">
                    Sales Issue <span style="color: red">*</span>
                   </td>
                   <td>
                    <div id="DivFilter1" style="display: block">
                     <asp:DropDownList ID="DropDownListSalesIssue" runat="server" AutoPostBack="True"
                      OnSelectedIndexChanged="DropDownListSalesIssue_SelectedIndexChanged" Width="450px">
                     </asp:DropDownList>
                     <input id="ButtonFilter" title="Filter" type="button" value="..." onclick="showFilter('DivFilter1','DivFilter2','ButtonSearchFilter');"
                      class="button" style="width: 30px" size="10" />
                     &nbsp;<asp:RequiredFieldValidator ID="RequiredFilter1" runat="server" ControlToValidate="DropDownListSalesIssue"
                      ErrorMessage="Required" Display="Dynamic" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator></div>
                    <div id="DivFilter2" style="display: none">
                     <table class="search-inside-box" width="450">
                      <tr>
                       <td style="text-align: left">
                        <table width="100%" border="0">
                         <tr>
                          <td nowrap="nowrap" style="text-align: left; width: 50px;">
                           &nbsp;
                          </td>
                          <td nowrap="nowrap">
                          </td>
                          <td style="vertical-align: top; width: 20px; height: 20px; text-align: right">
                           <img src="../../images/close.png" onclick="hideFilter('DivFilter1','DivFilter2');"
                            style="cursor: pointer" />
                          </td>
                         </tr>
                         <tr>
                          <td style="text-align: left; width: 75px; padding-left: 5px;" nowrap="nowrap">
                           Card Number
                          </td>
                          <td nowrap="noWrap">
                           <asp:TextBox ID="TextBoxFilterCardNumber" runat="server" CssClass="textbox" MaxLength="50"
                            Width="100px"></asp:TextBox>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td style="text-align: left; width: 75px; padding-left: 5px;" nowrap="nowrap">
                           Issue Number
                          </td>
                          <td nowrap="noWrap">
                           <asp:TextBox ID="TextBoxFilterIssueNumber" runat="server" CssClass="textbox" MaxLength="200"
                            Width="250px"></asp:TextBox>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td>
                          </td>
                          <td nowrap="noWrap">
                           <asp:Button ID="ButtonSearchFilter" runat="server" Text="Go" CssClass="search-inside-box"
                            Width="30px" OnClick="ButtonSearchFilter_Click"></asp:Button>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td>
                           &nbsp;
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                         </tr>
                        </table>
                       </td>
                      </tr>
                     </table>
                    </div>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Due Amount
                   </td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelDueAmount" runat="server" Text="-"></asp:Label></strong>
                   </td>
                  </tr>
                 </tbody>
                </table>
               </ContentTemplate>
              </asp:UpdatePanel>
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td>
                  Paid Amount <span style="color: red">*</span>
                 </td>
                 <td>
                  <asp:TextBox ID="TextBoxPaidAmount" runat="server" CssClass="textbox" MaxLength="20"
                   Width="100px"></asp:TextBox>
                  &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxPaidAmount"
                   Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                  <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxPaidAmount"
                   Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                   Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                  <strong>
                  <asp:Label ID="LabelValidateAmount" runat="server" Style="color: #FF0000" 
                   Text="Paid Amount Cannot be greater than the Due Amount" Visible="False"></asp:Label>
                  </strong>
                 </td>
                </tr>
                <tr>
                 <td style="width: 20%">
                  &nbsp;
                 </td>
                 <td>
                  <strong>
                  <asp:Label ID="LabelValidateDuplicate" runat="server" Style="color: #FF0000" 
                   Text="Sales Issue cannot be duplicated" Visible="False"></asp:Label>
                  </strong>
                 </td>
                </tr>
                <tr>
                 <td>
                 </td>
                 <td class="pageButtonArea">
                  <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                   Text="Add" ValidationGroup="ItemAdd" Width="70px" />
                 </td>
                </tr>
               </tbody>
              </table>
             </div>
            </fieldset>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
            <br />
            <fieldset>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Total Amount <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxTotalAmount" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxTotalAmount"
                  Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="TextBoxTotalAmount"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
                <td style="width: 20%">
                </td>
                <td style="width: 30%">
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Collector Commission <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxCollectorCommission" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>% &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                   runat="server" ControlToValidate="TextBoxCollectorCommission" Display="Dynamic"
                   ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBoxCollectorCommission"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
                <td style="width: 20%">
                 &nbsp;
                </td>
                <td style="width: 30%">
                 &nbsp;
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <table border="0" style="width: 100%">
             <tr>
              <td class="pageButtonArea">
               <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                OnClick="ButtonSubmit_Click" Width="70px" />
               &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                OnClick="ButtonReset_Click" Width="70px" />
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Search Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Collection Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchCollectionNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Date Collection
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchDateCollection" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateCollection"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="search"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Cash Collector
                </td>
                <td>
                 <asp:DropDownList ID="DropDownListSrchCashCollector" runat="server" Width="250px">
                 </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td>
                </td>
                <td class="pageButtonArea">
                 <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                  Width="70px" ValidationGroup="search" />
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
             CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
             OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
             OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
             <PagerSettings Mode="NumericFirstLast" />
             <RowStyle CssClass="grid-row-normal-stc" />
             <Columns>
              <asp:ButtonField CommandName="EditRecord" Text="Edit" Visible="false">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="ViewRecord" Text="View">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:BoundField HeaderText="Collection Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Date Collection">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Cash Collector">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Total Amount">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
             </Columns>
             <PagerStyle CssClass="grid-pager" />
             <EmptyDataTemplate>
              <div id="norec" class="no-record-msg">
               <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                <tbody>
                 <tr>
                  <td style="padding-left: 15px">
                   No Record(s) Found!
                  </td>
                 </tr>
                </tbody>
               </table>
              </div>
             </EmptyDataTemplate>
             <HeaderStyle CssClass="grid-header" />
             <AlternatingRowStyle CssClass="grid-row-alt-stc" />
            </asp:GridView>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Cash Collection Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr class="view-back">
                <td style="width: 20%;">
                 Collection Number
                </td>
                <td style="width: 1%; text-align: center;">
                 :
                </td>
                <td style="width: 79%;">
                 <asp:Label ID="LabelViewCollectionNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Date Collection
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewDateCollection" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Cash Collector
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCashCollector" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Total Amount
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewTotalAmount" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Commission Rate
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCommissionRate" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Commission Amount
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCommissionAmount" runat="server"></asp:Label>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <fieldset>
             <legend style="font-weight: bold">Customer Payment(s)</legend>
             <div style="padding: 10px;">
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td>
                  <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                   OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                   <PagerSettings Mode="NumericFirstLast" />
                   <RowStyle CssClass="grid-row-normal-stc" />
                   <Columns>
                    <asp:BoundField HeaderText="Card Number">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Customer">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Paid Amount">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                   </Columns>
                   <PagerStyle CssClass="grid-pager" />
                   <EmptyDataTemplate>
                    <div id="norec" class="no-record-msg">
                     <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                      <tbody>
                       <tr>
                        <td style="padding-left: 15px">
                         No Items(s) Found!
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                   </EmptyDataTemplate>
                   <HeaderStyle CssClass="grid-header" />
                   <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                  </asp:GridView>
                 </td>
                </tr>
               </tbody>
              </table>
             </div>
            </fieldset>
           </td>
          </tr>
         </table>
        </asp:Panel>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </asp:Panel>
 <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px;
     padding-top: 15px" align="right">
     <img src="../../../images/AccessDenied.jpg" />
    </td>
    <caption>
     <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold;
      color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute;
       top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
    </caption>
   </tr>
  </table>
 </asp:Panel>
 <script language="javascript">
  $("#ctl00_ContentPlaceHolder1_TextBoxDateGRN").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxDatePO").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxDateManufactured").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxDateExpiry").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateGRN").mask("99/99/9999");
 </script>
</asp:Content>
