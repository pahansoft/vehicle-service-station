﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class SalesIssueItem : SolutionObjectBase
    {
        public SalesIssueItem() { }
        public SalesIssueItem(int? id) { this.ID = id; }

        public SolutionObjects.SalesIssue SalesIssue { get; set; }
        public SolutionObjects.ItemStock ItemStock { get; set; }
        public decimal? IssuedQuantity { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Gross { get; set; }
        public decimal? Net { get; set; }
        public decimal? ReturnedQuantity { get; set; }

        // Read only property for stock update
        public bool IsNewRec { get; set; }
    }
}
