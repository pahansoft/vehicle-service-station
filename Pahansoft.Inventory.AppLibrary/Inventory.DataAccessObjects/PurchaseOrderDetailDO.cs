﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class PurchaseOrderDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreatePurchaseOrderDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseOrderID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.PurchaseOrder != null ? PurchaseOrderDetail.PurchaseOrder.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ProductItem != null ? PurchaseOrderDetail.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReceivedQty", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ReceivedQty);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LineTotal", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.LineTotal);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrderDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                PurchaseOrderDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditPurchaseOrderDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseOrderID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.PurchaseOrder != null ? PurchaseOrderDetail.PurchaseOrder.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductItemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ProductItem != null ? PurchaseOrderDetail.ProductItem.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReceivedQty", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ReceivedQty);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LineTotal", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.LineTotal);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrderDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeletePurchaseOrderDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrderDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrderDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                PurchaseOrderDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public SolutionObjects.PurchaseOrderDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseOrderDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int PurchaseOrderIDOrdinal = dr.GetOrdinal("PurchaseOrderID");
                        int ProductItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                        int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int QuantityOrdinal = dr.GetOrdinal("Quantity");
                        int ReceivedQtyOrdinal = dr.GetOrdinal("ReceivedQty");
                        int LineTotalOrdinal = dr.GetOrdinal("LineTotal");

                        PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrderDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PurchaseOrderIDOrdinal)) PurchaseOrderDetail.PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(PurchaseOrderIDOrdinal));
                        if (!dr.IsDBNull(ProductItemIDOrdinal)) PurchaseOrderDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(ProductItemIDOrdinal));
                        if (!dr.IsDBNull(UnitPriceOrdinal)) PurchaseOrderDetail.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(QuantityOrdinal)) PurchaseOrderDetail.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(ReceivedQtyOrdinal)) PurchaseOrderDetail.ReceivedQty = dr.GetDecimal(ReceivedQtyOrdinal);

                        if (!dr.IsDBNull(LineTotalOrdinal)) PurchaseOrderDetail.LineTotal = dr.GetDecimal(LineTotalOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrderDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrderDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseOrderDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var PurchaseOrderDetailList = new System.Collections.Generic.List<SolutionObjects.PurchaseOrderDetail>();
            SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int PurchaseOrderIDOrdinal = dr.GetOrdinal("PurchaseOrderID");
                    int ProductItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int QuantityOrdinal = dr.GetOrdinal("Quantity");
                    int ReceivedQtyOrdinal = dr.GetOrdinal("ReceivedQty");
                    int LineTotalOrdinal = dr.GetOrdinal("LineTotal");

                    while (dr.Read())
                    {
                        PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrderDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PurchaseOrderIDOrdinal)) PurchaseOrderDetail.PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(PurchaseOrderIDOrdinal));
                        if (!dr.IsDBNull(ProductItemIDOrdinal)) PurchaseOrderDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(ProductItemIDOrdinal));
                        if (!dr.IsDBNull(UnitPriceOrdinal)) PurchaseOrderDetail.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(QuantityOrdinal)) PurchaseOrderDetail.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(ReceivedQtyOrdinal)) PurchaseOrderDetail.ReceivedQty = dr.GetDecimal(ReceivedQtyOrdinal);
                        if (!dr.IsDBNull(LineTotalOrdinal)) PurchaseOrderDetail.LineTotal = dr.GetDecimal(LineTotalOrdinal);

                        PurchaseOrderDetailList.Add(PurchaseOrderDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrderDetailList;
        }

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrderDetail> GetAllFor(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseOrderDetailsForPurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseOrderID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrder.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var PurchaseOrderDetailList = new System.Collections.Generic.List<SolutionObjects.PurchaseOrderDetail>();
            SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int PurchaseOrderIDOrdinal = dr.GetOrdinal("PurchaseOrderID");
                    int ProductItemIDOrdinal = dr.GetOrdinal("ProductItemID");
                    int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int QuantityOrdinal = dr.GetOrdinal("Quantity");
                    int ReceivedQtyOrdinal = dr.GetOrdinal("ReceivedQty");
                    int LineTotalOrdinal = dr.GetOrdinal("LineTotal");

                    while (dr.Read())
                    {
                        PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrderDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PurchaseOrderIDOrdinal)) PurchaseOrderDetail.PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(PurchaseOrderIDOrdinal));
                        if (!dr.IsDBNull(ProductItemIDOrdinal)) PurchaseOrderDetail.ProductItem = new SolutionObjects.ProductItem(dr.GetInt32(ProductItemIDOrdinal));
                        if (!dr.IsDBNull(UnitPriceOrdinal)) PurchaseOrderDetail.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(QuantityOrdinal)) PurchaseOrderDetail.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(ReceivedQtyOrdinal)) PurchaseOrderDetail.ReceivedQty = dr.GetDecimal(ReceivedQtyOrdinal);
                        if (!dr.IsDBNull(LineTotalOrdinal)) PurchaseOrderDetail.LineTotal = dr.GetDecimal(LineTotalOrdinal);

                        PurchaseOrderDetailList.Add(PurchaseOrderDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrderDetailList;
        }

        #endregion
    }
}
