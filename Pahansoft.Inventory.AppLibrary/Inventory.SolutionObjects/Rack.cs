﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Rack : SolutionObjectBase
    {
        public Rack() { }
        public Rack(int? id) { this.ID = id; }

        public SolutionObjects.Store Store { get; set; }
        public SolutionObjects.Location Location { get; set; }
        public string Description { get; set; }
    }
}
