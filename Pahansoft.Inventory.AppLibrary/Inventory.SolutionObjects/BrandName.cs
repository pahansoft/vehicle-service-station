﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class BrandName : SolutionObjectBase
    {
        public BrandName() { }
        public BrandName(int? id) { this.ID = id; }

        public string Description { get; set; }
        public SolutionObjects.Manufacturer Manufacturer { get; set; }
    }
}
