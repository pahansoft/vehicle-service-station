﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class repo_rolq_Default : System.Web.UI.Page
{
    #region -Fill Dropdownlist -
    private void fillDropdownListStore()
    {
        Util.ClearDropdown(DropDownListStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
        {

            DropDownListStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
        }

    }

    private void fillDropdownListProductType()
    {
        Util.ClearDropdown(DropDownListProductType);
        foreach (SolutionObjects.ProductType producttype in new OperationObjects.ProductTypeOP().GetAllProductTypes())
        {

            DropDownListProductType.Items.Add(new ListItem(producttype.Description, producttype.ID.Value.ToString()));
        }

    }


    #endregion




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            fillDropdownListStore();
            fillDropdownListProductType();
        }
        PanelInformationMessage.Visible = false;
    }

    protected void DropDownListStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListLocation);
        foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(null, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
        {

            DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }



    }
    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListRack);
        foreach (SolutionObjects.Rack rack in new OperationObjects.RackOP().SearchRacks(string.Empty, null, new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue))))
        {

            DropDownListRack.Items.Add(new ListItem(rack.Description, rack.ID.Value.ToString()));
        }




    }



    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {

    }
    protected void ButtonPrint_Click(object sender, EventArgs e)
    {
        int? Store = null;

        if (DropDownListStore.SelectedIndex > 0)
            Store = int.Parse(DropDownListStore.SelectedValue);

        int? Location = null;

        if (DropDownListLocation.SelectedIndex > 0)
            Location = int.Parse(DropDownListLocation.SelectedValue);

        int? Rack = null;

        if (DropDownListRack.SelectedIndex > 0)
            Rack = int.Parse(DropDownListRack.SelectedValue);

        int? ProductType = null;

        if (DropDownListProductType.SelectedIndex > 0)
            ProductType = int.Parse(DropDownListProductType.SelectedValue);



        HtmlGenericControl dispval = new HtmlGenericControl();
        dispval.InnerHtml = "<script language='javascript'> print('" + Store + "','" + Location + "','" + Rack + "','" + ProductType + "')</script>";
        Page.Controls.Add(dispval);



    }
}
