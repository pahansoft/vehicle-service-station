﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SupplierDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.Supplier supplier)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateSupplier";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = supplier;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                supplier.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.Supplier supplier)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditSupplier";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, supplier.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, supplier.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = supplier;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.Supplier supplier)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteSupplier";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, supplier.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, supplier.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, supplier.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = supplier;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                supplier.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.Supplier Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetSupplier";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.Supplier supplier = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int nameOrdinal = dr.GetOrdinal("Name");
                        int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                        int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                        int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                        int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                        int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                        int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                        int faxOrdinal = dr.GetOrdinal("Fax");
                        int emailOrdinal = dr.GetOrdinal("Email");

                        supplier = new Inventory.SolutionObjects.Supplier(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) supplier.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) supplier.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) supplier.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) supplier.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) supplier.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) supplier.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) supplier.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) supplier.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) supplier.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) supplier.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) supplier.Email = dr.GetString(emailOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return supplier;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllSuppliers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> supplierList = new System.Collections.Generic.List<Inventory.SolutionObjects.Supplier>();
            Inventory.SolutionObjects.Supplier supplier;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        supplier = new Inventory.SolutionObjects.Supplier(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) supplier.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) supplier.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) supplier.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) supplier.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) supplier.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) supplier.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) supplier.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) supplier.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) supplier.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) supplier.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) supplier.Email = dr.GetString(emailOrdinal);

                        supplierList.Add(supplier);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return supplierList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> Search(string code, string name)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllSuppliers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, name != string.Empty ? name : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> supplierList = new System.Collections.Generic.List<Inventory.SolutionObjects.Supplier>();
            Inventory.SolutionObjects.Supplier supplier;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        supplier = new Inventory.SolutionObjects.Supplier(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) supplier.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) supplier.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) supplier.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) supplier.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) supplier.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) supplier.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) supplier.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) supplier.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) supplier.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) supplier.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) supplier.Email = dr.GetString(emailOrdinal);

                        supplierList.Add(supplier);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return supplierList;
        }

        #endregion
    }
}
