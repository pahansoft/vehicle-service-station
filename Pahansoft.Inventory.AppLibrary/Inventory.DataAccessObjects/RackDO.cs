﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class RackDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.Rack rack)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateRack";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, rack.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.Store != null ? rack.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.Location != null ? rack.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, rack.Description);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = rack;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                rack.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.Rack rack)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditRack";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, rack.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, rack.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.Store != null ? rack.Store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.Location != null ? rack.Location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, rack.Description);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = rack;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.Rack rack)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteRack";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, rack.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, rack.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, rack.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = rack;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                rack.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.Rack Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetRack";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.Rack rack = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int storeIDOrdinal = dr.GetOrdinal("StoreID");
                        int locationIDOrdinal = dr.GetOrdinal("LocationID");
                        int descriptionOrdinal = dr.GetOrdinal("Description");

                        rack = new Inventory.SolutionObjects.Rack(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) rack.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(storeIDOrdinal)) rack.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) rack.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(descriptionOrdinal)) rack.Description = dr.GetString(descriptionOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return rack;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllRacks";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> rackList = new System.Collections.Generic.List<Inventory.SolutionObjects.Rack>();
            Inventory.SolutionObjects.Rack rack;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int descriptionOrdinal = dr.GetOrdinal("Description");

                    while (dr.Read())
                    {
                        rack = new Inventory.SolutionObjects.Rack(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) rack.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(storeIDOrdinal)) rack.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) rack.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(descriptionOrdinal)) rack.Description = dr.GetString(descriptionOrdinal);

                        rackList.Add(rack);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return rackList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> Search(string description, SolutionObjects.Store store, SolutionObjects.Location location)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllRacks";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@StoreID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, store != null ? store.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@LocationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, location != null ? location.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, description != string.Empty ? description : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> rackList = new System.Collections.Generic.List<Inventory.SolutionObjects.Rack>();
            Inventory.SolutionObjects.Rack rack;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int storeIDOrdinal = dr.GetOrdinal("StoreID");
                    int locationIDOrdinal = dr.GetOrdinal("LocationID");
                    int descriptionOrdinal = dr.GetOrdinal("Description");

                    while (dr.Read())
                    {
                        rack = new Inventory.SolutionObjects.Rack(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) rack.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(storeIDOrdinal)) rack.Store = new SolutionObjects.Store(dr.GetInt32(storeIDOrdinal));
                        if (!dr.IsDBNull(locationIDOrdinal)) rack.Location = new SolutionObjects.Location(dr.GetInt32(locationIDOrdinal));
                        if (!dr.IsDBNull(descriptionOrdinal)) rack.Description = dr.GetString(descriptionOrdinal);

                        rackList.Add(rack);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return rackList;
        }

        #endregion
    }
}
