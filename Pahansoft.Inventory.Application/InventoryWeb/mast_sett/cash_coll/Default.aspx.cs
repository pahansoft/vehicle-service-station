﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_cash_coll_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.CashCollector _cashCollector;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxCode.Text = string.Empty;
        TextBoxName.Text = string.Empty;
        TextBoxAddressLine1.Text = string.Empty;
        TextBoxAddressLine2.Text = string.Empty;
        TextBoxAddressLine3.Text = string.Empty;
        TextBoxTelephoneHome.Text = string.Empty;
        TextBoxTelephoneMobile.Text = string.Empty;
        TextBoxTelephoneOffice.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxEmail.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchCode.Text = string.Empty;
        TextBoxSrchName.Text = string.Empty;
    }

    private void Search()
    {
        GridViewSearchList.DataSource = new OperationObjects.CashCollectorOP().SearchCashCollectors(TextBoxSrchCode.Text.Trim(), TextBoxSrchName.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            ViewState[_target] = new SolutionObjects.CashCollector();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.CashCollector();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _cashCollector = ViewState[_target] as SolutionObjects.CashCollector;
        _cashCollector.Code = TextBoxCode.Text.Trim();
        _cashCollector.Name = TextBoxName.Text.Trim();
        _cashCollector.AddressLine1 = TextBoxAddressLine1.Text.Trim();
        _cashCollector.AddressLine2 = TextBoxAddressLine2.Text.Trim();
        _cashCollector.AddressLine3 = TextBoxAddressLine3.Text.Trim();
        _cashCollector.TelephoneHome = TextBoxTelephoneHome.Text.Trim();
        _cashCollector.TelephoneMobile = TextBoxTelephoneMobile.Text.Trim();
        _cashCollector.TelephoneOffice = TextBoxTelephoneOffice.Text.Trim();
        _cashCollector.Fax = TextBoxFax.Text.Trim();
        _cashCollector.Email = TextBoxEmail.Text.Trim();
        _cashCollector.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.CashCollectorOP().SaveCashCollector(_cashCollector);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.CashCollector();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Code cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Code cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _cashCollector = ViewState[_target] as SolutionObjects.CashCollector;

        if (_cashCollector.ID != null)
        {
            _cashCollector = new OperationObjects.CashCollectorOP().GetCashCollector(_cashCollector.ID.Value);
            ViewState[_target] = _cashCollector;

            TextBoxCode.Text = _cashCollector.Code;
            TextBoxName.Text = _cashCollector.Name;
            TextBoxAddressLine1.Text = _cashCollector.AddressLine1;
            TextBoxAddressLine2.Text = _cashCollector.AddressLine2;
            TextBoxAddressLine3.Text = _cashCollector.AddressLine3;
            TextBoxTelephoneHome.Text = _cashCollector.TelephoneHome;
            TextBoxTelephoneMobile.Text = _cashCollector.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _cashCollector.TelephoneOffice;
            TextBoxFax.Text = _cashCollector.Fax;
            TextBoxEmail.Text = _cashCollector.Email;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.CashCollector();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _cashCollector = new OperationObjects.CashCollectorOP().GetCashCollector(_targetid);

            ClearAll();
            ViewState[_target] = _cashCollector;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxCode.Text = _cashCollector.Code;
            TextBoxName.Text = _cashCollector.Name;
            TextBoxAddressLine1.Text = _cashCollector.AddressLine1;
            TextBoxAddressLine2.Text = _cashCollector.AddressLine2;
            TextBoxAddressLine3.Text = _cashCollector.AddressLine3;
            TextBoxTelephoneHome.Text = _cashCollector.TelephoneHome;
            TextBoxTelephoneMobile.Text = _cashCollector.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _cashCollector.TelephoneOffice;
            TextBoxFax.Text = _cashCollector.Fax;
            TextBoxEmail.Text = _cashCollector.Email;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _cashCollector = new OperationObjects.CashCollectorOP().GetCashCollector(_targetid);
            _cashCollector.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.CashCollectorOP().DeleteCashCollector(_cashCollector);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _cashCollector = new OperationObjects.CashCollectorOP().GetCashCollector(_targetid);

            ViewState[_target] = _cashCollector;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewCode.Text = Util.FormatEmptyString(_cashCollector.Code);
            LabelViewName.Text = Util.FormatEmptyString(_cashCollector.Name);
            LabelViewAddressLine1.Text = Util.FormatEmptyString(_cashCollector.AddressLine1);
            LabelViewAddressLine2.Text = Util.FormatEmptyString(_cashCollector.AddressLine2);
            LabelViewAddressLine3.Text = Util.FormatEmptyString(_cashCollector.AddressLine3);
            LabelViewTelephoneHome.Text = Util.FormatEmptyString(_cashCollector.TelephoneHome);
            LabelViewTelephoneMobile.Text = Util.FormatEmptyString(_cashCollector.TelephoneMobile);
            LabelViewTelephoneOffice.Text = Util.FormatEmptyString(_cashCollector.TelephoneOffice);
            LabelViewFax.Text = Util.FormatEmptyString(_cashCollector.Fax);
            LabelViewEmail.Text = Util.FormatEmptyString(_cashCollector.Email);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _cashCollector = e.Row.DataItem as SolutionObjects.CashCollector;
            Util.EncodeString(e.Row.Cells[3], _cashCollector.Code, 50);
            Util.EncodeString(e.Row.Cells[4], _cashCollector.Name, 50);
            Util.EncodeString(e.Row.Cells[5], _cashCollector.AddressLine1, 50);
            Util.EncodeString(e.Row.Cells[6], _cashCollector.TelephoneMobile, 50);
            Util.EncodeString(e.Row.Cells[7], _cashCollector.Email, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _cashCollector.Name + "');");
                }
            }
        }
    }
}