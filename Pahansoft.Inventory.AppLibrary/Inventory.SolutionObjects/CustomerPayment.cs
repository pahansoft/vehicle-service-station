﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class CustomerPayment : SolutionObjectBase
    {
        public CustomerPayment() { }
        public CustomerPayment(int? id) { this.ID = id; }

        public SolutionObjects.Customer Customer { get; set; }
        public SolutionObjects.DebitCreditTypes DebitCreditType { get; set; }
        public decimal? Amount { get; set; }
        public int? JobCardID { get; set; }
        public bool IsCash { get; set; }
        public string CardNumber { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }

    }
}
