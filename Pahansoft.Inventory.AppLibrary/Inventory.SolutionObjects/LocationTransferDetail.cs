﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class LocationTransferDetail : SolutionObjectBase
    {
        public LocationTransferDetail() { }
        public LocationTransferDetail(int? id) { this.ID = id; }

        public SolutionObjects.LocationTransfer LocationTransfer { get; set; }
        public SolutionObjects.Store FromStore { get; set; }
        public SolutionObjects.Location FromLocation { get; set; }
        public SolutionObjects.Rack FromRack { get; set; }
        public SolutionObjects.Store ToStore { get; set; }
        public SolutionObjects.Location ToLocation { get; set; }
        public SolutionObjects.Rack ToRack { get; set; }
        public SolutionObjects.ItemStock ItemStock { get; set; }
        public decimal? Quantity { get; set; }
    }
}
