﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SalesIssueItemOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSalesIssueItem(Inventory.SolutionObjects.SalesIssueItem salesIssueItem)
        {
            if (salesIssueItem.ID == null)
                return new Inventory.DataAccessObjects.SalesIssueItemDO().Create(salesIssueItem);
            else
                return new Inventory.DataAccessObjects.SalesIssueItemDO().Edit(salesIssueItem);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSalesIssueItem(Inventory.SolutionObjects.SalesIssueItem salesIssueItem)
        {
            if (salesIssueItem.ID != null)
                return new Inventory.DataAccessObjects.SalesIssueItemDO().Delete(salesIssueItem);
            else
            {
                salesIssueItem.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = salesIssueItem;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.SalesIssueItem GetSalesIssueItem(int id)
        {
            return new Inventory.DataAccessObjects.SalesIssueItemDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> GetAllSalesIssueItems()
        {
            return new Inventory.DataAccessObjects.SalesIssueItemDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssueItem> GetAllFor(SolutionObjects.SalesIssue salesIssue)
        {
            return new Inventory.DataAccessObjects.SalesIssueItemDO().GetAllFor(salesIssue);
        }
    }
}
