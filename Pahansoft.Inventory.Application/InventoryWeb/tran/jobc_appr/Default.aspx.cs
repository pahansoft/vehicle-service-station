﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Globalization;

#endregion

public partial class tran_jobc_appr_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string target = "Target";
    public const string targetCustomers = "TargetCustomers";
    public const string targetSupervisors = "TargetSupervisors";
    public const string targetProducts = "TargetProducts";
    public const string targetServices = "TargetServices";

    SolutionObjects.JobCard JobCard;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxJobNumber.Text = "- AUTO GENERATED -";
        TextBoxVehicleNumber.Text = string.Empty;
        TextBoxStartDate.Text = DateTime.Today.ToShortDateString();
        TextBoxCustomer.Text = string.Empty;
        TextBoxEndDate.Text = string.Empty;
        TextBoxAddress.Text = string.Empty;
        TextBoxPreviousDue.Text = string.Empty;
        TextBoxTelephone.Text = string.Empty;
        TextBoxSupervisor.Text = string.Empty;
        TextBoxTotalGross.Text = string.Empty;
        TextBoxTotalTax.Text = string.Empty;
        TextBoxTotalDiscount.Text = string.Empty;
        TextBoxTotalNet.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();

        GridViewServiceAdd.DataSource = null;
        GridViewServiceAdd.DataBind();
        ClearService();
    }

    private void ClearItem()
    {
        LabelItemError.Visible = false;
        LabelItemError.Text = string.Empty;
        LabelAvailableQty.Text = string.Empty;

        TextBoxProductItem.Text = string.Empty;
        TextBoxDiscountPercentage.Text = string.Empty;
        TextBoxSellingPrice.Text = string.Empty;
        TextBoxDiscount.Text = string.Empty;
        TextBoxQuantity.Text = string.Empty;
        TextBoxTax.Text = string.Empty;
        TextBoxGrossAmount.Text = string.Empty;
        TextBoxNetAmount.Text = string.Empty;
    }

    private void ClearForVehicleNumber()
    {
        TextBoxCustomer.Text = string.Empty;
        TextBoxAddress.Text = string.Empty;
        TextBoxTelephone.Text = string.Empty;
        TextBoxPreviousDue.Text = string.Empty;
    }

    private void ClearForProductItem()
    {
        LabelItemError.Visible = false;
        LabelItemError.Text = string.Empty;
        LabelAvailableQty.Text = string.Empty;

        TextBoxDiscountPercentage.Text = string.Empty;
        TextBoxSellingPrice.Text = string.Empty;
        TextBoxDiscount.Text = string.Empty;
        TextBoxQuantity.Text = string.Empty;
        TextBoxTax.Text = string.Empty;
        TextBoxGrossAmount.Text = string.Empty;
        TextBoxNetAmount.Text = string.Empty;
    }

    private void ClearService()
    {
        LabelServiceError.Visible = false;
        LabelServiceError.Text = string.Empty;

        TextBoxService.Text = string.Empty;
        TextBoxServiceDiscountPercentage.Text = string.Empty;
        TextBoxServiceAmount.Text = string.Empty;
        TextBoxServiceDiscount.Text = string.Empty;
        TextBoxServiceNetAmount.Text = string.Empty;
        TextBoxServiceTax.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchJobNumber.Text = string.Empty;
        TextBoxSrchVehicleNumber.Text = string.Empty;
        TextBoxSrchCustomer.Text = string.Empty;
        CheckBoxSrchApprovedOnly.Checked = true;
    }

    private void FillDropDownList()
    {
        DropDownListPaymentType.Items.Add(new ListItem("Cash", "Cash"));
        DropDownListPaymentType.Items.Add(new ListItem("Card", "Card"));
    }

    private void Search()
    {
        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;

        int? CustomerID = null;
        if (TextBoxSrchCustomer.Text.Trim() != string.Empty)
        {
            var Customer = Customers.FirstOrDefault(t => t.Name == TextBoxSrchCustomer.Text);
            if (Customer != null)
                CustomerID = Customer.ID;
        }
        bool? ApprovedOnly = null;
        if (CheckBoxSrchApprovedOnly.Checked)
            ApprovedOnly = false;

        GridViewSearchList.DataSource = new OperationObjects.JobCardOP().Search(TextBoxSrchJobNumber.Text.Trim(), TextBoxSrchVehicleNumber.Text.Trim(), CustomerID, ApprovedOnly);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotal()
    {
        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;

        Decimal TotalGross = 0;
        Decimal TotalTax = 0;
        Decimal TotalDiscount = 0;
        Decimal TotalNet = 0;

        foreach (var item in JobCard.ItemList)
        {
            TotalGross += item.GrossAmount.Value;
            TotalTax += item.TaxAmount != null ? item.TaxAmount.Value : 0;
            TotalDiscount += item.DiscountAmount != null ? item.DiscountAmount.Value : 0;
            TotalNet += item.NetAmount.Value;
        }

        foreach (var item in JobCard.ServiceList)
        {
            TotalGross += item.GrossAmount.Value;
            TotalTax += item.TaxAmount != null ? item.TaxAmount.Value : 0;
            TotalDiscount += item.DiscountAmount != null ? item.DiscountAmount.Value : 0;
            TotalNet += item.NetAmount.Value;
        }

        TextBoxTotalGross.Text = TotalGross.ToString("F");
        TextBoxTotalTax.Text = TotalTax.ToString("F");
        TextBoxTotalDiscount.Text = TotalDiscount.ToString("F");
        TextBoxTotalNet.Text = TotalNet.ToString("F");
    }

    private void FillForEdit(int JobCardID)
    {
        JobCard = new OperationObjects.JobCardOP().Get(JobCardID);
        if (JobCard.Customer != null)
            JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);
        if (JobCard.Supervisor != null)
            JobCard.Supervisor = new OperationObjects.SupervisorOP().GetSupervisor(JobCard.Supervisor.ID.Value);

        HttpContext.Current.Session[target] = JobCard;

        TextBoxJobNumber.Text = JobCard.JobNumber;
        TextBoxVehicleNumber.Text = JobCard.VehicleNumber;
        TextBoxStartDate.Text = JobCard.StartDate != null ? JobCard.StartDate.Value.ToShortDateString() : string.Empty;
        if (JobCard.Customer != null)
            TextBoxCustomer.Text = JobCard.Customer.Name;
        TextBoxEndDate.Text = JobCard.EndDate != null ? JobCard.EndDate.Value.ToShortDateString() : string.Empty;
        TextBoxAddress.Text = JobCard.Address;
        //TextBoxPreviousDue.Text = JobCard.PreviousDue!=null?;
        TextBoxTelephone.Text = JobCard.TelephoneNumber;
        if (JobCard.Supervisor != null)
            TextBoxSupervisor.Text = JobCard.Supervisor.Name;

        TextBoxTotalGross.Text = JobCard.TotalGross != null ? JobCard.TotalGross.Value.ToString() : string.Empty;
        TextBoxTotalTax.Text = JobCard.TotalTax != null ? JobCard.TotalTax.Value.ToString() : string.Empty;
        TextBoxTotalDiscount.Text = JobCard.TotalDiscount != null ? JobCard.TotalDiscount.Value.ToString() : string.Empty;
        TextBoxTotalNet.Text = JobCard.TotalNet != null ? JobCard.TotalNet.Value.ToString() : string.Empty;
        TextBoxRemarks.Text = JobCard.Remarks;
        if (JobCard.Customer != null)
            CalculateDue(JobCard.Customer.ID.Value);

        TextBoxPaidAmount.Text = JobCard.TotalNet != null ? JobCard.TotalNet.Value.ToString() : string.Empty;

        GridViewItemAdd.DataSource = JobCard.ItemList;
        GridViewItemAdd.DataBind();

        GridViewServiceAdd.DataSource = JobCard.ServiceList;
        GridViewServiceAdd.DataBind();
    }

    private void FillForView(int JobCardID)
    {
        JobCard = new OperationObjects.JobCardOP().Get(JobCardID);
        if (JobCard.Customer != null)
            JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);
        if (JobCard.Supervisor != null)
            JobCard.Supervisor = new OperationObjects.SupervisorOP().GetSupervisor(JobCard.Supervisor.ID.Value);

        HttpContext.Current.Session[target] = JobCard;

        LabelViewJobNumber.Text = Util.FormatEmptyString(JobCard.JobNumber);
        LabelViewVehicleNumber.Text = Util.FormatEmptyString(JobCard.VehicleNumber);
        LabelViewStartDate.Text = Util.FormatEmptyString(JobCard.StartDate != null ? JobCard.StartDate.Value.ToShortDateString() : string.Empty);
        LabelViewCustomer.Text = Util.FormatEmptyString(JobCard.Customer != null ? JobCard.Customer.Name : string.Empty);
        LabelViewEndDate.Text = Util.FormatEmptyString(JobCard.EndDate != null ? JobCard.EndDate.Value.ToShortDateString() : string.Empty);
        LabelViewAddress.Text = Util.FormatEmptyString(JobCard.Address);
        LabelViewSupervisor.Text = Util.FormatEmptyString(JobCard.Supervisor != null ? JobCard.Supervisor.Name : string.Empty);
        LabelViewTelephone.Text = Util.FormatEmptyString(JobCard.TelephoneNumber);
        LabelViewRemarks.Text = Util.FormatEmptyString(JobCard.Remarks);
        LabelViewStatus.Text = Util.FormatEmptyString(JobCard.IsApproved ? "Approved" : "Pending");

        LabelViewTotalGross.Text = Util.FormatEmptyString(JobCard.TotalGross != null ? JobCard.TotalGross.Value.ToString() : string.Empty);
        LabelViewTotalTax.Text = Util.FormatEmptyString(JobCard.TotalTax != null ? JobCard.TotalTax.Value.ToString() : string.Empty);
        LabelViewTotalNet.Text = Util.FormatEmptyString(JobCard.TotalNet != null ? JobCard.TotalNet.Value.ToString() : string.Empty);
        LabelViewTotalDiscount.Text = Util.FormatEmptyString(JobCard.TotalDiscount != null ? JobCard.TotalDiscount.Value.ToString() : string.Empty);

        GridViewItemView.DataSource = JobCard.ItemList;
        GridViewItemView.DataBind();

        GridViewServiceView.DataSource = JobCard.ServiceList;
        GridViewServiceView.DataBind();

        var Payments = new OperationObjects.CustomerPaymentOP().Search(JobCard.Customer.ID, SolutionObjects.DebitCreditTypes.Cr, JobCard.ID);
        GridViewPaymentView.DataSource = Payments;
        GridViewPaymentView.DataBind();
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetProductItems(string Text)
    {
        var ProductItems = HttpContext.Current.Session[targetProducts] as IList<SolutionObjects.ItemStock>;

        var returnList = new List<string>();
        foreach (var item in ProductItems.Where(t => t.ProductItem.Code.ToLower().Contains(Text.ToLower()) || t.ProductItem.ReferenceNumber.ToLower().Contains(Text.ToLower()) || t.ProductItem.Description.ToLower().Contains(Text.ToLower())).ToList())
        {
            var sbDescription = new StringBuilder();
            sbDescription.Append(item.ProductItem.Code);
            sbDescription.Append(item.ProductItem.ReferenceNumber != string.Empty ? ("~" + item.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(item.ProductItem.Description);
            sbDescription.Append("|").Append(item.SellingPrice.Value.ToString("F"));
            returnList.Add(sbDescription.ToString());
        }

        return returnList;
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetCustomers(string Text)
    {
        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;

        var returnList = new List<string>();
        foreach (var item in Customers.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.Name.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Name);

        return returnList;
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetSupervisors(string Text)
    {
        var Supervisors = HttpContext.Current.Session[targetSupervisors] as IList<SolutionObjects.Supervisor>;

        var returnList = new List<string>();
        foreach (var item in Supervisors.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.Name.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Name);

        return returnList;
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetServices(string Text)
    {
        var Services = HttpContext.Current.Session[targetServices] as IList<SolutionObjects.Service>;

        var returnList = new List<string>();
        foreach (var item in Services.Where(t => t.Description.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Description);

        return returnList;
    }

    private void CalculateDue(int CustomerID)
    {
        Decimal due = 0;
        var list = new OperationObjects.CustomerPaymentOP().Search(CustomerID, null, null);
        foreach (var item in list.Where(t => t.DebitCreditType == SolutionObjects.DebitCreditTypes.Cr))
        {
            if (item.Amount != null)
                due += item.Amount.Value;
        }
        foreach (var item in list.Where(t => t.DebitCreditType == SolutionObjects.DebitCreditTypes.Dr))
        {
            if (item.Amount != null)
                due -= item.Amount.Value;
        }

        TextBoxPreviousDue.Text = due.ToString();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillDropDownList();
            ClearAll();
            JobCard = new SolutionObjects.JobCard();
            JobCard.ItemList = new CommonObjects.PropertyList<SolutionObjects.JobCardItem>();
            JobCard.ServiceList = new CommonObjects.PropertyList<SolutionObjects.JobCardService>();
            HttpContext.Current.Session[target] = JobCard;
            LabelPageOperation.Text = "Search";

            IList<SolutionObjects.ItemStock> itemStocks = new List<SolutionObjects.ItemStock>();
            foreach (var item in new OperationObjects.ItemStockOP().GetAllItemStocks())
            {
                if (item.CurrentQuantity > 0)
                {
                    item.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(item.ProductItem.ID.Value);
                    itemStocks.Add(item);
                }
            }
            HttpContext.Current.Session[targetProducts] = itemStocks;

            var Customers = new OperationObjects.CustomerOP().GetAllCustomers();
            HttpContext.Current.Session[targetCustomers] = Customers;

            var Supervisors = new OperationObjects.SupervisorOP().GetAllSupervisors();
            HttpContext.Current.Session[targetSupervisors] = Supervisors;

            var Services = new OperationObjects.ServiceOP().GetAllService();
            HttpContext.Current.Session[targetServices] = Services;

            ClearSearch();
            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }

    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        //LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    //protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    //{
    //    ClearAll();
    //    JobCard = new SolutionObjects.JobCard();
    //    JobCard.ItemList = new CommonObjects.PropertyList<SolutionObjects.JobCardItem>();
    //    JobCard.ServiceList = new CommonObjects.PropertyList<SolutionObjects.JobCardService>();
    //    HttpContext.Current.Session[target] = JobCard;

    //    PanelAddNew.Visible = true;
    //    PanelSearchDetails.Visible = false;
    //    PanelView.Visible = false;
    //    LabelPageOperation.Text = "Add New";
    //    LinkButtonAddNewOperation.Visible = false;
    //    LinkButtonSearchOperation.Visible = true;
    //}
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void TextBoxVehicleNumber_TextChanged(object sender, EventArgs e)
    {
        ClearForVehicleNumber();

        var VehicleDetails = new OperationObjects.VehicleDetailOP().Search(TextBoxVehicleNumber.Text.Trim(), null);
        if (VehicleDetails.Count == 1)
        {
            VehicleDetails[0].Customer = new OperationObjects.CustomerOP().GetCustomer(VehicleDetails[0].Customer.ID.Value);
            TextBoxCustomer.Text = VehicleDetails[0].Customer.Name;

            var sbAddr = new StringBuilder();
            if (VehicleDetails[0].Customer.AddressLine1 != string.Empty)
                sbAddr.Append(VehicleDetails[0].Customer.AddressLine1).Append(", ");
            if (VehicleDetails[0].Customer.AddressLine2 != string.Empty)
                sbAddr.Append(VehicleDetails[0].Customer.AddressLine2).Append(", ");
            if (VehicleDetails[0].Customer.AddressLine3 != string.Empty)
                sbAddr.Append(VehicleDetails[0].Customer.AddressLine3).Append(", ");
            if (sbAddr.Length > 0)
                TextBoxAddress.Text = sbAddr.ToString().Substring(0, sbAddr.Length - 2);

            if (VehicleDetails[0].Customer.TelephoneMobile != string.Empty)
                TextBoxTelephone.Text = VehicleDetails[0].Customer.TelephoneMobile;
            else if (VehicleDetails[0].Customer.TelephoneHome != string.Empty)
                TextBoxTelephone.Text = VehicleDetails[0].Customer.TelephoneHome;
            else
                TextBoxTelephone.Text = VehicleDetails[0].Customer.TelephoneOffice;

            CalculateDue(VehicleDetails[0].Customer.ID.Value);
            TextBoxCustomer.Focus();
        }
    }
    protected void TextBoxCustomer_TextChanged(object sender, EventArgs e)
    {
        TextBoxAddress.Text = string.Empty;
        TextBoxTelephone.Text = string.Empty;
        TextBoxPreviousDue.Text = string.Empty;

        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;

        if (TextBoxCustomer.Text.Trim() != string.Empty)
        {
            var customer = Customers.FirstOrDefault(t => t.Name == TextBoxCustomer.Text);
            if (customer != null)
            {
                var sbAddr = new StringBuilder();
                if (customer.AddressLine1 != string.Empty)
                    sbAddr.Append(customer.AddressLine1).Append(", ");
                if (customer.AddressLine2 != string.Empty)
                    sbAddr.Append(customer.AddressLine2).Append(", ");
                if (customer.AddressLine3 != string.Empty)
                    sbAddr.Append(customer.AddressLine3).Append(", ");
                if (sbAddr.Length > 0)
                    TextBoxAddress.Text = sbAddr.ToString().Substring(0, sbAddr.Length - 2);

                if (customer.TelephoneMobile != string.Empty)
                    TextBoxTelephone.Text = customer.TelephoneMobile;
                else if (customer.TelephoneHome != string.Empty)
                    TextBoxTelephone.Text = customer.TelephoneHome;
                else
                    TextBoxTelephone.Text = customer.TelephoneOffice;
                CalculateDue(customer.ID.Value);
                TextBoxEndDate.Focus();
            }
        }
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;
        JobCard.ItemList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        HttpContext.Current.Session[target] = JobCard;
        GridViewItemAdd.DataSource = JobCard.ItemList;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        UpdatePanelSummary.Update();
        UpdatePanelPayment.Update();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var Item = e.Row.DataItem as SolutionObjects.JobCardItem;
            Item.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(Item.ItemStock.ID.Value);
            Item.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(Item.ItemStock.ProductItem.ID.Value);

            Util.EncodeString(e.Row.Cells[1], Item.ItemStock.ProductItem.Description, 50);
            Util.EncodeString(e.Row.Cells[2], Item.Quantity.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[3], Item.UnitPrice.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[4], Item.GrossAmount.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[5], Item.DiscountPercentage != null ? Item.DiscountPercentage.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], Item.DiscountAmount != null ? Item.DiscountAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[7], Item.TaxAmount != null ? Item.TaxAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[8], Item.NetAmount.Value.ToString("F"), 20);
        }
    }
    protected void TextBoxProductItem_TextChanged(object sender, EventArgs e)
    {
        var ProductItems = HttpContext.Current.Session[targetProducts] as IList<SolutionObjects.ItemStock>;
        ClearForProductItem();
        if (TextBoxProductItem.Text.Trim() != string.Empty)
        {
            List<string> itemCode = TextBoxProductItem.Text.Trim().Split('~').ToList<string>();
            List<string> sellPrice = TextBoxProductItem.Text.Trim().Split('|').ToList<string>();

            var ItemStock = ProductItems.FirstOrDefault(t => t.ProductItem.Code == itemCode[0] && t.SellingPrice.Value.ToString("F") == sellPrice[1]);
            if (ItemStock != null)
            {
                TextBoxSellingPrice.Text = ItemStock.SellingPrice.Value.ToString("F");
                LabelAvailableQty.Text = ItemStock.CurrentQuantity.Value.ToString("F");
                TextBoxQuantity.Focus();
            }
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        var ItemStocks = HttpContext.Current.Session[targetProducts] as IList<SolutionObjects.ItemStock>;
        LabelItemError.Visible = false;

        int? ItemStockID = null;
        if (TextBoxProductItem.Text.Trim() != string.Empty)
        {
            List<string> itemCode = TextBoxProductItem.Text.Trim().Split('~').ToList<string>();
            List<string> sellPrice = TextBoxProductItem.Text.Trim().Split('|').ToList<string>();

            var ItemStock = ItemStocks.FirstOrDefault(t => t.ProductItem.Code == itemCode[0] && t.SellingPrice.Value.ToString("F") == sellPrice[1]);
            if (ItemStock == null)
            {
                LabelItemError.Text = "Invalid Product Item";
                LabelItemError.Visible = true;
                return;
            }
            else
                ItemStockID = ItemStock.ID;
        }

        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;
        foreach (var item in JobCard.ItemList)
        {
            if (item.ItemStock.ID == ItemStockID)
            {
                LabelItemError.Text = "Product Item already entered";
                LabelItemError.Visible = true;
                return;
            }
        }

        var JobCardItem = new SolutionObjects.JobCardItem();
        JobCardItem.JobCard = JobCard;
        JobCardItem.ItemStock = new SolutionObjects.ItemStock(ItemStockID);
        JobCardItem.Quantity = Util.ParseDecimal(TextBoxQuantity.Text.Trim());
        JobCardItem.UnitPrice = Util.ParseDecimal(TextBoxSellingPrice.Text.Trim());
        JobCardItem.GrossAmount = Util.ParseDecimal(TextBoxGrossAmount.Text.Trim());
        JobCardItem.DiscountPercentage = Util.ParseDecimal(TextBoxDiscountPercentage.Text.Trim());
        JobCardItem.DiscountAmount = Util.ParseDecimal(TextBoxDiscount.Text.Trim());
        JobCardItem.TaxAmount = Util.ParseDecimal(TextBoxTax.Text.Trim());
        JobCardItem.NetAmount = Util.ParseDecimal(TextBoxNetAmount.Text.Trim());
        JobCardItem.User = User.Identity.Name;
        JobCard.ItemList.Add(JobCardItem);

        HttpContext.Current.Session[target] = JobCard;
        GridViewItemAdd.DataSource = JobCard.ItemList;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        ClearItem();
        UpdatePanelSummary.Update();
        UpdatePanelPayment.Update();
    }
    protected void ButtonItemClear_Click(object sender, EventArgs e)
    {
        ClearItem();
    }

    protected void GridViewServiceAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;
        JobCard.ServiceList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        HttpContext.Current.Session[target] = JobCard;
        GridViewServiceAdd.DataSource = JobCard.ServiceList;
        GridViewServiceAdd.DataBind();
        DisplayTotal();
        UpdatePanelSummary.Update();
        UpdatePanelPayment.Update();
    }
    protected void GridViewServiceAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var Service = e.Row.DataItem as SolutionObjects.JobCardService;
            Service.Service = new OperationObjects.ServiceOP().GetService(Service.Service.ID.Value);

            Util.EncodeString(e.Row.Cells[1], Service.Service.Description, 50);
            Util.EncodeString(e.Row.Cells[2], Service.GrossAmount.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[3], Service.DiscountPercentage != null ? Service.DiscountPercentage.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[4], Service.DiscountAmount != null ? Service.DiscountAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], Service.TaxAmount != null ? Service.TaxAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], Service.NetAmount.Value.ToString("F"), 20);
        }
    }
    protected void ButtonServiceAdd_Click(object sender, EventArgs e)
    {
        var Services = HttpContext.Current.Session[targetServices] as IList<SolutionObjects.Service>;
        LabelServiceError.Visible = false;

        int? ServicesID = null;
        if (TextBoxService.Text.Trim() != string.Empty)
        {
            var Service = Services.FirstOrDefault(t => t.Description == TextBoxService.Text.Trim());
            if (Service == null)
            {
                LabelServiceError.Text = "Invalid Service";
                LabelServiceError.Visible = true;
                return;
            }
            else
                ServicesID = Service.ID;
        }

        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;
        foreach (var item in JobCard.ServiceList)
        {
            if (item.Service.ID == ServicesID)
            {
                LabelServiceError.Text = "Service already entered";
                LabelServiceError.Visible = true;
                return;
            }
        }

        var JobCardService = new SolutionObjects.JobCardService();
        JobCardService.JobCard = JobCard;
        JobCardService.Service = new SolutionObjects.Service(ServicesID);
        JobCardService.GrossAmount = Util.ParseDecimal(TextBoxServiceAmount.Text.Trim());
        JobCardService.DiscountPercentage = Util.ParseDecimal(TextBoxServiceDiscountPercentage.Text.Trim());
        JobCardService.DiscountAmount = Util.ParseDecimal(TextBoxServiceDiscount.Text.Trim());
        JobCardService.TaxAmount = Util.ParseDecimal(TextBoxServiceTax.Text.Trim());
        JobCardService.NetAmount = Util.ParseDecimal(TextBoxServiceNetAmount.Text.Trim());
        JobCardService.User = User.Identity.Name;
        JobCard.ServiceList.Add(JobCardService);

        HttpContext.Current.Session[target] = JobCard;
        GridViewServiceAdd.DataSource = JobCard.ServiceList;
        GridViewServiceAdd.DataBind();
        DisplayTotal();
        ClearService();
        UpdatePanelSummary.Update();
        UpdatePanelPayment.Update();
    }
    protected void ButtonServiceClear_Click(object sender, EventArgs e)
    {
        ClearService();
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;
        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;
        var Supervisors = HttpContext.Current.Session[targetSupervisors] as IList<SolutionObjects.Supervisor>;

        int? CustomerID = null;
        if (TextBoxCustomer.Text.Trim() != string.Empty)
        {
            var Customer = Customers.FirstOrDefault(t => t.Name == TextBoxCustomer.Text);
            if (Customer == null)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = "Invalid Customer";
                return;
            }
            else
                CustomerID = Customer.ID;
        }

        int? SupervisorID = null;
        if (TextBoxSupervisor.Text.Trim() != string.Empty)
        {
            var Supervisor = Supervisors.FirstOrDefault(t => t.Name == TextBoxSupervisor.Text);
            if (Supervisor == null)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = "Invalid Supervisor";
                return;
            }
            else
                SupervisorID = Supervisor.ID;
        }

        if (JobCard.ItemList.Count == 0 && JobCard.ServiceList.Count == 0)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = "At least one item or service needs to be entered";
            return;
        }

        JobCard.JobNumber = TextBoxJobNumber.Text.Trim();
        JobCard.VehicleNumber = TextBoxVehicleNumber.Text.Trim();
        JobCard.Customer = new SolutionObjects.Customer(CustomerID);
        JobCard.Address = TextBoxAddress.Text.Trim();
        JobCard.TelephoneNumber = TextBoxTelephone.Text.Trim();
        JobCard.StartDate = Util.ParseDate(TextBoxStartDate.Text.Trim());
        JobCard.EndDate = Util.ParseDate(TextBoxEndDate.Text.Trim());
        JobCard.Supervisor = new SolutionObjects.Supervisor(SupervisorID);
        JobCard.TotalGross = Util.ParseDecimal(TextBoxTotalGross.Text.Trim());
        JobCard.TotalDiscount = Util.ParseDecimal(TextBoxTotalDiscount.Text.Trim());
        JobCard.TotalTax = Util.ParseDecimal(TextBoxTotalTax.Text.Trim());
        JobCard.TotalNet = Util.ParseDecimal(TextBoxTotalNet.Text.Trim());
        JobCard.Remarks = TextBoxRemarks.Text.Trim();
        JobCard.IsApproved = true;
        JobCard.DateApproved = DateTime.Now;
        JobCard.ApprovedBy = User.Identity.Name;
        JobCard.User = User.Identity.Name;

        SolutionObjects.CustomerPayment Payment = null;
        if (TextBoxPaidAmount.Text.Trim() != string.Empty)
        {
            if (Util.ParseDecimal(TextBoxPaidAmount.Text.Trim()) != 0)
            {
                Payment = new SolutionObjects.CustomerPayment();
                Payment.Customer = JobCard.Customer;
                Payment.DebitCreditType = SolutionObjects.DebitCreditTypes.Cr;
                Payment.Amount = Util.ParseDecimal(TextBoxPaidAmount.Text.Trim());
                Payment.JobCardID = JobCard.ID.Value;
                Payment.IsCash = DropDownListPaymentType.SelectedIndex == 0;
                Payment.CardNumber = TextBoxCardNumber.Text.Trim();
                Payment.CardName = TextBoxCardName.Text.Trim();
                Payment.CardType = TextBoxCardType.Text.Trim();
                Payment.User = User.Identity.Name;
            }
        }

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.JobCardOP().Approve(JobCard, Payment);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record approved successfully";
                ClearAll();

                //JobCard = new SolutionObjects.JobCard();
                //JobCard.ItemList = new CommonObjects.PropertyList<SolutionObjects.JobCardItem>();
                //JobCard.ServiceList = new CommonObjects.PropertyList<SolutionObjects.JobCardService>();
                //HttpContext.Current.Session[target] = JobCard;

                PanelAddNew.Visible = false;
                PanelSearchDetails.Visible = true;
                PanelView.Visible = false;
                LabelPageOperation.Text = "Search";
                //LinkButtonAddNewOperation.Visible = true;
                LinkButtonSearchOperation.Visible = false;
                Search();
                HtmlGenericControl dispval = new HtmlGenericControl();
                dispval.InnerHtml = "<script language='javascript'> print('" + JobCard.ID.Value + "');</script>";
                Page.Controls.Add(dispval);
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        JobCard = HttpContext.Current.Session[target] as SolutionObjects.JobCard;

        if (JobCard.ID != null)
        {
            FillForEdit(JobCard.ID.Value);
        }
        else
        {
            JobCard = new SolutionObjects.JobCard();
            JobCard.ItemList = new CommonObjects.PropertyList<SolutionObjects.JobCardItem>();
            JobCard.ServiceList = new CommonObjects.PropertyList<SolutionObjects.JobCardService>();
            HttpContext.Current.Session[target] = JobCard;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("ApproveRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            ClearAll();
            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Approve";
            //LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            FillForEdit(_targetid);
        }

        //if (e.CommandName.Equals("DeleteRecord"))
        //{
        //    int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
        //    JobCard = new OperationObjects.JobCardOP().Get(_targetid);
        //    JobCard.User = User.Identity.Name;

        //    int _listCount = JobCard.ItemList.Count - 1;
        //    while (_listCount >= 0)
        //    {
        //        JobCard.ItemList.RemoveAt(_listCount);
        //        _listCount--;
        //    }

        //    _listCount = JobCard.ServiceList.Count - 1;
        //    while (_listCount >= 0)
        //    {
        //        JobCard.ServiceList.RemoveAt(_listCount);
        //        _listCount--;
        //    }

        //    try
        //    {
        //        CommonObjects.DataTransferObject _dtc = new OperationObjects.JobCardOP().Delete(JobCard);
        //        if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
        //            Search();
        //        else
        //            return;
        //    }
        //    catch (Exception)
        //    {
        //        return;
        //    }
        //}

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            //LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            FillForView(_targetid);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            JobCard = e.Row.DataItem as SolutionObjects.JobCard;
            if (JobCard.Customer != null)
                JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);

            Util.EncodeString(e.Row.Cells[2], JobCard.JobNumber, 20);
            Util.EncodeString(e.Row.Cells[3], JobCard.VehicleNumber, 20);
            Util.EncodeString(e.Row.Cells[4], JobCard.Customer != null ? JobCard.Customer.Name : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[5], JobCard.StartDate != null ? JobCard.StartDate.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], JobCard.EndDate != null ? JobCard.EndDate.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[7], JobCard.TotalNet != null ? JobCard.TotalNet.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[8], JobCard.IsApproved ? "Yes" : "No", 5);

            if (JobCard.IsApproved)
            {
                e.Row.Cells[0].Enabled = false;
            }
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var Item = e.Row.DataItem as SolutionObjects.JobCardItem;
            Item.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(Item.ItemStock.ID.Value);
            Item.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(Item.ItemStock.ProductItem.ID.Value);

            Util.EncodeString(e.Row.Cells[0], Item.ItemStock.ProductItem.Description, 50);
            Util.EncodeString(e.Row.Cells[1], Item.Quantity.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[2], Item.UnitPrice.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[3], Item.GrossAmount.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[4], Item.DiscountPercentage != null ? Item.DiscountPercentage.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], Item.DiscountAmount != null ? Item.DiscountAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], Item.TaxAmount != null ? Item.TaxAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[7], Item.NetAmount.Value.ToString("F"), 20);
        }
    }
    protected void GridViewServiceView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var Service = e.Row.DataItem as SolutionObjects.JobCardService;
            Service.Service = new OperationObjects.ServiceOP().GetService(Service.Service.ID.Value);

            Util.EncodeString(e.Row.Cells[0], Service.Service.Description, 50);
            Util.EncodeString(e.Row.Cells[1], Service.GrossAmount.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[2], Service.DiscountPercentage != null ? Service.DiscountPercentage.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[3], Service.DiscountAmount != null ? Service.DiscountAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[4], Service.TaxAmount != null ? Service.TaxAmount.Value.ToString("F") : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], Service.NetAmount.Value.ToString("F"), 20);
        }
    }
    protected void GridViewPaymentView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var Payment = e.Row.DataItem as SolutionObjects.CustomerPayment;

            Util.EncodeString(e.Row.Cells[0], Payment.IsCash ? "Cash" : "Card", 50);
            Util.EncodeString(e.Row.Cells[1], Payment.Amount.Value.ToString("F"), 20);
            Util.EncodeString(e.Row.Cells[2], Payment.CardNumber, 50);
            Util.EncodeString(e.Row.Cells[3], Payment.CardName, 50);
            Util.EncodeString(e.Row.Cells[4], Payment.CardType, 50);
        }
    }

    protected void DropDownListPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        TRCardNumber.Visible = false;
        TRCardName.Visible = false;
        TRCardType.Visible = false;

        if (DropDownListPaymentType.SelectedIndex == 1)
        {
            TRCardNumber.Visible = true;
            TRCardName.Visible = true;
            TRCardType.Visible = true;
        }
    }
}