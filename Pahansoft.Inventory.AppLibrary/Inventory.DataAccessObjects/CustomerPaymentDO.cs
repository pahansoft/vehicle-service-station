﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CustomerPaymentDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.CustomerPayment customerPayment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateCustomerPayment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.Customer != null ? customerPayment.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DebitCreditType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.DebitCreditType.ToString());
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Amount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, customerPayment.Amount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.JobCardID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsCash", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, customerPayment.IsCash);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardName", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardName);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardType);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customerPayment;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                customerPayment.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.CustomerPayment customerPayment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditCustomerPayment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, customerPayment.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.Customer != null ? customerPayment.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DebitCreditType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.DebitCreditType.ToString());
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Amount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, customerPayment.Amount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.JobCardID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsCash", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, customerPayment.IsCash);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardName", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardName);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.CardType);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customerPayment;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CustomerPayment customerPayment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteCustomerPayment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customerPayment.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, customerPayment.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customerPayment.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customerPayment;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                customerPayment.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.CustomerPayment Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetCustomerPayment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CustomerPayment customerPayment = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                        int DebitCreditTypeOrdinal = dr.GetOrdinal("DebitCreditType");
                        int AmountOrdinal = dr.GetOrdinal("Amount");
                        int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                        int IsCashOrdinal = dr.GetOrdinal("IsCash");
                        int CardNumberOrdinal = dr.GetOrdinal("CardNumber");
                        int CardNameOrdinal = dr.GetOrdinal("CardName");
                        int CardTypeOrdinal = dr.GetOrdinal("CardType");

                        customerPayment = new Inventory.SolutionObjects.CustomerPayment(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) customerPayment.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(CustomerIDOrdinal)) customerPayment.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(DebitCreditTypeOrdinal)) customerPayment.DebitCreditType = (SolutionObjects.DebitCreditTypes) Enum.Parse(typeof(SolutionObjects.DebitCreditTypes),dr.GetString(DebitCreditTypeOrdinal));
                        if (!dr.IsDBNull(AmountOrdinal)) customerPayment.Amount = dr.GetDecimal(AmountOrdinal);
                        if (!dr.IsDBNull(JobCardIDOrdinal)) customerPayment.JobCardID = dr.GetInt32(JobCardIDOrdinal);
                        if (!dr.IsDBNull(IsCashOrdinal)) customerPayment.IsCash = dr.GetBoolean(IsCashOrdinal);
                        if (!dr.IsDBNull(CardNumberOrdinal)) customerPayment.CardNumber = dr.GetString(CardNumberOrdinal);
                        if (!dr.IsDBNull(CardNameOrdinal)) customerPayment.CardName = dr.GetString(CardNameOrdinal);
                        if (!dr.IsDBNull(CardTypeOrdinal)) customerPayment.CardType = dr.GetString(CardTypeOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customerPayment;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCustomerPayments";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> customerPaymentList = new System.Collections.Generic.List<Inventory.SolutionObjects.CustomerPayment>();
            Inventory.SolutionObjects.CustomerPayment customerPayment;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int DebitCreditTypeOrdinal = dr.GetOrdinal("DebitCreditType");
                    int AmountOrdinal = dr.GetOrdinal("Amount");
                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int IsCashOrdinal = dr.GetOrdinal("IsCash");
                    int CardNumberOrdinal = dr.GetOrdinal("CardNumber");
                    int CardNameOrdinal = dr.GetOrdinal("CardName");
                    int CardTypeOrdinal = dr.GetOrdinal("CardType");

                    while (dr.Read())
                    {
                        customerPayment = new Inventory.SolutionObjects.CustomerPayment(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) customerPayment.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(CustomerIDOrdinal)) customerPayment.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(DebitCreditTypeOrdinal)) customerPayment.DebitCreditType = (SolutionObjects.DebitCreditTypes)Enum.Parse(typeof(SolutionObjects.DebitCreditTypes), dr.GetString(DebitCreditTypeOrdinal));
                        if (!dr.IsDBNull(AmountOrdinal)) customerPayment.Amount = dr.GetDecimal(AmountOrdinal);
                        if (!dr.IsDBNull(JobCardIDOrdinal)) customerPayment.JobCardID = dr.GetInt32(JobCardIDOrdinal);
                        if (!dr.IsDBNull(IsCashOrdinal)) customerPayment.IsCash = dr.GetBoolean(IsCashOrdinal);
                        if (!dr.IsDBNull(CardNumberOrdinal)) customerPayment.CardNumber = dr.GetString(CardNumberOrdinal);
                        if (!dr.IsDBNull(CardNameOrdinal)) customerPayment.CardName = dr.GetString(CardNameOrdinal);
                        if (!dr.IsDBNull(CardTypeOrdinal)) customerPayment.CardType = dr.GetString(CardTypeOrdinal);

                        customerPaymentList.Add(customerPayment);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customerPaymentList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> Search(int? CustomerID, SolutionObjects.DebitCreditTypes? DebitCreditType, int? JobCardID)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllCustomerPayments";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, CustomerID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DebitCreditType", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, DebitCreditType != null ? DebitCreditType.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> customerPaymentList = new System.Collections.Generic.List<Inventory.SolutionObjects.CustomerPayment>();
            Inventory.SolutionObjects.CustomerPayment customerPayment;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int DebitCreditTypeOrdinal = dr.GetOrdinal("DebitCreditType");
                    int AmountOrdinal = dr.GetOrdinal("Amount");
                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int IsCashOrdinal = dr.GetOrdinal("IsCash");
                    int CardNumberOrdinal = dr.GetOrdinal("CardNumber");
                    int CardNameOrdinal = dr.GetOrdinal("CardName");
                    int CardTypeOrdinal = dr.GetOrdinal("CardType");

                    while (dr.Read())
                    {
                        customerPayment = new Inventory.SolutionObjects.CustomerPayment(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) customerPayment.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(CustomerIDOrdinal)) customerPayment.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                        if (!dr.IsDBNull(DebitCreditTypeOrdinal)) customerPayment.DebitCreditType = (SolutionObjects.DebitCreditTypes)Enum.Parse(typeof(SolutionObjects.DebitCreditTypes), dr.GetString(DebitCreditTypeOrdinal));
                        if (!dr.IsDBNull(AmountOrdinal)) customerPayment.Amount = dr.GetDecimal(AmountOrdinal);
                        if (!dr.IsDBNull(JobCardIDOrdinal)) customerPayment.JobCardID = dr.GetInt32(JobCardIDOrdinal);
                        if (!dr.IsDBNull(IsCashOrdinal)) customerPayment.IsCash = dr.GetBoolean(IsCashOrdinal);
                        if (!dr.IsDBNull(CardNumberOrdinal)) customerPayment.CardNumber = dr.GetString(CardNumberOrdinal);
                        if (!dr.IsDBNull(CardNameOrdinal)) customerPayment.CardName = dr.GetString(CardNameOrdinal);
                        if (!dr.IsDBNull(CardTypeOrdinal)) customerPayment.CardType = dr.GetString(CardTypeOrdinal);

                        customerPaymentList.Add(customerPayment);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customerPaymentList;
        }

        #endregion
    }
}
