﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_purc_order_repo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int? OrderID = Util.ParseInt(Request.Params["OrderID"].ToString());

        Generate(OrderID.Value);
    }

    private void Generate(int OrderID)
    {
        var PO = new OperationObjects.PurchaseOrderOP().Get(OrderID);
        if (PO == null)
            return;
        if (PO.Supplier != null)
            PO.Supplier = new OperationObjects.SupplierOP().GetSupplier(PO.Supplier.ID.Value);

        var _sb = new StringBuilder();

        _sb.Append("<div style=\"page-break-after: always;\">");
        _sb.Append("<table style=\"width: 100%; font-family:Courier New;\">");

        #region - Page Header -

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"text-align: center; font-size:large; font-weight:bold;\">IMANTHA INTERNATIONALS (PVT) LTD - PURCHASE ORDER</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");
        _sb.Append("<table style=\"width: 100%; font-size:small;\">");
        _sb.Append("<tr>");
        _sb.Append("<td style=\"width : 10%; font-weight:bold;\">PO NUMBER</td>");
        _sb.Append("<td style=\"width : 20%;\">: " + PO.PONumber + "</td>");
        _sb.Append("<td style=\"width : 10%; font-weight:bold;\">PO DATE</td>");
        _sb.Append("<td style=\"width : 20%;\">: " + PO.DatePO.Value.ToShortDateString() + "</td>");
        _sb.Append("<td style=\"width : 10%; font-weight:bold;\">SUPPLIER</td>");
        _sb.Append("<td style=\"width : 30%;\">: " + (PO.Supplier != null ? PO.Supplier.Name : "-") + "</td>");
        _sb.Append("</tr>");
        _sb.Append("</table>");
        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td>&nbsp;</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #endregion

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");
        _sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 80%;\" cellpadding=\"5\">");
        _sb.Append("<tr style=\"font-size:small; font-weight:bold; height:30px; background-color: #F5F5F5\">");
        _sb.Append("<td style=\"width : 55%; \">PRODUCT ITEM</td>");
        _sb.Append("<td style=\"width : 15%; text-align:right;\">UNIT PRICE</td>");
        _sb.Append("<td style=\"width : 15%; text-align:right;\">QUANTITY</td>");
        _sb.Append("<td style=\"width : 15%; text-align:right;\">AMOUNT</td>");
        _sb.Append("</tr>");

        foreach (var item in PO.PurchaseOrderDetails)
        {
            item.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(item.ProductItem.ID.Value);

            var sbDescription = new StringBuilder();
            sbDescription.Append(item.ProductItem.Code);
            sbDescription.Append(item.ProductItem.ReferenceNumber != string.Empty ? ("-" + item.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(item.ProductItem.Description);

            _sb.Append("<tr style=\"font-size:small;\">");
            _sb.Append("<td>" + sbDescription.ToString() + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + item.UnitPrice.Value.ToString() + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + item.Quantity.Value.ToString() + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + (item.LineTotal != null ? item.LineTotal.Value.ToString() : "-") + "</td>");
            _sb.Append("</tr>");
        }
        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td colspan=\"3\" style=\"text-align:right;\">TOTAL</td>");
        _sb.Append("<td style=\"text-align:right;\">" + PO.TotalAmount.Value.ToString() + "</td>");
        _sb.Append("</tr>");

        _sb.Append("</table>");
        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">&nbsp;</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #region - Footer -

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");
        _sb.Append("<table style=\"width: 100%;\">");

        _sb.Append("<tr style=\"font-size:small;\">");
        _sb.Append("<td style=\"width : 50%\">" + ("Page [" + 1 + "] of [" + 1 + "]") + "</td>");
        _sb.Append("<td style=\"width : 50%; text-align: right;\">" + (DateTime.Today.ToShortDateString() + "&nbsp;&nbsp;" + DateTime.Now.ToShortTimeString()) + "</td>");
        _sb.Append("</tr>");

        _sb.Append("</table>");
        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #endregion

        _sb.Append("</table>");
        _sb.Append("</div>");

        LabelReportBody.Text = _sb.ToString();
    }
}