﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ProductItemOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveProductItem(Inventory.SolutionObjects.ProductItem productItem)
        {
            if (productItem.ID == null)
                return new Inventory.DataAccessObjects.ProductItemDO().Create(productItem);
            else
                return new Inventory.DataAccessObjects.ProductItemDO().Edit(productItem);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteProductItem(Inventory.SolutionObjects.ProductItem productItem)
        {
            if (productItem.ID != null)
                return new Inventory.DataAccessObjects.ProductItemDO().Delete(productItem);
            else
            {
                productItem.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = productItem;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.ProductItem GetProductItem(int id)
        {
            return new Inventory.DataAccessObjects.ProductItemDO().Find(id);
        }

        public Inventory.SolutionObjects.ProductItem GetProductItemProxy(int id)
        {
            return new Inventory.DataAccessObjects.ProductItemDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> GetAllProductItems()
        {
            return new Inventory.DataAccessObjects.ProductItemDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> SearchProductItems(string code, string description, string referenceNumber, SolutionObjects.ProductType productType, bool? BrandNewItemsOnly)
        {
            return new Inventory.DataAccessObjects.ProductItemDO().Search(code, description, referenceNumber, productType, BrandNewItemsOnly);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> SearchProductItemsForReport(string code, string description, string referenceNumber, int? productType, bool? BrandNewItemsOnly)
        {
            return new Inventory.DataAccessObjects.ProductItemDO().SearchForReport(code, description, referenceNumber, productType, BrandNewItemsOnly);
        }


        public Inventory.SolutionObjects.ProductItem GetproductCode()
        {
            return new Inventory.DataAccessObjects.ProductItemDO().AutoNum();
        }
    }
}
