﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class GRNDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.GRN grn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.GRNNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateGRN", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DateGRN);
            //Pahansoft.CommonObjects.DataHandler.AddSPParams("@PONumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.PONumber);
            //Pahansoft.CommonObjects.DataHandler.AddSPParams("@DatePO", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DatePO);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, grn.Supplier != null ? grn.Supplier.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalGrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalNetAmount);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsDirectGRN", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, grn.IsDirectGRN);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PaidAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.PaidAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BalanceAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.BalanceAmount);
            
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsApproved", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, grn.IsApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedBy", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.ApprovedBy);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = grn;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                grn.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.GRN grn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, grn.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, grn.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.GRNNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateGRN", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DateGRN);
            //Pahansoft.CommonObjects.DataHandler.AddSPParams("@PONumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.PONumber);
            //Pahansoft.CommonObjects.DataHandler.AddSPParams("@DatePO", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DatePO);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, grn.Supplier != null ? grn.Supplier.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalGrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.TotalNetAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsDirectGRN", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, grn.IsDirectGRN);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PaidAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.PaidAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BalanceAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, grn.BalanceAmount);
       
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsApproved", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, grn.IsApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, grn.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedBy", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.ApprovedBy);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = grn;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.GRN grn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, grn.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, grn.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, grn.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = grn;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                grn.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.GRN Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.GRN grn = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int gRNNumberOrdinal = dr.GetOrdinal("GRNNumber");
                        int dateGRNOrdinal = dr.GetOrdinal("DateGRN");
                        //int pONumberOrdinal = dr.GetOrdinal("PONumber");
                        //int datePOOrdinal = dr.GetOrdinal("DatePO");
                        int supplierIDOrdinal = dr.GetOrdinal("SupplierID");
                        int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int totalGrossAmountOrdinal = dr.GetOrdinal("TotalGrossAmount");
                        int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");

                        int IsDirectGRNOrdinal = dr.GetOrdinal("IsDirectGRN");
                        int PaidAmountOrdinal = dr.GetOrdinal("PaidAmount");
                        int BalanceAmountOrdinal = dr.GetOrdinal("BalanceAmount");

                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                        int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");

                        grn = new Inventory.SolutionObjects.GRN(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) grn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNNumberOrdinal)) grn.GRNNumber = dr.GetString(gRNNumberOrdinal);
                        if (!dr.IsDBNull(dateGRNOrdinal)) grn.DateGRN = dr.GetDateTime(dateGRNOrdinal);
                        if (!dr.IsDBNull(supplierIDOrdinal)) grn.Supplier = new SolutionObjects.Supplier(dr.GetInt32(supplierIDOrdinal));
                        if (!dr.IsDBNull(totalDiscountOrdinal)) grn.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) grn.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossAmountOrdinal)) grn.TotalGrossAmount = dr.GetDecimal(totalGrossAmountOrdinal);
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) grn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);

                        if (!dr.IsDBNull(IsDirectGRNOrdinal)) grn.IsDirectGRN = dr.GetBoolean(IsDirectGRNOrdinal);
                        if (!dr.IsDBNull(PaidAmountOrdinal)) grn.PaidAmount = dr.GetDecimal(PaidAmountOrdinal);
                        if (!dr.IsDBNull(BalanceAmountOrdinal)) grn.BalanceAmount = dr.GetDecimal(BalanceAmountOrdinal);

                        if (!dr.IsDBNull(remarksOrdinal)) grn.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) grn.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) grn.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) grn.ApprovedBy = dr.GetString(ApprovedByOrdinal);

                        grn.GRNDetailList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
                        foreach (SolutionObjects.GRNDetail gRNDetail in new DataAccessObjects.GRNDetailDO().GetAllFor(grn))
                            grn.GRNDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return grn;
        }

        public Inventory.SolutionObjects.GRN FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetGRN";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.GRN grn = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int gRNNumberOrdinal = dr.GetOrdinal("GRNNumber");
                        int dateGRNOrdinal = dr.GetOrdinal("DateGRN");
                        //int pONumberOrdinal = dr.GetOrdinal("PONumber");
                        //int datePOOrdinal = dr.GetOrdinal("DatePO");
                        int supplierIDOrdinal = dr.GetOrdinal("SupplierID");
                        int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int totalGrossAmountOrdinal = dr.GetOrdinal("TotalGrossAmount");
                        int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");

                        int IsDirectGRNOrdinal = dr.GetOrdinal("IsDirectGRN");
                        int PaidAmountOrdinal = dr.GetOrdinal("PaidAmount");
                        int BalanceAmountOrdinal = dr.GetOrdinal("BalanceAmount");

                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                        int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");

                        grn = new Inventory.SolutionObjects.GRN(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) grn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNNumberOrdinal)) grn.GRNNumber = dr.GetString(gRNNumberOrdinal);
                        if (!dr.IsDBNull(dateGRNOrdinal)) grn.DateGRN = dr.GetDateTime(dateGRNOrdinal);
                        //if (!dr.IsDBNull(pONumberOrdinal)) grn.PONumber = dr.GetString(pONumberOrdinal);
                        //if (!dr.IsDBNull(datePOOrdinal)) grn.DatePO = dr.GetDateTime(datePOOrdinal);
                        if (!dr.IsDBNull(supplierIDOrdinal)) grn.Supplier = new SolutionObjects.Supplier(dr.GetInt32(supplierIDOrdinal));
                        if (!dr.IsDBNull(totalDiscountOrdinal)) grn.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) grn.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossAmountOrdinal)) grn.TotalGrossAmount = dr.GetDecimal(totalGrossAmountOrdinal);
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) grn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);

                        if (!dr.IsDBNull(IsDirectGRNOrdinal)) grn.IsDirectGRN = dr.GetBoolean(IsDirectGRNOrdinal);
                        if (!dr.IsDBNull(PaidAmountOrdinal)) grn.PaidAmount = dr.GetDecimal(PaidAmountOrdinal);
                        if (!dr.IsDBNull(BalanceAmountOrdinal)) grn.BalanceAmount = dr.GetDecimal(BalanceAmountOrdinal);

                        if (!dr.IsDBNull(remarksOrdinal)) grn.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) grn.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) grn.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) grn.ApprovedBy = dr.GetString(ApprovedByOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return grn;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllGRNs";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> grnList = new System.Collections.Generic.List<Inventory.SolutionObjects.GRN>();
            Inventory.SolutionObjects.GRN grn;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int gRNNumberOrdinal = dr.GetOrdinal("GRNNumber");
                    int dateGRNOrdinal = dr.GetOrdinal("DateGRN");
                    //int pONumberOrdinal = dr.GetOrdinal("PONumber");
                    //int datePOOrdinal = dr.GetOrdinal("DatePO");
                    int supplierIDOrdinal = dr.GetOrdinal("SupplierID");
                    int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int totalGrossAmountOrdinal = dr.GetOrdinal("TotalGrossAmount");
                    int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");

                    int IsDirectGRNOrdinal = dr.GetOrdinal("IsDirectGRN");
                    int PaidAmountOrdinal = dr.GetOrdinal("PaidAmount");
                    int BalanceAmountOrdinal = dr.GetOrdinal("BalanceAmount");

                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                    int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");

                    while (dr.Read())
                    {
                        grn = new Inventory.SolutionObjects.GRN(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) grn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNNumberOrdinal)) grn.GRNNumber = dr.GetString(gRNNumberOrdinal);
                        if (!dr.IsDBNull(dateGRNOrdinal)) grn.DateGRN = dr.GetDateTime(dateGRNOrdinal);
                        //if (!dr.IsDBNull(pONumberOrdinal)) grn.PONumber = dr.GetString(pONumberOrdinal);
                        //if (!dr.IsDBNull(datePOOrdinal)) grn.DatePO = dr.GetDateTime(datePOOrdinal);
                        if (!dr.IsDBNull(supplierIDOrdinal)) grn.Supplier = new SolutionObjects.Supplier(dr.GetInt32(supplierIDOrdinal));
                        if (!dr.IsDBNull(totalDiscountOrdinal)) grn.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) grn.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossAmountOrdinal)) grn.TotalGrossAmount = dr.GetDecimal(totalGrossAmountOrdinal);
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) grn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);

                        if (!dr.IsDBNull(IsDirectGRNOrdinal)) grn.IsDirectGRN = dr.GetBoolean(IsDirectGRNOrdinal);
                        if (!dr.IsDBNull(PaidAmountOrdinal)) grn.PaidAmount = dr.GetDecimal(PaidAmountOrdinal);
                        if (!dr.IsDBNull(BalanceAmountOrdinal)) grn.BalanceAmount = dr.GetDecimal(BalanceAmountOrdinal);

                        if (!dr.IsDBNull(remarksOrdinal)) grn.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) grn.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) grn.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) grn.ApprovedBy = dr.GetString(ApprovedByOrdinal);

                        grnList.Add(grn);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return grnList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> Search(string gRNNumber, DateTime? dateGRN, SolutionObjects.Supplier supplier, bool? approveOnly)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllGRNs";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNNumber != string.Empty ? gRNNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateGRN", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, dateGRN != null ? dateGRN : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, supplier != null ? supplier.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApproveOnly", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, approveOnly);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> grnList = new System.Collections.Generic.List<Inventory.SolutionObjects.GRN>();
            Inventory.SolutionObjects.GRN grn;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int gRNNumberOrdinal = dr.GetOrdinal("GRNNumber");
                    int dateGRNOrdinal = dr.GetOrdinal("DateGRN");
                    //int pONumberOrdinal = dr.GetOrdinal("PONumber");
                    //int datePOOrdinal = dr.GetOrdinal("DatePO");
                    int supplierIDOrdinal = dr.GetOrdinal("SupplierID");
                    int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int totalGrossAmountOrdinal = dr.GetOrdinal("TotalGrossAmount");
                    int totalNetAmountOrdinal = dr.GetOrdinal("TotalNetAmount");

                    int IsDirectGRNOrdinal = dr.GetOrdinal("IsDirectGRN");
                    int PaidAmountOrdinal = dr.GetOrdinal("PaidAmount");
                    int BalanceAmountOrdinal = dr.GetOrdinal("BalanceAmount");

                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int IsApprovedOrdinal = dr.GetOrdinal("IsApproved");
                    int DateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int ApprovedByOrdinal = dr.GetOrdinal("ApprovedBy");

                    while (dr.Read())
                    {
                        grn = new Inventory.SolutionObjects.GRN(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) grn.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(gRNNumberOrdinal)) grn.GRNNumber = dr.GetString(gRNNumberOrdinal);
                        if (!dr.IsDBNull(dateGRNOrdinal)) grn.DateGRN = dr.GetDateTime(dateGRNOrdinal);
                        //if (!dr.IsDBNull(pONumberOrdinal)) grn.PONumber = dr.GetString(pONumberOrdinal);
                        //if (!dr.IsDBNull(datePOOrdinal)) grn.DatePO = dr.GetDateTime(datePOOrdinal);
                        if (!dr.IsDBNull(supplierIDOrdinal)) grn.Supplier = new SolutionObjects.Supplier(dr.GetInt32(supplierIDOrdinal));
                        if (!dr.IsDBNull(totalDiscountOrdinal)) grn.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) grn.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossAmountOrdinal)) grn.TotalGrossAmount = dr.GetDecimal(totalGrossAmountOrdinal);
                        if (!dr.IsDBNull(totalNetAmountOrdinal)) grn.TotalNetAmount = dr.GetDecimal(totalNetAmountOrdinal);

                        if (!dr.IsDBNull(IsDirectGRNOrdinal)) grn.IsDirectGRN = dr.GetBoolean(IsDirectGRNOrdinal);
                        if (!dr.IsDBNull(PaidAmountOrdinal)) grn.PaidAmount = dr.GetDecimal(PaidAmountOrdinal);
                        if (!dr.IsDBNull(BalanceAmountOrdinal)) grn.BalanceAmount = dr.GetDecimal(BalanceAmountOrdinal);

                        if (!dr.IsDBNull(remarksOrdinal)) grn.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(IsApprovedOrdinal)) grn.IsApproved = dr.GetBoolean(IsApprovedOrdinal);
                        if (!dr.IsDBNull(DateApprovedOrdinal)) grn.DateApproved = dr.GetDateTime(DateApprovedOrdinal);
                        if (!dr.IsDBNull(ApprovedByOrdinal)) grn.ApprovedBy = dr.GetString(ApprovedByOrdinal);

                        grnList.Add(grn);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return grnList;
        }

        #endregion
    }
}
