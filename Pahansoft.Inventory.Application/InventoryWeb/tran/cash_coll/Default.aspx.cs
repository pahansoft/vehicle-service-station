﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.UI.HtmlControls;

#endregion

public partial class tran_cash_coll_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.CashCollectionSummary _cashCollectionSummary;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxCollectionNumber.Text = string.Empty;
        TextBoxDateCollection.Text = DateTime.Today.ToShortDateString();
        DropDownListCashCollector.SelectedIndex = 0;
        TextBoxTotalAmount.Text = string.Empty;
        TextBoxCollectorCommission.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        Util.ClearDropdown(DropDownListSalesIssue);
        LabelDueAmount.Text = "-";
        TextBoxPaidAmount.Text = string.Empty;

        TextBoxFilterCardNumber.Text = string.Empty;
        TextBoxFilterIssueNumber.Text = string.Empty;

        LabelValidateAmount.Visible = false;
        LabelValidateDuplicate.Visible = false;
    }

    private void ClearSearch()
    {
        TextBoxSrchCollectionNumber.Text = string.Empty;
        TextBoxSrchDateCollection.Text = string.Empty;
        DropDownListSrchCashCollector.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListCashCollector);
        Util.ClearDropdown(DropDownListSrchCashCollector);
        foreach (SolutionObjects.CashCollector cashCollector in new OperationObjects.CashCollectorOP().GetAllCashCollectors())
        {
            DropDownListCashCollector.Items.Add(new ListItem(cashCollector.Name, cashCollector.ID.Value.ToString()));
            DropDownListSrchCashCollector.Items.Add(new ListItem(cashCollector.Name, cashCollector.ID.Value.ToString()));
        }
    }

    private void Search()
    {
        DateTime? _dateCollection = Util.ParseDate(TextBoxSrchDateCollection.Text.Trim());
        SolutionObjects.CashCollector _cashCollector = null;
        if (DropDownListSrchCashCollector.SelectedIndex != 0)
            _cashCollector = new SolutionObjects.CashCollector(int.Parse(DropDownListSrchCashCollector.SelectedValue));

        GridViewSearchList.DataSource = new OperationObjects.CashCollectionSummaryOP().SearchCashCollectionSummaries(TextBoxSrchCollectionNumber.Text.Trim(), _dateCollection, _cashCollector);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotals()
    {
        _cashCollectionSummary = ViewState[_target] as SolutionObjects.CashCollectionSummary;

        Decimal _totalAmount = 0;

        foreach (SolutionObjects.CashCollectionDetail cCashCollectionDetail in _cashCollectionSummary.CashCollectionDetailList)
            _totalAmount += cCashCollectionDetail.Amount != null ? cCashCollectionDetail.Amount.Value : 0;

        TextBoxTotalAmount.Text = _totalAmount != 0 ? _totalAmount.ToString("F") : string.Empty;
    }

    private SolutionObjects.CashCollectionSummary CreateCustomerPayment(SolutionObjects.CashCollectionSummary _cashCollectionSummary)
    {
        decimal _currentDue = 0;
        _cashCollectionSummary.CustomerPaymentList = new CommonObjects.PropertyList<SolutionObjects.CustomerPayment>();
        //foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in _cashCollectionSummary.CashCollectionDetailList)
        //{
        //    SolutionObjects.CustomerPayment _customerPayment;
        //    cashCollectionDetail.SalesIssue = new OperationObjects.SalesIssueOP().GetSalesIssueProxy(cashCollectionDetail.SalesIssue.ID.Value);

        //    IList<SolutionObjects.CustomerPayment> _customerPaymentList = new OperationObjects.CustomerPaymentOP().GetAllFor(cashCollectionDetail.SalesIssue);
        //    if (_customerPaymentList.Count > 0)
        //    {
        //        _currentDue = _customerPaymentList[0].DueAmount.Value;

        //        _customerPayment = _customerPaymentList[0];
        //        _customerPayment.DueAmount -= cashCollectionDetail.Amount;
        //    }
        //    else
        //    {
        //        _currentDue = cashCollectionDetail.SalesIssue.TotalNet.Value;

        //        _customerPayment = new SolutionObjects.CustomerPayment();
        //        _customerPayment.SalesIssue = cashCollectionDetail.SalesIssue;
        //        _customerPayment.Customer = cashCollectionDetail.SalesIssue.Customer;
        //        _customerPayment.NetAmount = cashCollectionDetail.SalesIssue.TotalNet;
        //        _customerPayment.DueAmount = cashCollectionDetail.SalesIssue.TotalNet - cashCollectionDetail.Amount;
        //        _customerPayment.CashCollectionSummary = _cashCollectionSummary;
        //    }
        //    _customerPayment.User = User.Identity.Name;

        //    _customerPayment.CustomerPaymentEntryList = new CommonObjects.PropertyList<SolutionObjects.CustomerPaymentEntry>();
        //    SolutionObjects.CustomerPaymentEntry _customerPaymentEntry = new SolutionObjects.CustomerPaymentEntry();
        //    _customerPaymentEntry.CustomerPayment = _customerPayment;
        //    _customerPaymentEntry.DatePaid = _cashCollectionSummary.DateCollected;
        //    _customerPaymentEntry.AmountDue = _currentDue;
        //    _customerPaymentEntry.AmountPaid = cashCollectionDetail.Amount;
        //    _customerPaymentEntry.PaymentType = SolutionObjects.PaymentTypes.CashCollector;
        //    _customerPaymentEntry.CashCollector = _cashCollectionSummary.CashCollector;
        //    _customerPaymentEntry.User = User.Identity.Name;
        //    _customerPayment.CustomerPaymentEntryList.Add(_customerPaymentEntry);

        //    _cashCollectionSummary.CustomerPaymentList.Add(_customerPayment);
        //}

        return _cashCollectionSummary;
    }

    private SolutionObjects.CashCollectionSummary CreateCashCollectorCommission(SolutionObjects.CashCollectionSummary _cashCollectionSummary)
    {
        _cashCollectionSummary.CashCollectorCommissionList = new CommonObjects.PropertyList<SolutionObjects.CashCollectorCommission>();

        if (_cashCollectionSummary.CommissionRate != null && _cashCollectionSummary.CommissionRate != 0)
        {
            SolutionObjects.CashCollectorCommission _cashCollectorCommission = new SolutionObjects.CashCollectorCommission();
            _cashCollectorCommission.CashCollectionSummary = _cashCollectionSummary;
            _cashCollectorCommission.CashCollector = _cashCollectionSummary.CashCollector;
            _cashCollectorCommission.DateCommission = _cashCollectionSummary.DateCollected;
            _cashCollectorCommission.CommissionRate = _cashCollectionSummary.CommissionRate;
            _cashCollectorCommission.CommissionAmount = _cashCollectionSummary.CommissionAmount;
            _cashCollectorCommission.User = User.Identity.Name;
            _cashCollectionSummary.CashCollectorCommissionList.Add(_cashCollectorCommission);
        }

        return _cashCollectionSummary;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _cashCollectionSummary = new SolutionObjects.CashCollectionSummary();
            _cashCollectionSummary.CashCollectionDetailList = new CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail>();

            ViewState[_target] = _cashCollectionSummary;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _cashCollectionSummary = new SolutionObjects.CashCollectionSummary();
        _cashCollectionSummary.CashCollectionDetailList = new CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail>();
        ViewState[_target] = _cashCollectionSummary;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _cashCollectionSummary = ViewState[_target] as SolutionObjects.CashCollectionSummary;
        _cashCollectionSummary.CashCollectionDetailList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _cashCollectionSummary;
        GridViewItemAdd.DataSource = _cashCollectionSummary.CashCollectionDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.CashCollectionDetail _cashCollectionDetail = e.Row.DataItem as SolutionObjects.CashCollectionDetail;
            _cashCollectionDetail.SalesIssue = new OperationObjects.SalesIssueOP().GetSalesIssueProxy(_cashCollectionDetail.SalesIssue.ID.Value);
            _cashCollectionDetail.SalesIssue.Customer = new OperationObjects.CustomerOP().GetCustomer(_cashCollectionDetail.SalesIssue.Customer.ID.Value);

            Util.EncodeString(e.Row.Cells[1], _cashCollectionDetail.SalesIssue != null ? _cashCollectionDetail.SalesIssue.CardNumber : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _cashCollectionDetail.SalesIssue.Customer != null ? _cashCollectionDetail.SalesIssue.Customer.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[3], _cashCollectionDetail.Amount != null ? _cashCollectionDetail.Amount.Value.ToString("F") : string.Empty, 50);
        }
    }
    protected void DropDownListSalesIssue_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LabelDueAmount.Text = "-";
        //if (DropDownListSalesIssue.SelectedIndex != 0)
        //{
        //    IList<SolutionObjects.CustomerPayment> _customerPaymentList = new OperationObjects.CustomerPaymentOP().GetAllFor(new SolutionObjects.SalesIssue(int.Parse(DropDownListSalesIssue.SelectedValue)));
        //    if (_customerPaymentList.Count != 0)
        //        LabelDueAmount.Text = _customerPaymentList[0].DueAmount.Value.ToString();
        //    else
        //    {
        //        SolutionObjects.SalesIssue _salesIssue = new OperationObjects.SalesIssueOP().GetSalesIssueProxy(int.Parse(DropDownListSalesIssue.SelectedValue));
        //        LabelDueAmount.Text = _salesIssue.TotalNet.Value.ToString();
        //    }
        //}
    }
    protected void ButtonSearchFilter_Click(object sender, EventArgs e)
    {
        LabelDueAmount.Text = "-";
        Util.ClearDropdown(DropDownListSalesIssue);
        IList<SolutionObjects.SalesIssue> _list = new OperationObjects.SalesIssueOP().SearchSalesIssues(TextBoxFilterIssueNumber.Text.Trim(), TextBoxFilterCardNumber.Text.Trim(), null, null, null);

        foreach (SolutionObjects.SalesIssue salesIssue in _list)
        {
            salesIssue.Customer = new OperationObjects.CustomerOP().GetCustomer(salesIssue.Customer.ID.Value);
            DropDownListSalesIssue.Items.Add(new ListItem(salesIssue.CardNumber + " - " + salesIssue.Customer.Name, salesIssue.ID.Value.ToString()));
        }

        //if (_list.Count == 1)
        //{
        //    DropDownListSalesIssue.SelectedIndex = 1;
        //    IList<SolutionObjects.CustomerPayment> _customerPaymentList = new OperationObjects.CustomerPaymentOP().GetAllFor(_list[0]);
        //    if (_customerPaymentList.Count != 0)
        //        LabelDueAmount.Text = _customerPaymentList[0].DueAmount.Value.ToString();
        //    else
        //    {
        //        SolutionObjects.SalesIssue _salesIssue = new OperationObjects.SalesIssueOP().GetSalesIssueProxy(int.Parse(DropDownListSalesIssue.SelectedValue));
        //        LabelDueAmount.Text = _salesIssue.TotalNet.Value.ToString();
        //    }
        //}

    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        LabelValidateAmount.Visible = false;
        LabelValidateDuplicate.Visible = false;
        _cashCollectionSummary = ViewState[_target] as SolutionObjects.CashCollectionSummary;

        bool _hasFound = false;
        foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in _cashCollectionSummary.CashCollectionDetailList)
        {
            if (DropDownListSalesIssue.SelectedValue == cashCollectionDetail.SalesIssue.ID.Value.ToString())
            {
                _hasFound = true;
                break;
            }
        }
        if (_hasFound)
        {
            LabelValidateDuplicate.Visible = true;
            return;
        }

        if (Util.ParseDecimal(TextBoxPaidAmount.Text.Trim()) > Util.ParseDecimal(LabelDueAmount.Text))
        {
            LabelValidateAmount.Visible = true;
            return;
        }

        SolutionObjects.CashCollectionDetail _cashCollectionDetail = new SolutionObjects.CashCollectionDetail();
        _cashCollectionDetail.CashCollectionSummary = _cashCollectionSummary;
        if (DropDownListSalesIssue.SelectedIndex != 0)
            _cashCollectionDetail.SalesIssue = new SolutionObjects.SalesIssue(int.Parse(DropDownListSalesIssue.SelectedValue));
        _cashCollectionDetail.Amount = Util.ParseDecimal(TextBoxPaidAmount.Text.Trim());
        _cashCollectionDetail.User = User.Identity.Name;
        _cashCollectionSummary.CashCollectionDetailList.Add(_cashCollectionDetail);

        ViewState[_target] = _cashCollectionSummary;
        GridViewItemAdd.DataSource = _cashCollectionSummary.CashCollectionDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _cashCollectionSummary = ViewState[_target] as SolutionObjects.CashCollectionSummary;

        if (_cashCollectionSummary.ID == null)
            _cashCollectionSummary.CashCollectionNumber = Util.GenereteSequenceNumber("CC", DateTime.Today.Year, DateTime.Today.Month);
        _cashCollectionSummary.DateCollected = Util.ParseDate(TextBoxDateCollection.Text.Trim());
        if (DropDownListCashCollector.SelectedIndex != 0)
            _cashCollectionSummary.CashCollector = new SolutionObjects.CashCollector(int.Parse(DropDownListCashCollector.SelectedValue));
        else
            _cashCollectionSummary.CashCollector = null;
        _cashCollectionSummary.TotalAmount = Util.ParseDecimal(TextBoxTotalAmount.Text.Trim());
        _cashCollectionSummary.CommissionRate = Util.ParseDecimal(TextBoxCollectorCommission.Text.Trim());
        _cashCollectionSummary.CommissionAmount = _cashCollectionSummary.TotalAmount * _cashCollectionSummary.CommissionRate / 100;
        _cashCollectionSummary.User = User.Identity.Name;

        CreateCustomerPayment(_cashCollectionSummary);
        CreateCashCollectorCommission(_cashCollectionSummary);

        try
        {
            bool _isNew = false;
            if (_cashCollectionSummary.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.CashCollectionSummaryOP().SaveCashCollectionSummary(_cashCollectionSummary);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>Collection Number : " + _cashCollectionSummary.CashCollectionNumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                _cashCollectionSummary = new SolutionObjects.CashCollectionSummary();
                _cashCollectionSummary.CashCollectionDetailList = new CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail>();
                ViewState[_target] = _cashCollectionSummary;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _cashCollectionSummary = new SolutionObjects.CashCollectionSummary();
        _cashCollectionSummary.CashCollectionDetailList = new CommonObjects.PropertyList<SolutionObjects.CashCollectionDetail>();
        ViewState[_target] = _cashCollectionSummary;
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _cashCollectionSummary = new OperationObjects.CashCollectionSummaryOP().GetCashCollectionSummary(_targetid);
            _cashCollectionSummary.User = User.Identity.Name;

            int _listCount = _cashCollectionSummary.CashCollectionDetailList.Count - 1;
            while (_listCount >= 0)
            {
                _cashCollectionSummary.CashCollectionDetailList.RemoveAt(_listCount);
                _listCount--;
            }

            _listCount = _cashCollectionSummary.CashCollectorCommissionList.Count - 1;
            while (_listCount >= 0)
            {
                _cashCollectionSummary.CashCollectorCommissionList.RemoveAt(_listCount);
                _listCount--;
            }

            _listCount = _cashCollectionSummary.CustomerPaymentList.Count - 1;
            //while (_listCount >= 0)
            //{
            //    int _innerListCount = _cashCollectionSummary.CustomerPaymentList[_listCount].CustomerPaymentEntryList.Count - 1;
            //    while (_innerListCount >= 0)
            //    {
            //        _cashCollectionSummary.CustomerPaymentList[_listCount].CustomerPaymentEntryList.RemoveAt(_innerListCount);
            //        _innerListCount--;
            //    }

            //    _cashCollectionSummary.CustomerPaymentList.RemoveAt(_listCount);
            //    _listCount--;
            //}

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.CashCollectionSummaryOP().DeleteCashCollectionSummary(_cashCollectionSummary);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _cashCollectionSummary = new OperationObjects.CashCollectionSummaryOP().GetCashCollectionSummary(_targetid);

            ViewState[_target] = _cashCollectionSummary;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewCollectionNumber.Text = Util.FormatEmptyString(_cashCollectionSummary.CashCollectionNumber);
            LabelViewDateCollection.Text = Util.FormatEmptyString(_cashCollectionSummary.DateCollected != null ? _cashCollectionSummary.DateCollected.Value.ToShortDateString() : string.Empty);
            if (_cashCollectionSummary.CashCollector != null)
                LabelViewCashCollector.Text = new OperationObjects.CashCollectorOP().GetCashCollector(_cashCollectionSummary.CashCollector.ID.Value).Name;
            else
                LabelViewCashCollector.Text = "-";
            LabelViewTotalAmount.Text = Util.FormatEmptyString(_cashCollectionSummary.TotalAmount != null ? _cashCollectionSummary.TotalAmount.Value.ToString("F") : string.Empty);
            LabelViewCommissionRate.Text = Util.FormatEmptyString(_cashCollectionSummary.CommissionRate != null ? _cashCollectionSummary.CommissionRate.Value.ToString("F") : string.Empty);
            LabelViewCommissionAmount.Text = Util.FormatEmptyString(_cashCollectionSummary.CommissionAmount != null ? _cashCollectionSummary.CommissionAmount.Value.ToString("F") : string.Empty);

            GridViewItemView.DataSource = _cashCollectionSummary.CashCollectionDetailList;
            GridViewItemView.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _cashCollectionSummary = e.Row.DataItem as SolutionObjects.CashCollectionSummary;
            if (_cashCollectionSummary.CashCollector != null)
                _cashCollectionSummary.CashCollector = new OperationObjects.CashCollectorOP().GetCashCollector(_cashCollectionSummary.CashCollector.ID.Value);

            Util.EncodeString(e.Row.Cells[3], _cashCollectionSummary.CashCollectionNumber, 20);
            Util.EncodeString(e.Row.Cells[4], _cashCollectionSummary.DateCollected != null ? _cashCollectionSummary.DateCollected.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], _cashCollectionSummary.CashCollector != null ? _cashCollectionSummary.CashCollector.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[6], _cashCollectionSummary.TotalAmount != null ? _cashCollectionSummary.TotalAmount.Value.ToString("F") : string.Empty, 20);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _cashCollectionSummary.CashCollectionNumber + "');");
                }
            }
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.CashCollectionDetail _cashCollectionDetail = e.Row.DataItem as SolutionObjects.CashCollectionDetail;
            _cashCollectionDetail.SalesIssue = new OperationObjects.SalesIssueOP().GetSalesIssueProxy(_cashCollectionDetail.SalesIssue.ID.Value);
            _cashCollectionDetail.SalesIssue.Customer = new OperationObjects.CustomerOP().GetCustomer(_cashCollectionDetail.SalesIssue.Customer.ID.Value);

            Util.EncodeString(e.Row.Cells[0], _cashCollectionDetail.SalesIssue != null ? _cashCollectionDetail.SalesIssue.CardNumber : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[1], _cashCollectionDetail.SalesIssue.Customer != null ? _cashCollectionDetail.SalesIssue.Customer.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _cashCollectionDetail.Amount != null ? _cashCollectionDetail.Amount.Value.ToString("F") : string.Empty, 50);
        }
    }
}