﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CashCollectorCommissionDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateCashCollectorCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectorCommission.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CashCollector != null ? cashCollectorCommission.CashCollector.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CashCollectionSummary != null ? cashCollectorCommission.CashCollectionSummary.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCommission", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, cashCollectorCommission.DateCommission);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CommissionAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectorCommission;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectorCommission.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditCashCollectorCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectorCommission.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectorCommission.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectorID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CashCollector != null ? cashCollectorCommission.CashCollector.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CashCollectionSummary != null ? cashCollectorCommission.CashCollectionSummary.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCommission", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, cashCollectorCommission.DateCommission);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectorCommission.CommissionAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectorCommission;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteCashCollectorCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectorCommission.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectorCommission.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectorCommission.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectorCommission;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectorCommission.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.CashCollectorCommission Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetCashCollectorCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                        int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                        int dateCommissionOrdinal = dr.GetOrdinal("DateCommission");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                        cashCollectorCommission = new Inventory.SolutionObjects.CashCollectorCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectorCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectorCommission.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectorCommission.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(dateCommissionOrdinal)) cashCollectorCommission.DateCommission = dr.GetDateTime(dateCommissionOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectorCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectorCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectorCommission;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCashCollectorCommissions";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> cashCollectorCommissionList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectorCommission>();
            Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                    int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                    int dateCommissionOrdinal = dr.GetOrdinal("DateCommission");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        cashCollectorCommission = new Inventory.SolutionObjects.CashCollectorCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectorCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectorCommission.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectorCommission.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(dateCommissionOrdinal)) cashCollectorCommission.DateCommission = dr.GetDateTime(dateCommissionOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectorCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectorCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        cashCollectorCommissionList.Add(cashCollectorCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectorCommissionList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> GetAllFor(SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCashCollectorCommissionsForCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectorCommission> cashCollectorCommissionList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectorCommission>();
            Inventory.SolutionObjects.CashCollectorCommission cashCollectorCommission;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectorIDOrdinal = dr.GetOrdinal("CashCollectorID");
                    int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                    int dateCommissionOrdinal = dr.GetOrdinal("DateCommission");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        cashCollectorCommission = new Inventory.SolutionObjects.CashCollectorCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectorCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectorIDOrdinal)) cashCollectorCommission.CashCollector = new SolutionObjects.CashCollector(dr.GetInt32(cashCollectorIDOrdinal));
                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectorCommission.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(dateCommissionOrdinal)) cashCollectorCommission.DateCommission = dr.GetDateTime(dateCommissionOrdinal);
                        if (!dr.IsDBNull(commissionRateOrdinal)) cashCollectorCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) cashCollectorCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        cashCollectorCommissionList.Add(cashCollectorCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectorCommissionList;
        }

        #endregion
    }
}
