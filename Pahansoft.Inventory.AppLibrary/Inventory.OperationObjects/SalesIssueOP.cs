﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SalesIssueOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSalesIssue(Inventory.SolutionObjects.SalesIssue salesIssue)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (salesIssue.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.SalesIssueDO().Create(salesIssue);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new SalesIssueItem List
                    foreach (SolutionObjects.SalesIssueItem salesIssueItem in salesIssue.SalesIssueItemList)
                    {
                        salesIssueItem.IsNewRec = true;
                        innerDTO = new DataAccessObjects.SalesIssueItemDO().Create(salesIssueItem);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    //// Add new CustomerPayment List
                    //foreach (SolutionObjects.CustomerPayment customerPayment in salesIssue.CustomerPaymentList)
                    //{
                    //    innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(customerPayment);
                    //    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    //        throw new Exception(innerDTO.Message);

                    //    // Add new CustomerPaymentEntry List
                    //    foreach (SolutionObjects.CustomerPaymentEntry customerPaymentEntry in customerPayment.CustomerPaymentEntryList)
                    //    {
                    //        innerDTO = new DataAccessObjects.CustomerPaymentEntryDO().Create(customerPaymentEntry);
                    //        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    //            throw new Exception(innerDTO.Message);
                    //    }
                    //}

                    //// Add new SalesRefCommission List
                    //foreach (SolutionObjects.SalesRefCommission salesRefCommission in salesIssue.SalesRefCommissionList)
                    //{
                    //    innerDTO = new DataAccessObjects.SalesRefCommissionDO().Create(salesRefCommission);
                    //    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    //        throw new Exception(innerDTO.Message);
                    //}

                    CreateItemStock(salesIssue);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in salesIssue.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.SalesIssueDO().Edit(salesIssue);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit SalesIssueItem List
                    foreach (SolutionObjects.SalesIssueItem salesIssueItem in salesIssue.SalesIssueItemList)
                    {
                        if (salesIssueItem.ID == null)
                        {
                            salesIssueItem.IsNewRec = true;
                            innerDTO = new DataAccessObjects.SalesIssueItemDO().Create(salesIssueItem);
                        }
                        else
                            innerDTO = new DataAccessObjects.SalesIssueItemDO().Edit(salesIssueItem);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete SalesIssueItem List
                    foreach (SolutionObjects.SalesIssueItem salesIssueItem in salesIssue.SalesIssueItemList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.SalesIssueItemDO().Delete(salesIssueItem);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add, Edit CustomerPayment List
                    foreach (SolutionObjects.CustomerPayment customerPayment in salesIssue.CustomerPaymentList)
                    {
                        if (customerPayment.ID == null)
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(customerPayment);
                        else
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Edit(customerPayment);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete CustomerPayment List
                    foreach (SolutionObjects.CustomerPayment customerPayment in salesIssue.CustomerPaymentList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.CustomerPaymentDO().Delete(customerPayment);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add, Edit SalesRefCommission List
                    foreach (SolutionObjects.SalesRefCommission salesRefCommission in salesIssue.SalesRefCommissionList)
                    {
                        if (salesRefCommission.ID == null)
                        {
                            innerDTO = new DataAccessObjects.SalesRefCommissionDO().Create(salesRefCommission);
                        }
                        else
                            innerDTO = new DataAccessObjects.SalesRefCommissionDO().Edit(salesRefCommission);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete SalesRefCommission List
                    foreach (SolutionObjects.SalesRefCommission salesRefCommission in salesIssue.SalesRefCommissionList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.SalesRefCommissionDO().Delete(salesRefCommission);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(salesIssue);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in salesIssue.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSalesIssue(Inventory.SolutionObjects.SalesIssue salesIssue)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.SalesIssueDO().Delete(salesIssue);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete SalesIssueItem List
                foreach (SolutionObjects.SalesIssueItem salesIssueItem in salesIssue.SalesIssueItemList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.SalesIssueItemDO().Delete(salesIssueItem);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Delete CustomerPayment List
                foreach (SolutionObjects.CustomerPayment customerPayment in salesIssue.CustomerPaymentList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.CustomerPaymentDO().Delete(customerPayment);

                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Delete SalesRefCommission List
                foreach (SolutionObjects.SalesRefCommission salesRefCommission in salesIssue.SalesRefCommissionList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.SalesRefCommissionDO().Delete(salesRefCommission);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                CreateItemStock(salesIssue);

                // Add, Edit ItemStock List
                foreach (SolutionObjects.ItemStock itemStock in salesIssue.ItemStockList)
                {
                    innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.SalesIssue GetSalesIssue(int id)
        {
            return new Inventory.DataAccessObjects.SalesIssueDO().Find(id);
        }

        public Inventory.SolutionObjects.SalesIssue GetSalesIssueProxy(int id)
        {
            return new Inventory.DataAccessObjects.SalesIssueDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> GetAllSalesIssues()
        {
            return new Inventory.DataAccessObjects.SalesIssueDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> SearchSalesIssues(string issueNumber, string cardNumber, DateTime? dateIssue, SolutionObjects.Customer customer, SolutionObjects.SalesRef salesRef)
        {
            return new Inventory.DataAccessObjects.SalesIssueDO().Search(issueNumber, cardNumber, dateIssue, customer, salesRef);
        }

        private SolutionObjects.SalesIssue CreateItemStock(SolutionObjects.SalesIssue _salesIssue)
        {
            _salesIssue.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            SolutionObjects.ItemStock _itemStock = null;
            SolutionObjects.ItemStockTransaction _itemStockTransaction;
            foreach (SolutionObjects.SalesIssueItem salesIssueItem in _salesIssue.SalesIssueItemList)
            {
                if (!salesIssueItem.IsNewRec)
                    continue;

                _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(salesIssueItem.ItemStock.ID.Value);
                _itemStock.CurrentQuantity -= salesIssueItem.IssuedQuantity;
                _itemStock.User = _salesIssue.User;
                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _itemStock;
                _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.SalesIssue;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _salesIssue.ID;
                _itemStockTransaction.TransactionDetailID = salesIssueItem.ID;
                _itemStockTransaction.ReferenceNumber = _salesIssue.IssueNumber;
                _itemStockTransaction.Store = _itemStock.Store;
                _itemStockTransaction.Location = _itemStock.Location;
                _itemStockTransaction.Rack = _itemStock.Rack;
                _itemStockTransaction.Quantity = salesIssueItem.IssuedQuantity;
                _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                _itemStockTransaction.User = _salesIssue.User;
                _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _salesIssue.ItemStockList.Add(_itemStock);
            }

            foreach (SolutionObjects.SalesIssueItem salesIssueItem in _salesIssue.SalesIssueItemList.RemovedList)
            {
                _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(salesIssueItem.ItemStock.ID.Value);
                _itemStock.CurrentQuantity += salesIssueItem.IssuedQuantity;
                _itemStock.User = _salesIssue.User;
                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _itemStock;
                _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.SalesIssue;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _salesIssue.ID;
                _itemStockTransaction.TransactionDetailID = salesIssueItem.ID;
                _itemStockTransaction.ReferenceNumber = _salesIssue.IssueNumber;
                _itemStockTransaction.Store = _itemStock.Store;
                _itemStockTransaction.Location = _itemStock.Location;
                _itemStockTransaction.Rack = _itemStock.Rack;
                _itemStockTransaction.Quantity = salesIssueItem.IssuedQuantity;
                _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                _itemStockTransaction.Remarks = "Delete SalesIssue Detail";
                _itemStockTransaction.User = _salesIssue.User;
                _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _salesIssue.ItemStockList.Add(_itemStock);
            }

            return _salesIssue;
        }
    }
}
