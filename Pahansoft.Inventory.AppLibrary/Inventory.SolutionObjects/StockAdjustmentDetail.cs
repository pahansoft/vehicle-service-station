﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class StockAdjustmentDetail : SolutionObjectBase
    {
        public StockAdjustmentDetail() { }
        public StockAdjustmentDetail(int? id) { this.ID = id; }

        public SolutionObjects.StockAdjustment StockAdjustment { get; set; }
        public SolutionObjects.ItemStock ItemStock { get; set; }
        public SolutionObjects.AdjustmentTypes? AdjustmentType { get; set; }
        public SolutionObjects.StockTypes? StockType { get; set; }
        public SolutionObjects.Store Store { get; set; }
        public SolutionObjects.Location Location { get; set; }
        public SolutionObjects.Rack Rack { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? ReturnQuantity { get; set; }
        public string Remarks { get; set; }
    }
}
