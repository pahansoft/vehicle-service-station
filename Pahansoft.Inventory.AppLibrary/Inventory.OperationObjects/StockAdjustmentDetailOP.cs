﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class StockAdjustmentDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveStockAdjustmentDetail(Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail)
        {
            if (stockAdjustmentDetail.ID == null)
                return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().Create(stockAdjustmentDetail);
            else
                return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().Edit(stockAdjustmentDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteStockAdjustmentDetail(Inventory.SolutionObjects.StockAdjustmentDetail stockAdjustmentDetail)
        {
            if (stockAdjustmentDetail.ID != null)
                return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().Delete(stockAdjustmentDetail);
            else
            {
                stockAdjustmentDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = stockAdjustmentDetail;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.StockAdjustmentDetail GetStockAdjustmentDetail(int id)
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> GetAllStockAdjustmentDetails()
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustmentDetail> GetAllFor(SolutionObjects.StockAdjustment stockAdjustment)
        {
            return new Inventory.DataAccessObjects.StockAdjustmentDetailDO().GetAllFor(stockAdjustment);
        }
    }
}
