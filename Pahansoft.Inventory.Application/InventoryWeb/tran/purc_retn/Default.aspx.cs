﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_purc_retn_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    public const string _targetGRN = "TargetGRN";
    SolutionObjects.GRN _grn;
    SolutionObjects.PurchaseReturn _purchaseReturn;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxDateReturn.Text = DateTime.Today.ToShortDateString();
        TextBoxTotalNet.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        DropDownListProductItem.SelectedIndex = 0;
        LabelDescription.Text = "-";
        LabelAvailableQuantity.Text = "-";
        TextBoxReturnedQuantity.Text = string.Empty;
        TextBoxReturnedPrice.Text = string.Empty;
        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
    }

    private void ClearSearch()
    {
        TextBoxSrchGRNNumber.Text = string.Empty;
        TextBoxSrchDateGRN.Text = string.Empty;
        TextBoxSrchPONumber.Text = string.Empty;
        DropDownListSrchSupplier.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListSrchSupplier);
        foreach (SolutionObjects.Supplier supplier in new OperationObjects.SupplierOP().GetAllSuppliers())
            DropDownListSrchSupplier.Items.Add(new ListItem(supplier.Name, supplier.ID.Value.ToString()));
    }

    private void Search()
    {
        DateTime? _dateGRN = Util.ParseDate(TextBoxSrchDateGRN.Text.Trim());
        SolutionObjects.Supplier _supplier = null;
        if (DropDownListSrchSupplier.SelectedIndex != 0)
            _supplier = new SolutionObjects.Supplier(int.Parse(DropDownListSrchSupplier.SelectedValue));

        //GridViewSearchList.DataSource = new OperationObjects.GRNOP().SearchGRNs(TextBoxSrchGRNNumber.Text.Trim(), _dateGRN, TextBoxSrchPONumber.Text.Trim(), _supplier);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotals()
    {
        _purchaseReturn = ViewState[_target] as SolutionObjects.PurchaseReturn;

        Decimal _totalNet = 0;
        foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in _purchaseReturn.PurchaseReturnItemList)
        {
            _totalNet += purchaseReturnItem.ReturnedPrice.Value * purchaseReturnItem.ReturnedQuantity.Value;
        }

        TextBoxTotalNet.Text = _totalNet != 0 ? _totalNet.ToString("F") : string.Empty;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            Util.ClearDropdown(DropDownListProductItem);
            ClearAll();
            FillDropDownList();
            _purchaseReturn = new SolutionObjects.PurchaseReturn();
            _purchaseReturn.PurchaseReturnItemList = new CommonObjects.PropertyList<SolutionObjects.PurchaseReturnItem>();
            ViewState[_target] = _purchaseReturn;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonSearchOperation.Visible = false;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _grn = new OperationObjects.GRNOP().GetGRN(_targetid);

            ClearAll();
            ViewState[_targetGRN] = _grn;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Add New";
            LinkButtonSearchOperation.Visible = true;

            LabelViewGRNNumber.Text = Util.FormatEmptyString(_grn.GRNNumber);
            LabelViewDateGRN.Text = Util.FormatEmptyString(_grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty);
            //LabelViewPONumber.Text = Util.FormatEmptyString(_grn.PONumber);
            //LabelViewDatePO.Text = Util.FormatEmptyString(_grn.DatePO != null ? _grn.DatePO.Value.ToShortDateString() : string.Empty);
            if (_grn.Supplier != null)
                LabelViewSupplier.Text = new OperationObjects.SupplierOP().GetSupplier(_grn.Supplier.ID.Value).Name;
            else
                LabelViewSupplier.Text = "-";
            LabelViewTotalTax.Text = Util.FormatEmptyString(_grn.TotalTax != null ? _grn.TotalTax.Value.ToString("F") : string.Empty);
            LabelViewTotalDiscount.Text = Util.FormatEmptyString(_grn.TotalDiscount != null ? _grn.TotalDiscount.Value.ToString("F") : string.Empty);
            LabelViewTotalGross.Text = Util.FormatEmptyString(_grn.TotalGrossAmount != null ? _grn.TotalGrossAmount.Value.ToString("F") : string.Empty);
            LabelViewTotalNet.Text = Util.FormatEmptyString(_grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty);

            Util.ClearDropdown(DropDownListProductItem);
            foreach (SolutionObjects.GRNDetail gRNDetail in _grn.GRNDetailList)
            {
                gRNDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(gRNDetail.ProductItem.ID.Value);
                DropDownListProductItem.Items.Add(new ListItem(gRNDetail.ProductItem.Code + " - " + gRNDetail.ProductItem.Description + " [" + gRNDetail.UnitPrice.Value.ToString("F") + "]", gRNDetail.ID.Value.ToString()));
            }

            GridViewItemAdd.DataSource = null;
            GridViewItemAdd.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _grn = e.Row.DataItem as SolutionObjects.GRN;
            if (_grn.Supplier != null)
                _grn.Supplier = new OperationObjects.SupplierOP().GetSupplier(_grn.Supplier.ID.Value);

            Util.EncodeString(e.Row.Cells[1], _grn.GRNNumber, 20);
            Util.EncodeString(e.Row.Cells[2], _grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty, 20);
            //Util.EncodeString(e.Row.Cells[3], _grn.PONumber, 20);
            Util.EncodeString(e.Row.Cells[4], _grn.Supplier != null ? _grn.Supplier.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[5], _grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty, 20);
        }
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _purchaseReturn = ViewState[_target] as SolutionObjects.PurchaseReturn;
        _purchaseReturn.PurchaseReturnItemList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _purchaseReturn;
        GridViewItemAdd.DataSource = _purchaseReturn.PurchaseReturnItemList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.PurchaseReturnItem _purchaseReturnItem = e.Row.DataItem as SolutionObjects.PurchaseReturnItem;
            _purchaseReturnItem.GRNDetail = new OperationObjects.GRNDetailOP().GetGRNDetail(_purchaseReturnItem.GRNDetail.ID.Value);
            _purchaseReturnItem.GRNDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItem(_purchaseReturnItem.GRNDetail.ProductItem.ID.Value);
            _purchaseReturnItem.GRNDetail.Store = new OperationObjects.StoreOP().GetStore(_purchaseReturnItem.GRNDetail.Store.ID.Value);
            _purchaseReturnItem.GRNDetail.Location = new OperationObjects.LocationOP().GetLocation(_purchaseReturnItem.GRNDetail.Location.ID.Value);
            _purchaseReturnItem.GRNDetail.Rack = new OperationObjects.RackOP().GetRack(_purchaseReturnItem.GRNDetail.Rack.ID.Value);

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_purchaseReturnItem.GRNDetail.Store != null ? _purchaseReturnItem.GRNDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_purchaseReturnItem.GRNDetail.Location != null ? _purchaseReturnItem.GRNDetail.Location.Description : "-").Append("<br>");
            _sbLocation.Append(_purchaseReturnItem.GRNDetail.Rack != null ? _purchaseReturnItem.GRNDetail.Rack.Description : "-");

            StringBuilder _sbPrice = new StringBuilder();
            _sbPrice.Append("UNIT: ").Append(_purchaseReturnItem.GRNDetail.UnitPrice != null ? _purchaseReturnItem.GRNDetail.UnitPrice.Value.ToString("F") : "-").Append("<br>");
            _sbPrice.Append("SELL: ").Append(_purchaseReturnItem.GRNDetail.SellingPrice != null ? _purchaseReturnItem.GRNDetail.SellingPrice.Value.ToString("F") : "-");

            Util.EncodeString(e.Row.Cells[1], _purchaseReturnItem.GRNDetail.ProductItem != null ? _purchaseReturnItem.GRNDetail.ProductItem.Code + "-" + _purchaseReturnItem.GRNDetail.ProductItem.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _purchaseReturnItem.GRNDetail.LotNumber, 20);
            e.Row.Cells[3].Text = _sbPrice.ToString();
            e.Row.Cells[4].Text = _sbLocation.ToString();
            Util.EncodeString(e.Row.Cells[5], _purchaseReturnItem.ReturnedQuantity.Value.ToString(), 20);
            Util.EncodeString(e.Row.Cells[6], _purchaseReturnItem.ReturnedPrice.Value.ToString("F"), 20);
        }
    }
    protected void DropDownListProductItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        StringBuilder _sbDescription = new StringBuilder();
        decimal  _availableQty = 0;
        TextBoxReturnedPrice.Text = string.Empty;

        if (DropDownListProductItem.SelectedIndex != 0)
        {
            SolutionObjects.GRNDetail _gRNDetail = new OperationObjects.GRNDetailOP().GetGRNDetail(int.Parse(DropDownListProductItem.SelectedValue));
            _sbDescription.Append("Lot &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + _gRNDetail.LotNumber).Append("<br>");
            _sbDescription.Append("Price &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + _gRNDetail.UnitPrice.Value.ToString("F") + " / " + _gRNDetail.SellingPrice.Value.ToString("F")).Append("<br>");
            _sbDescription.Append("Qty &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + _gRNDetail.Quantity.Value.ToString()).Append("<br>");
            _sbDescription.Append("Returned Qty &nbsp;&nbsp;: " + (_gRNDetail.ReturnedQuantity != null ? _gRNDetail.ReturnedQuantity.Value.ToString() : "0"));

            //if (_gRNDetail.ReturnedQuantity == null)
            //    _availableQty = _gRNDetail.Quantity.Value;
            //else
            //    _availableQty = _gRNDetail.Quantity.Value - _gRNDetail.ReturnedQuantity.Value;

            TextBoxReturnedPrice.Text = _gRNDetail.UnitPrice.Value.ToString("F");
        }
        LabelDescription.Text = _sbDescription.ToString();
        LabelAvailableQuantity.Text = _availableQty.ToString();
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
        _purchaseReturn = ViewState[_target] as SolutionObjects.PurchaseReturn;

        bool _hasFound = false;
        foreach (SolutionObjects.PurchaseReturnItem purchaseReturnItem in _purchaseReturn.PurchaseReturnItemList)
        {
            if (DropDownListProductItem.SelectedValue == purchaseReturnItem.GRNDetail.ID.Value.ToString())
            {
                _hasFound = true;
                break;
            }
        }
        if (_hasFound)
        {
            LabelValidateMultiple.Visible = true;
            return;
        }

        if (Util.ParseInt(TextBoxReturnedQuantity.Text.Trim()) > Util.ParseInt(LabelAvailableQuantity.Text))
        {
            LabelValidateQuantity.Visible = true;
            return;
        }

        SolutionObjects.PurchaseReturnItem _purchaseReturnItem = new SolutionObjects.PurchaseReturnItem();
        _purchaseReturnItem.PurchaseReturn = _purchaseReturn;
        if (DropDownListProductItem.SelectedIndex != 0)
            _purchaseReturnItem.GRNDetail = new SolutionObjects.GRNDetail(int.Parse(DropDownListProductItem.SelectedValue));
        _purchaseReturnItem.ReturnedQuantity = Util.ParseInt(TextBoxReturnedQuantity.Text.Trim());
        _purchaseReturnItem.ReturnedPrice = Util.ParseDecimal(TextBoxReturnedPrice.Text.Trim());
        _purchaseReturnItem.User = User.Identity.Name;
        _purchaseReturn.PurchaseReturnItemList.Add(_purchaseReturnItem);

        ViewState[_target] = _purchaseReturn;
        GridViewItemAdd.DataSource = _purchaseReturn.PurchaseReturnItemList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
        ClearItem();
    }

    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _purchaseReturn = ViewState[_target] as SolutionObjects.PurchaseReturn;
        _grn = ViewState[_targetGRN] as SolutionObjects.GRN;

        if (_purchaseReturn.ID == null)
            _purchaseReturn.ReturnNumber = Util.GenereteSequenceNumber("PRN", DateTime.Today.Year, DateTime.Today.Month);
        _purchaseReturn.DateReturn = Util.ParseDate(TextBoxDateReturn.Text.Trim());
        _purchaseReturn.GRN = _grn;
        _purchaseReturn.TotalNetAmount = Util.ParseDecimal(TextBoxTotalNet.Text.Trim());
        _purchaseReturn.Remarks = TextBoxRemarks.Text.Trim();
        _purchaseReturn.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.PurchaseReturnOP().SavePurchaseReturn(_purchaseReturn);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully<br>Return Number : " + _purchaseReturn.ReturnNumber;

                ClearAll();
                _purchaseReturn = new SolutionObjects.PurchaseReturn();
                _purchaseReturn.PurchaseReturnItemList = new CommonObjects.PropertyList<SolutionObjects.PurchaseReturnItem>();
                ViewState[_target] = _purchaseReturn;

                PanelAddNew.Visible = false;
                PanelSearchDetails.Visible = true;
                LabelPageOperation.Text = "Search";
                LinkButtonSearchOperation.Visible = false;
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _purchaseReturn = new SolutionObjects.PurchaseReturn();
        _purchaseReturn.PurchaseReturnItemList = new CommonObjects.PropertyList<SolutionObjects.PurchaseReturnItem>();
        ViewState[_target] = _purchaseReturn;
    }
}