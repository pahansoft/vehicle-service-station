﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class VehicleDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.VehicleDetail VehicleDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateVehicleDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleDetail.VehicleNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, VehicleDetail.Customer.ID);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = VehicleDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                VehicleDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.VehicleDetail VehicleDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditVehicleDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, VehicleDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, VehicleDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleDetail.VehicleNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, VehicleDetail.Customer.ID);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = VehicleDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.VehicleDetail VehicleDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteVehicleDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, VehicleDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, VehicleDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = VehicleDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                VehicleDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.VehicleDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetVehicleDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.VehicleDetail VehicleDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                        int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");

                        VehicleDetail = new Inventory.SolutionObjects.VehicleDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) VehicleDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(VehicleNumberOrdinal)) VehicleDetail.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) VehicleDetail.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return VehicleDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.VehicleDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllVehicleDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var VehicleDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.VehicleDetail>();
            Inventory.SolutionObjects.VehicleDetail VehicleDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");

                    while (dr.Read())
                    {
                        VehicleDetail = new Inventory.SolutionObjects.VehicleDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) VehicleDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(VehicleNumberOrdinal)) VehicleDetail.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) VehicleDetail.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));

                        VehicleDetailList.Add(VehicleDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return VehicleDetailList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.VehicleDetail> Search(string VehicleNumber, int? CustomerID)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllVehicleDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@VehicleNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, VehicleNumber != string.Empty ? VehicleNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, CustomerID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var VehicleDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.VehicleDetail>();
            Inventory.SolutionObjects.VehicleDetail VehicleDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int VehicleNumberOrdinal = dr.GetOrdinal("VehicleNumber");
                    int CustomerIDOrdinal = dr.GetOrdinal("CustomerID");

                    while (dr.Read())
                    {
                        VehicleDetail = new Inventory.SolutionObjects.VehicleDetail(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) VehicleDetail.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(VehicleNumberOrdinal)) VehicleDetail.VehicleNumber = dr.GetString(VehicleNumberOrdinal);
                        if (!dr.IsDBNull(CustomerIDOrdinal)) VehicleDetail.Customer = new SolutionObjects.Customer(dr.GetInt32(CustomerIDOrdinal));

                        VehicleDetailList.Add(VehicleDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return VehicleDetailList;
        }

        #endregion
    }
}
