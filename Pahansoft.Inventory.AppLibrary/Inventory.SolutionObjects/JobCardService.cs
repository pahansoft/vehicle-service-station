﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class JobCardService : SolutionObjectBase
    {
        public JobCardService() { }
        public JobCardService(int? id) { this.ID = id; }

        public SolutionObjects.JobCard JobCard { get; set; }
        public SolutionObjects.Service Service { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? NetAmount { get; set; }
    }
}
