﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class PurchaseReturn : SolutionObjectBase
    {
        public PurchaseReturn() { }
        public PurchaseReturn(int? id) { this.ID = id; }

        public string ReturnNumber { get; set; }
        public DateTime? DateReturn { get; set; }
        public SolutionObjects.GRN GRN { get; set; }
        public decimal? TotalNetAmount { get; set; }
        public string Remarks { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.PurchaseReturnItem> PurchaseReturnItemList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
