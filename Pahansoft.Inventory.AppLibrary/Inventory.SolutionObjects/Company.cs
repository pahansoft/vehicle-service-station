﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Company : SolutionObjectBase
    {
        public Company() { }
        public Company(int? id) { this.ID = id; }

        public string Description { get; set; }
    }
}
