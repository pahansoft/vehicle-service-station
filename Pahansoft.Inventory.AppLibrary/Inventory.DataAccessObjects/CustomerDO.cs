﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CustomerDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.Customer customer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateCustomer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                customer.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.Customer customer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditCustomer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customer.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, customer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Name);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine1", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine1);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine2", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine2);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AddressLine3", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.AddressLine3);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneHome", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneHome);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneMobile", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneMobile);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TelephoneOffice", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.TelephoneOffice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Fax", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Fax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Email", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.Email);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.Customer customer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteCustomer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customer.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, customer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, customer.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = customer;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                customer.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.Customer Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetCustomer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.Customer customer = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int nameOrdinal = dr.GetOrdinal("Name");
                        int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                        int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                        int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                        int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                        int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                        int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                        int faxOrdinal = dr.GetOrdinal("Fax");
                        int emailOrdinal = dr.GetOrdinal("Email");

                        customer = new Inventory.SolutionObjects.Customer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) customer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) customer.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) customer.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) customer.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) customer.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) customer.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) customer.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) customer.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) customer.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) customer.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) customer.Email = dr.GetString(emailOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customer;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllCustomers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> customerList = new System.Collections.Generic.List<Inventory.SolutionObjects.Customer>();
            Inventory.SolutionObjects.Customer customer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        customer = new Inventory.SolutionObjects.Customer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) customer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) customer.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) customer.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) customer.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) customer.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) customer.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) customer.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) customer.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) customer.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) customer.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) customer.Email = dr.GetString(emailOrdinal);

                        customerList.Add(customer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customerList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> Search(string code, string name)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllCustomers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Name", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, name != string.Empty ? name : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> customerList = new System.Collections.Generic.List<Inventory.SolutionObjects.Customer>();
            Inventory.SolutionObjects.Customer customer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int addressLine1Ordinal = dr.GetOrdinal("AddressLine1");
                    int addressLine2Ordinal = dr.GetOrdinal("AddressLine2");
                    int addressLine3Ordinal = dr.GetOrdinal("AddressLine3");
                    int telephoneHomeOrdinal = dr.GetOrdinal("TelephoneHome");
                    int telephoneMobileOrdinal = dr.GetOrdinal("TelephoneMobile");
                    int telephoneOfficeOrdinal = dr.GetOrdinal("TelephoneOffice");
                    int faxOrdinal = dr.GetOrdinal("Fax");
                    int emailOrdinal = dr.GetOrdinal("Email");

                    while (dr.Read())
                    {
                        customer = new Inventory.SolutionObjects.Customer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) customer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) customer.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(nameOrdinal)) customer.Name = dr.GetString(nameOrdinal);
                        if (!dr.IsDBNull(addressLine1Ordinal)) customer.AddressLine1 = dr.GetString(addressLine1Ordinal);
                        if (!dr.IsDBNull(addressLine2Ordinal)) customer.AddressLine2 = dr.GetString(addressLine2Ordinal);
                        if (!dr.IsDBNull(addressLine3Ordinal)) customer.AddressLine3 = dr.GetString(addressLine3Ordinal);
                        if (!dr.IsDBNull(telephoneHomeOrdinal)) customer.TelephoneHome = dr.GetString(telephoneHomeOrdinal);
                        if (!dr.IsDBNull(telephoneMobileOrdinal)) customer.TelephoneMobile = dr.GetString(telephoneMobileOrdinal);
                        if (!dr.IsDBNull(telephoneOfficeOrdinal)) customer.TelephoneOffice = dr.GetString(telephoneOfficeOrdinal);
                        if (!dr.IsDBNull(faxOrdinal)) customer.Fax = dr.GetString(faxOrdinal);
                        if (!dr.IsDBNull(emailOrdinal)) customer.Email = dr.GetString(emailOrdinal);

                        customerList.Add(customer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return customerList;
        }

        #endregion
    }
}
