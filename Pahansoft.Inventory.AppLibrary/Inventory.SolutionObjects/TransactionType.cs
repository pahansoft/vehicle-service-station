﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    public enum TransactionTypes
    {
        GRN,
        SalesIssue,
        OpeningBalance,
        SalesReturn,
        PurchaseReturn,
        StockAdjustment,
        LocationTransfer,
        JobCard
    }
}
