﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_prod_type_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.ProductType _productType;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxDescription.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchDescription.Text = string.Empty;
    }

    private void Search()
    {
        GridViewSearchList.DataSource = new OperationObjects.ProductTypeOP().SearchProductTypes(TextBoxSrchDescription.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            ViewState[_target] = new SolutionObjects.ProductType();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.ProductType();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _productType = ViewState[_target] as SolutionObjects.ProductType;
        _productType.Description = TextBoxDescription.Text.Trim();
        _productType.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.ProductTypeOP().SaveProductType(_productType);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.ProductType();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _productType = ViewState[_target] as SolutionObjects.ProductType;

        if (_productType.ID != null)
        {
            _productType = new OperationObjects.ProductTypeOP().GetProductType(_productType.ID.Value);
            ViewState[_target] = _productType;

            TextBoxDescription.Text = _productType.Description;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.ProductType();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _productType = new OperationObjects.ProductTypeOP().GetProductType(_targetid);

            ClearAll();
            ViewState[_target] = _productType;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxDescription.Text = _productType.Description;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _productType = new OperationObjects.ProductTypeOP().GetProductType(_targetid);
            _productType.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.ProductTypeOP().DeleteProductType(_productType);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _productType = e.Row.DataItem as SolutionObjects.ProductType;
            Util.EncodeString(e.Row.Cells[2], _productType.Description, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _productType.Description + "');");
                }
            }
        }
    }
}