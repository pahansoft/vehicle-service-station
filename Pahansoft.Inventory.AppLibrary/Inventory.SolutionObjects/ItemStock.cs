﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class ItemStock : SolutionObjectBase
    {
        public ItemStock() { }
        public ItemStock(int? id) { this.ID = id; }

        public SolutionObjects.ProductItem ProductItem { get; set; }
        public SolutionObjects.StockTypes? StockTypes { get; set; }
        public string LotNumber { get; set; }
        public SolutionObjects.Store Store { get; set; }
        public SolutionObjects.Location Location { get; set; }
        public SolutionObjects.Rack Rack { get; set; }
        public decimal? OpeningQuantity { get; set; }
        public decimal? CurrentQuantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? SellingPrice { get; set; }
        public DateTime? DateManufactured { get; set; }
        public DateTime? DateExpiry { get; set; }
        public string Remarks { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction> ItemStockTransactionList { get; set; }
    }
}
