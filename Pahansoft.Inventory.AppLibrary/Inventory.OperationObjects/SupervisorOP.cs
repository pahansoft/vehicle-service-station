﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SupervisorOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSupervisor(Inventory.SolutionObjects.Supervisor Supervisor)
        {
            if (Supervisor.ID == null)
                return new Inventory.DataAccessObjects.SupervisorDO().Create(Supervisor);
            else
                return new Inventory.DataAccessObjects.SupervisorDO().Edit(Supervisor);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSupervisor(Inventory.SolutionObjects.Supervisor Supervisor)
        {
            if (Supervisor.ID != null)
                return new Inventory.DataAccessObjects.SupervisorDO().Delete(Supervisor);
            else
            {
                Supervisor.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = Supervisor;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Supervisor GetSupervisor(int id)
        {
            return new Inventory.DataAccessObjects.SupervisorDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> GetAllSupervisors()
        {
            return new Inventory.DataAccessObjects.SupervisorDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supervisor> SearchSupervisor(string code, string name)
        {
            return new Inventory.DataAccessObjects.SupervisorDO().Search(code, name);
        }
    }
}