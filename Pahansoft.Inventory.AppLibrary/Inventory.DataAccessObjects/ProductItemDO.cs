﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class ProductItemDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.ProductItem productItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateProductItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@OtherDescription", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.OtherDescription);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BrandNameID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.BrandName != null ? productItem.BrandName.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ManufacturerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.Manufacturer != null ? productItem.Manufacturer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ProductType != null ? productItem.ProductType.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ROL", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ROL);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ROQuantity", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ROQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CountryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.Country != null ? productItem.Country.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.SalesRef != null ? productItem.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Model", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Model);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Colour", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Colour);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CostPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, productItem.CostPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, productItem.SellingPrice);
            
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsBrandNewItem", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, productItem.IsBrandNewItem);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = productItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                productItem.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.ProductItem productItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditProductItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, productItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Code);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@OtherDescription", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.OtherDescription);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.ReferenceNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BrandNameID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.BrandName != null ? productItem.BrandName.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ManufacturerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.Manufacturer != null ? productItem.Manufacturer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ProductType != null ? productItem.ProductType.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ROL", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ROL);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ROQuantity", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ROQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CountryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.Country != null ? productItem.Country.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.SalesRef != null ? productItem.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Model", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Model);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Colour", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.Colour);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CostPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, productItem.CostPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SellingPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, productItem.SellingPrice);
            
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsBrandNewItem", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, productItem.IsBrandNewItem);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = productItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.ProductItem productItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteProductItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productItem.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, productItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, productItem.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = productItem;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                productItem.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.ProductItem Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetProductItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ProductItem productItem = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int descriptionOrdinal = dr.GetOrdinal("Description");
                        int otherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");

                        int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                        int brandNameIDOrdinal = dr.GetOrdinal("BrandNameID");
                        int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");
                        int productTypeIDOrdinal = dr.GetOrdinal("ProductTypeID");
                        int rOLOrdinal = dr.GetOrdinal("ROL");
                        int rOQuantityOrdinal = dr.GetOrdinal("ROQuantity");
                        int countryIDOrdinal = dr.GetOrdinal("CountryID");
                        int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                        int modelOrdinal = dr.GetOrdinal("Model");
                        int colourOrdinal = dr.GetOrdinal("Colour");
                        int costpriceOrdinal = dr.GetOrdinal("CostPrice");
                        int sellingpriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int IsBrandNewItemOrdinal = dr.GetOrdinal("IsBrandNewItem");

                        productItem = new Inventory.SolutionObjects.ProductItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) productItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) productItem.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(descriptionOrdinal)) productItem.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(otherDescriptionOrdinal)) productItem.OtherDescription = dr.GetString(otherDescriptionOrdinal);

                        if (!dr.IsDBNull(referenceNumberOrdinal)) productItem.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(brandNameIDOrdinal)) productItem.BrandName = new SolutionObjects.BrandName(dr.GetInt32(brandNameIDOrdinal));
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) productItem.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                        if (!dr.IsDBNull(productTypeIDOrdinal)) productItem.ProductType = new SolutionObjects.ProductType(dr.GetInt32(productTypeIDOrdinal));
                        if (!dr.IsDBNull(rOLOrdinal)) productItem.ROL = dr.GetInt32(rOLOrdinal);
                        if (!dr.IsDBNull(rOQuantityOrdinal)) productItem.ROQuantity = dr.GetInt32(rOQuantityOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) productItem.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) productItem.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(modelOrdinal)) productItem.Model = dr.GetString(modelOrdinal);
                        if (!dr.IsDBNull(colourOrdinal)) productItem.Colour = dr.GetString(colourOrdinal);
                        if (!dr.IsDBNull(costpriceOrdinal)) productItem.CostPrice = dr.GetDecimal(costpriceOrdinal);
                        if (!dr.IsDBNull(sellingpriceOrdinal)) productItem.SellingPrice = dr.GetDecimal(sellingpriceOrdinal);
                        if (!dr.IsDBNull(IsBrandNewItemOrdinal)) productItem.IsBrandNewItem = dr.GetBoolean(IsBrandNewItemOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItem;
        }

        public Inventory.SolutionObjects.ProductItem FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetProductItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ProductItem productItem = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int codeOrdinal = dr.GetOrdinal("Code");
                        int descriptionOrdinal = dr.GetOrdinal("Description");
                        int otherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");

                        int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                        int brandNameIDOrdinal = dr.GetOrdinal("BrandNameID");
                        int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");
                        int productTypeIDOrdinal = dr.GetOrdinal("ProductTypeID");
                        int rOLOrdinal = dr.GetOrdinal("ROL");
                        int rOQuantityOrdinal = dr.GetOrdinal("ROQuantity");
                        int countryIDOrdinal = dr.GetOrdinal("CountryID");
                        int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                        int modelOrdinal = dr.GetOrdinal("Model");
                        int colourOrdinal = dr.GetOrdinal("Colour");
                        int costpriceOrdinal = dr.GetOrdinal("CostPrice");
                        int sellingpriceOrdinal = dr.GetOrdinal("SellingPrice");
                        int IsBrandNewItemOrdinal = dr.GetOrdinal("IsBrandNewItem");

                        productItem = new Inventory.SolutionObjects.ProductItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) productItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) productItem.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(descriptionOrdinal)) productItem.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(otherDescriptionOrdinal)) productItem.OtherDescription = dr.GetString(otherDescriptionOrdinal);

                        if (!dr.IsDBNull(referenceNumberOrdinal)) productItem.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(brandNameIDOrdinal)) productItem.BrandName = new SolutionObjects.BrandName(dr.GetInt32(brandNameIDOrdinal));
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) productItem.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                        if (!dr.IsDBNull(productTypeIDOrdinal)) productItem.ProductType = new SolutionObjects.ProductType(dr.GetInt32(productTypeIDOrdinal));
                        if (!dr.IsDBNull(rOLOrdinal)) productItem.ROL = dr.GetInt32(rOLOrdinal);
                        if (!dr.IsDBNull(rOQuantityOrdinal)) productItem.ROQuantity = dr.GetInt32(rOQuantityOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) productItem.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) productItem.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(modelOrdinal)) productItem.Model = dr.GetString(modelOrdinal);
                        if (!dr.IsDBNull(colourOrdinal)) productItem.Colour = dr.GetString(colourOrdinal);
                        if (!dr.IsDBNull(costpriceOrdinal)) productItem.CostPrice = dr.GetDecimal(costpriceOrdinal);
                        if (!dr.IsDBNull(sellingpriceOrdinal)) productItem.SellingPrice = dr.GetDecimal(sellingpriceOrdinal);


                        if (!dr.IsDBNull(IsBrandNewItemOrdinal)) productItem.IsBrandNewItem = dr.GetBoolean(IsBrandNewItemOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItem;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllProductItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> productItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.ProductItem>();
            Inventory.SolutionObjects.ProductItem productItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int otherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");

                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int brandNameIDOrdinal = dr.GetOrdinal("BrandNameID");
                    int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");
                    int productTypeIDOrdinal = dr.GetOrdinal("ProductTypeID");
                    int rOLOrdinal = dr.GetOrdinal("ROL");
                    int rOQuantityOrdinal = dr.GetOrdinal("ROQuantity");
                    int countryIDOrdinal = dr.GetOrdinal("CountryID");
                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int modelOrdinal = dr.GetOrdinal("Model");
                    int colourOrdinal = dr.GetOrdinal("Colour");
                    int costpriceOrdinal = dr.GetOrdinal("CostPrice");
                    int sellingpriceOrdinal = dr.GetOrdinal("SellingPrice");
                    int IsBrandNewItemOrdinal = dr.GetOrdinal("IsBrandNewItem");

                    while (dr.Read())
                    {
                        productItem = new Inventory.SolutionObjects.ProductItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) productItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) productItem.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(descriptionOrdinal)) productItem.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(otherDescriptionOrdinal)) productItem.OtherDescription = dr.GetString(otherDescriptionOrdinal);

                        if (!dr.IsDBNull(referenceNumberOrdinal)) productItem.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(brandNameIDOrdinal)) productItem.BrandName = new SolutionObjects.BrandName(dr.GetInt32(brandNameIDOrdinal));
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) productItem.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                        if (!dr.IsDBNull(productTypeIDOrdinal)) productItem.ProductType = new SolutionObjects.ProductType(dr.GetInt32(productTypeIDOrdinal));
                        if (!dr.IsDBNull(rOLOrdinal)) productItem.ROL = dr.GetInt32(rOLOrdinal);
                        if (!dr.IsDBNull(rOQuantityOrdinal)) productItem.ROQuantity = dr.GetInt32(rOQuantityOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) productItem.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) productItem.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(modelOrdinal)) productItem.Model = dr.GetString(modelOrdinal);
                        if (!dr.IsDBNull(colourOrdinal)) productItem.Colour = dr.GetString(colourOrdinal);
                        if (!dr.IsDBNull(costpriceOrdinal)) productItem.CostPrice = dr.GetDecimal(costpriceOrdinal);
                        if (!dr.IsDBNull(sellingpriceOrdinal)) productItem.SellingPrice = dr.GetDecimal(sellingpriceOrdinal); 
                        if (!dr.IsDBNull(IsBrandNewItemOrdinal)) productItem.IsBrandNewItem = dr.GetBoolean(IsBrandNewItemOrdinal);

                        productItemList.Add(productItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItemList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> Search(string code, string description, string referenceNumber, SolutionObjects.ProductType productType, bool? BrandNewItemsOnly)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllProductItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, description != string.Empty ? description : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, referenceNumber != string.Empty ? referenceNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productType != null ? productType.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BrandNewItemsOnly", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, BrandNewItemsOnly);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> productItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.ProductItem>();
            Inventory.SolutionObjects.ProductItem productItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int otherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");

                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int brandNameIDOrdinal = dr.GetOrdinal("BrandNameID");
                    int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");
                    int productTypeIDOrdinal = dr.GetOrdinal("ProductTypeID");
                    int rOLOrdinal = dr.GetOrdinal("ROL");
                    int rOQuantityOrdinal = dr.GetOrdinal("ROQuantity");
                    int countryIDOrdinal = dr.GetOrdinal("CountryID");
                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int modelOrdinal = dr.GetOrdinal("Model");
                    int colourOrdinal = dr.GetOrdinal("Colour");
                    int IsBrandNewItemOrdinal = dr.GetOrdinal("IsBrandNewItem");

                    while (dr.Read())
                    {
                        productItem = new Inventory.SolutionObjects.ProductItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) productItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) productItem.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(descriptionOrdinal)) productItem.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(otherDescriptionOrdinal)) productItem.OtherDescription = dr.GetString(otherDescriptionOrdinal);

                        if (!dr.IsDBNull(referenceNumberOrdinal)) productItem.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(brandNameIDOrdinal)) productItem.BrandName = new SolutionObjects.BrandName(dr.GetInt32(brandNameIDOrdinal));
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) productItem.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                        if (!dr.IsDBNull(productTypeIDOrdinal)) productItem.ProductType = new SolutionObjects.ProductType(dr.GetInt32(productTypeIDOrdinal));
                        if (!dr.IsDBNull(rOLOrdinal)) productItem.ROL = dr.GetInt32(rOLOrdinal);
                        if (!dr.IsDBNull(rOQuantityOrdinal)) productItem.ROQuantity = dr.GetInt32(rOQuantityOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) productItem.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) productItem.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(modelOrdinal)) productItem.Model = dr.GetString(modelOrdinal);
                        if (!dr.IsDBNull(colourOrdinal)) productItem.Colour = dr.GetString(colourOrdinal);
                        if (!dr.IsDBNull(IsBrandNewItemOrdinal)) productItem.IsBrandNewItem = dr.GetBoolean(IsBrandNewItemOrdinal);

                        productItemList.Add(productItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItemList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> SearchForReport(string code, string description, string referenceNumber,int? productType, bool? BrandNewItemsOnly)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllProductItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Code", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, code != string.Empty ? code : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, description != string.Empty ? description : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReferenceNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, referenceNumber != string.Empty ? referenceNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ProductTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, productType != null ? productType : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@BrandNewItemsOnly", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, BrandNewItemsOnly);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.ProductItem> productItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.ProductItem>();
            Inventory.SolutionObjects.ProductItem productItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int codeOrdinal = dr.GetOrdinal("Code");
                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int otherDescriptionOrdinal = dr.GetOrdinal("OtherDescription");

                    int referenceNumberOrdinal = dr.GetOrdinal("ReferenceNumber");
                    int brandNameIDOrdinal = dr.GetOrdinal("BrandNameID");
                    int manufacturerIDOrdinal = dr.GetOrdinal("ManufacturerID");
                    int productTypeIDOrdinal = dr.GetOrdinal("ProductTypeID");
                    int rOLOrdinal = dr.GetOrdinal("ROL");
                    int rOQuantityOrdinal = dr.GetOrdinal("ROQuantity");
                    int countryIDOrdinal = dr.GetOrdinal("CountryID");
                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int modelOrdinal = dr.GetOrdinal("Model");
                    int colourOrdinal = dr.GetOrdinal("Colour");
                    int IsBrandNewItemOrdinal = dr.GetOrdinal("IsBrandNewItem");
                    int CostPriceOrdinal = dr.GetOrdinal("CostPrice");
                    int SellingPriceOrdinal = dr.GetOrdinal("SellingPrice");

                    while (dr.Read())
                    {
                        productItem = new Inventory.SolutionObjects.ProductItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) productItem.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(codeOrdinal)) productItem.Code = dr.GetString(codeOrdinal);
                        if (!dr.IsDBNull(descriptionOrdinal)) productItem.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(otherDescriptionOrdinal)) productItem.OtherDescription = dr.GetString(otherDescriptionOrdinal);

                        if (!dr.IsDBNull(referenceNumberOrdinal)) productItem.ReferenceNumber = dr.GetString(referenceNumberOrdinal);
                        if (!dr.IsDBNull(brandNameIDOrdinal)) productItem.BrandName = new SolutionObjects.BrandName(dr.GetInt32(brandNameIDOrdinal));
                        if (!dr.IsDBNull(manufacturerIDOrdinal)) productItem.Manufacturer = new SolutionObjects.Manufacturer(dr.GetInt32(manufacturerIDOrdinal));
                        if (!dr.IsDBNull(productTypeIDOrdinal)) productItem.ProductType = new SolutionObjects.ProductType(dr.GetInt32(productTypeIDOrdinal));
                        if (!dr.IsDBNull(rOLOrdinal)) productItem.ROL = dr.GetInt32(rOLOrdinal);
                        if (!dr.IsDBNull(rOQuantityOrdinal)) productItem.ROQuantity = dr.GetInt32(rOQuantityOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) productItem.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) productItem.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(modelOrdinal)) productItem.Model = dr.GetString(modelOrdinal);
                        if (!dr.IsDBNull(colourOrdinal)) productItem.Colour = dr.GetString(colourOrdinal);
                        if (!dr.IsDBNull(IsBrandNewItemOrdinal)) productItem.IsBrandNewItem = dr.GetBoolean(IsBrandNewItemOrdinal);
                        if (!dr.IsDBNull(CostPriceOrdinal)) productItem.CostPrice = dr.GetDecimal(CostPriceOrdinal);
                        if (!dr.IsDBNull(SellingPriceOrdinal)) productItem.SellingPrice = dr.GetDecimal(SellingPriceOrdinal);

                        productItemList.Add(productItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItemList;
        }


        public Inventory.SolutionObjects.ProductItem AutoNum()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetItemCode";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.ProductItem productItem = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int CodeOrdinal = dr.GetOrdinal("Code");
                        productItem = new SolutionObjects.ProductItem();
                        if (!dr.IsDBNull(CodeOrdinal)) productItem.AutoNum = dr.GetInt32(CodeOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return productItem;
        }
        #endregion
    }
}
