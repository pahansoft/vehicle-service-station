﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_rack_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.Rack _rack;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        DropDownListStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListLocation);
        TextBoxDescription.Text = string.Empty;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListStore);
        Util.ClearDropdown(DropDownListSrchStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
        {
            DropDownListStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
            DropDownListSrchStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
        }
    }

    private void ClearSearch()
    {
        DropDownListSrchStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListSrchLocation);
        TextBoxSrchDescription.Text = string.Empty;
    }

    private void Search()
    {
        SolutionObjects.Store _store = null;
        if (DropDownListSrchStore.SelectedIndex != 0)
            _store = new SolutionObjects.Store(int.Parse(DropDownListSrchStore.SelectedValue));

        SolutionObjects.Location _location = null;
        if (DropDownListSrchLocation.SelectedIndex != 0)
            _location = new SolutionObjects.Location(int.Parse(DropDownListSrchLocation.SelectedValue));


        GridViewSearchList.DataSource = new OperationObjects.RackOP().SearchRacks(TextBoxSrchDescription.Text.Trim(), _store, _location);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            ViewState[_target] = new SolutionObjects.Rack();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.Rack();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void DropDownListStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListLocation);
        if (DropDownListStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
                DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _rack = ViewState[_target] as SolutionObjects.Rack;
        if (DropDownListStore.SelectedIndex != 0)
            _rack.Store = new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue));
        else
            _rack.Store = null;
        if (DropDownListLocation.SelectedIndex != 0)
            _rack.Location = new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue));
        else
            _rack.Location = null;
        _rack.Description = TextBoxDescription.Text.Trim();

        _rack.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.RackOP().SaveRack(_rack);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.Rack();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _rack = ViewState[_target] as SolutionObjects.Rack;

        if (_rack.ID != null)
        {
            _rack = new OperationObjects.RackOP().GetRack(_rack.ID.Value);
            ViewState[_target] = _rack;

            if (_rack.Store != null)
            {
                DropDownListStore.SelectedValue = _rack.Store.ID.Value.ToString();
                if (DropDownListStore.SelectedIndex != 0)
                {
                    foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
                        DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
                }
            }
            if (_rack.Location != null)
                DropDownListLocation.SelectedValue = _rack.Location.ID.Value.ToString();
            TextBoxDescription.Text = _rack.Description;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.Rack();
        }
    }

    protected void DropDownListSrchStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListSrchLocation);
        if (DropDownListSrchStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListSrchStore.SelectedValue))))
                DropDownListSrchLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _rack = new OperationObjects.RackOP().GetRack(_targetid);

            ClearAll();
            ViewState[_target] = _rack;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            if (_rack.Store != null)
            {
                DropDownListStore.SelectedValue = _rack.Store.ID.Value.ToString();
                if (DropDownListStore.SelectedIndex != 0)
                {
                    foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
                        DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
                }
            }
            if (_rack.Location != null)
                DropDownListLocation.SelectedValue = _rack.Location.ID.Value.ToString();
            TextBoxDescription.Text = _rack.Description;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _rack = new OperationObjects.RackOP().GetRack(_targetid);
            _rack.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.RackOP().DeleteRack(_rack);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _rack = e.Row.DataItem as SolutionObjects.Rack;
            if (_rack.Store != null)
                _rack.Store = new OperationObjects.StoreOP().GetStore(_rack.Store.ID.Value);
            if (_rack.Location != null)
                _rack.Location = new OperationObjects.LocationOP().GetLocation(_rack.Location.ID.Value);
            Util.EncodeString(e.Row.Cells[2], _rack.Store != null ? _rack.Store.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[3], _rack.Location != null ? _rack.Location.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[4], _rack.Description, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _rack.Description + "');");
                }
            }
        }
    }
}