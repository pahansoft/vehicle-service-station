﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="tran_jobc_appr_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-1.10.0.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-ui.min.js"></script>

    <link href="../../inc_fils/css/jquery-ui.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            SearchCustomer("ctl00_ContentPlaceHolder1_TextBoxCustomer");
            SearchCustomer("ctl00_ContentPlaceHolder1_TextBoxSrchCustomer");
            SearchSupervisor("ctl00_ContentPlaceHolder1_TextBoxSupervisor");
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
            SearchService("ctl00_ContentPlaceHolder1_TextBoxService");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
            SearchService("ctl00_ContentPlaceHolder1_TextBoxService");
        });

        function SearchProductItem(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetProductItems",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchCustomer(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetCustomers",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchSupervisor(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetSupervisors",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchService(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetServices",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function itemTotCalc() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxSellingPrice').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxQuantity').value;
            var txtValue3 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxDiscount').value;
            var txtValue4 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxTax').value;

            var gross = 0;
            var net = 0;
            if (txtValue1 && txtValue2) {
                if (parseFloat(txtValue1) != 0 && parseFloat(txtValue2) != 0) {
                    gross = parseFloat(txtValue1) * parseFloat(txtValue2);
                    net = gross;
                }
                if (txtValue3)
                    if (parseFloat(txtValue3) != 0)
                        net = net - parseFloat(txtValue3);
                if (txtValue4)
                    if (parseFloat(txtValue4) != 0)
                        net = net + parseFloat(txtValue4);
            }

            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxGrossAmount').value = gross;
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxNetAmount').value = net;
        }

        function calcDiscountAmt() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxGrossAmount').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxDiscountPercentage').value;

            var discount = 0;
            if (txtValue1 && txtValue2) {
                if (parseFloat(txtValue1) != 0 && parseFloat(txtValue2) != 0) {
                    discount = parseFloat(txtValue1) * parseFloat(txtValue2) / 100;
                }
            }
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxDiscount').value = discount;
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxNetAmount').value = (parseFloat(txtValue1) - discount).toFixed(2); ;

        }

        function serviceTotCalc() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceAmount').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceDiscount').value;
            var txtValue3 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceTax').value;

            var net = 0;
            if (txtValue1) {
                net = parseFloat(txtValue1);
                if (txtValue2)
                    if (parseFloat(txtValue2) != 0)
                        net = net - parseFloat(txtValue2);
                if (txtValue3)
                    if (parseFloat(txtValue3) != 0)
                        net = net + parseFloat(txtValue3);
            }

            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceNetAmount').value = net;
        }

        function calcServiceDiscountAmt() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceAmount').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceDiscountPercentage').value;

            var discount = 0;
            if (txtValue1 && txtValue2) {
                if (parseFloat(txtValue1) != 0 && parseFloat(txtValue2) != 0) {
                    discount = parseFloat(txtValue1) * parseFloat(txtValue2) / 100;
                }
            }
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceDiscount').value = discount;
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxServiceNetAmount').value = (parseFloat(txtValue1) - discount).toFixed(2);

        }

        function print(jobCardID) {
            var WinPrint = window.open("repo.aspx?JobCardID=" + jobCardID, '', 'left=5,top=5,width=1005,height=700,toolbar=0,scrollbars=1,status=0');
            WinPrint.focus();
        }

    </script>

    <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">Job Card Approval -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonSearchOperation_Click">Search</asp:LinkButton>
                                            <%--<asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New</asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddNew" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Job Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">Job Number <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="TextBoxJobNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px" ReadOnly="true" Text="- AUTO GENERATED -" TabIndex="1"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 15%">Vehicle Number <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="TextBoxVehicleNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                                        Width="150px" AutoPostBack="True" OnTextChanged="TextBoxVehicleNumber_TextChanged" TabIndex="6"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                        ControlToValidate="TextBoxVehicleNumber" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Start Date <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxStartDate" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px" TabIndex="2"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                        ControlToValidate="TextBoxStartDate" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxStartDate"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                                <td>Customer <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxCustomer" runat="server" CssClass="textbox" MaxLength="200" Width="250px" AutoPostBack="True" OnTextChanged="TextBoxCustomer_TextChanged" TabIndex="7"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxCustomer"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>End Date 
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxEndDate" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px" TabIndex="3"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]
                                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxEndDate"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                                <td>Address 
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxAddress" runat="server" CssClass="textbox" MaxLength="200" Width="250px" TabIndex="8"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Previous Due 
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxPreviousDue" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px" ReadOnly="true" TabIndex="4"></asp:TextBox>
                                                                </td>
                                                                <td>Telephone 
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxTelephone" runat="server" CssClass="textbox" MaxLength="50" Width="150px" TabIndex="9"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supervisor <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSupervisor" runat="server" CssClass="textbox" MaxLength="200" Width="250px" TabIndex="5"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="TextBoxSupervisor"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>

                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table style="width: 100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                                    OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                                                                                    DataKeyNames="ID" AutoGenerateColumns="False">
                                                                                    <PagerSettings Mode="NumericFirstLast" />
                                                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                                                    <Columns>
                                                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="10px" />
                                                                                        </asp:ButtonField>
                                                                                        <asp:BoundField HeaderText="Product Item">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Quantity">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Selling Price">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Gross">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Discount %">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Discount">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Tax">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Net">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid-pager" />
                                                                                    <EmptyDataTemplate>
                                                                                        <div id="norec" class="no-record-msg">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="padding-left: 15px">No Items(s) Found!
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </EmptyDataTemplate>
                                                                                    <HeaderStyle CssClass="grid-header" />
                                                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">Product Item <span style="color: red">*</span>
                                                                            </td>
                                                                            <td style="width: 55%">
                                                                                <asp:TextBox ID="TextBoxProductItem" runat="server" CssClass="textbox" MaxLength="400" Width="400px" TabIndex="100" AutoPostBack="True" OnTextChanged="TextBoxProductItem_TextChanged"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxProductItem"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Discount Percentage
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxDiscountPercentage" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="calcDiscountAmt();" TabIndex="104"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBoxDiscountPercentage"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Selling Price <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxSellingPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="101" onkeyup="itemTotCalc();"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                            <td>Discount
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="itemTotCalc();" TabIndex="105"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="TextBoxDiscount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">Quantity <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="itemTotCalc();" TabIndex="102"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="1000000" MinimumValue="1"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                                &nbsp;<asp:Label ID="LabelAvailableQty" runat="server" Style="font-weight: 700; color: #00CC66" Text=""></asp:Label>
                                                                            </td>
                                                                            <td>Tax
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxTax" runat="server" CssClass="textbox" MaxLength="20" Width="100px" onkeyup="itemTotCalc();" TabIndex="106"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="TextBoxTax"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gross Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxGrossAmount" runat="server" CssClass="textbox"
                                                                                    MaxLength="20" Width="100px" TabIndex="103"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                                                                                    ControlToValidate="TextBoxGrossAmount" Display="Dynamic"
                                                                                    ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator17" runat="server"
                                                                                    ControlToValidate="TextBoxGrossAmount" Display="Dynamic" ErrorMessage="Invalid"
                                                                                    MaximumValue="99999999999" MinimumValue="0" Type="Double"
                                                                                    ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                            <td>Net Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxNetAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="107"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="LabelItemError" runat="server" Style="font-weight: 700; color: #FF0000"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td class="pageButtonArea" colspan="3">
                                                                                <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                                                                                    Text="Add" ValidationGroup="ItemAdd" Width="70px" TabIndex="108" />
                                                                                &nbsp;
                                                                                <asp:Button ID="ButtonItemClear" runat="server" CssClass="button" OnClick="ButtonItemClear_Click" Text="Clear" Width="70px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Service(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table style="width: 100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <asp:GridView ID="GridViewServiceAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                                    OnRowDataBound="GridViewServiceAdd_RowDataBound" OnRowCommand="GridViewServiceAdd_RowCommand"
                                                                                    DataKeyNames="ID" AutoGenerateColumns="False">
                                                                                    <PagerSettings Mode="NumericFirstLast" />
                                                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                                                    <Columns>
                                                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="10px" />
                                                                                        </asp:ButtonField>
                                                                                        <asp:BoundField HeaderText="Service">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Gross">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Discount %">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Discount">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Tax">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Net">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid-pager" />
                                                                                    <EmptyDataTemplate>
                                                                                        <div id="norec" class="no-record-msg">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="padding-left: 15px">No Items(s) Found!
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </EmptyDataTemplate>
                                                                                    <HeaderStyle CssClass="grid-header" />
                                                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">Service <span style="color: red">*</span>
                                                                            </td>
                                                                            <td style="width: 40%">
                                                                                <asp:TextBox ID="TextBoxService" runat="server" CssClass="textbox" MaxLength="400" Width="250px" TabIndex="200"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxService"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ServiceAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Discount Percentage
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxServiceDiscountPercentage" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="calcServiceDiscountAmt();" TabIndex="203"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="TextBoxServiceDiscountPercentage"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ServiceAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxServiceAmount" runat="server" CssClass="textbox"
                                                                                    MaxLength="20" Width="100px" TabIndex="201" onkeyup="serviceTotCalc();"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                                                                    ControlToValidate="TextBoxServiceAmount" Display="Dynamic"
                                                                                    ErrorMessage="Required" ValidationGroup="ServiceAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator9" runat="server"
                                                                                    ControlToValidate="TextBoxServiceAmount" Display="Dynamic" ErrorMessage="Invalid"
                                                                                    MaximumValue="99999999999" MinimumValue="0" Type="Double"
                                                                                    ValidationGroup="ServiceAdd"></asp:RangeValidator>
                                                                            </td>
                                                                            <td>Discount
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxServiceDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="serviceTotCalc();" TabIndex="204"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="TextBoxServiceDiscount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ServiceAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Tax
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxServiceTax" runat="server" CssClass="textbox" MaxLength="20" Width="100px" onkeyup="serviceTotCalc();" TabIndex="202"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="TextBoxServiceTax"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ServiceAdd"></asp:RangeValidator>
                                                                            </td>
                                                                            <td>Net Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxServiceNetAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="205"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxServiceNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ServiceAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="TextBoxServiceNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ServiceAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="LabelServiceError" runat="server" Style="font-weight: 700; color: #FF0000"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td class="pageButtonArea" colspan="3">
                                                                                <asp:Button ID="ButtonServiceAdd" runat="server" CssClass="button" OnClick="ButtonServiceAdd_Click"
                                                                                    Text="Add" ValidationGroup="ServiceAdd" Width="70px" TabIndex="206" />
                                                                                &nbsp;
                                                                                <asp:Button ID="ButtonServiceClear" runat="server" CssClass="button" OnClick="ButtonServiceClear_Click" Text="Clear" Width="70px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanelSummary" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 15%">Total Gross <span style="color: Red;">*</span>
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <asp:TextBox ID="TextBoxTotalGross" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                                Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                        <td style="width: 15%">Total Tax
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <asp:TextBox ID="TextBoxTotalTax" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="TextBoxTax"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Net <span style="color: Red;">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxTotalNet" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                                Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                        <td>Total Discount
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxTotalDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RangeValidator ID="RangeValidator22" runat="server" ControlToValidate="TextBoxTotalDiscount"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="vertical-align: text-top; padding-top: 3px">Remarks
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Payment</legend>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanelPayment" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 15%">Payment Type
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="DropDownListPaymentType" runat="server" CssClass="dropdownlist-small" AutoPostBack="True" OnSelectedIndexChanged="DropDownListPaymentType_SelectedIndexChanged" Width="100px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Paid Amount 
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxPaidAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="TextBoxPaidAmount" Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0" Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="TRCardNumber" visible="false">
                                                                        <td>Card Number 
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxCardNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                                                Width="250px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="TRCardName" visible="false">
                                                                        <td>Card Name 
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxCardName" runat="server" CssClass="textbox" MaxLength="50"
                                                                                Width="250px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="TRCardType" visible="false">
                                                                        <td>Card Type 
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxCardType" runat="server" CssClass="textbox" MaxLength="50"
                                                                                Width="250px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td class="pageButtonArea">
                                                            <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Approve" ValidationGroup="submit"
                                                                OnClick="ButtonSubmit_Click" Width="70px" />
                                                            &nbsp;<asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                                                                OnClick="ButtonReset_Click" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchDetails" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Search Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">Job Number
                                                                </td>
                                                                <td>&nbsp;<asp:TextBox ID="TextBoxSrchJobNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                    Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">Vehicle Number
                                                                </td>
                                                                <td>&nbsp;<asp:TextBox ID="TextBoxSrchVehicleNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                    Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Customer
                                                                </td>
                                                                <td>&nbsp;<asp:TextBox ID="TextBoxSrchCustomer" runat="server" CssClass="textbox" MaxLength="250" Width="250px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Pending Only</td>
                                                                <td>
                                                                    <asp:CheckBox ID="CheckBoxSrchApprovedOnly" runat="server" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="pageButtonArea">&nbsp;<asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                                                                    Width="70px" ValidationGroup="search" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
                                                    OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
                                                    OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                    <Columns>
                                                        <asp:ButtonField CommandName="ApproveRecord" Text="Approve">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="ViewRecord" Text="View">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField HeaderText="Job Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Vehicle Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Customer">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Start Date">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="End Date">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Net Amount">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Approved">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="grid-pager" />
                                                    <EmptyDataTemplate>
                                                        <div id="norec" class="no-record-msg">
                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-left: 15px">No Record(s) Found!
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="grid-header" />
                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Job Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 15%;">Job Number
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 34%;">
                                                                    <asp:Label ID="LabelViewJobNumber" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%;">Vehicle Number
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 34%;">
                                                                    <asp:Label ID="LabelViewVehicleNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Start Date
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewStartDate" runat="server"></asp:Label>
                                                                </td>
                                                                <td>Customer 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewCustomer" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>End Date 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewEndDate" runat="server"></asp:Label>
                                                                </td>
                                                                <td>Address 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewAddress" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supervisor 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewSupervisor" runat="server"></asp:Label>
                                                                </td>
                                                                <td>Telephone 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTelephone" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Status 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:Label ID="LabelViewStatus" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Remarks 
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 15%;">Total Gross
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 34%;">
                                                                    <asp:Label ID="LabelViewTotalGross" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 15%;">Total Tax
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 34%;">
                                                                    <asp:Label ID="LabelViewTotalTax" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Net
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalNet" runat="server"></asp:Label>
                                                                </td>
                                                                <td>Total Discount
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalDiscount" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="Product Item">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Quantity">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Selling Price">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Gross">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Discount %">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Discount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Tax">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Net">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Service(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewServiceView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewServiceView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="Service">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Gross">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Discount %">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Discount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Tax">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Net">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">Payment</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewPaymentView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewPaymentView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="Payment Type">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Paid Amount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Card Number">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Card Name">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Card Type">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
    <%--<script language="javascript">
        $("#ctl00_ContentPlaceHolder1_TextBoxDateGRN").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDatePO").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateManufactured").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateExpiry").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateGRN").mask("99/99/9999");
    </script>--%>
</asp:Content>

