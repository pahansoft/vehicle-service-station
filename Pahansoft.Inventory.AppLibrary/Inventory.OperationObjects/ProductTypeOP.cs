﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ProductTypeOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveProductType(Inventory.SolutionObjects.ProductType productType)
        {
            if (productType.ID == null)
                return new Inventory.DataAccessObjects.ProductTypeDO().Create(productType);
            else
                return new Inventory.DataAccessObjects.ProductTypeDO().Edit(productType);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteProductType(Inventory.SolutionObjects.ProductType productType)
        {
            if (productType.ID != null)
                return new Inventory.DataAccessObjects.ProductTypeDO().Delete(productType);
            else
            {
                productType.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = productType;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.ProductType GetProductType(int id)
        {
            return new Inventory.DataAccessObjects.ProductTypeDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductType> GetAllProductTypes()
        {
            return new Inventory.DataAccessObjects.ProductTypeDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ProductType> SearchProductTypes(string description)
        {
            return new Inventory.DataAccessObjects.ProductTypeDO().Search(description);
        }
    }
}
