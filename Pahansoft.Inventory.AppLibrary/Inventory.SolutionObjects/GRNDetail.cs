﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class GRNDetail : SolutionObjectBase
    {
        public GRNDetail() { }
        public GRNDetail(int? id) { this.ID = id; }

        public SolutionObjects.GRN GRN { get; set; }
        public SolutionObjects.ProductItem ProductItem { get; set; }
        public SolutionObjects.StockTypes? StockType { get; set; }
        public string LotNumber { get; set; }
        public SolutionObjects.Store Store { get; set; }
        public SolutionObjects.Location Location { get; set; }
        public SolutionObjects.Rack Rack { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? SellingPrice { get; set; }
        public DateTime? DateManufactured { get; set; }
        public DateTime? DateExpiry { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? NetAmount { get; set; }
        public string Remarks { get; set; }
        //Add for sales return
        public decimal? ReturnedQuantity { get; set; }
        public decimal? FreeQuantity { get; set; }
        public SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail { get; set; }

        // Read only property for stock update
        public bool IsNewRec { get; set; }

    }
}
