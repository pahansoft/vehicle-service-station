﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_stor_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.Store _store;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxDescription.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchDescription.Text = string.Empty;
    }

    private void Search()
    {
        GridViewSearchList.DataSource = new OperationObjects.StoreOP().SearchStores(TextBoxSrchDescription.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            ViewState[_target] = new SolutionObjects.Store();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.Store();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _store = ViewState[_target] as SolutionObjects.Store;
        _store.Description = TextBoxDescription.Text.Trim();
        _store.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.StoreOP().SaveStore(_store);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.Store();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _store = ViewState[_target] as SolutionObjects.Store;

        if (_store.ID != null)
        {
            _store = new OperationObjects.StoreOP().GetStore(_store.ID.Value);
            ViewState[_target] = _store;

            TextBoxDescription.Text = _store.Description;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.Store();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _store = new OperationObjects.StoreOP().GetStore(_targetid);

            ClearAll();
            ViewState[_target] = _store;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxDescription.Text = _store.Description;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _store = new OperationObjects.StoreOP().GetStore(_targetid);
            _store.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.StoreOP().DeleteStore(_store);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _store = e.Row.DataItem as SolutionObjects.Store;
            Util.EncodeString(e.Row.Cells[2], _store.Description, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _store.Description + "');");
                }
            }
        }
    }
}