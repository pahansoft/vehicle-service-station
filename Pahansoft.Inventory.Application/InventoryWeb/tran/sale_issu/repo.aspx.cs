﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_sale_issu_repo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int? _salesIssueID = Util.ParseInt(Request.Params["SalesIssueID"].ToString());
        Generate(_salesIssueID.Value);
    }

    private void Generate(int salesIssueID)
    {
        SolutionObjects.SalesIssue _salesIssue = new OperationObjects.SalesIssueOP().GetSalesIssue(salesIssueID);
        _salesIssue.Customer = new OperationObjects.CustomerOP().GetCustomer(_salesIssue.Customer.ID.Value);

        StringBuilder _sb = new StringBuilder();

        _sb.Append("<div style=\"page-break-after: always;\">");
        _sb.Append("<table style=\"width: 100%; font-family:Courier New;\">");

        #region - Page Header -

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");
        _sb.Append("<table style=\"width: 100%;\">");

        _sb.Append("<tr>");
        _sb.Append("<td colspan=\"6\" style=\"text-align: center; font-size:18px; font-weight:bold;\">PRESTIGE HOMES & POOL BUILDERS (PVT) LTD</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td colspan=\"6\" style=\"text-align: center; font-size:25px; font-weight:bold;\">SALES INVOICE</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td colspan=\"6\" style=\"text-align: center; font-size:18px; font-weight:bold;\">&nbsp;</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td style=\"width : 13%\"></td>");
        _sb.Append("<td style=\"width : 2%\"></td>");
        _sb.Append("<td style=\"width : 35%\"></td>");
        _sb.Append("<td style=\"width : 13%\"></td>");
        _sb.Append("<td style=\"width : 2%\"></td>");
        _sb.Append("<td style=\"width : 35%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td style=\"width : 13%\">ISSUE NUMBER</td>");
        _sb.Append("<td style=\"width : 2%\">:</td>");
        _sb.Append("<td style=\"width : 35%\">" + _salesIssue.IssueNumber + "</td>");
        _sb.Append("<td>DATE ISSUE</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + _salesIssue.DateIssue.Value.ToShortDateString() + "</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td>CUSTOMER</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + _salesIssue.Customer.Name.ToUpper() + "</td>");
        _sb.Append("<td>INITIAL PAYMENT</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + (_salesIssue.InitialPayment != null ? _salesIssue.InitialPayment.Value.ToString("F") : "-") + "</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td>TOTAL GROSS</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + (_salesIssue.TotalGross != null ? _salesIssue.TotalGross.Value.ToString("F") : "-") + "</td>");
        _sb.Append("<td>TOTAL DISCOUNT</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + (_salesIssue.TotalDiscount != null ? _salesIssue.TotalDiscount.Value.ToString("F") : "-") + "</td>");
        _sb.Append("</tr>");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td>TOTAL NET</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + (_salesIssue.TotalNet != null ? _salesIssue.TotalNet.Value.ToString("F") : "-") + "</td>");
        _sb.Append("<td>TOTAL TAX</td>");
        _sb.Append("<td>:</td>");
        _sb.Append("<td>" + (_salesIssue.TotalTax != null ? _salesIssue.TotalTax.Value.ToString("F") : "-") + "</td>");
        _sb.Append("</tr>");

        _sb.Append("</table>");
        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #endregion

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">&nbsp;</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");
        _sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\" cellpadding=\"3\">");

        _sb.Append("<tr style=\"font-size:small; font-weight:bold;\">");
        _sb.Append("<td style=\"width : 50%\">ITEM</td>");
        _sb.Append("<td style=\"width : 10%; text-align:right;\">QUANTITY</td>");
        _sb.Append("<td style=\"width : 10%; text-align:right;\">DISCOUNT</td>");
        _sb.Append("<td style=\"width : 10%; text-align:right;\">TAX</td>");
        _sb.Append("<td style=\"width : 10%; text-align:right;\">GROSS</td>");
        _sb.Append("<td style=\"width : 10%; text-align:right;\">NET</td>");
        _sb.Append("</tr>");

        foreach (SolutionObjects.SalesIssueItem salesIssueItem in _salesIssue.SalesIssueItemList)
        {
            salesIssueItem.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(salesIssueItem.ItemStock.ID.Value);
            salesIssueItem.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(salesIssueItem.ItemStock.ProductItem.ID.Value);

            _sb.Append("<tr style=\"font-size:small;\">");
            _sb.Append("<td>" + (salesIssueItem.ItemStock.ProductItem.Code.ToUpper() + "-" + salesIssueItem.ItemStock.ProductItem.Description.ToUpper()) + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + salesIssueItem.IssuedQuantity.Value.ToString() + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + (salesIssueItem.Discount != null ? salesIssueItem.Discount.Value.ToString("F") : "-") + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + (salesIssueItem.Tax != null ? salesIssueItem.Tax.Value.ToString("F") : "-") + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + (salesIssueItem.Gross != null ? salesIssueItem.Gross.Value.ToString("F") : "-") + "</td>");
            _sb.Append("<td style=\"text-align:right;\">" + (salesIssueItem.Net != null ? salesIssueItem.Net.Value.ToString("F") : "-") + "</td>");
            _sb.Append("</tr>");
        }

        _sb.Append("</table>");
        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">&nbsp;</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #region - Footer -

        _sb.Append("<tr>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("<td style=\"width: 96%;\">");

        _sb.Append("<table style=\"width: 100%;\">");
        _sb.Append("<tr style=\"font-size:11px;\">");
        _sb.Append("<td style=\"width : 85%; text-align: center;\">THIS IS A COMPUTER GENERATED DOCUMENT</td>");
        _sb.Append("<td style=\"width : 15%\">" + DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "</td>");
        _sb.Append("</tr>");

        _sb.Append("</td>");
        _sb.Append("<td style=\"width: 2%\"></td>");
        _sb.Append("</tr>");

        #endregion

        _sb.Append("</table>");
        _sb.Append("</div>");

        LabelReportBody.Text = _sb.ToString();
    }

}