﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class JobCardOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(SolutionObjects.JobCard JobCard)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (JobCard.ID == null)
                {
                    dto = new DataAccessObjects.JobCardDO().Create(JobCard);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new JobCardItem List
                    foreach (var item in JobCard.ItemList)
                    {
                        innerDTO = new DataAccessObjects.JobCardItemDO().Create(item);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add new JobCardService List
                    foreach (var item in JobCard.ServiceList)
                    {
                        innerDTO = new DataAccessObjects.JobCardServiceDO().Create(item);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new DataAccessObjects.JobCardDO().Edit(JobCard);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit JobCardItem List
                    foreach (var item in JobCard.ItemList)
                    {
                        if (item.ID == null)
                            innerDTO = new DataAccessObjects.JobCardItemDO().Create(item);
                        else
                            innerDTO = new DataAccessObjects.JobCardItemDO().Edit(item);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                    // Delete JobCardItem List
                    foreach (var item in JobCard.ItemList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.JobCardItemDO().Delete(item);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add, Edit JobCardService List
                    foreach (var item in JobCard.ServiceList)
                    {
                        if (item.ID == null)
                            innerDTO = new DataAccessObjects.JobCardServiceDO().Create(item);
                        else
                            innerDTO = new DataAccessObjects.JobCardServiceDO().Edit(item);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                    // Delete JobCardService List
                    foreach (var item in JobCard.ServiceList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.JobCardServiceDO().Delete(item);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject Approve(SolutionObjects.JobCard JobCard, SolutionObjects.CustomerPayment Payment)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            var CustomerPayment = new SolutionObjects.CustomerPayment();
            CustomerPayment.Customer = JobCard.Customer;
            CustomerPayment.DebitCreditType = SolutionObjects.DebitCreditTypes.Dr;
            CustomerPayment.Amount = JobCard.TotalNet;
            CustomerPayment.JobCardID = JobCard.ID.Value;
            CustomerPayment.IsCash = true;
            CustomerPayment.User = JobCard.User;

            SolutionObjects.VehicleDetail VehicleDetail = null;
            if (JobCard.VehicleNumber != string.Empty)
            {
                var VehicleDetails = new DataAccessObjects.VehicleDetailDO().Search(JobCard.VehicleNumber, null);
                if (VehicleDetails.Count == 1)
                {
                    VehicleDetail = VehicleDetails[0];
                    VehicleDetail.Customer = new SolutionObjects.Customer(JobCard.Customer.ID.Value);
                    VehicleDetail.User = JobCard.User;
                }
                else
                {
                    VehicleDetail = new SolutionObjects.VehicleDetail();
                    VehicleDetail.VehicleNumber = JobCard.VehicleNumber;
                    VehicleDetail.Customer = new SolutionObjects.Customer(JobCard.Customer.ID.Value);
                    VehicleDetail.User = JobCard.User;
                }
            }

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new DataAccessObjects.JobCardDO().Edit(JobCard);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                if (VehicleDetail != null)
                {
                    if (VehicleDetail.ID == null)
                        innerDTO = new DataAccessObjects.VehicleDetailDO().Create(VehicleDetail);
                    else
                        innerDTO = new DataAccessObjects.VehicleDetailDO().Edit(VehicleDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Add, Edit JobCardItem List
                foreach (var item in JobCard.ItemList)
                {
                    if (item.ID == null)
                        innerDTO = new DataAccessObjects.JobCardItemDO().Create(item);
                    else
                        innerDTO = new DataAccessObjects.JobCardItemDO().Edit(item);

                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }
                // Delete JobCardItem List
                foreach (var item in JobCard.ItemList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.JobCardItemDO().Delete(item);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Add, Edit JobCardService List
                foreach (var item in JobCard.ServiceList)
                {
                    if (item.ID == null)
                        innerDTO = new DataAccessObjects.JobCardServiceDO().Create(item);
                    else
                        innerDTO = new DataAccessObjects.JobCardServiceDO().Edit(item);

                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }
                // Delete JobCardService List
                foreach (var item in JobCard.ServiceList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.JobCardServiceDO().Delete(item);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Create Customer Payments DR
                innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(CustomerPayment);
                if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(innerDTO.Message);

                // Create Customer Payments CR
                if (Payment != null)
                {
                    innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(Payment);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                CreateItemStock(JobCard);

                // Add, Edit ItemStock List
                foreach (var item in JobCard.ItemStockList)
                {
                    innerDTO = new ItemStockOP().SaveItemStock(item);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.JobCard JobCard)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new DataAccessObjects.JobCardDO().Delete(JobCard);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete JobCardItem List
                foreach (var item in JobCard.ItemList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.JobCardItemDO().Delete(item);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public SolutionObjects.JobCard Get(int id)
        {
            return new DataAccessObjects.JobCardDO().Find(id);
        }

        public SolutionObjects.JobCard GetProxy(int id)
        {
            return new DataAccessObjects.JobCardDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCard> GetAll()
        {
            return new DataAccessObjects.JobCardDO().GetAll();
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCard> Search(string JobNumber, string VehicleNumber, int? CustomerID, bool? ApproveOnly)
        {
            return new DataAccessObjects.JobCardDO().Search(JobNumber, VehicleNumber, CustomerID, ApproveOnly);
        }

        private SolutionObjects.JobCard CreateItemStock(SolutionObjects.JobCard JobCard)
        {
            JobCard.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            foreach (var item in JobCard.ItemList)
            {
                var itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(item.ItemStock.ID.Value);
                itemStock.CurrentQuantity -= item.Quantity;
                itemStock.User = item.User;
                itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                var itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                itemStockTransaction.ItemStock = itemStock;
                itemStockTransaction.ProductItem = itemStock.ProductItem;
                itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.JobCard;
                itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                itemStockTransaction.TransactionID = JobCard.ID;
                itemStockTransaction.TransactionDetailID = item.ID;
                itemStockTransaction.ReferenceNumber = item.JobCard.JobNumber;
                itemStockTransaction.Store = itemStock.Store;
                itemStockTransaction.Location = itemStock.Location;
                itemStockTransaction.Rack = itemStock.Rack;
                itemStockTransaction.Quantity = item.Quantity;
                itemStockTransaction.UnitPrice = itemStock.UnitPrice;
                itemStockTransaction.SellingPrice = item.UnitPrice;
                itemStockTransaction.User = JobCard.User;
                itemStock.ItemStockTransactionList.Add(itemStockTransaction);

                JobCard.ItemStockList.Add(itemStock);
            }

            return JobCard;
        }
    }
}
