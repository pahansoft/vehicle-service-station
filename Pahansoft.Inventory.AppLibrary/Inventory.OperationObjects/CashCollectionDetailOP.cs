﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CashCollectionDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCashCollectionDetail(Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail)
        {
            if (cashCollectionDetail.ID == null)
                return new Inventory.DataAccessObjects.CashCollectionDetailDO().Create(cashCollectionDetail);
            else
                return new Inventory.DataAccessObjects.CashCollectionDetailDO().Edit(cashCollectionDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCashCollectionDetail(Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail)
        {
            if (cashCollectionDetail.ID != null)
                return new Inventory.DataAccessObjects.CashCollectionDetailDO().Delete(cashCollectionDetail);
            else
            {
                cashCollectionDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = cashCollectionDetail;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.CashCollectionDetail GetCashCollectionDetail(int id)
        {
            return new Inventory.DataAccessObjects.CashCollectionDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> GetAllCashCollectionDetails()
        {
            return new Inventory.DataAccessObjects.CashCollectionDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> GetAllFor(SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            return new Inventory.DataAccessObjects.CashCollectionDetailDO().GetAllFor(cashCollectionSummary);
        }
    }
}
