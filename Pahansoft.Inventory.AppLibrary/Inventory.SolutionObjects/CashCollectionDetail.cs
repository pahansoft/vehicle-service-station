﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class CashCollectionDetail : SolutionObjectBase
    {
        public CashCollectionDetail() { }
        public CashCollectionDetail(int? id) { this.ID = id; }

        public SolutionObjects.CashCollectionSummary CashCollectionSummary { get; set; }
        public SolutionObjects.SalesIssue SalesIssue { get; set; }
        public decimal? Amount { get; set; }
    }
}
