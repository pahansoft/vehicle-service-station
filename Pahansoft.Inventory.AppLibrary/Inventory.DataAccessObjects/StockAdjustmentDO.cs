﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class StockAdjustmentDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AdjustmentNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.AdjustmentNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateAdjustment", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, stockAdjustment.DateAdjustment);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovalStatus", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.ApprovalStatus != null ? stockAdjustment.ApprovalStatus.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, stockAdjustment.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UserApproved", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.UserApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedReason", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.ApprovedReason);
            
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustment;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                stockAdjustment.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustment.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, stockAdjustment.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AdjustmentNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.AdjustmentNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateAdjustment", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, stockAdjustment.DateAdjustment);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovalStatus", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.ApprovalStatus != null ? stockAdjustment.ApprovalStatus.Value.ToString() : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateApproved", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, stockAdjustment.DateApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UserApproved", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.UserApproved);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovedReason", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.ApprovedReason);
           
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustment;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.StockAdjustment stockAdjustment)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, stockAdjustment.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, stockAdjustment.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, stockAdjustment.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = stockAdjustment;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                stockAdjustment.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.StockAdjustment Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.StockAdjustment stockAdjustment = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int adjustmentNumberOrdinal = dr.GetOrdinal("AdjustmentNumber");
                        int dateAdjustmentOrdinal = dr.GetOrdinal("DateAdjustment");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int approvalStatusOrdinal = dr.GetOrdinal("ApprovalStatus");
                        int dateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int userApprovedOrdinal = dr.GetOrdinal("UserApproved");
                        int approvedReasonOrdinal = dr.GetOrdinal("ApprovedReason");

                        stockAdjustment = new Inventory.SolutionObjects.StockAdjustment(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustment.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(adjustmentNumberOrdinal)) stockAdjustment.AdjustmentNumber = dr.GetString(adjustmentNumberOrdinal);
                        if (!dr.IsDBNull(dateAdjustmentOrdinal)) stockAdjustment.DateAdjustment = dr.GetDateTime(dateAdjustmentOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustment.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(approvalStatusOrdinal)) stockAdjustment.ApprovalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), dr.GetString(approvalStatusOrdinal));
                        if (!dr.IsDBNull(dateApprovedOrdinal)) stockAdjustment.DateApproved = dr.GetDateTime(dateApprovedOrdinal);
                        if (!dr.IsDBNull(userApprovedOrdinal)) stockAdjustment.UserApproved = dr.GetString(userApprovedOrdinal);
                        if (!dr.IsDBNull(approvedReasonOrdinal)) stockAdjustment.ApprovedReason = dr.GetString(approvedReasonOrdinal);

                        stockAdjustment.StockAdjustmentDetailList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.StockAdjustmentDetail>();
                        foreach (SolutionObjects.StockAdjustmentDetail gRNDetail in new DataAccessObjects.StockAdjustmentDetailDO().GetAllFor(stockAdjustment))
                            stockAdjustment.StockAdjustmentDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustment;
        }

        public Inventory.SolutionObjects.StockAdjustment FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetStockAdjustment";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.StockAdjustment stockAdjustment = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int adjustmentNumberOrdinal = dr.GetOrdinal("AdjustmentNumber");
                        int dateAdjustmentOrdinal = dr.GetOrdinal("DateAdjustment");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int approvalStatusOrdinal = dr.GetOrdinal("ApprovalStatus");
                        int dateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                        int userApprovedOrdinal = dr.GetOrdinal("UserApproved");
                        int approvedReasonOrdinal = dr.GetOrdinal("ApprovedReason");

                        stockAdjustment = new Inventory.SolutionObjects.StockAdjustment(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustment.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(adjustmentNumberOrdinal)) stockAdjustment.AdjustmentNumber = dr.GetString(adjustmentNumberOrdinal);
                        if (!dr.IsDBNull(dateAdjustmentOrdinal)) stockAdjustment.DateAdjustment = dr.GetDateTime(dateAdjustmentOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustment.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(approvalStatusOrdinal)) stockAdjustment.ApprovalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), dr.GetString(approvalStatusOrdinal));
                        if (!dr.IsDBNull(dateApprovedOrdinal)) stockAdjustment.DateApproved = dr.GetDateTime(dateApprovedOrdinal);
                        if (!dr.IsDBNull(userApprovedOrdinal)) stockAdjustment.UserApproved = dr.GetString(userApprovedOrdinal);
                        if (!dr.IsDBNull(approvedReasonOrdinal)) stockAdjustment.ApprovedReason = dr.GetString(approvedReasonOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustment;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllStockAdjustments";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> stockAdjustmentList = new System.Collections.Generic.List<Inventory.SolutionObjects.StockAdjustment>();
            Inventory.SolutionObjects.StockAdjustment stockAdjustment;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int adjustmentNumberOrdinal = dr.GetOrdinal("AdjustmentNumber");
                    int dateAdjustmentOrdinal = dr.GetOrdinal("DateAdjustment");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int approvalStatusOrdinal = dr.GetOrdinal("ApprovalStatus");
                    int dateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int userApprovedOrdinal = dr.GetOrdinal("UserApproved");
                    int approvedReasonOrdinal = dr.GetOrdinal("ApprovedReason");

                    while (dr.Read())
                    {
                        stockAdjustment = new Inventory.SolutionObjects.StockAdjustment(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustment.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(adjustmentNumberOrdinal)) stockAdjustment.AdjustmentNumber = dr.GetString(adjustmentNumberOrdinal);
                        if (!dr.IsDBNull(dateAdjustmentOrdinal)) stockAdjustment.DateAdjustment = dr.GetDateTime(dateAdjustmentOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustment.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(approvalStatusOrdinal)) stockAdjustment.ApprovalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), dr.GetString(approvalStatusOrdinal));
                        if (!dr.IsDBNull(dateApprovedOrdinal)) stockAdjustment.DateApproved = dr.GetDateTime(dateApprovedOrdinal);
                        if (!dr.IsDBNull(userApprovedOrdinal)) stockAdjustment.UserApproved = dr.GetString(userApprovedOrdinal);
                        if (!dr.IsDBNull(approvedReasonOrdinal)) stockAdjustment.ApprovedReason = dr.GetString(approvedReasonOrdinal);

                        stockAdjustmentList.Add(stockAdjustment);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustmentList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> Search(string adjustmentNumber, DateTime? dateAdjustment, SolutionObjects.ApprovalStatusses? approvalStatus)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllStockAdjustments";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@AdjustmentNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, adjustmentNumber != string.Empty ? adjustmentNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateAdjustment", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, dateAdjustment != null ? dateAdjustment : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ApprovalStatus", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, approvalStatus != null ? approvalStatus.Value.ToString() : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.StockAdjustment> stockAdjustmentList = new System.Collections.Generic.List<Inventory.SolutionObjects.StockAdjustment>();
            Inventory.SolutionObjects.StockAdjustment stockAdjustment;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int adjustmentNumberOrdinal = dr.GetOrdinal("AdjustmentNumber");
                    int dateAdjustmentOrdinal = dr.GetOrdinal("DateAdjustment");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int approvalStatusOrdinal = dr.GetOrdinal("ApprovalStatus");
                    int dateApprovedOrdinal = dr.GetOrdinal("DateApproved");
                    int userApprovedOrdinal = dr.GetOrdinal("UserApproved");
                    int approvedReasonOrdinal = dr.GetOrdinal("ApprovedReason");

                    while (dr.Read())
                    {
                        stockAdjustment = new Inventory.SolutionObjects.StockAdjustment(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) stockAdjustment.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(adjustmentNumberOrdinal)) stockAdjustment.AdjustmentNumber = dr.GetString(adjustmentNumberOrdinal);
                        if (!dr.IsDBNull(dateAdjustmentOrdinal)) stockAdjustment.DateAdjustment = dr.GetDateTime(dateAdjustmentOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) stockAdjustment.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(approvalStatusOrdinal)) stockAdjustment.ApprovalStatus = (SolutionObjects.ApprovalStatusses)Enum.Parse(typeof(SolutionObjects.ApprovalStatusses), dr.GetString(approvalStatusOrdinal));
                        if (!dr.IsDBNull(dateApprovedOrdinal)) stockAdjustment.DateApproved = dr.GetDateTime(dateApprovedOrdinal);
                        if (!dr.IsDBNull(userApprovedOrdinal)) stockAdjustment.UserApproved = dr.GetString(userApprovedOrdinal);
                        if (!dr.IsDBNull(approvedReasonOrdinal)) stockAdjustment.ApprovedReason = dr.GetString(approvedReasonOrdinal);

                        stockAdjustmentList.Add(stockAdjustment);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return stockAdjustmentList;
        }

        #endregion
    }
}
