﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class PurchaseReturnItemDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.PurchaseReturnItem gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreatePurchaseReturnItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseReturnID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.PurchaseReturn != null ? gRNDetail.PurchaseReturn.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.GRNDetail != null ? gRNDetail.GRNDetail.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedPrice);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                gRNDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.PurchaseReturnItem gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditPurchaseReturnItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, gRNDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseReturnID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.PurchaseReturn != null ? gRNDetail.PurchaseReturn.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GRNDetailID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.GRNDetail != null ? gRNDetail.GRNDetail.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedQuantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedQuantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ReturnedPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, gRNDetail.ReturnedPrice);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.PurchaseReturnItem gRNDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeletePurchaseReturnItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, gRNDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, gRNDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, gRNDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = gRNDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                gRNDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.PurchaseReturnItem Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseReturnItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.PurchaseReturnItem gRNDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int purchaseReturnIDOrdinal = dr.GetOrdinal("PurchaseReturnID");
                        int gRNDetailIDOrdinal = dr.GetOrdinal("GRNDetailID");
                        int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                        int returnedPriceOrdinal = dr.GetOrdinal("ReturnedPrice");

                        gRNDetail = new Inventory.SolutionObjects.PurchaseReturnItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(purchaseReturnIDOrdinal)) gRNDetail.PurchaseReturn = new SolutionObjects.PurchaseReturn(dr.GetInt32(purchaseReturnIDOrdinal));
                        if (!dr.IsDBNull(gRNDetailIDOrdinal)) gRNDetail.GRNDetail = new SolutionObjects.GRNDetail(dr.GetInt32(gRNDetailIDOrdinal));
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetDecimal(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(returnedPriceOrdinal)) gRNDetail.ReturnedPrice = dr.GetDecimal(returnedPriceOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseReturnItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> gRNDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.PurchaseReturnItem>();
            Inventory.SolutionObjects.PurchaseReturnItem gRNDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int purchaseReturnIDOrdinal = dr.GetOrdinal("PurchaseReturnID");
                    int gRNDetailIDOrdinal = dr.GetOrdinal("GRNDetailID");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                    int returnedPriceOrdinal = dr.GetOrdinal("ReturnedPrice");

                    while (dr.Read())
                    {
                        gRNDetail = new Inventory.SolutionObjects.PurchaseReturnItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(purchaseReturnIDOrdinal)) gRNDetail.PurchaseReturn = new SolutionObjects.PurchaseReturn(dr.GetInt32(purchaseReturnIDOrdinal));
                        if (!dr.IsDBNull(gRNDetailIDOrdinal)) gRNDetail.GRNDetail = new SolutionObjects.GRNDetail(dr.GetInt32(gRNDetailIDOrdinal));
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetInt32(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(returnedPriceOrdinal)) gRNDetail.ReturnedPrice = dr.GetDecimal(returnedPriceOrdinal);

                        gRNDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetailList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> GetAllFor(SolutionObjects.PurchaseReturn purchaseReturn)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseReturnItemsForPurchaseReturn";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PurchaseReturnID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, purchaseReturn.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> gRNDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.PurchaseReturnItem>();
            Inventory.SolutionObjects.PurchaseReturnItem gRNDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int purchaseReturnIDOrdinal = dr.GetOrdinal("PurchaseReturnID");
                    int gRNDetailIDOrdinal = dr.GetOrdinal("GRNDetailID");
                    int returnedQuantityOrdinal = dr.GetOrdinal("ReturnedQuantity");
                    int returnedPriceOrdinal = dr.GetOrdinal("ReturnedPrice");

                    while (dr.Read())
                    {
                        gRNDetail = new Inventory.SolutionObjects.PurchaseReturnItem(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) gRNDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(purchaseReturnIDOrdinal)) gRNDetail.PurchaseReturn = new SolutionObjects.PurchaseReturn(dr.GetInt32(purchaseReturnIDOrdinal));
                        if (!dr.IsDBNull(gRNDetailIDOrdinal)) gRNDetail.GRNDetail = new SolutionObjects.GRNDetail(dr.GetInt32(gRNDetailIDOrdinal));
                        if (!dr.IsDBNull(returnedQuantityOrdinal)) gRNDetail.ReturnedQuantity = dr.GetInt32(returnedQuantityOrdinal);
                        if (!dr.IsDBNull(returnedPriceOrdinal)) gRNDetail.ReturnedPrice = dr.GetDecimal(returnedPriceOrdinal);

                        gRNDetailList.Add(gRNDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return gRNDetailList;
        }

        #endregion
    }
}
