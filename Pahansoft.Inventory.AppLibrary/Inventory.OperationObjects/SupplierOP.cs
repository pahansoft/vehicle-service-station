﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class SupplierOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveSupplier(Inventory.SolutionObjects.Supplier supplier)
        {
            if (supplier.ID == null)
                return new Inventory.DataAccessObjects.SupplierDO().Create(supplier);
            else
                return new Inventory.DataAccessObjects.SupplierDO().Edit(supplier);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteSupplier(Inventory.SolutionObjects.Supplier supplier)
        {
            if (supplier.ID != null)
                return new Inventory.DataAccessObjects.SupplierDO().Delete(supplier);
            else
            {
                supplier.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = supplier;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Supplier GetSupplier(int id)
        {
            return new Inventory.DataAccessObjects.SupplierDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> GetAllSuppliers()
        {
            return new Inventory.DataAccessObjects.SupplierDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Supplier> SearchSuppliers(string code, string name)
        {
            return new Inventory.DataAccessObjects.SupplierDO().Search(code, name);
        }
    }
}
