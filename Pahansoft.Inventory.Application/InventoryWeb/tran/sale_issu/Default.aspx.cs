﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.UI.HtmlControls;

#endregion

public partial class tran_sale_issu_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.SalesIssue _salesIssue;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxIssueNumber.Text = string.Empty;
        TextBoxDateIssue.Text = DateTime.Today.ToShortDateString();
        DropDownListCustomer.SelectedIndex = 0;
        TextBoxTotalGross.Text = string.Empty;
        TextBoxTotalTax.Text = string.Empty;
        TextBoxTotalDiscount.Text = string.Empty;
        TextBoxTotalNet.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;
        CheckBoxCreditSale.Checked = false;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        Util.ClearDropdown(DropDownListProductItem);
        LabelAvailableQuantity.Text = "-";
        LabelSellingPrice.Text = "-";
        LabelLocation.Text = "-";
        TextBoxQuantity.Text = string.Empty;
        TextBoxDiscount.Text = string.Empty;
        TextBoxTax.Text = string.Empty;
        TextBoxGrossAmount.Text = string.Empty;
        TextBoxNetAmount.Text = string.Empty;

        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
    }

    private void ClearSearch()
    {
        TextBoxSrchIssueNumber.Text = string.Empty;
        TextBoxSrchCardNumber.Text = string.Empty;
        TextBoxSrchDateIssue.Text = string.Empty;
        DropDownListSrchCustomer.SelectedIndex = 0;
        DropDownListSrchSalesRef.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListCustomer);
        Util.ClearDropdown(DropDownListSrchCustomer);
        foreach (SolutionObjects.Customer customer in new OperationObjects.CustomerOP().GetAllCustomers())
        {
            DropDownListCustomer.Items.Add(new ListItem(customer.Name, customer.ID.Value.ToString()));
            DropDownListSrchCustomer.Items.Add(new ListItem(customer.Name, customer.ID.Value.ToString()));
        }

        Util.ClearDropdown(DropDownListSrchSalesRef);
        foreach (SolutionObjects.SalesRef salesRef in new OperationObjects.SalesRefOP().GetAllSalesRefs())
            DropDownListSrchSalesRef.Items.Add(new ListItem(salesRef.Name, salesRef.ID.Value.ToString()));
    }

    private void Search()
    {
        DateTime? _dateIssue = Util.ParseDate(TextBoxSrchDateIssue.Text.Trim());
        SolutionObjects.Customer _customer = null;
        if (DropDownListSrchCustomer.SelectedIndex != 0)
            _customer = new SolutionObjects.Customer(int.Parse(DropDownListSrchCustomer.SelectedValue));
        SolutionObjects.SalesRef _salesRef = null;
        if (DropDownListSrchSalesRef.SelectedIndex != 0)
            _salesRef = new SolutionObjects.SalesRef(int.Parse(DropDownListSrchSalesRef.SelectedValue));

        GridViewSearchList.DataSource = new OperationObjects.SalesIssueOP().SearchSalesIssues(TextBoxSrchIssueNumber.Text.Trim(), TextBoxSrchCardNumber.Text.Trim(), _dateIssue, _customer, _salesRef);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotals()
    {
        _salesIssue = ViewState[_target] as SolutionObjects.SalesIssue;

        Decimal _totalDiscount = 0;
        Decimal _totalTax = 0;
        Decimal _totalGross = 0;
        Decimal _totalNet = 0;

        foreach (SolutionObjects.SalesIssueItem salesIssueItem in _salesIssue.SalesIssueItemList)
        {
            _totalDiscount += salesIssueItem.Discount != null ? salesIssueItem.Discount.Value : 0;
            _totalTax += salesIssueItem.Tax != null ? salesIssueItem.Tax.Value : 0;
            _totalGross += salesIssueItem.Gross != null ? salesIssueItem.Gross.Value : 0;
            _totalNet += salesIssueItem.Net != null ? salesIssueItem.Net.Value : 0;
        }

        TextBoxTotalDiscount.Text = _totalDiscount != 0 ? _totalDiscount.ToString("F") : string.Empty;
        TextBoxTotalTax.Text = _totalTax != 0 ? _totalTax.ToString("F") : string.Empty;
        TextBoxTotalGross.Text = _totalGross != 0 ? _totalGross.ToString("F") : string.Empty;
        TextBoxTotalNet.Text = _totalNet != 0 ? _totalNet.ToString("F") : string.Empty;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _salesIssue = new SolutionObjects.SalesIssue();
            _salesIssue.SalesIssueItemList = new CommonObjects.PropertyList<SolutionObjects.SalesIssueItem>();

            ViewState[_target] = _salesIssue;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _salesIssue = new SolutionObjects.SalesIssue();
        _salesIssue.SalesIssueItemList = new CommonObjects.PropertyList<SolutionObjects.SalesIssueItem>();
        ViewState[_target] = _salesIssue;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }

    protected void ButtonSearchFilter_Click(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListProductItem);
        foreach (SolutionObjects.ItemStock itemStock in new OperationObjects.ItemStockOP().FilterSearch(TextBoxFilterCode.Text.Trim(), TextBoxFilterDescription.Text.Trim(), TextBoxFilterLotNumber.Text.Trim(), SolutionObjects.StockTypes.Trading))
        {
            itemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(itemStock.ProductItem.ID.Value);
            DropDownListProductItem.Items.Add(new ListItem(itemStock.ProductItem.Code + " - " + itemStock.ProductItem.Description + " [" + itemStock.UnitPrice.Value.ToString("F") + " / " + itemStock.SellingPrice.Value.ToString("F") + "]", itemStock.ID.Value.ToString()));
        }
    }
    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _salesIssue = ViewState[_target] as SolutionObjects.SalesIssue;
        _salesIssue.SalesIssueItemList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _salesIssue;
        GridViewItemAdd.DataSource = _salesIssue.SalesIssueItemList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.SalesIssueItem _salesIssueItem = e.Row.DataItem as SolutionObjects.SalesIssueItem;
            _salesIssueItem.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_salesIssueItem.ItemStock.ID.Value);
            _salesIssueItem.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_salesIssueItem.ItemStock.ProductItem.ID.Value);

            Util.EncodeString(e.Row.Cells[1], _salesIssueItem.ItemStock.ProductItem != null ? _salesIssueItem.ItemStock.ProductItem.Code + "-" + _salesIssueItem.ItemStock.ProductItem.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _salesIssueItem.IssuedQuantity != null ? _salesIssueItem.IssuedQuantity.Value.ToString() : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[3], _salesIssueItem.Discount != null ? _salesIssueItem.Discount.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[4], _salesIssueItem.Tax != null ? _salesIssueItem.Tax.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[5], _salesIssueItem.Gross != null ? _salesIssueItem.Gross.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[6], _salesIssueItem.Net != null ? _salesIssueItem.Net.Value.ToString("F") : string.Empty, 50);
        }
    }
    protected void DropDownListProductItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        LabelAvailableQuantity.Text = "-";
        LabelSellingPrice.Text = "-";
        LabelLocation.Text = "-";

        StringBuilder _sbDescription = new StringBuilder();
        if (DropDownListProductItem.SelectedIndex != 0)
        {
            SolutionObjects.ItemStock _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(int.Parse(DropDownListProductItem.SelectedValue));
            LabelAvailableQuantity.Text = _itemStock.CurrentQuantity.Value.ToString();
            LabelSellingPrice.Text = _itemStock.SellingPrice.Value.ToString("F");

            _itemStock.Store = new OperationObjects.StoreOP().GetStore(_itemStock.Store.ID.Value);
            _itemStock.Location = new OperationObjects.LocationOP().GetLocation(_itemStock.Location.ID.Value);
            _itemStock.Rack = new OperationObjects.RackOP().GetRack(_itemStock.Rack.ID.Value);

            _sbDescription.Append(_itemStock.Store.Description).Append("<br>");
            _sbDescription.Append(_itemStock.Location.Description).Append("<br>");
            _sbDescription.Append(_itemStock.Rack.Description);
            LabelLocation.Text = _sbDescription.ToString();
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
        _salesIssue = ViewState[_target] as SolutionObjects.SalesIssue;

        bool _hasFound = false;
        foreach (SolutionObjects.SalesIssueItem salesIssueItem in _salesIssue.SalesIssueItemList)
        {
            if (DropDownListProductItem.SelectedValue == salesIssueItem.ItemStock.ID.Value.ToString())
            {
                _hasFound = true;
                break;
            }
        }
        if (_hasFound)
        {
            LabelValidateMultiple.Visible = true;
            return;
        }

        if (Util.ParseInt(TextBoxQuantity.Text.Trim()) > Util.ParseInt(LabelAvailableQuantity.Text))
        {
            LabelValidateQuantity.Visible = true;
            return;
        }

        SolutionObjects.SalesIssueItem _salesIssueItem = new SolutionObjects.SalesIssueItem();
        _salesIssueItem.SalesIssue = _salesIssue;
        if (DropDownListProductItem.SelectedIndex != 0)
            _salesIssueItem.ItemStock = new SolutionObjects.ItemStock(int.Parse(DropDownListProductItem.SelectedValue));
        _salesIssueItem.IssuedQuantity = Util.ParseInt(TextBoxQuantity.Text.Trim());
        _salesIssueItem.Discount = Util.ParseDecimal(TextBoxDiscount.Text.Trim());
        _salesIssueItem.Tax = Util.ParseDecimal(TextBoxTax.Text.Trim());
        _salesIssueItem.Gross = Util.ParseDecimal(TextBoxGrossAmount.Text.Trim());
        _salesIssueItem.Net = Util.ParseDecimal(TextBoxNetAmount.Text.Trim());
        _salesIssueItem.User = User.Identity.Name;
        _salesIssue.SalesIssueItemList.Add(_salesIssueItem);

        ViewState[_target] = _salesIssue;
        GridViewItemAdd.DataSource = _salesIssue.SalesIssueItemList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _salesIssue = ViewState[_target] as SolutionObjects.SalesIssue;

        if (_salesIssue.ID == null)
            _salesIssue.IssueNumber = Util.GenereteSequenceNumber("SI", DateTime.Today.Year, DateTime.Today.Month);
        _salesIssue.DateIssue = Util.ParseDate(TextBoxDateIssue.Text.Trim());
        if (DropDownListCustomer.SelectedIndex != 0)
            _salesIssue.Customer = new SolutionObjects.Customer(int.Parse(DropDownListCustomer.SelectedValue));
        else
            _salesIssue.Customer = null;
        _salesIssue.InitialPayment = Util.ParseDecimal(TextBoxInitialPayment.Text.Trim());
        _salesIssue.TotalTax = Util.ParseDecimal(TextBoxTotalTax.Text.Trim());
        _salesIssue.TotalDiscount = Util.ParseDecimal(TextBoxTotalDiscount.Text.Trim());
        _salesIssue.TotalGross = Util.ParseDecimal(TextBoxTotalGross.Text.Trim());
        _salesIssue.TotalNet = Util.ParseDecimal(TextBoxTotalNet.Text.Trim());
        _salesIssue.Remarks = TextBoxRemarks.Text.Trim();
        _salesIssue.IsCredit = CheckBoxCreditSale.Checked;
        _salesIssue.User = User.Identity.Name;

        try
        {
            bool _isNew = false;
            if (_salesIssue.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.SalesIssueOP().SaveSalesIssue(_salesIssue);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>Issue Number : " + _salesIssue.IssueNumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                HtmlGenericControl dispval = new HtmlGenericControl();
                dispval.InnerHtml = "<script language='javascript'> print('" + _salesIssue.ID.Value + "');</script>";
                Page.Controls.Add(dispval);

                ClearAll();
                _salesIssue = new SolutionObjects.SalesIssue();
                _salesIssue.SalesIssueItemList = new CommonObjects.PropertyList<SolutionObjects.SalesIssueItem>();
                ViewState[_target] = _salesIssue;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _salesIssue = new SolutionObjects.SalesIssue();
        _salesIssue.SalesIssueItemList = new CommonObjects.PropertyList<SolutionObjects.SalesIssueItem>();
        ViewState[_target] = _salesIssue;
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _salesIssue = new OperationObjects.SalesIssueOP().GetSalesIssue(_targetid);
            _salesIssue.User = User.Identity.Name;

            int _listCount = _salesIssue.SalesIssueItemList.Count - 1;
            while (_listCount >= 0)
            {
                _salesIssue.SalesIssueItemList.RemoveAt(_listCount);
                _listCount--;
            }

            _listCount = _salesIssue.SalesRefCommissionList.Count - 1;
            while (_listCount >= 0)
            {
                _salesIssue.SalesRefCommissionList.RemoveAt(_listCount);
                _listCount--;
            }

            _listCount = _salesIssue.CustomerPaymentList.Count - 1;
            //while (_listCount >= 0)
            //{
            //    int _innerListCount = _salesIssue.CustomerPaymentList[_listCount].CustomerPaymentEntryList.Count - 1;
            //    while (_innerListCount >= 0)
            //    {
            //        _salesIssue.CustomerPaymentList[_listCount].CustomerPaymentEntryList.RemoveAt(_innerListCount);
            //        _innerListCount--;
            //    }

            //    _salesIssue.CustomerPaymentList.RemoveAt(_listCount);
            //    _listCount--;
            //}

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.SalesIssueOP().DeleteSalesIssue(_salesIssue);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _salesIssue = new OperationObjects.SalesIssueOP().GetSalesIssue(_targetid);

            ViewState[_target] = _salesIssue;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewIssueNumber.Text = Util.FormatEmptyString(_salesIssue.IssueNumber);
            LabelViewCardNumber.Text = Util.FormatEmptyString(_salesIssue.CardNumber);
            LabelViewDateIssue.Text = Util.FormatEmptyString(_salesIssue.DateIssue != null ? _salesIssue.DateIssue.Value.ToShortDateString() : string.Empty);
            if (_salesIssue.Customer != null)
                LabelViewCustomer.Text = new OperationObjects.CustomerOP().GetCustomer(_salesIssue.Customer.ID.Value).Name;
            else
                LabelViewCustomer.Text = "-";
            LabelViewRefCommisionAmount.Text = Util.FormatEmptyString(_salesIssue.CommissionAmount != null ? _salesIssue.CommissionAmount.Value.ToString("F") : string.Empty);
            LabelViewInitialPayment.Text = Util.FormatEmptyString(_salesIssue.InitialPayment != null ? _salesIssue.InitialPayment.Value.ToString("F") : string.Empty);
            LabelViewTotalDiscount.Text = Util.FormatEmptyString(_salesIssue.TotalDiscount != null ? _salesIssue.TotalDiscount.Value.ToString("F") : string.Empty);
            LabelViewTotalTax.Text = Util.FormatEmptyString(_salesIssue.TotalTax != null ? _salesIssue.TotalTax.Value.ToString("F") : string.Empty);
            LabelViewTotalGross.Text = Util.FormatEmptyString(_salesIssue.TotalGross != null ? _salesIssue.TotalGross.Value.ToString("F") : string.Empty);
            LabelViewTotalNet.Text = Util.FormatEmptyString(_salesIssue.TotalNet != null ? _salesIssue.TotalNet.Value.ToString("F") : string.Empty);
            LabelViewRemarks.Text = Util.FormatEmptyString(_salesIssue.Remarks);
            LabelViewCreditSale.Text = _salesIssue.IsCredit ? "Yes" : "No";

            GridViewItemView.DataSource = _salesIssue.SalesIssueItemList;
            GridViewItemView.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _salesIssue = e.Row.DataItem as SolutionObjects.SalesIssue;
            if (_salesIssue.Customer != null)
                _salesIssue.Customer = new OperationObjects.CustomerOP().GetCustomer(_salesIssue.Customer.ID.Value);

            Util.EncodeString(e.Row.Cells[3], _salesIssue.IssueNumber, 20);
            Util.EncodeString(e.Row.Cells[4], _salesIssue.CardNumber, 20);
            Util.EncodeString(e.Row.Cells[5], _salesIssue.DateIssue != null ? _salesIssue.DateIssue.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], _salesIssue.Customer != null ? _salesIssue.Customer.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[7], _salesIssue.IsCredit ? "Yes" : "No", 5);
            Util.EncodeString(e.Row.Cells[8], _salesIssue.TotalNet != null ? _salesIssue.TotalNet.Value.ToString("F") : string.Empty, 20);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _salesIssue.CardNumber + "');");
                }
            }
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.SalesIssueItem _salesIssueItem = e.Row.DataItem as SolutionObjects.SalesIssueItem;
            _salesIssueItem.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_salesIssueItem.ItemStock.ID.Value);
            _salesIssueItem.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_salesIssueItem.ItemStock.ProductItem.ID.Value);

            Util.EncodeString(e.Row.Cells[0], _salesIssueItem.ItemStock.ProductItem != null ? _salesIssueItem.ItemStock.ProductItem.Code + "-" + _salesIssueItem.ItemStock.ProductItem.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[1], _salesIssueItem.IssuedQuantity != null ? _salesIssueItem.IssuedQuantity.Value.ToString() : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _salesIssueItem.Discount != null ? _salesIssueItem.Discount.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[3], _salesIssueItem.Tax != null ? _salesIssueItem.Tax.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[4], _salesIssueItem.Gross != null ? _salesIssueItem.Gross.Value.ToString("F") : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[5], _salesIssueItem.Net != null ? _salesIssueItem.Net.Value.ToString("F") : string.Empty, 50);
        }
    }
    protected void LinkButtonCalculate_Click(object sender, EventArgs e)
    {
        decimal _unitPrice = 0;
        decimal _quantity = 0;
        decimal _discount = 0;
        decimal _tax = 0;
        decimal _gross = 0;
        decimal _net = 0;

        _unitPrice = Util.ParseDecimal(LabelSellingPrice.Text.Trim()) != null ? Util.ParseDecimal(LabelSellingPrice.Text.Trim()).Value : 0;
        _quantity = Util.ParseInt(TextBoxQuantity.Text.Trim()) != null ? Util.ParseInt(TextBoxQuantity.Text.Trim()).Value : 0;
        _gross = _unitPrice * _quantity;

        if (TextBoxDiscount.Text.Trim() != string.Empty)
            _discount = Util.ParseDecimal(TextBoxDiscount.Text.Trim()) != null ? Util.ParseDecimal(TextBoxDiscount.Text.Trim()).Value : 0;

        if (TextBoxTax.Text.Trim() != string.Empty)
            _tax = Util.ParseDecimal(TextBoxTax.Text.Trim()) != null ? Util.ParseDecimal(TextBoxTax.Text.Trim()).Value : 0;

        _net = _gross - _discount + _tax;

        TextBoxGrossAmount.Text = _gross.ToString("F");
        TextBoxNetAmount.Text = _net.ToString("F");
    }
}