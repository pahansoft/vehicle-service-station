﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CustomerPaymentOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(Inventory.SolutionObjects.CustomerPayment CustomerPayment)
        {
            if (CustomerPayment.ID == null)
                return new Inventory.DataAccessObjects.CustomerPaymentDO().Create(CustomerPayment);
            else
                return new Inventory.DataAccessObjects.CustomerPaymentDO().Edit(CustomerPayment);
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CustomerPayment CustomerPayment)
        {
            if (CustomerPayment.ID != null)
                return new Inventory.DataAccessObjects.CustomerPaymentDO().Delete(CustomerPayment);
            else
            {
                CustomerPayment.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = CustomerPayment;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.CustomerPayment Get(int id)
        {
            return new Inventory.DataAccessObjects.CustomerPaymentDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> GetAll()
        {
            return new Inventory.DataAccessObjects.CustomerPaymentDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CustomerPayment> Search(int? CustomerID, SolutionObjects.DebitCreditTypes? DebitCreditType, int? JobCardID)
        {
            return new Inventory.DataAccessObjects.CustomerPaymentDO().Search(CustomerID, DebitCreditType, JobCardID);
        }
    }
}
