﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SalesRefCommissionDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.SalesRefCommission salesRefCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateSalesRefCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRefCommission.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.SalesRef != null ? salesRefCommission.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCommision", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, salesRefCommission.DateCommision);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.SalesIssue != null ? salesRefCommission.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesRefCommission.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesRefCommission.CommissionAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRefCommission;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesRefCommission.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.SalesRefCommission salesRefCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditSalesRefCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesRefCommission.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRefCommission.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.SalesRef != null ? salesRefCommission.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateCommision", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, salesRefCommission.DateCommision);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.SalesIssue != null ? salesRefCommission.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesRefCommission.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesRefCommission.CommissionAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRefCommission;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.SalesRefCommission salesRefCommission)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteSalesRefCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRefCommission.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesRefCommission.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesRefCommission.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesRefCommission;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesRefCommission.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.SalesRefCommission Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetSalesRefCommission";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.SalesRefCommission salesRefCommission = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                        int dateCommisionOrdinal = dr.GetOrdinal("DateCommision");
                        int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                        salesRefCommission = new Inventory.SolutionObjects.SalesRefCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRefCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesRefCommission.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(dateCommisionOrdinal)) salesRefCommission.DateCommision = dr.GetDateTime(dateCommisionOrdinal);
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesRefCommission.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesRefCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesRefCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRefCommission;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllSalesRefCommissions";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> salesRefCommissionList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesRefCommission>();
            Inventory.SolutionObjects.SalesRefCommission salesRefCommission;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int dateCommisionOrdinal = dr.GetOrdinal("DateCommision");
                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        salesRefCommission = new Inventory.SolutionObjects.SalesRefCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRefCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesRefCommission.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(dateCommisionOrdinal)) salesRefCommission.DateCommision = dr.GetDateTime(dateCommisionOrdinal);
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesRefCommission.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesRefCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesRefCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        salesRefCommissionList.Add(salesRefCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRefCommissionList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> GetAllFor(SolutionObjects.SalesIssue salesIssue)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllSalesRefCommissionsForSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesRefCommission> salesRefCommissionList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesRefCommission>();
            Inventory.SolutionObjects.SalesRefCommission salesRefCommission;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int dateCommisionOrdinal = dr.GetOrdinal("DateCommision");
                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");

                    while (dr.Read())
                    {
                        salesRefCommission = new Inventory.SolutionObjects.SalesRefCommission(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesRefCommission.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesRefCommission.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(dateCommisionOrdinal)) salesRefCommission.DateCommision = dr.GetDateTime(dateCommisionOrdinal);
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) salesRefCommission.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesRefCommission.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesRefCommission.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);

                        salesRefCommissionList.Add(salesRefCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesRefCommissionList;
        }

        #endregion
    }
}
