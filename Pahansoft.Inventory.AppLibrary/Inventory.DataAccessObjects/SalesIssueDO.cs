﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class SalesIssueDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.SalesIssue salesIssue)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IssueNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.IssueNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.CardNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateIssue", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, salesIssue.DateIssue);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.Customer != null ? salesIssue.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.SalesRef != null ? salesIssue.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.CommissionAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalGross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNet", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalNet);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@InitialPayment", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.InitialPayment);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsCredit", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, salesIssue.IsCredit);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssue;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesIssue.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.SalesIssue salesIssue)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesIssue.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IssueNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.IssueNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.CardNumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateIssue", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, salesIssue.DateIssue);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.Customer != null ? salesIssue.Customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.SalesRef != null ? salesIssue.SalesRef.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionRate", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.CommissionRate);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CommissionAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.CommissionAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalDiscount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalDiscount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalTax", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalTax);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalGross", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalGross);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalNet", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.TotalNet);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.Remarks);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@InitialPayment", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, salesIssue.InitialPayment);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IsCredit", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, command, salesIssue.IsCredit);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssue;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.SalesIssue salesIssue)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesIssue.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, salesIssue.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, salesIssue.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = salesIssue;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                salesIssue.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.SalesIssue Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.SalesIssue salesIssue = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int issueNumberOrdinal = dr.GetOrdinal("IssueNumber");
                        int cardNumberOrdinal = dr.GetOrdinal("CardNumber");
                        int dateIssueOrdinal = dr.GetOrdinal("DateIssue");
                        int customerIDOrdinal = dr.GetOrdinal("CustomerID");
                        int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");
                        int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int totalGrossOrdinal = dr.GetOrdinal("TotalGross");
                        int totalNetOrdinal = dr.GetOrdinal("TotalNet");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int initialPaymentOrdinal = dr.GetOrdinal("InitialPayment");
                        int isCreditOrdinal = dr.GetOrdinal("IsCredit");

                        salesIssue = new Inventory.SolutionObjects.SalesIssue(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssue.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(issueNumberOrdinal)) salesIssue.IssueNumber = dr.GetString(issueNumberOrdinal);
                        if (!dr.IsDBNull(cardNumberOrdinal)) salesIssue.CardNumber = dr.GetString(cardNumberOrdinal);
                        if (!dr.IsDBNull(dateIssueOrdinal)) salesIssue.DateIssue = dr.GetDateTime(dateIssueOrdinal);
                        if (!dr.IsDBNull(customerIDOrdinal)) salesIssue.Customer = new SolutionObjects.Customer(dr.GetInt32(customerIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesIssue.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesIssue.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesIssue.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                        if (!dr.IsDBNull(totalDiscountOrdinal)) salesIssue.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) salesIssue.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossOrdinal)) salesIssue.TotalGross = dr.GetDecimal(totalGrossOrdinal);
                        if (!dr.IsDBNull(totalNetOrdinal)) salesIssue.TotalNet = dr.GetDecimal(totalNetOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) salesIssue.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(initialPaymentOrdinal)) salesIssue.InitialPayment = dr.GetDecimal(initialPaymentOrdinal);
                        if (!dr.IsDBNull(isCreditOrdinal)) salesIssue.IsCredit = dr.GetBoolean(isCreditOrdinal);

                        salesIssue.SalesIssueItemList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.SalesIssueItem>();
                        foreach (SolutionObjects.SalesIssueItem salesIssueItem in new DataAccessObjects.SalesIssueItemDO().GetAllFor(salesIssue))
                            salesIssue.SalesIssueItemList.Add(salesIssueItem);

                        salesIssue.SalesRefCommissionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.SalesRefCommission>();
                        foreach (SolutionObjects.SalesRefCommission salesRefCommission in new DataAccessObjects.SalesRefCommissionDO().GetAllFor(salesIssue))
                            salesIssue.SalesRefCommissionList.Add(salesRefCommission);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssue;
        }

        public Inventory.SolutionObjects.SalesIssue FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetSalesIssue";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.SalesIssue salesIssue = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int issueNumberOrdinal = dr.GetOrdinal("IssueNumber");
                        int cardNumberOrdinal = dr.GetOrdinal("CardNumber");
                        int dateIssueOrdinal = dr.GetOrdinal("DateIssue");
                        int customerIDOrdinal = dr.GetOrdinal("CustomerID");
                        int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                        int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                        int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");
                        int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                        int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                        int totalGrossOrdinal = dr.GetOrdinal("TotalGross");
                        int totalNetOrdinal = dr.GetOrdinal("TotalNet");
                        int remarksOrdinal = dr.GetOrdinal("Remarks");
                        int initialPaymentOrdinal = dr.GetOrdinal("InitialPayment");
                        int isCreditOrdinal = dr.GetOrdinal("IsCredit");

                        salesIssue = new Inventory.SolutionObjects.SalesIssue(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssue.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(issueNumberOrdinal)) salesIssue.IssueNumber = dr.GetString(issueNumberOrdinal);
                        if (!dr.IsDBNull(cardNumberOrdinal)) salesIssue.CardNumber = dr.GetString(cardNumberOrdinal);
                        if (!dr.IsDBNull(dateIssueOrdinal)) salesIssue.DateIssue = dr.GetDateTime(dateIssueOrdinal);
                        if (!dr.IsDBNull(customerIDOrdinal)) salesIssue.Customer = new SolutionObjects.Customer(dr.GetInt32(customerIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesIssue.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesIssue.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesIssue.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                        if (!dr.IsDBNull(totalDiscountOrdinal)) salesIssue.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) salesIssue.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossOrdinal)) salesIssue.TotalGross = dr.GetDecimal(totalGrossOrdinal);
                        if (!dr.IsDBNull(totalNetOrdinal)) salesIssue.TotalNet = dr.GetDecimal(totalNetOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) salesIssue.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(initialPaymentOrdinal)) salesIssue.InitialPayment = dr.GetDecimal(initialPaymentOrdinal);
                        if (!dr.IsDBNull(isCreditOrdinal)) salesIssue.IsCredit = dr.GetBoolean(isCreditOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssue;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllSalesIssues";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> salesIssueList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesIssue>();
            Inventory.SolutionObjects.SalesIssue salesIssue;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int issueNumberOrdinal = dr.GetOrdinal("IssueNumber");
                    int cardNumberOrdinal = dr.GetOrdinal("CardNumber");
                    int dateIssueOrdinal = dr.GetOrdinal("DateIssue");
                    int customerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");
                    int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int totalGrossOrdinal = dr.GetOrdinal("TotalGross");
                    int totalNetOrdinal = dr.GetOrdinal("TotalNet");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int initialPaymentOrdinal = dr.GetOrdinal("InitialPayment");
                    int isCreditOrdinal = dr.GetOrdinal("IsCredit");

                    while (dr.Read())
                    {
                        salesIssue = new Inventory.SolutionObjects.SalesIssue(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssue.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(issueNumberOrdinal)) salesIssue.IssueNumber = dr.GetString(issueNumberOrdinal);
                        if (!dr.IsDBNull(cardNumberOrdinal)) salesIssue.CardNumber = dr.GetString(cardNumberOrdinal);
                        if (!dr.IsDBNull(dateIssueOrdinal)) salesIssue.DateIssue = dr.GetDateTime(dateIssueOrdinal);
                        if (!dr.IsDBNull(customerIDOrdinal)) salesIssue.Customer = new SolutionObjects.Customer(dr.GetInt32(customerIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesIssue.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesIssue.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesIssue.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                        if (!dr.IsDBNull(totalDiscountOrdinal)) salesIssue.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) salesIssue.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossOrdinal)) salesIssue.TotalGross = dr.GetDecimal(totalGrossOrdinal);
                        if (!dr.IsDBNull(totalNetOrdinal)) salesIssue.TotalNet = dr.GetDecimal(totalNetOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) salesIssue.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(initialPaymentOrdinal)) salesIssue.InitialPayment = dr.GetDecimal(initialPaymentOrdinal);
                        if (!dr.IsDBNull(isCreditOrdinal)) salesIssue.IsCredit = dr.GetBoolean(isCreditOrdinal);

                        salesIssueList.Add(salesIssue);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssueList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> Search(string issueNumber, string cardNumber, DateTime? dateIssue, SolutionObjects.Customer customer, SolutionObjects.SalesRef salesRef)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllSalesIssues";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@IssueNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, issueNumber != string.Empty ? issueNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CardNumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cardNumber != string.Empty ? cardNumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DateIssue", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, dateIssue != null ? dateIssue : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CustomerID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, customer != null ? customer.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesRefID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, salesRef != null ? salesRef.ID : null);
            
            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.SalesIssue> salesIssueList = new System.Collections.Generic.List<Inventory.SolutionObjects.SalesIssue>();
            Inventory.SolutionObjects.SalesIssue salesIssue;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int issueNumberOrdinal = dr.GetOrdinal("IssueNumber");
                    int cardNumberOrdinal = dr.GetOrdinal("CardNumber");
                    int dateIssueOrdinal = dr.GetOrdinal("DateIssue");
                    int customerIDOrdinal = dr.GetOrdinal("CustomerID");
                    int salesRefIDOrdinal = dr.GetOrdinal("SalesRefID");
                    int commissionRateOrdinal = dr.GetOrdinal("CommissionRate");
                    int commissionAmountOrdinal = dr.GetOrdinal("CommissionAmount");
                    int totalDiscountOrdinal = dr.GetOrdinal("TotalDiscount");
                    int totalTaxOrdinal = dr.GetOrdinal("TotalTax");
                    int totalGrossOrdinal = dr.GetOrdinal("TotalGross");
                    int totalNetOrdinal = dr.GetOrdinal("TotalNet");
                    int remarksOrdinal = dr.GetOrdinal("Remarks");
                    int initialPaymentOrdinal = dr.GetOrdinal("InitialPayment");
                    int isCreditOrdinal = dr.GetOrdinal("IsCredit");

                    while (dr.Read())
                    {
                        salesIssue = new Inventory.SolutionObjects.SalesIssue(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) salesIssue.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(issueNumberOrdinal)) salesIssue.IssueNumber = dr.GetString(issueNumberOrdinal);
                        if (!dr.IsDBNull(cardNumberOrdinal)) salesIssue.CardNumber = dr.GetString(cardNumberOrdinal);
                        if (!dr.IsDBNull(dateIssueOrdinal)) salesIssue.DateIssue = dr.GetDateTime(dateIssueOrdinal);
                        if (!dr.IsDBNull(customerIDOrdinal)) salesIssue.Customer = new SolutionObjects.Customer(dr.GetInt32(customerIDOrdinal));
                        if (!dr.IsDBNull(salesRefIDOrdinal)) salesIssue.SalesRef = new SolutionObjects.SalesRef(dr.GetInt32(salesRefIDOrdinal));
                        if (!dr.IsDBNull(commissionRateOrdinal)) salesIssue.CommissionRate = dr.GetDecimal(commissionRateOrdinal);
                        if (!dr.IsDBNull(commissionAmountOrdinal)) salesIssue.CommissionAmount = dr.GetDecimal(commissionAmountOrdinal);
                        if (!dr.IsDBNull(totalDiscountOrdinal)) salesIssue.TotalDiscount = dr.GetDecimal(totalDiscountOrdinal);
                        if (!dr.IsDBNull(totalTaxOrdinal)) salesIssue.TotalTax = dr.GetDecimal(totalTaxOrdinal);
                        if (!dr.IsDBNull(totalGrossOrdinal)) salesIssue.TotalGross = dr.GetDecimal(totalGrossOrdinal);
                        if (!dr.IsDBNull(totalNetOrdinal)) salesIssue.TotalNet = dr.GetDecimal(totalNetOrdinal);
                        if (!dr.IsDBNull(remarksOrdinal)) salesIssue.Remarks = dr.GetString(remarksOrdinal);
                        if (!dr.IsDBNull(initialPaymentOrdinal)) salesIssue.InitialPayment = dr.GetDecimal(initialPaymentOrdinal);
                        if (!dr.IsDBNull(isCreditOrdinal)) salesIssue.IsCredit = dr.GetBoolean(isCreditOrdinal);

                        salesIssueList.Add(salesIssue);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return salesIssueList;
        }

        #endregion
    }
}
