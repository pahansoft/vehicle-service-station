﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_loca_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.Location _location;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        DropDownListStore.SelectedIndex = 0;
        TextBoxDescription.Text = string.Empty;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListStore);
        Util.ClearDropdown(DropDownListSrchStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
        {
            DropDownListStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
            DropDownListSrchStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
        }

    }

    private void ClearSearch()
    {
        DropDownListSrchStore.SelectedIndex = 0;
        TextBoxSrchDescription.Text = string.Empty;
    }

    private void Search()
    {
        SolutionObjects.Store _store = null;
        if (DropDownListSrchStore.SelectedIndex != 0)
            _store = new SolutionObjects.Store(int.Parse(DropDownListSrchStore.SelectedValue));

        GridViewSearchList.DataSource = new OperationObjects.LocationOP().SearchLocations(TextBoxSrchDescription.Text.Trim(), _store);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            ViewState[_target] = new SolutionObjects.Location();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.Location();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _location = ViewState[_target] as SolutionObjects.Location;
        if (DropDownListStore.SelectedIndex != 0)
            _location.Store = new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue));
        else
            _location.Store = null;
        _location.Description = TextBoxDescription.Text.Trim();

        _location.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.LocationOP().SaveLocation(_location);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.Location();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _location = ViewState[_target] as SolutionObjects.Location;

        if (_location.ID != null)
        {
            _location = new OperationObjects.LocationOP().GetLocation(_location.ID.Value);
            ViewState[_target] = _location;

            if (_location.Store != null)
                DropDownListStore.SelectedValue = _location.Store.ID.Value.ToString();
            TextBoxDescription.Text = _location.Description;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.Location();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _location = new OperationObjects.LocationOP().GetLocation(_targetid);

            ClearAll();
            ViewState[_target] = _location;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            if (_location.Store != null)
                DropDownListStore.SelectedValue = _location.Store.ID.Value.ToString();
            TextBoxDescription.Text = _location.Description;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _location = new OperationObjects.LocationOP().GetLocation(_targetid);
            _location.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.LocationOP().DeleteLocation(_location);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _location = e.Row.DataItem as SolutionObjects.Location;
            if (_location.Store != null)
                _location.Store = new OperationObjects.StoreOP().GetStore(_location.Store.ID.Value);
            Util.EncodeString(e.Row.Cells[2], _location.Store != null ? _location.Store.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[3], _location.Description, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _location.Description + "');");
                }
            }
        }
    }
}