﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.Services;
using System.Web.UI.HtmlControls;

#endregion

public partial class tran_grn_appr_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    public const string _targetProducts = "TargetProducts";
    public const string _targetSuppliers = "TargetSuppliers";
    SolutionObjects.GRN GRN;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxGRNNumber.Text = "- AUTO GENERATED -";
        TextBoxDateGRN.Text = DateTime.Today.ToShortDateString();
        TextBoxSupplier.Text = string.Empty;
        TextBoxTotalGross.Text = string.Empty;
        TextBoxTotalNet.Text = string.Empty;
        TextBoxTotalTax.Text = string.Empty;
        TextBoxTotalDiscount.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewPOAdd.DataSource = null;
        GridViewPOAdd.DataBind();

        GridViewPOItemAdd.DataSource = null;
        GridViewPOItemAdd.DataBind();

        TextBoxLotNumber.Text = string.Empty;
        DropDownListStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);
        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        LabelItemError.Visible = false;
        LabelPODetailID.Text = string.Empty;
        TextBoxProductItem.Text = string.Empty;
        TextBoxQuantity.Text = string.Empty;
        TextBoxUnitPrice.Text = string.Empty;
        TextBoxSellingPrice.Text = string.Empty;
        TextBoxDiscount.Text = string.Empty;
        TextBoxTax.Text = string.Empty;
        TextBoxGrossAmount.Text = string.Empty;
        TextBoxNetAmount.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchGRNNumber.Text = string.Empty;
        TextBoxSrchDateGRN.Text = string.Empty;
        TextBoxSrchSupplier.Text = string.Empty;
        CheckBoxSrchPendingOnly.Checked = true;
    }

    private void Search()
    {
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        SolutionObjects.Supplier Supplier = null;
        if (TextBoxSrchSupplier.Text.Trim() != string.Empty)
            Supplier = Suppliers.FirstOrDefault(t => t.Name == TextBoxSrchSupplier.Text);

        bool? ApproveOnly = null;
        if (CheckBoxSrchPendingOnly.Checked)
            ApproveOnly = false;

        GridViewSearchList.DataSource = new OperationObjects.GRNOP().SearchGRNs(TextBoxSrchGRNNumber.Text.Trim(), Util.ParseDate(TextBoxSrchDateGRN.Text.Trim()), Supplier, ApproveOnly);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotal()
    {
        GRN = HttpContext.Current.Session[_target] as SolutionObjects.GRN;

        Decimal TotalGross = 0;
        Decimal TotalTax = 0;
        Decimal TotalDiscount = 0;
        Decimal TotalNet = 0;

        foreach (var item in GRN.GRNDetailList)
        {
            TotalGross += item.GrossAmount.Value;
            TotalTax += item.Tax != null ? item.Tax.Value : 0;
            TotalDiscount += item.Discount != null ? item.Discount.Value : 0;
            TotalNet += item.NetAmount.Value;
        }

        TextBoxTotalGross.Text = TotalGross.ToString("F");
        TextBoxTotalTax.Text = TotalTax.ToString("F");
        TextBoxTotalDiscount.Text = TotalDiscount.ToString("F");
        TextBoxTotalNet.Text = TotalNet.ToString("F");
    }

    private void FillForEdit(int GRNID)
    {
        GRN = new OperationObjects.GRNOP().GetGRN(GRNID);
        if (GRN.Supplier != null)
            GRN.Supplier = new OperationObjects.SupplierOP().GetSupplier(GRN.Supplier.ID.Value);

        HttpContext.Current.Session[_target] = GRN;

        TextBoxGRNNumber.Text = GRN.GRNNumber;
        TextBoxDateGRN.Text = GRN.DateGRN != null ? GRN.DateGRN.Value.ToShortDateString() : DateTime.Today.ToShortDateString();
        if (GRN.Supplier != null)
            TextBoxSupplier.Text = GRN.Supplier.Name;
        TextBoxTotalGross.Text = GRN.TotalGrossAmount != null ? GRN.TotalGrossAmount.Value.ToString() : string.Empty;
        TextBoxTotalTax.Text = GRN.TotalTax != null ? GRN.TotalTax.Value.ToString() : string.Empty;
        TextBoxTotalDiscount.Text = GRN.TotalDiscount != null ? GRN.TotalDiscount.Value.ToString() : string.Empty;
        TextBoxTotalNet.Text = GRN.TotalNetAmount != null ? GRN.TotalNetAmount.Value.ToString() : string.Empty;
        TextBoxRemarks.Text = GRN.Remarks;

        GridViewItemAdd.DataSource = GRN.GRNDetailList;
        GridViewItemAdd.DataBind();

        FillPOs();
    }

    private void FillForView(int PurchaseOrderID)
    {
        //PurchaseOrder = new OperationObjects.PurchaseOrderOP().Get(PurchaseOrderID);
        //if (PurchaseOrder.Supplier != null)
        //    PurchaseOrder.Supplier = new OperationObjects.SupplierOP().GetSupplier(PurchaseOrder.Supplier.ID.Value);

        //HttpContext.Current.Session[_target] = PurchaseOrder;

        //LabelViewPONumber.Text = Util.FormatEmptyString(PurchaseOrder.PONumber);
        //LabelViewPODate.Text = Util.FormatEmptyString(PurchaseOrder.DatePO != null ? PurchaseOrder.DatePO.Value.ToShortDateString() : DateTime.Today.ToShortDateString());
        //LabelViewSupplier.Text = Util.FormatEmptyString(PurchaseOrder.Supplier != null ? PurchaseOrder.Supplier.Name : string.Empty);
        //LabelViewTotalAmount.Text = Util.FormatEmptyString(PurchaseOrder.TotalAmount != null ? PurchaseOrder.TotalAmount.Value.ToString() : string.Empty);
        //LabelViewRemarks.Text = Util.FormatEmptyString(PurchaseOrder.Remarks);

        //GridViewItemView.DataSource = PurchaseOrder.PurchaseOrderDetails;
        //GridViewItemView.DataBind();
    }

    private void FillDropDownLists()
    {
        Util.ClearDropdown(DropDownListStore);
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);
        foreach (var item in new OperationObjects.StoreOP().GetAllStores())
        {
            DropDownListStore.Items.Add(new ListItem(item.Description, item.ID.Value.ToString()));
        }
    }

    private void FillLocation()
    {
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);
        if (DropDownListStore.SelectedIndex != 0)
        {
            foreach (var item in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
            {
                DropDownListLocation.Items.Add(new ListItem(item.Description, item.ID.Value.ToString()));
            }
        }
    }

    private void FillSubLocation()
    {
        Util.ClearDropdown(DropDownListRack);
        if (DropDownListLocation.SelectedIndex != 0)
        {
            foreach (var item in new OperationObjects.RackOP().SearchRacks(string.Empty, null, new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue))))
            {
                DropDownListRack.Items.Add(new ListItem(item.Description, item.ID.Value.ToString()));
            }
        }
    }

    private void FillPOs()
    {
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;
        GridViewPOAdd.DataSource = null;
        GridViewPOAdd.DataBind();

        GridViewPOItemAdd.DataSource = null;
        GridViewPOItemAdd.DataBind();

        if (TextBoxSupplier.Text.Trim() != string.Empty)
        {
            var supplier = Suppliers.FirstOrDefault(t => t.Name == TextBoxSupplier.Text);
            if (supplier != null)
            {
                var POs = new OperationObjects.PurchaseOrderOP().Search(string.Empty, null, supplier.ID.Value);

                GridViewPOAdd.DataSource = POs;
                GridViewPOAdd.DataBind();
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetProductItems(string Text)
    {
        var ProductItems = HttpContext.Current.Session[_targetProducts] as IList<SolutionObjects.ProductItem>;

        var returnList = new List<string>();
        foreach (var item in ProductItems.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.ReferenceNumber.ToLower().Contains(Text.ToLower()) || t.Description.ToLower().Contains(Text.ToLower())).ToList())
        {
            var sbDescription = new StringBuilder();
            sbDescription.Append(item.Code);
            sbDescription.Append(item.ReferenceNumber != string.Empty ? ("-" + item.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(item.Description);
            returnList.Add(sbDescription.ToString());
        }

        return returnList;
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetSuppliers(string Text)
    {
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        var returnList = new List<string>();
        foreach (var item in Suppliers.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.Name.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Name);

        return returnList;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillDropDownLists();
            ClearSearch();
            LabelPageOperation.Text = "Search";

            var ProductItems = new OperationObjects.ProductItemOP().GetAllProductItems();
            HttpContext.Current.Session[_targetProducts] = ProductItems;

            var Suppliers = new OperationObjects.SupplierOP().GetAllSuppliers();
            HttpContext.Current.Session[_targetSuppliers] = Suppliers;

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonSearchOperation.Visible = false;
    }

    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void TextBoxSupplier_TextChanged(object sender, EventArgs e)
    {
        FillPOs();
    }
    protected void GridViewPOAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("AddRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewPOAdd.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            var PODetails = new OperationObjects.PurchaseOrderDetailOP().GetAllFor(new SolutionObjects.PurchaseOrder(_targetid));

            GridViewPOItemAdd.DataSource = PODetails;
            GridViewPOItemAdd.DataBind();
        }
    }
    protected void GridViewPOAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var PurchaseOrder = e.Row.DataItem as SolutionObjects.PurchaseOrder;

            Util.EncodeString(e.Row.Cells[1], PurchaseOrder.PONumber, 20);
            Util.EncodeString(e.Row.Cells[2], PurchaseOrder.DatePO.Value.ToShortDateString(), 20);
            Util.EncodeString(e.Row.Cells[3], PurchaseOrder.TotalAmount != null ? PurchaseOrder.TotalAmount.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[4], PurchaseOrder.Remarks, 100);
        }
    }
    protected void GridViewPOItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("AssignRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewPOItemAdd.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            var PODetail = new OperationObjects.PurchaseOrderDetailOP().Get(_targetid);
            PODetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(PODetail.ProductItem.ID.Value);

            var sbDescription = new StringBuilder();
            sbDescription.Append(PODetail.ProductItem.Code);
            sbDescription.Append(PODetail.ProductItem.ReferenceNumber != string.Empty ? ("-" + PODetail.ProductItem.ReferenceNumber) : string.Empty);
            sbDescription.Append("-").Append(PODetail.ProductItem.Description);

            ClearItem();
            LabelPODetailID.Text = PODetail.ID.Value.ToString();
            TextBoxProductItem.Text = sbDescription.ToString();
            TextBoxQuantity.Text = PODetail.Quantity.Value.ToString();
            TextBoxUnitPrice.Text = PODetail.UnitPrice.Value.ToString();
            TextBoxGrossAmount.Text = PODetail.LineTotal.Value.ToString();
        }
    }
    protected void GridViewPOItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var PurchaseOrderDetail = e.Row.DataItem as SolutionObjects.PurchaseOrderDetail;
            PurchaseOrderDetail.PurchaseOrder = new OperationObjects.PurchaseOrderOP().GetProxy(PurchaseOrderDetail.PurchaseOrder.ID.Value);
            PurchaseOrderDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(PurchaseOrderDetail.ProductItem.ID.Value);

            Util.EncodeString(e.Row.Cells[1], PurchaseOrderDetail.PurchaseOrder.PONumber, 20);
            Util.EncodeString(e.Row.Cells[2], PurchaseOrderDetail.ProductItem.Code, 20);
            Util.EncodeString(e.Row.Cells[3], PurchaseOrderDetail.ProductItem.ReferenceNumber, 20);
            Util.EncodeString(e.Row.Cells[4], PurchaseOrderDetail.ProductItem.Description, 50);
            Util.EncodeString(e.Row.Cells[5], PurchaseOrderDetail.Quantity != null ? PurchaseOrderDetail.Quantity.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], PurchaseOrderDetail.ReceivedQty != null ? PurchaseOrderDetail.ReceivedQty.Value.ToString() : string.Empty, 20);
        }
    }
    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GRN = HttpContext.Current.Session[_target] as SolutionObjects.GRN;
        GRN.GRNDetailList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        HttpContext.Current.Session[_target] = GRN;
        GridViewItemAdd.DataSource = GRN.GRNDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        UpdatePanelSummary.Update();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var GRNDetail = e.Row.DataItem as SolutionObjects.GRNDetail;
            if (GRNDetail.PurchaseOrderDetail != null)
            {
                GRNDetail.PurchaseOrderDetail = new OperationObjects.PurchaseOrderDetailOP().Get(GRNDetail.PurchaseOrderDetail.ID.Value);
                GRNDetail.PurchaseOrderDetail.PurchaseOrder = new OperationObjects.PurchaseOrderOP().Get(GRNDetail.PurchaseOrderDetail.PurchaseOrder.ID.Value);
            }
            GRNDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(GRNDetail.ProductItem.ID.Value);
            GRNDetail.Store = new OperationObjects.StoreOP().GetStore(GRNDetail.Store.ID.Value);
            GRNDetail.Location = new OperationObjects.LocationOP().GetLocation(GRNDetail.Location.ID.Value);
            GRNDetail.Rack = new OperationObjects.RackOP().GetRack(GRNDetail.Rack.ID.Value);

            var sbProduct = new StringBuilder();
            sbProduct.Append(GRNDetail.ProductItem.Code).Append("</br>");
            if (GRNDetail.ProductItem.ReferenceNumber != string.Empty)
                sbProduct.Append(GRNDetail.ProductItem.ReferenceNumber).Append("</br>");
            sbProduct.Append(GRNDetail.ProductItem.Description);

            var sbLocation = new StringBuilder();
            sbLocation.Append(GRNDetail.Store.Description).Append("</br>");
            sbLocation.Append(GRNDetail.Location.Description).Append("</br>");
            sbLocation.Append(GRNDetail.Rack.Description);

            var sbPrice = new StringBuilder();
            sbPrice.Append("Unit : ").Append(GRNDetail.UnitPrice.Value.ToString()).Append("</br>");
            sbPrice.Append("Sell : ").Append(GRNDetail.SellingPrice.Value.ToString());

            var sbAmount = new StringBuilder();
            sbAmount.Append("Grs : ").Append(GRNDetail.GrossAmount.Value.ToString()).Append("</br>");
            sbAmount.Append("Dis : ").Append(GRNDetail.Discount != null ? GRNDetail.Discount.Value.ToString() : "-").Append("</br>");
            sbAmount.Append("Tax : ").Append(GRNDetail.Tax != null ? GRNDetail.Tax.Value.ToString() : "-").Append("</br>");
            sbAmount.Append("Net : ").Append(GRNDetail.NetAmount.Value.ToString());

            Util.EncodeString(e.Row.Cells[1], GRNDetail.PurchaseOrderDetail != null ? GRNDetail.PurchaseOrderDetail.PurchaseOrder.PONumber : string.Empty, 20);
            e.Row.Cells[2].Text = sbProduct.ToString();
            Util.EncodeString(e.Row.Cells[3], GRNDetail.LotNumber, 50);
            e.Row.Cells[4].Text = sbLocation.ToString();
            Util.EncodeString(e.Row.Cells[5], GRNDetail.Quantity != null ? GRNDetail.Quantity.Value.ToString() : string.Empty, 20);
            e.Row.Cells[6].Text = sbPrice.ToString();
            e.Row.Cells[7].Text = sbAmount.ToString();
        }
    }
    protected void DropDownListStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillLocation();
        DropDownListLocation.Focus();
    }
    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSubLocation();
        DropDownListRack.Focus();
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        var ProductItems = HttpContext.Current.Session[_targetProducts] as IList<SolutionObjects.ProductItem>;
        LabelItemError.Visible = false;

        int? ProductItemID = null;
        if (TextBoxProductItem.Text.Trim() != string.Empty)
        {
            List<string> itemCode = TextBoxProductItem.Text.Trim().Split('-').ToList<string>();
            var ProductItem = ProductItems.FirstOrDefault(t => t.Code == itemCode[0]);
            if (ProductItem == null)
            {
                LabelItemError.Text = "Invalid Product Item";
                LabelItemError.Visible = true;
                return;
            }
            else
                ProductItemID = ProductItem.ID;
        }

        GRN = HttpContext.Current.Session[_target] as SolutionObjects.GRN;
        foreach (var item in GRN.GRNDetailList)
        {
            if (LabelPODetailID.Text != string.Empty)
            {
                if (item.PurchaseOrderDetail != null)
                {
                    if (item.PurchaseOrderDetail.ID == Util.ParseInt(LabelPODetailID.Text))
                    {
                        LabelItemError.Text = "Item already assigned";
                        LabelItemError.Visible = true;
                        return;
                    }
                }
            }
        }

        var GRNDetail = new SolutionObjects.GRNDetail();
        GRNDetail.GRN = GRN;
        if (LabelPODetailID.Text != string.Empty)
            GRNDetail.PurchaseOrderDetail = new SolutionObjects.PurchaseOrderDetail(int.Parse(LabelPODetailID.Text));
        GRNDetail.ProductItem = new SolutionObjects.ProductItem(ProductItemID.Value);
        GRNDetail.StockType = SolutionObjects.StockTypes.Trading;
        GRNDetail.LotNumber = TextBoxLotNumber.Text.Trim();
        GRNDetail.Store = new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue));
        GRNDetail.Location = new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue));
        GRNDetail.Rack = new SolutionObjects.Rack(int.Parse(DropDownListRack.SelectedValue));
        GRNDetail.Quantity = Util.ParseDecimal(TextBoxQuantity.Text.Trim());
        GRNDetail.UnitPrice = Util.ParseDecimal(TextBoxUnitPrice.Text.Trim());
        GRNDetail.SellingPrice = Util.ParseDecimal(TextBoxSellingPrice.Text.Trim());
        GRNDetail.Discount = Util.ParseDecimal(TextBoxDiscount.Text.Trim());
        GRNDetail.Tax = Util.ParseDecimal(TextBoxTax.Text.Trim());
        GRNDetail.GrossAmount = Util.ParseDecimal(TextBoxGrossAmount.Text.Trim());
        GRNDetail.NetAmount = Util.ParseDecimal(TextBoxNetAmount.Text.Trim());
        GRNDetail.User = User.Identity.Name;
        GRN.GRNDetailList.Add(GRNDetail);

        HttpContext.Current.Session[_target] = GRN;
        GridViewItemAdd.DataSource = GRN.GRNDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotal();
        ClearItem();
        UpdatePanelSummary.Update();
    }
    protected void ButtonItemClear_Click(object sender, EventArgs e)
    {
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        GRN = HttpContext.Current.Session[_target] as SolutionObjects.GRN;
        var Suppliers = HttpContext.Current.Session[_targetSuppliers] as IList<SolutionObjects.Supplier>;

        int? SupplierID = null;
        if (TextBoxSupplier.Text.Trim() != string.Empty)
        {
            var supplier = Suppliers.FirstOrDefault(t => t.Name == TextBoxSupplier.Text);
            if (supplier == null)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = "Invalid Supplier";
                return;
            }
            else
                SupplierID = supplier.ID;
        }

        if (GRN.GRNDetailList.Count == 0)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = "At least one item needs to be entered";
            return;
        }

        GRN.GRNNumber = TextBoxGRNNumber.Text.Trim();
        GRN.DateGRN = Util.ParseDate(TextBoxDateGRN.Text.Trim());
        GRN.Supplier = new SolutionObjects.Supplier(SupplierID);
        GRN.TotalGrossAmount = Util.ParseDecimal(TextBoxTotalGross.Text.Trim());
        GRN.TotalTax = Util.ParseDecimal(TextBoxTotalTax.Text.Trim());
        GRN.TotalDiscount = Util.ParseDecimal(TextBoxTotalDiscount.Text.Trim());
        GRN.TotalNetAmount = Util.ParseDecimal(TextBoxTotalNet.Text.Trim());
        GRN.Remarks = TextBoxRemarks.Text.Trim();
        GRN.IsApproved = true;
        GRN.DateApproved = DateTime.Now;
        GRN.ApprovedBy = User.Identity.Name;
        GRN.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.GRNOP().ApproveGRN(GRN);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record approved successfully";

                ClearAll();
                GRN = new SolutionObjects.GRN();
                GRN.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
                HttpContext.Current.Session[_target] = GRN;

                PanelAddNew.Visible = false;
                PanelSearchDetails.Visible = true;
                PanelView.Visible = false;
                LabelPageOperation.Text = "Search";
                LinkButtonSearchOperation.Visible = false;
                Search();
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        GRN = HttpContext.Current.Session[_target] as SolutionObjects.GRN;

        if (GRN.ID != null)
        {
            FillForEdit(GRN.ID.Value);
        }
        else
        {
            GRN = new SolutionObjects.GRN();
            GRN.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
            HttpContext.Current.Session[_target] = GRN;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            ClearAll();
            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Approve";
            LinkButtonSearchOperation.Visible = true;

            FillForEdit(_targetid);
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            //int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            //PanelAddNew.Visible = false;
            //PanelSearchDetails.Visible = false;
            //PanelView.Visible = true;
            //LabelPageOperation.Text = "View";
            //LinkButtonAddNewOperation.Visible = false;
            //LinkButtonSearchOperation.Visible = true;

            //FillForView(_targetid);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GRN = e.Row.DataItem as SolutionObjects.GRN;
            if (GRN.Supplier != null)
                GRN.Supplier = new OperationObjects.SupplierOP().GetSupplier(GRN.Supplier.ID.Value);

            Util.EncodeString(e.Row.Cells[2], GRN.GRNNumber, 20);
            Util.EncodeString(e.Row.Cells[3], GRN.DateGRN != null ? GRN.DateGRN.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[4], GRN.Supplier != null ? GRN.Supplier.Name : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[5], GRN.TotalNetAmount != null ? GRN.TotalNetAmount.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], GRN.IsApproved ? "Yes" : "No", 5);

            if (GRN.IsApproved)
            {
                e.Row.Cells[0].Enabled = false;
            }
        }
    }
}