﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ItemStockTransactionOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveItemStockTransaction(Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction)
        {
            if (itemStockTransaction.ID == null)
                return new Inventory.DataAccessObjects.ItemStockTransactionDO().Create(itemStockTransaction);
            else
                return new Inventory.DataAccessObjects.ItemStockTransactionDO().Edit(itemStockTransaction);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteItemStockTransaction(Inventory.SolutionObjects.ItemStockTransaction itemStockTransaction)
        {
            if (itemStockTransaction.ID != null)
                return new Inventory.DataAccessObjects.ItemStockTransactionDO().Delete(itemStockTransaction);
            else
            {
                itemStockTransaction.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = itemStockTransaction;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.ItemStockTransaction GetItemStockTransaction(int id)
        {
            return new Inventory.DataAccessObjects.ItemStockTransactionDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> GetAllItemStockTransactions()
        {
            return new Inventory.DataAccessObjects.ItemStockTransactionDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.ItemStockTransaction> GetAllFor(SolutionObjects.ItemStock itemStock)
        {
            return new Inventory.DataAccessObjects.ItemStockTransactionDO().GetAllFor(itemStock);
        }
    }
}
