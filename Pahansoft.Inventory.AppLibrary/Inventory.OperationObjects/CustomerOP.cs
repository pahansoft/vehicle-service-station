﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CustomerOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCustomer(Inventory.SolutionObjects.Customer customer)
        {
            if (customer.ID == null)
                return new Inventory.DataAccessObjects.CustomerDO().Create(customer);
            else
                return new Inventory.DataAccessObjects.CustomerDO().Edit(customer);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCustomer(Inventory.SolutionObjects.Customer customer)
        {
            if (customer.ID != null)
                return new Inventory.DataAccessObjects.CustomerDO().Delete(customer);
            else
            {
                customer.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = customer;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Customer GetCustomer(int id)
        {
            return new Inventory.DataAccessObjects.CustomerDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> GetAllCustomers()
        {
            return new Inventory.DataAccessObjects.CustomerDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Customer> SearchCustomers(string code, string name)
        {
            return new Inventory.DataAccessObjects.CustomerDO().Search(code, name);
        }
    }
}
