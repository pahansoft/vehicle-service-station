﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_loca_tran_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.LocationTransfer _locationTransfer;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxTransferNumber.Text = string.Empty;
        TextBoxDateTransfer.Text = DateTime.Today.ToShortDateString();
        TextBoxReferenceNumber.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        DropDownListFromStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListFromLocation);
        Util.ClearDropdown(DropDownListFromRack);
        DropDownListToStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListToLocation);
        Util.ClearDropdown(DropDownListToRack);
        Util.ClearDropdown(DropDownListProductItem);
        LabelAvailableQuantity.Text = "-";
        TextBoxTransferQuantity.Text = string.Empty;
        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
    }

    private void ClearSearch()
    {
        TextBoxSrchTransferNumber.Text = string.Empty;
        TextBoxSrchDateTransfer.Text = string.Empty;
        TextBoxSrchReferenceNumber.Text = string.Empty;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListFromStore);
        Util.ClearDropdown(DropDownListToStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
        {
            DropDownListFromStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
            DropDownListToStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));
        }

    }

    private void Search()
    {
        DateTime? _dateTransfer = Util.ParseDate(TextBoxSrchDateTransfer.Text.Trim());

        GridViewSearchList.DataSource = new OperationObjects.LocationTransferOP().SearchLocationTransfers(TextBoxSrchTransferNumber.Text.Trim(), _dateTransfer, TextBoxSrchReferenceNumber.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _locationTransfer = new SolutionObjects.LocationTransfer();
            _locationTransfer.LocationTransferDetailList = new CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail>();
            ViewState[_target] = _locationTransfer;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _locationTransfer = new SolutionObjects.LocationTransfer();
        _locationTransfer.LocationTransferDetailList = new CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail>();
        ViewState[_target] = _locationTransfer;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }

    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _locationTransfer = ViewState[_target] as SolutionObjects.LocationTransfer;
        _locationTransfer.LocationTransferDetailList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _locationTransfer;
        GridViewItemAdd.DataSource = _locationTransfer.LocationTransferDetailList;
        GridViewItemAdd.DataBind();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.LocationTransferDetail _locationTransferDetail = e.Row.DataItem as SolutionObjects.LocationTransferDetail;
            if (_locationTransferDetail.ItemStock != null)
            {
                _locationTransferDetail.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_locationTransferDetail.ItemStock.ID.Value);
                _locationTransferDetail.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_locationTransferDetail.ItemStock.ProductItem.ID.Value);
            }
            if (_locationTransferDetail.FromStore != null)
                _locationTransferDetail.FromStore = new OperationObjects.StoreOP().GetStore(_locationTransferDetail.FromStore.ID.Value);
            if (_locationTransferDetail.FromLocation != null)
                _locationTransferDetail.FromLocation = new OperationObjects.LocationOP().GetLocation(_locationTransferDetail.FromLocation.ID.Value);
            if (_locationTransferDetail.FromRack != null)
                _locationTransferDetail.FromRack = new OperationObjects.RackOP().GetRack(_locationTransferDetail.FromRack.ID.Value);
            if (_locationTransferDetail.ToStore != null)
                _locationTransferDetail.ToStore = new OperationObjects.StoreOP().GetStore(_locationTransferDetail.ToStore.ID.Value);
            if (_locationTransferDetail.ToLocation != null)
                _locationTransferDetail.ToLocation = new OperationObjects.LocationOP().GetLocation(_locationTransferDetail.ToLocation.ID.Value);
            if (_locationTransferDetail.ToRack != null)
                _locationTransferDetail.ToRack = new OperationObjects.RackOP().GetRack(_locationTransferDetail.ToRack.ID.Value);

            StringBuilder _sbFromLocation = new StringBuilder();
            _sbFromLocation.Append(_locationTransferDetail.FromStore != null ? _locationTransferDetail.FromStore.Description : "-").Append("<br>");
            _sbFromLocation.Append(_locationTransferDetail.FromLocation != null ? _locationTransferDetail.FromLocation.Description : "-").Append("<br>");
            _sbFromLocation.Append(_locationTransferDetail.FromRack != null ? _locationTransferDetail.FromRack.Description : "-");

            StringBuilder _sbToLocation = new StringBuilder();
            _sbToLocation.Append(_locationTransferDetail.ToStore != null ? _locationTransferDetail.ToStore.Description : "-").Append("<br>");
            _sbToLocation.Append(_locationTransferDetail.ToLocation != null ? _locationTransferDetail.ToLocation.Description : "-").Append("<br>");
            _sbToLocation.Append(_locationTransferDetail.ToRack != null ? _locationTransferDetail.ToRack.Description : "-");

            StringBuilder _sbProductItem = new StringBuilder();
            _sbProductItem.Append(_locationTransferDetail.ItemStock.ProductItem != null ? _locationTransferDetail.ItemStock.ProductItem.Code + "-" + _locationTransferDetail.ItemStock.ProductItem.Description : "-").Append("<br>");
            _sbProductItem.Append("[" + _locationTransferDetail.ItemStock.UnitPrice.Value.ToString("F") + " / " + _locationTransferDetail.ItemStock.SellingPrice.Value.ToString("F") + "]");

            e.Row.Cells[1].Text = _sbFromLocation.ToString();
            e.Row.Cells[2].Text = _sbToLocation.ToString();
            e.Row.Cells[3].Text = _sbProductItem.ToString();
            e.Row.Cells[4].Text = _locationTransferDetail.Quantity.ToString();
        }
    }
    protected void DropDownListFromStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListFromLocation);
        Util.ClearDropdown(DropDownListFromRack);
        Util.ClearDropdown(DropDownListProductItem);

        if (DropDownListFromStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListFromStore.SelectedValue))))
                DropDownListFromLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void DropDownListFromLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListFromRack);
        Util.ClearDropdown(DropDownListProductItem);

        if (DropDownListFromLocation.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Rack rack in new OperationObjects.RackOP().SearchRacks(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListFromStore.SelectedValue)), new SolutionObjects.Location(int.Parse(DropDownListFromLocation.SelectedValue))))
                DropDownListFromRack.Items.Add(new ListItem(rack.Description, rack.ID.Value.ToString()));
        }
    }
    protected void DropDownListFromRack_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListProductItem);

        if (DropDownListFromRack.SelectedIndex != 0)
        {
            foreach (SolutionObjects.ItemStock itemStock in new OperationObjects.ItemStockOP().Search(null, new SolutionObjects.Store(int.Parse(DropDownListFromStore.SelectedValue)), new SolutionObjects.Location(int.Parse(DropDownListFromLocation.SelectedValue)), new SolutionObjects.Rack(int.Parse(DropDownListFromRack.SelectedValue)), SolutionObjects.StockTypes.Trading, string.Empty, null, null))
            {
                itemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(itemStock.ProductItem.ID.Value);
                DropDownListProductItem.Items.Add(new ListItem(itemStock.ProductItem.Code + " - " + itemStock.ProductItem.Description + " [" + itemStock.UnitPrice.Value.ToString("F") + " / " + itemStock.SellingPrice.Value.ToString("F") + "]", itemStock.ID.Value.ToString()));
            }
        }
    }
    protected void DropDownListToStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListToLocation);
        Util.ClearDropdown(DropDownListToRack);

        if (DropDownListToStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListToStore.SelectedValue))))
                DropDownListToLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void DropDownListToLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListToRack);

        if (DropDownListToLocation.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Rack rack in new OperationObjects.RackOP().SearchRacks(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListToStore.SelectedValue)), new SolutionObjects.Location(int.Parse(DropDownListToLocation.SelectedValue))))
                DropDownListToRack.Items.Add(new ListItem(rack.Description, rack.ID.Value.ToString()));
        }
    }
    protected void DropDownListProductItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        LabelAvailableQuantity.Text = "-";
        if (DropDownListProductItem.SelectedIndex != 0)
        {
            SolutionObjects.ItemStock _itemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(int.Parse(DropDownListProductItem.SelectedValue));
            LabelAvailableQuantity.Text = _itemStock.CurrentQuantity.Value.ToString();
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        LabelValidateQuantity.Visible = false;
        LabelValidateMultiple.Visible = false;
        LabelValidateMultiple.Text = "Product Item already entered";
        _locationTransfer = ViewState[_target] as SolutionObjects.LocationTransfer;

        bool _hasFound = false;
        foreach (SolutionObjects.LocationTransferDetail locationTransferDetail in _locationTransfer.LocationTransferDetailList)
        {
            if (DropDownListProductItem.SelectedValue == locationTransferDetail.ItemStock.ID.Value.ToString())
            {
                _hasFound = true;
                break;
            }
        }
        if (_hasFound)
        {
            LabelValidateMultiple.Visible = true;
            return;
        }

        if (Util.ParseInt(TextBoxTransferQuantity.Text.Trim()) > Util.ParseInt(LabelAvailableQuantity.Text))
        {
            LabelValidateQuantity.Visible = true;
            return;
        }

        if (DropDownListFromRack.SelectedValue == DropDownListToRack.SelectedValue)
        {
            LabelValidateMultiple.Visible = true;
            LabelValidateMultiple.Text = "From Sub Location and To Sub Location can't be same";
            return;
        }

        SolutionObjects.LocationTransferDetail _locationTransferDetail = new SolutionObjects.LocationTransferDetail();
        _locationTransferDetail.LocationTransfer = _locationTransfer;
        _locationTransferDetail.FromStore = new SolutionObjects.Store(int.Parse(DropDownListFromStore.SelectedValue));
        _locationTransferDetail.FromLocation = new SolutionObjects.Location(int.Parse(DropDownListFromLocation.SelectedValue));
        _locationTransferDetail.FromRack = new SolutionObjects.Rack(int.Parse(DropDownListFromRack.SelectedValue));
        _locationTransferDetail.ToStore = new SolutionObjects.Store(int.Parse(DropDownListToStore.SelectedValue));
        _locationTransferDetail.ToLocation = new SolutionObjects.Location(int.Parse(DropDownListToLocation.SelectedValue));
        _locationTransferDetail.ToRack = new SolutionObjects.Rack(int.Parse(DropDownListToRack.SelectedValue));
        _locationTransferDetail.ItemStock = new SolutionObjects.ItemStock(int.Parse(DropDownListProductItem.SelectedValue));
        _locationTransferDetail.Quantity = Util.ParseInt(TextBoxTransferQuantity.Text.Trim());
        _locationTransferDetail.User = User.Identity.Name;
        _locationTransfer.LocationTransferDetailList.Add(_locationTransferDetail);

        ViewState[_target] = _locationTransfer;
        GridViewItemAdd.DataSource = _locationTransfer.LocationTransferDetailList;
        GridViewItemAdd.DataBind();
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _locationTransfer = ViewState[_target] as SolutionObjects.LocationTransfer;

        if (_locationTransfer.ID == null)
            _locationTransfer.TransferNumber = Util.GenereteSequenceNumber("LTR", DateTime.Today.Year, DateTime.Today.Month);
        _locationTransfer.DateTransfer = Util.ParseDate(TextBoxDateTransfer.Text.Trim());
        _locationTransfer.ReferenceNumber = TextBoxReferenceNumber.Text.Trim();
        _locationTransfer.Remarks = TextBoxRemarks.Text.Trim();
        _locationTransfer.User = User.Identity.Name;

        try
        {
            bool _isNew = false;
            if (_locationTransfer.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.LocationTransferOP().SaveLocationTransfer(_locationTransfer);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>Transfer Number : " + _locationTransfer.TransferNumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                _locationTransfer = new SolutionObjects.LocationTransfer();
                _locationTransfer.LocationTransferDetailList = new CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail>();
                ViewState[_target] = _locationTransfer;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _locationTransfer = ViewState[_target] as SolutionObjects.LocationTransfer;

        if (_locationTransfer.ID != null)
        {
            _locationTransfer = new OperationObjects.LocationTransferOP().GetLocationTransfer(_locationTransfer.ID.Value);
            ViewState[_target] = _locationTransfer;

            TextBoxTransferNumber.Text = _locationTransfer.TransferNumber;
            TextBoxDateTransfer.Text = _locationTransfer.DateTransfer != null ? _locationTransfer.DateTransfer.Value.ToShortDateString() : string.Empty;
            TextBoxReferenceNumber.Text = _locationTransfer.ReferenceNumber;
            TextBoxRemarks.Text = _locationTransfer.Remarks.Trim();

            GridViewItemAdd.DataSource = _locationTransfer.LocationTransferDetailList;
            GridViewItemAdd.DataBind();
        }
        else
        {
            _locationTransfer = new SolutionObjects.LocationTransfer();
            _locationTransfer.LocationTransferDetailList = new CommonObjects.PropertyList<SolutionObjects.LocationTransferDetail>();
            ViewState[_target] = _locationTransfer;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _locationTransfer = new OperationObjects.LocationTransferOP().GetLocationTransfer(_targetid);

            ClearAll();
            ViewState[_target] = _locationTransfer;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxTransferNumber.Text = _locationTransfer.TransferNumber;
            TextBoxDateTransfer.Text = _locationTransfer.DateTransfer != null ? _locationTransfer.DateTransfer.Value.ToShortDateString() : string.Empty;
            TextBoxReferenceNumber.Text = _locationTransfer.ReferenceNumber;
            TextBoxRemarks.Text = _locationTransfer.Remarks.Trim();

            GridViewItemAdd.DataSource = _locationTransfer.LocationTransferDetailList;
            GridViewItemAdd.DataBind();
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _locationTransfer = new OperationObjects.LocationTransferOP().GetLocationTransfer(_targetid);
            _locationTransfer.User = User.Identity.Name;

            int _listCount = _locationTransfer.LocationTransferDetailList.Count - 1;
            while (_listCount >= 0)
            {
                _locationTransfer.LocationTransferDetailList.RemoveAt(_listCount);
                _listCount--;
            }

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.LocationTransferOP().DeleteLocationTransfer(_locationTransfer);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _locationTransfer = new OperationObjects.LocationTransferOP().GetLocationTransfer(_targetid);

            ViewState[_target] = _locationTransfer;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewTransferNumber.Text = Util.FormatEmptyString(_locationTransfer.TransferNumber);
            LabelViewDateTransfer.Text = Util.FormatEmptyString(_locationTransfer.DateTransfer != null ? _locationTransfer.DateTransfer.Value.ToShortDateString() : string.Empty);
            LabelViewReferenceNumber.Text = Util.FormatEmptyString(_locationTransfer.ReferenceNumber);
            LabelViewRemarks.Text = Util.FormatEmptyString(_locationTransfer.Remarks.Trim());

            GridViewItemView.DataSource = _locationTransfer.LocationTransferDetailList;
            GridViewItemView.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _locationTransfer = e.Row.DataItem as SolutionObjects.LocationTransfer;

            Util.EncodeString(e.Row.Cells[3], _locationTransfer.TransferNumber, 20);
            Util.EncodeString(e.Row.Cells[4], _locationTransfer.DateTransfer != null ? _locationTransfer.DateTransfer.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], _locationTransfer.ReferenceNumber, 20);
            Util.EncodeString(e.Row.Cells[6], _locationTransfer.Remarks, 20);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _locationTransfer.TransferNumber + "');");
                }
            }
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.LocationTransferDetail _locationTransferDetail = e.Row.DataItem as SolutionObjects.LocationTransferDetail;
            if (_locationTransferDetail.ItemStock != null)
            {
                _locationTransferDetail.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(_locationTransferDetail.ItemStock.ID.Value);
                _locationTransferDetail.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(_locationTransferDetail.ItemStock.ProductItem.ID.Value);
            }
            if (_locationTransferDetail.FromStore != null)
                _locationTransferDetail.FromStore = new OperationObjects.StoreOP().GetStore(_locationTransferDetail.FromStore.ID.Value);
            if (_locationTransferDetail.FromLocation != null)
                _locationTransferDetail.FromLocation = new OperationObjects.LocationOP().GetLocation(_locationTransferDetail.FromLocation.ID.Value);
            if (_locationTransferDetail.FromRack != null)
                _locationTransferDetail.FromRack = new OperationObjects.RackOP().GetRack(_locationTransferDetail.FromRack.ID.Value);
            if (_locationTransferDetail.ToStore != null)
                _locationTransferDetail.ToStore = new OperationObjects.StoreOP().GetStore(_locationTransferDetail.ToStore.ID.Value);
            if (_locationTransferDetail.ToLocation != null)
                _locationTransferDetail.ToLocation = new OperationObjects.LocationOP().GetLocation(_locationTransferDetail.ToLocation.ID.Value);
            if (_locationTransferDetail.ToRack != null)
                _locationTransferDetail.ToRack = new OperationObjects.RackOP().GetRack(_locationTransferDetail.ToRack.ID.Value);

            StringBuilder _sbFromLocation = new StringBuilder();
            _sbFromLocation.Append(_locationTransferDetail.FromStore != null ? _locationTransferDetail.FromStore.Description : "-").Append("<br>");
            _sbFromLocation.Append(_locationTransferDetail.FromLocation != null ? _locationTransferDetail.FromLocation.Description : "-").Append("<br>");
            _sbFromLocation.Append(_locationTransferDetail.FromRack != null ? _locationTransferDetail.FromRack.Description : "-");

            StringBuilder _sbToLocation = new StringBuilder();
            _sbToLocation.Append(_locationTransferDetail.ToStore != null ? _locationTransferDetail.ToStore.Description : "-").Append("<br>");
            _sbToLocation.Append(_locationTransferDetail.ToLocation != null ? _locationTransferDetail.ToLocation.Description : "-").Append("<br>");
            _sbToLocation.Append(_locationTransferDetail.ToRack != null ? _locationTransferDetail.ToRack.Description : "-");

            StringBuilder _sbProductItem = new StringBuilder();
            _sbProductItem.Append(_locationTransferDetail.ItemStock.ProductItem != null ? _locationTransferDetail.ItemStock.ProductItem.Code + "-" + _locationTransferDetail.ItemStock.ProductItem.Description : "-").Append("<br>");
            _sbProductItem.Append("[" + _locationTransferDetail.ItemStock.UnitPrice.Value.ToString("F") + " / " + _locationTransferDetail.ItemStock.SellingPrice.Value.ToString("F") + "]");

            e.Row.Cells[0].Text = _sbFromLocation.ToString();
            e.Row.Cells[1].Text = _sbToLocation.ToString();
            e.Row.Cells[2].Text = _sbProductItem.ToString();
            e.Row.Cells[3].Text = _locationTransferDetail.Quantity.ToString();
        }
    }

}