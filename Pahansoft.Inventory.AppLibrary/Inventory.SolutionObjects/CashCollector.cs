﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class CashCollector : SolutionObjectBase
    {
        public CashCollector() { }
        public CashCollector(int? id) { this.ID = id; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string TelephoneHome { get; set; }
        public string TelephoneMobile { get; set; }
        public string TelephoneOffice { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}
