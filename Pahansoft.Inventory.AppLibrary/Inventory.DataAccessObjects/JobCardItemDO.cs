﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class JobCardItemDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.JobCardItem JobCardItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateJobCardItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.JobCard != null ? JobCardItem.JobCard.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.ItemStock != null ? JobCardItem.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountPercentage", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.DiscountPercentage);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.DiscountAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TaxAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.TaxAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.NetAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCardItem.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.JobCardItem JobCardItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditJobCardItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCardItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardItem.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.JobCard != null ? JobCardItem.JobCard.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ItemStockID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.ItemStock != null ? JobCardItem.ItemStock.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Quantity", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.Quantity);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@UnitPrice", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.UnitPrice);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountPercentage", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.DiscountPercentage);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.DiscountAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TaxAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.TaxAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardItem.NetAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardItem;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.JobCardItem JobCardItem)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteJobCardItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardItem.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCardItem.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardItem.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardItem;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCardItem.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.JobCardItem Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetJobCardItem";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.JobCardItem JobCardItem = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                        int ItemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                        int QuantityOrdinal = dr.GetOrdinal("Quantity");
                        int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                        int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                        int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                        int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                        int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                        int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                        JobCardItem = new Inventory.SolutionObjects.JobCardItem(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardItem.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardItem.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ItemStockIDOrdinal)) JobCardItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(ItemStockIDOrdinal));
                        if (!dr.IsDBNull(QuantityOrdinal)) JobCardItem.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(UnitPriceOrdinal)) JobCardItem.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardItem.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardItem.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardItem.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardItem.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardItem.NetAmount = dr.GetDecimal(NetAmountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardItem;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardItem> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllJobCardItems";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardItem> JobCardItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCardItem>();
            Inventory.SolutionObjects.JobCardItem JobCardItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int ItemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int QuantityOrdinal = dr.GetOrdinal("Quantity");
                    int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                    int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                    int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                    int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                    while (dr.Read())
                    {
                        JobCardItem = new Inventory.SolutionObjects.JobCardItem(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardItem.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardItem.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ItemStockIDOrdinal)) JobCardItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(ItemStockIDOrdinal));
                        if (!dr.IsDBNull(QuantityOrdinal)) JobCardItem.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(UnitPriceOrdinal)) JobCardItem.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardItem.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardItem.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardItem.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardItem.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardItem.NetAmount = dr.GetDecimal(NetAmountOrdinal);

                        JobCardItemList.Add(JobCardItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardItemList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardItem> GetAllFor(SolutionObjects.JobCard JobCard)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllJobCardItemsForJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardItem> JobCardItemList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCardItem>();
            Inventory.SolutionObjects.JobCardItem JobCardItem;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int ItemStockIDOrdinal = dr.GetOrdinal("ItemStockID");
                    int QuantityOrdinal = dr.GetOrdinal("Quantity");
                    int UnitPriceOrdinal = dr.GetOrdinal("UnitPrice");
                    int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                    int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                    int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                    int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                    while (dr.Read())
                    {
                        JobCardItem = new Inventory.SolutionObjects.JobCardItem(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardItem.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardItem.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ItemStockIDOrdinal)) JobCardItem.ItemStock = new SolutionObjects.ItemStock(dr.GetInt32(ItemStockIDOrdinal));
                        if (!dr.IsDBNull(QuantityOrdinal)) JobCardItem.Quantity = dr.GetDecimal(QuantityOrdinal);
                        if (!dr.IsDBNull(UnitPriceOrdinal)) JobCardItem.UnitPrice = dr.GetDecimal(UnitPriceOrdinal);
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardItem.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardItem.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardItem.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardItem.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardItem.NetAmount = dr.GetDecimal(NetAmountOrdinal);

                        JobCardItemList.Add(JobCardItem);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardItemList;
        }

        #endregion
    }
}
