﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="tran_grn_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
    <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">GRN -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonSearchOperation_Click">Search GRN</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New GRN</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%">GRN Number <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:TextBox ID="TextBoxGRNNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 20%">&nbsp;</td>
                                                                <td style="width: 30%">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date GRN <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxDateGRN" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                        ControlToValidate="TextBoxDateGRN" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxDateGRN"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supplier <span style="color: Red;">*</span>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:DropDownList ID="DropDownListSupplier" runat="server" Width="250px">
                                                                    </asp:DropDownList>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListSupplier"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <Triggers>
              <asp:PostBackTrigger ControlID="GridViewItemAdd" />
              <asp:PostBackTrigger ControlID="ButtonItemAdd" />
              <asp:PostBackTrigger ControlID="DropDownListStore" />
              <asp:PostBackTrigger ControlID="DropDownListLocation" />
             </Triggers>
             <ContentTemplate>--%>
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                                                                            DataKeyNames="ID" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                                                    <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                                                                </asp:ButtonField>
                                                                                <asp:BoundField HeaderText="Product Item">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Lot Number">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Location">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Quantity">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Price">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Date">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Amount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 20%">&nbsp;
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Product Item <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="DropDownListProductItem" runat="server" Width="250px">
                                                                        </asp:DropDownList>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListProductItem"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Lot Number <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxLotNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxLotNumber"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="DropDownListStore" />
                                                                <asp:PostBackTrigger ControlID="DropDownListLocation" />
                                                                <asp:PostBackTrigger ControlID="DropDownListProductItem" />
                                                            </Triggers>
                                                            <ContentTemplate>
                                                                <table style="width: 100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 20%">Store <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListStore" runat="server" Width="250px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="DropDownListStore_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStore"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Location <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListLocation" runat="server" Width="250px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DropDownListLocation"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Sub Location <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListRack" runat="server" Width="250px">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DropDownListRack"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 20%">Quantity <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxQuantity"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                        <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="TextBoxQuantity"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="1000000" MinimumValue="1"
                                                                            Type="Integer" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Unit Price <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxUnitPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                        <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                            Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Selling Price <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxSellingPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                        <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                            Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date Manufactured
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxDateManufactured" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;[dd.mm/yyyy]<asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="TextBoxDateManufactured"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                            Type="Date" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Discount
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="TextBoxDiscount"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                            Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tax
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxTax" runat="server" CssClass="textbox" MaxLength="20" Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="TextBoxTax"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                            Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <strong>
                                                                            <asp:LinkButton ID="LinkButtonCalculate" runat="server" Font-Underline="False"
                                                                                OnClick="LinkButtonCalculate_Click">Calculate</asp:LinkButton>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gross Amount <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxGrossAmount" runat="server" CssClass="textbox"
                                                                            MaxLength="20" Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                                                                            ControlToValidate="TextBoxGrossAmount" Display="Dynamic"
                                                                            ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                        <asp:RangeValidator ID="RangeValidator17" runat="server"
                                                                            ControlToValidate="TextBoxGrossAmount" Display="Dynamic" ErrorMessage="Invalid"
                                                                            MaximumValue="99999999999" MinimumValue="0" Type="Double"
                                                                            ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Net Amount <span style="color: red">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxNetAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                            Width="100px"></asp:TextBox>
                                                                        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                            Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                        <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                            Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                            Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td class="pageButtonArea">
                                                                        <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                                                                            Text="Add" ValidationGroup="ItemAdd" Width="70px" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
                                                <br />
                                                <fieldset>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%">Total Gross <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:TextBox ID="TextBoxTotalGross" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                        Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                                <td style="width: 20%">Total Tax
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:TextBox ID="TextBoxTotalTax" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="TextBoxTax"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                        Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Net <span style="color: Red;">*</span>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxTotalNet" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                        Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                                <td>Total Discount
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxTotalDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;<asp:RangeValidator ID="RangeValidator22" runat="server" ControlToValidate="TextBoxTotalDiscount"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                        Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="vertical-align: text-top; padding-top: 3px">Remarks
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td class="pageButtonArea">
                                                            <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                                                                OnClick="ButtonSubmit_Click" Width="70px" />
                                                            &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                   OnClick="ButtonReset_Click" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Search Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 20%">GRN Number
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchGRNNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Date GRN
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchDateGRN" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateGRN"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="search"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%">Supplier
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DropDownListSrchSupplier" runat="server" Width="250px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="pageButtonArea">
                                                                    <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                                                                        Width="70px" ValidationGroup="search" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
                                                    OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
                                                    OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                    <Columns>
                                                        <asp:ButtonField CommandName="EditRecord" Text="Edit">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="ViewRecord" Text="View">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField HeaderText="GRN Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Date GRN">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>

                                                        <asp:BoundField HeaderText="Supplier">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Net Amount">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="grid-pager" />
                                                    <EmptyDataTemplate>
                                                        <div id="norec" class="no-record-msg">
                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-left: 15px">No Record(s) Found!
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="grid-header" />
                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 20%;">GRN Number
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 79%;">
                                                                    <asp:Label ID="LabelViewGRNNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date GRN
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewDateGRN" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>PO Number
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewPONumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date PO
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewDatePO" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Supplier
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewSupplier" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Tax
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalTax" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Total Discount
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalDiscount" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Gross
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalGross" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Total Net
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalNet" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="Product Item">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Lot Number">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Location">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Quantity">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Price">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Date">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Amount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
    <script language="javascript">
        $("#ctl00_ContentPlaceHolder1_TextBoxDateGRN").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDatePO").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateManufactured").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateExpiry").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateGRN").mask("99/99/9999");
    </script>
</asp:Content>
