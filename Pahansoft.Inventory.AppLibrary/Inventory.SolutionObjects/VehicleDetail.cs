﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class VehicleDetail : SolutionObjectBase
    {
        public VehicleDetail() { }
        public VehicleDetail(int? id) { this.ID = id; }

        public string VehicleNumber { get; set; }
        public SolutionObjects.Customer Customer { get; set; }
    }
}
