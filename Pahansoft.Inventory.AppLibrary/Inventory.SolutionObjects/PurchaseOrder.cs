﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class PurchaseOrder : SolutionObjectBase
    {
        public PurchaseOrder() { }
        public PurchaseOrder(int? id) { this.ID = id; }

        public string PONumber { get; set; }
        public DateTime? DatePO { get; set; }
        public SolutionObjects.Supplier Supplier { get; set; }
        public decimal? TotalAmount { get; set; }
        public string Remarks { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
    }
}
