﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class PurchaseOrderDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreatePurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PONumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.PONumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DatePO", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, PurchaseOrder.DatePO);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrder.Supplier != null ? PurchaseOrder.Supplier.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrder.TotalAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrder;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                PurchaseOrder.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditPurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrder.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, PurchaseOrder.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PONumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.PONumber);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DatePO", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, PurchaseOrder.DatePO);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrder.Supplier != null ? PurchaseOrder.Supplier.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TotalAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, PurchaseOrder.TotalAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Remarks", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.Remarks);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrder;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeletePurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, PurchaseOrder.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, PurchaseOrder.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PurchaseOrder.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = PurchaseOrder;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                PurchaseOrder.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public SolutionObjects.PurchaseOrder Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            SolutionObjects.PurchaseOrder PurchaseOrder = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int PONumberOrdinal = dr.GetOrdinal("PONumber");
                        int DatePOOrdinal = dr.GetOrdinal("DatePO");
                        int SupplierIDOrdinal = dr.GetOrdinal("SupplierID");
                        int TotalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                        int RemarksOrdinal = dr.GetOrdinal("Remarks");

                        PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrder.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PONumberOrdinal)) PurchaseOrder.PONumber = dr.GetString(PONumberOrdinal);
                        if (!dr.IsDBNull(DatePOOrdinal)) PurchaseOrder.DatePO = dr.GetDateTime(DatePOOrdinal);
                        if (!dr.IsDBNull(SupplierIDOrdinal)) PurchaseOrder.Supplier = new SolutionObjects.Supplier(dr.GetInt32(SupplierIDOrdinal));
                        if (!dr.IsDBNull(TotalAmountOrdinal)) PurchaseOrder.TotalAmount = dr.GetDecimal(TotalAmountOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) PurchaseOrder.Remarks = dr.GetString(RemarksOrdinal);

                        PurchaseOrder.PurchaseOrderDetails = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.PurchaseOrderDetail>();
                        foreach (SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail in new DataAccessObjects.PurchaseOrderDetailDO().GetAllFor(PurchaseOrder))
                            PurchaseOrder.PurchaseOrderDetails.Add(PurchaseOrderDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrder;
        }

        public SolutionObjects.PurchaseOrder FindProxy(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetPurchaseOrder";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            SolutionObjects.PurchaseOrder PurchaseOrder = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int PONumberOrdinal = dr.GetOrdinal("PONumber");
                        int DatePOOrdinal = dr.GetOrdinal("DatePO");
                        int SupplierIDOrdinal = dr.GetOrdinal("SupplierID");
                        int TotalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                        int RemarksOrdinal = dr.GetOrdinal("Remarks");

                        PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrder.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PONumberOrdinal)) PurchaseOrder.PONumber = dr.GetString(PONumberOrdinal);
                        if (!dr.IsDBNull(DatePOOrdinal)) PurchaseOrder.DatePO = dr.GetDateTime(DatePOOrdinal);
                        if (!dr.IsDBNull(SupplierIDOrdinal)) PurchaseOrder.Supplier = new SolutionObjects.Supplier(dr.GetInt32(SupplierIDOrdinal));
                        if (!dr.IsDBNull(TotalAmountOrdinal)) PurchaseOrder.TotalAmount = dr.GetDecimal(TotalAmountOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) PurchaseOrder.Remarks = dr.GetString(RemarksOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrder;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrder> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllPurchaseOrders";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var PurchaseOrderList = new System.Collections.Generic.List<SolutionObjects.PurchaseOrder>();
            SolutionObjects.PurchaseOrder PurchaseOrder;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int PONumberOrdinal = dr.GetOrdinal("PONumber");
                    int DatePOOrdinal = dr.GetOrdinal("DatePO");
                    int SupplierIDOrdinal = dr.GetOrdinal("SupplierID");
                    int TotalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                    int RemarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrder.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PONumberOrdinal)) PurchaseOrder.PONumber = dr.GetString(PONumberOrdinal);
                        if (!dr.IsDBNull(DatePOOrdinal)) PurchaseOrder.DatePO = dr.GetDateTime(DatePOOrdinal);
                        if (!dr.IsDBNull(SupplierIDOrdinal)) PurchaseOrder.Supplier = new SolutionObjects.Supplier(dr.GetInt32(SupplierIDOrdinal));
                        if (!dr.IsDBNull(TotalAmountOrdinal)) PurchaseOrder.TotalAmount = dr.GetDecimal(TotalAmountOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) PurchaseOrder.Remarks = dr.GetString(RemarksOrdinal);

                        PurchaseOrderList.Add(PurchaseOrder);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrderList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrder> Search(string PONumber,DateTime? DatePO, int? SupplierID)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_FindAllPurchaseOrders";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@PONumber", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, PONumber != string.Empty ? PONumber : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DatePO", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, command, DatePO);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SupplierID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, SupplierID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            var PurchaseOrderList = new System.Collections.Generic.List<SolutionObjects.PurchaseOrder>();
            SolutionObjects.PurchaseOrder PurchaseOrder;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int PONumberOrdinal = dr.GetOrdinal("PONumber");
                    int DatePOOrdinal = dr.GetOrdinal("DatePO");
                    int SupplierIDOrdinal = dr.GetOrdinal("SupplierID");
                    int TotalAmountOrdinal = dr.GetOrdinal("TotalAmount");
                    int RemarksOrdinal = dr.GetOrdinal("Remarks");

                    while (dr.Read())
                    {
                        PurchaseOrder = new SolutionObjects.PurchaseOrder(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) PurchaseOrder.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(PONumberOrdinal)) PurchaseOrder.PONumber = dr.GetString(PONumberOrdinal);
                        if (!dr.IsDBNull(DatePOOrdinal)) PurchaseOrder.DatePO = dr.GetDateTime(DatePOOrdinal);
                        if (!dr.IsDBNull(SupplierIDOrdinal)) PurchaseOrder.Supplier = new SolutionObjects.Supplier(dr.GetInt32(SupplierIDOrdinal));
                        if (!dr.IsDBNull(TotalAmountOrdinal)) PurchaseOrder.TotalAmount = dr.GetDecimal(TotalAmountOrdinal);
                        if (!dr.IsDBNull(RemarksOrdinal)) PurchaseOrder.Remarks = dr.GetString(RemarksOrdinal);

                        PurchaseOrderList.Add(PurchaseOrder);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return PurchaseOrderList;
        }

        #endregion
    }
}
