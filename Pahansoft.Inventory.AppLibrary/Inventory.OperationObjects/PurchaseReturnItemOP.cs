﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class PurchaseReturnItemOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SavePurchaseReturnItem(Inventory.SolutionObjects.PurchaseReturnItem purchaseReturnItem)
        {
            if (purchaseReturnItem.ID == null)
                return new Inventory.DataAccessObjects.PurchaseReturnItemDO().Create(purchaseReturnItem);
            else
                return new Inventory.DataAccessObjects.PurchaseReturnItemDO().Edit(purchaseReturnItem);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeletePurchaseReturnItem(Inventory.SolutionObjects.PurchaseReturnItem purchaseReturnItem)
        {
            if (purchaseReturnItem.ID != null)
                return new Inventory.DataAccessObjects.PurchaseReturnItemDO().Delete(purchaseReturnItem);
            else
            {
                purchaseReturnItem.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = purchaseReturnItem;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.PurchaseReturnItem GetPurchaseReturnItem(int id)
        {
            return new Inventory.DataAccessObjects.PurchaseReturnItemDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> GetAllPurchaseReturnItems()
        {
            return new Inventory.DataAccessObjects.PurchaseReturnItemDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.PurchaseReturnItem> GetAllFor(SolutionObjects.PurchaseReturn purchaseReturn)
        {
            return new Inventory.DataAccessObjects.PurchaseReturnItemDO().GetAllFor(purchaseReturn);
        }
    }
}
