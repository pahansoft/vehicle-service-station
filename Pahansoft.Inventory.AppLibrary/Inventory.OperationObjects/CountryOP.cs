﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ContryOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCountry(Inventory.SolutionObjects.Country country)
        {
            if (country.ID == null)
                return new Inventory.DataAccessObjects.ContryDO().Create(country);
            else
                return new Inventory.DataAccessObjects.ContryDO().Edit(country);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCountry(Inventory.SolutionObjects.Country country)
        {
            if (country.ID != null)
                return new Inventory.DataAccessObjects.ContryDO().Delete(country);
            else
            {
                country.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = country;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Country GetCountry(int id)
        {
            return new Inventory.DataAccessObjects.ContryDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Country> GetAllCountries()
        {
            return new Inventory.DataAccessObjects.ContryDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Country> SearchCountries(string description)
        {
            return new Inventory.DataAccessObjects.ContryDO().Search(description);
        }
    }
}
