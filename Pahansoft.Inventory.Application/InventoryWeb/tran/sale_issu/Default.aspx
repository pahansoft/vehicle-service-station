﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
 CodeFile="Default.aspx.cs" Inherits="tran_sale_issu_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
 <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
 <script src="../../inc_fils/js/auto_comp/common.js" type="text/javascript"></script>
 <script type="text/javascript">
  function print(salesIssueID) {
   var WinPrint = window.open("repo.aspx?SalesIssueID=" + salesIssueID, '', 'left=5,top=5,width=1005,height=700,toolbar=0,scrollbars=1,status=0');
   WinPrint.focus();
  }
 </script>
 <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground">
     <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
      <tr>
       <td class="pageHedder">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
         <tr>
          <td style="height: 17px">
           Sales Issue -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
           <asp:ScriptManager ID="ScriptManager1" runat="server">
           </asp:ScriptManager>
          </td>
          <td align="right" style="height: 17px">
           <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonSearchOperation_Click">Search Sales Issue</asp:LinkButton>
           <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
            OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New Sales Issue</asp:LinkButton>
          </td>
         </tr>
        </table>
       </td>
      </tr>
      <tr>
       <td>
        <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke;
             border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid;
             border-bottom: gainsboro 1px solid;" cellpadding="0" cellspacing="0">
             <tr>
              <td style="height: 50px">
               <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px; width: 35px">
                   <img src="../../images/warning.png" />
                  </td>
                  <td>
                   <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                  </td>
                 </tr>
                </table>
               </asp:Panel>
               <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                 <tr>
                  <td style="padding-left: 20px;">
                   <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                   &nbsp;
                  </td>
                  <td align="right" style="padding-right: 20px">
                   <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                    Style="border-top-style: none; border-right-style: none; border-left-style: none;
                    border-bottom-style: none" BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                    Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                  </td>
                 </tr>
                </table>
               </asp:Panel>
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Sales Issue Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Issue Number <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxIssueNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                </td>
                <td style="width: 20%">
                 Date Issue <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxDateIssue" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                  ControlToValidate="TextBoxDateIssue" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxDateIssue"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td>
                 Customer <span style="color: Red;">*</span>
                </td>
                <td colspan="3">
                 <asp:DropDownList ID="DropDownListCustomer" runat="server" Width="250px">
                 </asp:DropDownList>
                 &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListCustomer"
                  Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <Triggers>
              <asp:PostBackTrigger ControlID="GridViewItemAdd" />
              <asp:PostBackTrigger ControlID="ButtonItemAdd" />
              <asp:PostBackTrigger ControlID="DropDownListProductItem" />
             </Triggers>
             <ContentTemplate>
              <fieldset>
               <legend style="font-weight: bold">Issue Item(s)</legend>
               <div style="padding: 10px;">
                <table style="width: 100%" border="0">
                 <tbody>
                  <tr>
                   <td colspan="2">
                    <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                     OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                     DataKeyNames="ID" AutoGenerateColumns="False">
                     <PagerSettings Mode="NumericFirstLast" />
                     <RowStyle CssClass="grid-row-normal-stc" />
                     <Columns>
                      <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                       <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                      </asp:ButtonField>
                      <asp:BoundField HeaderText="Product Item">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Quantity">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Discount">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Tax">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Gross">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Net">
                       <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                        Wrap="False" />
                       <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                      </asp:BoundField>
                     </Columns>
                     <PagerStyle CssClass="grid-pager" />
                     <EmptyDataTemplate>
                      <div id="norec" class="no-record-msg">
                       <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                        <tbody>
                         <tr>
                          <td style="padding-left: 15px">
                           No Items(s) Found!
                          </td>
                         </tr>
                        </tbody>
                       </table>
                      </div>
                     </EmptyDataTemplate>
                     <HeaderStyle CssClass="grid-header" />
                     <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                    </asp:GridView>
                   </td>
                  </tr>
                  <tr>
                   <td style="width: 20%">
                    &nbsp;
                   </td>
                   <td>
                   </td>
                  </tr>
                  <tr>
                   <td style="width: 20%; vertical-align: text-top; border-top-style: 3px">
                    Product Item <span style="color: red">*</span>
                   </td>
                   <td>
                    <div id="DivFilter1" style="display: block">
                     <asp:DropDownList ID="DropDownListProductItem" runat="server" AutoPostBack="True"
                      OnSelectedIndexChanged="DropDownListProductItem_SelectedIndexChanged" Width="450px">
                     </asp:DropDownList>
                     <input id="ButtonFilter" title="Filter" type="button" value="..." onclick="showFilter('DivFilter1','DivFilter2','ButtonSearchFilter');"
                      class="button" style="width: 30px" size="10" />
                     &nbsp;<asp:RequiredFieldValidator ID="RequiredFilter1" runat="server" ControlToValidate="DropDownListProductItem"
                      ErrorMessage="Required" Display="Dynamic" ValidationGroup="AddItem"></asp:RequiredFieldValidator></div>
                    <div id="DivFilter2" style="display: none">
                     <table class="search-inside-box" width="450">
                      <tr>
                       <td style="text-align: left">
                        <table width="100%" border="0">
                         <tr>
                          <td nowrap="nowrap" style="text-align: left; width: 50px;">
                           &nbsp;
                          </td>
                          <td nowrap="nowrap">
                          </td>
                          <td style="vertical-align: top; width: 20px; height: 20px; text-align: right">
                           <img src="../../images/close.png" onclick="hideFilter('DivFilter1','DivFilter2');"
                            style="cursor: pointer" />
                          </td>
                         </tr>
                         <tr>
                          <td style="text-align: left; width: 75px; padding-left: 5px;" nowrap="nowrap">
                           Code
                          </td>
                          <td nowrap="noWrap">
                           <asp:TextBox ID="TextBoxFilterCode" runat="server" CssClass="textbox" MaxLength="50"
                            Width="100px"></asp:TextBox>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td style="text-align: left; width: 75px; padding-left: 5px;" nowrap="nowrap">
                           Description
                          </td>
                          <td nowrap="noWrap">
                           <asp:TextBox ID="TextBoxFilterDescription" runat="server" CssClass="textbox" MaxLength="200"
                            Width="250px"></asp:TextBox>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr style="display: none">
                          <td style="text-align: left; width: 75px; padding-left: 5px;" nowrap="nowrap">
                           Lot Number
                          </td>
                          <td nowrap="noWrap">
                           <asp:TextBox ID="TextBoxFilterLotNumber" runat="server" CssClass="textbox" MaxLength="50"
                            Width="100px"></asp:TextBox>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td>
                          </td>
                          <td nowrap="noWrap">
                           <asp:Button ID="ButtonSearchFilter" runat="server" Text="Go" CssClass="search-inside-box"
                            Width="30px" OnClick="ButtonSearchFilter_Click"></asp:Button>
                          </td>
                          <td>
                          </td>
                         </tr>
                         <tr>
                          <td>
                           &nbsp;
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                         </tr>
                        </table>
                       </td>
                      </tr>
                     </table>
                    </div>
                   </td>
                  </tr>
                  <tr>
                   <td style="vertical-align: text-top; border-top-style: 3px; height: 20px">
                    Available Quantity
                   </td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelAvailableQuantity" runat="server" Text="-"></asp:Label></strong>
                   </td>
                  </tr>
                  <tr>
                   <td style="vertical-align: text-top; border-top-style: 3px; height: 20px">
                    Selling Price</td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelSellingPrice" runat="server" Text="-"></asp:Label></strong>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td style="vertical-align: text-top; border-top-style: 3px">
                    Location
                   </td>
                   <td>
                    <strong>
                    <asp:Label ID="LabelLocation" runat="server" Text="-"></asp:Label>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Quantity <span style="color: red">*</span>
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="textbox" MaxLength="20"
                     Width="100px"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="TextBoxQuantity"
                     Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="TextBoxQuantity"
                     Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999" MinimumValue="0" Type="Integer"
                     ValidationGroup="ItemAdd"></asp:RangeValidator>
                    <strong>
                     <asp:Label ID="LabelValidateQuantity" runat="server" Style="color: #FF0000" Text="Quantity Cannot be greater than the Available Quantity"
                      Visible="False"></asp:Label>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Discount
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxDiscount" runat="server" CssClass="textbox" MaxLength="20"
                     Width="100px"></asp:TextBox>
                    &nbsp;<asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="TextBoxDiscount"
                     Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                     Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Tax
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxTax" runat="server" CssClass="textbox" MaxLength="20" Width="100px"></asp:TextBox>
                    &nbsp;<asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="TextBoxTax"
                     Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                     Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    &nbsp;</td>
                   <td>
                    <strong>
                    <asp:LinkButton ID="LinkButtonCalculate" runat="server" Font-Underline="False" 
                     OnClick="LinkButtonCalculate_Click">Calculate</asp:LinkButton>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Gross Amount <span style="color: red">*</span>
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxGrossAmount" runat="server" CssClass="textbox" 
                     MaxLength="20" Width="100px"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                     ControlToValidate="TextBoxGrossAmount" Display="Dynamic" 
                     ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator17" runat="server" 
                     ControlToValidate="TextBoxGrossAmount" Display="Dynamic" ErrorMessage="Invalid" 
                     MaximumValue="99999999999" MinimumValue="0" Type="Double" 
                     ValidationGroup="ItemAdd"></asp:RangeValidator>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Net Amount <span style="color: red">*</span>
                   </td>
                   <td>
                    <asp:TextBox ID="TextBoxNetAmount" runat="server" CssClass="textbox" MaxLength="20"
                     Width="100px"></asp:TextBox>
                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxNetAmount"
                     Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="TextBoxNetAmount"
                     Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                     Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                   </td>
                  </tr>
                  <tr>
                   <td>
                    &nbsp;
                   </td>
                   <td>
                    <strong>
                     <asp:Label ID="LabelValidateMultiple" runat="server" Style="color: #FF0000" Text="Product Item already entered"
                      Visible="False"></asp:Label>
                    </strong>
                   </td>
                  </tr>
                  <tr>
                   <td>
                   </td>
                   <td class="pageButtonArea">
                    <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                     Text="Add" ValidationGroup="ItemAdd" Width="70px" />
                   </td>
                  </tr>
                 </tbody>
                </table>
               </div>
              </fieldset>
             </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <fieldset>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Credit Sale <span style="color: Red;">*</span>
                </td>
                <td>
                 <asp:CheckBox ID="CheckBoxCreditSale" runat="server" />
                </td>
                <td style="width: 20%">
                 Initial Payment
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxInitialPayment" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxInitialPayment"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Total Gross <span style="color: Red;">*</span>
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxTotalGross" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxTotalGross"
                  Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="TextBoxTotalGross"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
                <td style="width: 20%">
                 Total Tax
                </td>
                <td style="width: 30%">
                 <asp:TextBox ID="TextBoxTotalTax" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="TextBoxTax"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td>
                 Total Net <span style="color: Red;">*</span>
                </td>
                <td>
                 <asp:TextBox ID="TextBoxTotalNet" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBoxTotalNet"
                  Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                 <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="TextBoxTotalNet"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
                <td>
                 Total Discount
                </td>
                <td>
                 <asp:TextBox ID="TextBoxTotalDiscount" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;<asp:RangeValidator ID="RangeValidator22" runat="server" ControlToValidate="TextBoxTotalDiscount"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                  Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="vertical-align: text-top; padding-top: 3px">
                 Remarks
                </td>
                <td colspan="3">
                 <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                  Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <table border="0" style="width: 100%">
             <tr>
              <td class="pageButtonArea">
               <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                OnClick="ButtonSubmit_Click" Width="70px" />
               &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                OnClick="ButtonReset_Click" Width="70px" />
              </td>
             </tr>
            </table>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Search Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr>
                <td style="width: 20%">
                 Issue Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchIssueNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Card Number
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchCardNumber" runat="server" CssClass="textbox" MaxLength="20"
                  Width="150px"></asp:TextBox>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Date Issue
                </td>
                <td>
                 <asp:TextBox ID="TextBoxSrchDateIssue" runat="server" CssClass="textbox" MaxLength="20"
                  Width="100px"></asp:TextBox>
                 &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateIssue"
                  Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                  Type="Date" ValidationGroup="search"></asp:RangeValidator>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Customer
                </td>
                <td>
                 <asp:DropDownList ID="DropDownListSrchCustomer" runat="server" Width="250px">
                 </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td style="width: 20%">
                 Importer
                </td>
                <td>
                 <asp:DropDownList ID="DropDownListSrchSalesRef" runat="server" Width="250px">
                 </asp:DropDownList>
                </td>
               </tr>
               <tr>
                <td>
                </td>
                <td class="pageButtonArea">
                 <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                  Width="70px" ValidationGroup="search" />
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
             CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
             OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
             OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
             <PagerSettings Mode="NumericFirstLast" />
             <RowStyle CssClass="grid-row-normal-stc" />
             <Columns>
              <asp:ButtonField CommandName="EditRecord" Text="Edit" Visible="false">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:ButtonField CommandName="ViewRecord" Text="View">
               <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                Width="20px" />
              </asp:ButtonField>
              <asp:BoundField HeaderText="Issue Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Card Number">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Date Issue">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Customer">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Credit Sale">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Net Amount">
               <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                Wrap="False" />
               <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
              </asp:BoundField>
             </Columns>
             <PagerStyle CssClass="grid-pager" />
             <EmptyDataTemplate>
              <div id="norec" class="no-record-msg">
               <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                <tbody>
                 <tr>
                  <td style="padding-left: 15px">
                   No Record(s) Found!
                  </td>
                 </tr>
                </tbody>
               </table>
              </div>
             </EmptyDataTemplate>
             <HeaderStyle CssClass="grid-header" />
             <AlternatingRowStyle CssClass="grid-row-alt-stc" />
            </asp:GridView>
           </td>
          </tr>
         </table>
        </asp:Panel>
        <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
          <tr>
           <td class="pageDataArea">
            <fieldset>
             <legend style="font-weight: bold">Sales Issue Details</legend>
             <div style="padding: 10px;">
              <table border="0" style="width: 100%">
               <tr class="view-back">
                <td style="width: 20%;">
                 Issue Number
                </td>
                <td style="width: 1%; text-align: center;">
                 :
                </td>
                <td style="width: 79%;">
                 <asp:Label ID="LabelViewIssueNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Card Number
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCardNumber" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Date Issue
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewDateIssue" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Customer
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCustomer" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Ref Commision Amount
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewRefCommisionAmount" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Credit Sale
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewCreditSale" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Initial Payment
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewInitialPayment" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Total Discount
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewTotalDiscount" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Total Tax
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewTotalTax" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Total Gross
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewTotalGross" runat="server"></asp:Label>
                </td>
               </tr>
               <tr class="view-back">
                <td>
                 Total Net
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewTotalNet" runat="server"></asp:Label>
                </td>
               </tr>
               <tr>
                <td>
                 Remarks
                </td>
                <td style="text-align: center;">
                 :
                </td>
                <td>
                 <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                </td>
               </tr>
              </table>
             </div>
            </fieldset>
            <br />
            <fieldset>
             <legend style="font-weight: bold">Issue Item(s)</legend>
             <div style="padding: 10px;">
              <table style="width: 100%" border="0">
               <tbody>
                <tr>
                 <td>
                  <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                   OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                   <PagerSettings Mode="NumericFirstLast" />
                   <RowStyle CssClass="grid-row-normal-stc" />
                   <Columns>
                    <asp:BoundField HeaderText="Product Item">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Quantity">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Discount">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Tax">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Gross">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Net">
                     <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                      Wrap="False" />
                     <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                    </asp:BoundField>
                   </Columns>
                   <PagerStyle CssClass="grid-pager" />
                   <EmptyDataTemplate>
                    <div id="norec" class="no-record-msg">
                     <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                      <tbody>
                       <tr>
                        <td style="padding-left: 15px">
                         No Items(s) Found!
                        </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                   </EmptyDataTemplate>
                   <HeaderStyle CssClass="grid-header" />
                   <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                  </asp:GridView>
                 </td>
                </tr>
               </tbody>
              </table>
             </div>
            </fieldset>
           </td>
          </tr>
         </table>
        </asp:Panel>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </asp:Panel>
 <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
   <tr>
    <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px;
     padding-top: 15px" align="right">
     <img src="../../../images/AccessDenied.jpg" />
    </td>
    <caption>
     <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold;
      color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute;
       top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
    </caption>
   </tr>
  </table>
 </asp:Panel>
 <script language="javascript">
  $("#ctl00_ContentPlaceHolder1_TextBoxDateIssue").mask("99/99/9999");
  $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateIssue").mask("99/99/9999");
 </script>
</asp:Content>
