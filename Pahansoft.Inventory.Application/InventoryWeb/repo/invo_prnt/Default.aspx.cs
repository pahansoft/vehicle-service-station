﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.Web.Services;
using System.Web.UI.HtmlControls;

#endregion

public partial class repo_invo_prnt_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string targetCustomers = "TargetCustomers";

    #endregion

    #region - Private Methods -

    private void Search()
    {
        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;

        int? CustomerID = null;
        if (TextBoxSrchCustomer.Text.Trim() != string.Empty)
        {
            var Customer = Customers.FirstOrDefault(t => t.Name == TextBoxSrchCustomer.Text);
            if (Customer != null)
                CustomerID = Customer.ID;
        }
        bool? ApprovedOnly = null;
        //if (CheckBoxSrchApprovedOnly.Checked)
            ApprovedOnly = true;

        GridViewSearchList.DataSource = new OperationObjects.JobCardOP().Search(TextBoxSrchJobNumber.Text.Trim(), TextBoxSrchVehicleNumber.Text.Trim(), CustomerID, ApprovedOnly);
        GridViewSearchList.DataBind();
    }

    [WebMethod(EnableSession = true)]
    public static List<string> GetCustomers(string Text)
    {
        var Customers = HttpContext.Current.Session[targetCustomers] as IList<SolutionObjects.Customer>;

        var returnList = new List<string>();
        foreach (var item in Customers.Where(t => t.Code.ToLower().Contains(Text.ToLower()) || t.Name.ToLower().Contains(Text.ToLower())).ToList())
            returnList.Add(item.Name);

        return returnList;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var Customers = new OperationObjects.CustomerOP().GetAllCustomers();
            HttpContext.Current.Session[targetCustomers] = Customers;

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("GenerateRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());

            HtmlGenericControl dispval = new HtmlGenericControl();
            dispval.InnerHtml = "<script language='javascript'> print('" + _targetid + "');</script>";
            Page.Controls.Add(dispval);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var JobCard = e.Row.DataItem as SolutionObjects.JobCard;
            if (JobCard.Customer != null)
                JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);

            Util.EncodeString(e.Row.Cells[1], JobCard.JobNumber, 20);
            Util.EncodeString(e.Row.Cells[2], JobCard.VehicleNumber, 20);
            Util.EncodeString(e.Row.Cells[3], JobCard.Customer != null ? JobCard.Customer.Name : string.Empty, 100);
            Util.EncodeString(e.Row.Cells[4], JobCard.StartDate != null ? JobCard.StartDate.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[5], JobCard.EndDate != null ? JobCard.EndDate.Value.ToShortDateString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[6], JobCard.TotalNet != null ? JobCard.TotalNet.Value.ToString() : string.Empty, 20);
            Util.EncodeString(e.Row.Cells[7], JobCard.IsApproved ? "Yes" : "No", 5);
        }
    }
}