﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Store : SolutionObjectBase
    {
        public Store() { }
        public Store(int? id) { this.ID = id; }

        public string Description { get; set; }
    }
}
