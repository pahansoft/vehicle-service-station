﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class CashCollectionSummaryOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveCashCollectionSummary(Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (cashCollectionSummary.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.CashCollectionSummaryDO().Create(cashCollectionSummary);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new CashCollectionDetail List
                    foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in cashCollectionSummary.CashCollectionDetailList)
                    {
                        innerDTO = new DataAccessObjects.CashCollectionDetailDO().Create(cashCollectionDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add new CustomerPayment List
                    foreach (SolutionObjects.CustomerPayment customerPayment in cashCollectionSummary.CustomerPaymentList)
                    {
                        if (customerPayment.ID == null)
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(customerPayment);
                        else
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Edit(customerPayment);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add new CashCollectorCommission List
                    foreach (SolutionObjects.CashCollectorCommission cashCollectorCommission in cashCollectionSummary.CashCollectorCommissionList)
                    {
                        innerDTO = new DataAccessObjects.CashCollectorCommissionDO().Create(cashCollectorCommission);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.CashCollectionSummaryDO().Edit(cashCollectionSummary);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit CashCollectionDetail List
                    foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in cashCollectionSummary.CashCollectionDetailList)
                    {
                        if (cashCollectionDetail.ID == null)
                            innerDTO = new DataAccessObjects.CashCollectionDetailDO().Create(cashCollectionDetail);
                        else
                            innerDTO = new DataAccessObjects.CashCollectionDetailDO().Edit(cashCollectionDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete CashCollectionDetail List
                    foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in cashCollectionSummary.CashCollectionDetailList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.CashCollectionDetailDO().Delete(cashCollectionDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add, Edit CustomerPayment List
                    foreach (SolutionObjects.CustomerPayment customerPayment in cashCollectionSummary.CustomerPaymentList)
                    {
                        if (customerPayment.ID == null)
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Create(customerPayment);
                        else
                            innerDTO = new DataAccessObjects.CustomerPaymentDO().Edit(customerPayment);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);

                    }

                    // Delete CustomerPayment List
                    foreach (SolutionObjects.CustomerPayment customerPayment in cashCollectionSummary.CustomerPaymentList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.CustomerPaymentDO().Delete(customerPayment);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Add, Edit CashCollectorCommission List
                    foreach (SolutionObjects.CashCollectorCommission cashCollectorCommission in cashCollectionSummary.CashCollectorCommissionList)
                    {
                        if (cashCollectorCommission.ID == null)
                            innerDTO = new DataAccessObjects.CashCollectorCommissionDO().Create(cashCollectorCommission);
                        else
                            innerDTO = new DataAccessObjects.CashCollectorCommissionDO().Edit(cashCollectorCommission);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete CashCollectorCommission List
                    foreach (SolutionObjects.CashCollectorCommission cashCollectorCommission in cashCollectionSummary.CashCollectorCommissionList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.CashCollectorCommissionDO().Delete(cashCollectorCommission);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteCashCollectionSummary(Inventory.SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.CashCollectionSummaryDO().Delete(cashCollectionSummary);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete CashCollectionDetail List
                foreach (SolutionObjects.CashCollectionDetail cashCollectionDetail in cashCollectionSummary.CashCollectionDetailList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.CashCollectionDetailDO().Delete(cashCollectionDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Delete CustomerPayment List
                foreach (SolutionObjects.CustomerPayment customerPayment in cashCollectionSummary.CustomerPaymentList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.CustomerPaymentDO().Delete(customerPayment);

                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                // Delete CashCollectorCommission List
                foreach (SolutionObjects.CashCollectorCommission cashCollectorCommission in cashCollectionSummary.CashCollectorCommissionList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.CashCollectorCommissionDO().Delete(cashCollectorCommission);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.CashCollectionSummary GetCashCollectionSummary(int id)
        {
            return new Inventory.DataAccessObjects.CashCollectionSummaryDO().Find(id);
        }

        public Inventory.SolutionObjects.CashCollectionSummary GetCashCollectionSummaryProxy(int id)
        {
            return new Inventory.DataAccessObjects.CashCollectionSummaryDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> GetAllCashCollectionSummaries()
        {
            return new Inventory.DataAccessObjects.CashCollectionSummaryDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionSummary> SearchCashCollectionSummaries(string cashCollectionNumber, DateTime? dateCollected, SolutionObjects.CashCollector cashCollector)
        {
            return new Inventory.DataAccessObjects.CashCollectionSummaryDO().Search(cashCollectionNumber, dateCollected, cashCollector);
        }

    }
}
