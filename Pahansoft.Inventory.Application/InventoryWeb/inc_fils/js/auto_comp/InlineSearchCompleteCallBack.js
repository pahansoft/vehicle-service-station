﻿// JScript File

addLoadListener(initAJAXCall);
var _finalLookupKey = null;
var _mainSearchBox = null;
var _lastUpdatedNumber = 0;
var _pageSize = 10;
var _formObjectType = "Object";
var _formObjectMultiple = "Objects";

var _objectSearchResultList = null;

function initAJAXCall() {
    var objList = document.forms[0].elements;
    for (var i = 0; i < objList.length; i++) {
        if (objList[i].className == "pageInlineSearchTextBox") {
            objList[i].onkeyup = initLoadObjectCall;
        }
    }
}

function initLoadObjectCall() {
    _lastUpdatedNumber++;
    var _innerSerachQuery = this.value;
    _mainSearchBox = this;
    _finalLookupKey = _innerSerachQuery;
    setTimeout("UpdateFormObjectList(" + _lastUpdatedNumber + ")", 500);
}

function UpdateFormObjectList(key) {

    if (key == _lastUpdatedNumber) {
        var _innerSerachQuery = _mainSearchBox.value;
        _mainSearchBox.className = "pageInlineSearchTextBox_busy";
        _mainSearchBox.blur();

        document.getElementById("TD_pageInlineSearchResult").innerHTML = "<span style=\"font-size:14px;color:#b0b0b0;padding-left: 0px; padding-right: 0px;\"><img src=\"../../../images/loadingSmall.gif\" /> Please Wait ! " + _formObjectType + " List is updating.</span>";

        if (_innerSerachQuery == "")
        {
            _objectSearchResultList = null;
            document.getElementById("TD_pageInlineSearchResult").innerHTML = document.getElementById("PageInlineSearchHint").innerHTML;
            _mainSearchBox.className = "pageInlineSearchTextBox";
            _mainSearchBox.focus();
            return;
        }

        var xhr;
        try {
            xhr = new XMLHttpRequest();
        }
        catch (error) {
            try {
                xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (error) {
                xhr = null;
            }
        }

        if (xhr != null) {
            xhr.open('POST', '?callType=ajaxGetLookUpList&LookupKey=' + _innerSerachQuery, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200 || xhr.status == 304) {

                        var _responseObjectListString = xhr.responseText;

                        if (_responseObjectListString == "0") {
                            document.getElementById("TD_pageInlineSearchResult").innerHTML = "<span style=\"font-size:14px;color:#b0b0b0;padding-left: 0px; padding-right: 0px;\">No " + _formObjectMultiple + " found for '" + _innerSerachQuery + "'</span>";
                            document.getElementById("HiddenFieldSearchResultTable").value = "";
                            _mainSearchBox.className = "pageInlineSearchTextBox";
                            _mainSearchBox.focus();
                        }
                        else {
                            if (_responseObjectListString != "") {
                                var _returnedXMLobject = ConverttoXMLObject(_responseObjectListString);
                                var _returnedObjectsList = _returnedXMLobject.documentElement.getElementsByTagName("MainObject");

                                if (_returnedObjectsList.length > 0) {
                                    _objectSearchResultList = _returnedObjectsList;
                                    DrawFormObjectListTable(0, true);
                                }
                                else {
                                    document.getElementById("TD_pageInlineSearchResult").innerHTML = "<span style=\"font-size:14px;color:#b0b0b0;padding-left: 0px; padding-right: 0px;\">No " + _formObjectMultiple + " found for '" + _innerSerachQuery + "'</span>";
                                    document.getElementById("HiddenFieldSearchResultTable").value = "";
                                }
                                _mainSearchBox.className = "pageInlineSearchTextBox";
                                _mainSearchBox.focus();

                            }
                        }
                    }
                    else {
                        alert('error');
                    }
                }
            };

            xhr.send(null);

            return false;
        }
        else {
            return true;
        }
    }

}

function DrawFormObjectListTable(pageIndex, isLoding) {

    var _startingIndex = pageIndex * _pageSize;
    var _endingIndex = (pageIndex + 1) * _pageSize;
    if (_endingIndex > _objectSearchResultList.length)
        _endingIndex = _objectSearchResultList.length;

    var _tableText = "<table id=\"TABLE_SearchResult\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" class=\"pageInlineSearchResultMainTable\">";
    for (var i = _startingIndex; i < _endingIndex; i++) {
        _tableText += "<tr class=\"pageInlineSearchResulTR\" onclick=\"BlinkListMenue(this,'" + _objectSearchResultList[i].getAttribute("ServerID") + "');\"><td style=\"border-right: #cccccc 1px solid;\">";
        _tableText += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td class=\"pageInlineSearchResulInsideTable\"><img src=\"../../../images/selectionRow.png\" /></td><td>";

        if (_objectSearchResultList[i].getAttribute("ServerText").length > 30) {
            _tableText += "<span title='" + _objectSearchResultList[i].getAttribute("ServerText").replace(/#@1@#/g, "&") + "'>" + _objectSearchResultList[i].getAttribute("ServerText").replace(/#@1@#/g, "&").substr(0, 30) + "...</span>";
        }
        else {
            _tableText += _objectSearchResultList[i].getAttribute("ServerText").replace(/#@1@#/g, "&");
        }
        _tableText += "</td></tr></table>";        
        _tableText += "</td></tr>";
    }
    _tableText += "<tr class=\"TD_pageInlineSearchResultR\"><td colspan=\"2\">";
    var _drawingpageIndex = 1;

    do {
        if (_drawingpageIndex != 1)
            _tableText += ",";

        if (pageIndex == (_drawingpageIndex - 1)) {
            _tableText += _drawingpageIndex;
        }
        else {
            _tableText += "<span style=\"cursor:pointer;padding-left: 1px;padding-right: 1px;text-decoration: underline;\" onclick=\"DrawFormObjectListTable(" + (_drawingpageIndex - 1) + ")\">" + _drawingpageIndex + "</span>";
        }
        _drawingpageIndex++;
    } while (_objectSearchResultList.length > ((_drawingpageIndex - 1) * _pageSize))
    _tableText += "</td></tr>";
    _tableText += "</table>";

    document.getElementById("TD_pageInlineSearchResult").innerHTML = _tableText;
    document.getElementById("HiddenFieldSearchResultTable").value = _tableText;    
}


function ConverttoXMLObject(text) {

    text = text.replace(/&/g, "#@1@#");
    if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.async = 'false';
        doc.loadXML(text);
    } else {
        var parser = new DOMParser();
        var doc = parser.parseFromString(text, 'text/xml');
    }
    return doc;
}

function addLoadListener(fn) {
    if (typeof window.addEventListener != 'undefined') window.addEventListener('load', fn, false);
    else if (typeof document.addEventListener != 'undefined') document.addEventListener('load', fn, false);
    else if (typeof window.attachEvent != 'undefined') window.attachEvent('onload', fn);
    else {
        var oldfn = window.onload
        if (typeof window.onload != 'function') window.onload = fn;
        else window.onload = function () { oldfn(); fn(); }
    }
}




