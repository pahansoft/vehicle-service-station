﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class PurchaseOrderOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (PurchaseOrder.ID == null)
                {
                    dto = new DataAccessObjects.PurchaseOrderDO().Create(PurchaseOrder);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new PurchaseOrderDetail List
                    foreach (SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail in PurchaseOrder.PurchaseOrderDetails)
                    {
                        innerDTO = new DataAccessObjects.PurchaseOrderDetailDO().Create(PurchaseOrderDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new DataAccessObjects.PurchaseOrderDO().Edit(PurchaseOrder);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit PurchaseOrderDetail List
                    foreach (SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail in PurchaseOrder.PurchaseOrderDetails)
                    {
                        if (PurchaseOrderDetail.ID == null)
                            innerDTO = new DataAccessObjects.PurchaseOrderDetailDO().Create(PurchaseOrderDetail);
                        else
                            innerDTO = new DataAccessObjects.PurchaseOrderDetailDO().Edit(PurchaseOrderDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete PurchaseOrderDetail List
                    foreach (SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail in PurchaseOrder.PurchaseOrderDetails.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.PurchaseOrderDetailDO().Delete(PurchaseOrderDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.PurchaseOrder PurchaseOrder)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new DataAccessObjects.PurchaseOrderDO().Delete(PurchaseOrder);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete PurchaseOrderDetail List
                foreach (SolutionObjects.PurchaseOrderDetail PurchaseOrderDetail in PurchaseOrder.PurchaseOrderDetails.RemovedList)
                {
                    innerDTO = new DataAccessObjects.PurchaseOrderDetailDO().Delete(PurchaseOrderDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                scope.Complete();
            }
            return dto;
        }

        public SolutionObjects.PurchaseOrder Get(int id)
        {
            return new DataAccessObjects.PurchaseOrderDO().Find(id);
        }

        public SolutionObjects.PurchaseOrder GetProxy(int id)
        {
            return new DataAccessObjects.PurchaseOrderDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrder> GetAll()
        {
            return new DataAccessObjects.PurchaseOrderDO().GetAll();
        }

        public System.Collections.Generic.IList<SolutionObjects.PurchaseOrder> Search(string PONumber, DateTime? DatePO, int? SupplierID)
        {
            return new DataAccessObjects.PurchaseOrderDO().Search(PONumber, DatePO, SupplierID);
        }

    }
}
