﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class CashCollectionDetailDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateCashCollectionDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.CashCollectionSummary != null ? cashCollectionDetail.CashCollectionSummary.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.SalesIssue != null ? cashCollectionDetail.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Amount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionDetail.Amount);
           
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectionDetail.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditCashCollectionDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectionDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionDetail.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.CashCollectionSummary != null ? cashCollectionDetail.CashCollectionSummary.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@SalesIssueID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.SalesIssue != null ? cashCollectionDetail.SalesIssue.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Amount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, cashCollectionDetail.Amount);
           
            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionDetail;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteCashCollectionDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionDetail.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, cashCollectionDetail.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, cashCollectionDetail.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = cashCollectionDetail;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                cashCollectionDetail.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.CashCollectionDetail Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetCashCollectionDetail";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                        int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                        int amountOrdinal = dr.GetOrdinal("Amount");

                        cashCollectionDetail = new Inventory.SolutionObjects.CashCollectionDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectionDetail.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) cashCollectionDetail.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(amountOrdinal)) cashCollectionDetail.Amount = dr.GetDecimal(amountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionDetail;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCashCollectionDetails";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> cashCollectionDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectionDetail>();
            Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int amountOrdinal = dr.GetOrdinal("Amount");

                    while (dr.Read())
                    {
                        cashCollectionDetail = new Inventory.SolutionObjects.CashCollectionDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectionDetail.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) cashCollectionDetail.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(amountOrdinal)) cashCollectionDetail.Amount = dr.GetDecimal(amountOrdinal);

                        cashCollectionDetailList.Add(cashCollectionDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionDetailList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> GetAllFor(SolutionObjects.CashCollectionSummary cashCollectionSummary)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllCashCollectionDetailsForCashCollectionSummary";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CashCollectionSummaryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, cashCollectionSummary.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.CashCollectionDetail> cashCollectionDetailList = new System.Collections.Generic.List<Inventory.SolutionObjects.CashCollectionDetail>();
            Inventory.SolutionObjects.CashCollectionDetail cashCollectionDetail;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int cashCollectionSummaryIDOrdinal = dr.GetOrdinal("CashCollectionSummaryID");
                    int salesIssueIDOrdinal = dr.GetOrdinal("SalesIssueID");
                    int amountOrdinal = dr.GetOrdinal("Amount");

                    while (dr.Read())
                    {
                        cashCollectionDetail = new Inventory.SolutionObjects.CashCollectionDetail(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) cashCollectionDetail.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(cashCollectionSummaryIDOrdinal)) cashCollectionDetail.CashCollectionSummary = new SolutionObjects.CashCollectionSummary(dr.GetInt32(cashCollectionSummaryIDOrdinal));
                        if (!dr.IsDBNull(salesIssueIDOrdinal)) cashCollectionDetail.SalesIssue = new SolutionObjects.SalesIssue(dr.GetInt32(salesIssueIDOrdinal));
                        if (!dr.IsDBNull(amountOrdinal)) cashCollectionDetail.Amount = dr.GetDecimal(amountOrdinal);

                        cashCollectionDetailList.Add(cashCollectionDetail);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return cashCollectionDetailList;
        }

        #endregion
    }
}
