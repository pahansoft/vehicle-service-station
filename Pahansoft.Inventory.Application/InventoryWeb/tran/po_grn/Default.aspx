﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="tran_po_grn_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-1.10.0.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-ui.min.js"></script>

    <link href="../../inc_fils/css/jquery-ui.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            SearchSupplier("ctl00_ContentPlaceHolder1_TextBoxSupplier");
            SearchSupplier("ctl00_ContentPlaceHolder1_TextBoxSrchSupplier");
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
        });

        function SearchProductItem(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetProductItems",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchSupplier(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetSuppliers",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function totCalc() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxUnitPrice').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxQuantity').value;
            var txtValue3 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxDiscount').value;
            var txtValue4 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxTax').value;

            var gross = 0;
            var net = 0;
            if (txtValue1 && txtValue2) {
                if (parseFloat(txtValue1) != 0 && parseFloat(txtValue2) != 0) {
                    gross = parseFloat(txtValue1) * parseFloat(txtValue2);
                    net = gross;
                }
                if (txtValue3)
                    if (parseFloat(txtValue3) != 0)
                        net = net - parseFloat(txtValue3);
                if (txtValue4)
                    if (parseFloat(txtValue4) != 0)
                        net = net + parseFloat(txtValue4);
            }

            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxGrossAmount').value = gross;
            document.getElementById('ctl00_ContentPlaceHolder1_TextBoxNetAmount').value = net;
        }

        //function print(orderID) {
        //    var WinPrint = window.open("repo.aspx?OrderID=" + orderID, '', 'left=5,top=5,width=1005,height=700,toolbar=0,scrollbars=1,status=0');
        //    WinPrint.focus();
        //}

    </script>

    <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">GRN -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonSearchOperation_Click">Search</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">GRN Number <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="TextBoxGRNNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 15%">Date GRN <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="TextBoxDateGRN" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                        ControlToValidate="TextBoxDateGRN" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="TextBoxDateGRN"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supplier <span style="color: Red;">*</span>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="TextBoxSupplier" runat="server" CssClass="textbox" MaxLength="400" Width="400px" AutoPostBack="True" OnTextChanged="TextBoxSupplier_TextChanged"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxSupplier"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="GridViewPOAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                        OnRowDataBound="GridViewPOAdd_RowDataBound" OnRowCommand="GridViewPOAdd_RowCommand"
                                                                        DataKeyNames="ID" AutoGenerateColumns="False">
                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                        <RowStyle CssClass="grid-row-normal-stc" />
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="AddRecord" Text="Add">
                                                                                <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="10px" />
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField HeaderText="PO Number">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="PO Date">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Total Amount">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Remarks">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <PagerStyle CssClass="grid-pager" />
                                                                        <EmptyDataTemplate>
                                                                            <div id="norec" class="no-record-msg">
                                                                                <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding-left: 15px">No Items(s) Found!
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </EmptyDataTemplate>
                                                                        <HeaderStyle CssClass="grid-header" />
                                                                        <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Items</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:GridView ID="GridViewPOItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                        OnRowDataBound="GridViewPOItemAdd_RowDataBound" OnRowCommand="GridViewPOItemAdd_RowCommand"
                                                                        DataKeyNames="ID" AutoGenerateColumns="False">
                                                                        <PagerSettings Mode="NumericFirstLast" />
                                                                        <RowStyle CssClass="grid-row-normal-stc" />
                                                                        <Columns>
                                                                            <asp:ButtonField CommandName="AssignRecord" Text="Assign">
                                                                                <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="10px" />
                                                                            </asp:ButtonField>
                                                                            <asp:BoundField HeaderText="PO Number">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Item Code">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Part Number">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Item Name">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Req. Qty">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="Assigned Qty">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>
                                                                            <%--<asp:BoundField HeaderText="PO Rate">
                                                                                <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                    Wrap="False" />
                                                                                <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                            </asp:BoundField>--%>
                                                                        </Columns>
                                                                        <PagerStyle CssClass="grid-pager" />
                                                                        <EmptyDataTemplate>
                                                                            <div id="norec" class="no-record-msg">
                                                                                <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding-left: 15px">No Items(s) Found!
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </EmptyDataTemplate>
                                                                        <HeaderStyle CssClass="grid-header" />
                                                                        <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table style="width: 100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 15%">Product Item <span style="color: red">*</span>
                                                                            </td>
                                                                            <td style="width: 55%">
                                                                                <asp:TextBox ID="TextBoxProductItem" runat="server" CssClass="textbox" MaxLength="400" Width="400px" TabIndex="100"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxProductItem"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td style="width: 15%">Unit Price <span style="color: red">*</span></td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TextBoxUnitPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="totCalc();" TabIndex="106"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Lot Number <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxLotNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="101"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxLotNumber"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Selling Price <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxSellingPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="107"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="TextBoxSellingPrice"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">Store <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListStore" runat="server" Width="250px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="DropDownListStore_SelectedIndexChanged" TabIndex="102">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListStore"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Discount
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="totCalc();" TabIndex="108"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="TextBoxDiscount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Location <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListLocation" runat="server" Width="250px" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="DropDownListLocation_SelectedIndexChanged" TabIndex="103">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DropDownListLocation"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Tax
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxTax" runat="server" CssClass="textbox" MaxLength="20" Width="100px" onkeyup="totCalc();" TabIndex="109"></asp:TextBox>
                                                                                &nbsp;<asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="TextBoxTax"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Sub Location <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="DropDownListRack" runat="server" Width="250px" TabIndex="104">
                                                                                </asp:DropDownList>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DropDownListRack"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td>Gross Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxGrossAmount" runat="server" CssClass="textbox"
                                                                                    MaxLength="20" Width="100px" TabIndex="110"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                                                                                    ControlToValidate="TextBoxGrossAmount" Display="Dynamic"
                                                                                    ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator17" runat="server"
                                                                                    ControlToValidate="TextBoxGrossAmount" Display="Dynamic" ErrorMessage="Invalid"
                                                                                    MaximumValue="99999999999" MinimumValue="0" Type="Double"
                                                                                    ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">Quantity <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="totCalc();" TabIndex="105"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="1000000" MinimumValue="1"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                            <td>Net Amount <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxNetAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" TabIndex="111"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="TextBoxNetAmount"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:Label ID="LabelItemError" runat="server" Style="font-weight: 700; color: #FF0000"></asp:Label>
                                                                                <asp:Label ID="LabelPODetailID" runat="server" Visible="False"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td class="pageButtonArea" colspan="3">
                                                                                <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                                                                                    Text="Add" ValidationGroup="ItemAdd" Width="70px" />
                                                                                &nbsp;
                                                                                <asp:Button ID="ButtonItemClear" runat="server" CssClass="button" OnClick="ButtonItemClear_Click" Text="Clear" Width="70px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td colspan="3"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                                    OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                                                                                    DataKeyNames="ID" AutoGenerateColumns="False">
                                                                                    <PagerSettings Mode="NumericFirstLast" />
                                                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                                                    <Columns>
                                                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="10px" />
                                                                                        </asp:ButtonField>
                                                                                        <asp:BoundField HeaderText="PO Number">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Product Item">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Lot Number">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Location">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Quantity">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Price">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Amount">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid-pager" />
                                                                                    <EmptyDataTemplate>
                                                                                        <div id="norec" class="no-record-msg">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="padding-left: 15px">No Items(s) Found!
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </EmptyDataTemplate>
                                                                                    <HeaderStyle CssClass="grid-header" />
                                                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanelSummary" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 15%">Total Gross <span style="color: Red;">*</span>
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <asp:TextBox ID="TextBoxTotalGross" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                                Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="TextBoxTotalGross"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                        <td style="width: 15%">Total Tax
                                                                        </td>
                                                                        <td style="width: 35%">
                                                                            <asp:TextBox ID="TextBoxTotalTax" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="TextBoxTax"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Net <span style="color: Red;">*</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxTotalNet" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                                Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                            <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="TextBoxTotalNet"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                        <td>Total Discount
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxTotalDiscount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px"></asp:TextBox>
                                                                            &nbsp;<asp:RangeValidator ID="RangeValidator22" runat="server" ControlToValidate="TextBoxTotalDiscount"
                                                                                Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                Type="Double" ValidationGroup="submit"></asp:RangeValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="vertical-align: text-top; padding-top: 3px">Remarks
                                                                        </td>
                                                                        <td colspan="3">
                                                                            <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td class="pageButtonArea">
                                                            <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                                                                OnClick="ButtonSubmit_Click" Width="70px" />
                                                            &nbsp;
               <asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                   OnClick="ButtonReset_Click" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Search Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">GRN Number
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchGRNNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date GRN
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchDateGRN" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchDateGRN"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="search"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supplier
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchSupplier" runat="server" CssClass="textbox" MaxLength="400" Width="400px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="pageButtonArea">
                                                                    <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                                                                        Width="70px" ValidationGroup="search" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
                                                    OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
                                                    OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                    <Columns>
                                                        <asp:ButtonField CommandName="EditRecord" Text="Edit">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="ViewRecord" Text="View">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField HeaderText="GRN Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Date GRN">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Supplier">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Net Amount">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Approved">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="grid-pager" />
                                                    <EmptyDataTemplate>
                                                        <div id="norec" class="no-record-msg">
                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-left: 15px">No Record(s) Found!
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="grid-header" />
                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 20%;">GRN Number
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 79%;">
                                                                    <asp:Label ID="LabelViewGRNNumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date GRN
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewDateGRN" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Supplier
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewSupplier" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Gross
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalGross" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Total Tax
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalTax" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Discount
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalDiscount" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Total Net
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalNet" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Remarks
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Status
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewStatus" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">GRN Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="PO Number">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Product Item">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Lot Number">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Location">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Quantity">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Price">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Amount">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
    <%--<script language="javascript">
        $("#ctl00_ContentPlaceHolder1_TextBoxDateGRN").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDatePO").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateManufactured").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxDateExpiry").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxSrchDateGRN").mask("99/99/9999");
    </script>--%>
</asp:Content>
