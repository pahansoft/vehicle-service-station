﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion;

public partial class mast_sett_sale_sref_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.SalesRef _salesRef;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxCode.Text = string.Empty;
        TextBoxName.Text = string.Empty;
        TextBoxAddressLine1.Text = string.Empty;
        TextBoxAddressLine2.Text = string.Empty;
        TextBoxAddressLine3.Text = string.Empty;
        TextBoxTelephoneHome.Text = string.Empty;
        TextBoxTelephoneMobile.Text = string.Empty;
        TextBoxTelephoneOffice.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxEmail.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchCode.Text = string.Empty;
        TextBoxSrchName.Text = string.Empty;
    }

    private void Search()
    {
        GridViewSearchList.DataSource = new OperationObjects.SalesRefOP().SearchSalesRefs(TextBoxSrchCode.Text.Trim(), TextBoxSrchName.Text.Trim());
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            ViewState[_target] = new SolutionObjects.SalesRef();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.SalesRef();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _salesRef = ViewState[_target] as SolutionObjects.SalesRef;
        _salesRef.Code = TextBoxCode.Text.Trim();
        _salesRef.Name = TextBoxName.Text.Trim();
        _salesRef.AddressLine1 = TextBoxAddressLine1.Text.Trim();
        _salesRef.AddressLine2 = TextBoxAddressLine2.Text.Trim();
        _salesRef.AddressLine3 = TextBoxAddressLine3.Text.Trim();
        _salesRef.TelephoneHome = TextBoxTelephoneHome.Text.Trim();
        _salesRef.TelephoneMobile = TextBoxTelephoneMobile.Text.Trim();
        _salesRef.TelephoneOffice = TextBoxTelephoneOffice.Text.Trim();
        _salesRef.Fax = TextBoxFax.Text.Trim();
        _salesRef.Email = TextBoxEmail.Text.Trim();
        _salesRef.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.SalesRefOP().SaveSalesRef(_salesRef);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.SalesRef();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Code cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Code cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _salesRef = ViewState[_target] as SolutionObjects.SalesRef;

        if (_salesRef.ID != null)
        {
            _salesRef = new OperationObjects.SalesRefOP().GetSalesRef(_salesRef.ID.Value);
            ViewState[_target] = _salesRef;

            TextBoxCode.Text = _salesRef.Code;
            TextBoxName.Text = _salesRef.Name;
            TextBoxAddressLine1.Text = _salesRef.AddressLine1;
            TextBoxAddressLine2.Text = _salesRef.AddressLine2;
            TextBoxAddressLine3.Text = _salesRef.AddressLine3;
            TextBoxTelephoneHome.Text = _salesRef.TelephoneHome;
            TextBoxTelephoneMobile.Text = _salesRef.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _salesRef.TelephoneOffice;
            TextBoxFax.Text = _salesRef.Fax;
            TextBoxEmail.Text = _salesRef.Email;
        }
        else
        {
            ViewState[_target] = new SolutionObjects.SalesRef();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _salesRef = new OperationObjects.SalesRefOP().GetSalesRef(_targetid);

            ClearAll();
            ViewState[_target] = _salesRef;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxCode.Text = _salesRef.Code;
            TextBoxName.Text = _salesRef.Name;
            TextBoxAddressLine1.Text = _salesRef.AddressLine1;
            TextBoxAddressLine2.Text = _salesRef.AddressLine2;
            TextBoxAddressLine3.Text = _salesRef.AddressLine3;
            TextBoxTelephoneHome.Text = _salesRef.TelephoneHome;
            TextBoxTelephoneMobile.Text = _salesRef.TelephoneMobile;
            TextBoxTelephoneOffice.Text = _salesRef.TelephoneOffice;
            TextBoxFax.Text = _salesRef.Fax;
            TextBoxEmail.Text = _salesRef.Email;
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _salesRef = new OperationObjects.SalesRefOP().GetSalesRef(_targetid);
            _salesRef.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.SalesRefOP().DeleteSalesRef(_salesRef);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _salesRef = new OperationObjects.SalesRefOP().GetSalesRef(_targetid);

            ViewState[_target] = _salesRef;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewCode.Text = Util.FormatEmptyString(_salesRef.Code);
            LabelViewName.Text = Util.FormatEmptyString(_salesRef.Name);
            LabelViewAddressLine1.Text = Util.FormatEmptyString(_salesRef.AddressLine1);
            LabelViewAddressLine2.Text = Util.FormatEmptyString(_salesRef.AddressLine2);
            LabelViewAddressLine3.Text = Util.FormatEmptyString(_salesRef.AddressLine3);
            LabelViewTelephoneHome.Text = Util.FormatEmptyString(_salesRef.TelephoneHome);
            LabelViewTelephoneMobile.Text = Util.FormatEmptyString(_salesRef.TelephoneMobile);
            LabelViewTelephoneOffice.Text = Util.FormatEmptyString(_salesRef.TelephoneOffice);
            LabelViewFax.Text = Util.FormatEmptyString(_salesRef.Fax);
            LabelViewEmail.Text = Util.FormatEmptyString(_salesRef.Email);
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _salesRef = e.Row.DataItem as SolutionObjects.SalesRef;
            Util.EncodeString(e.Row.Cells[3], _salesRef.Code, 50);
            Util.EncodeString(e.Row.Cells[4], _salesRef.Name, 50);
            Util.EncodeString(e.Row.Cells[5], _salesRef.AddressLine1, 50);
            Util.EncodeString(e.Row.Cells[6], _salesRef.TelephoneMobile, 50);
            Util.EncodeString(e.Row.Cells[7], _salesRef.Email, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _salesRef.Name + "');");
                }
            }
        }
    }
}