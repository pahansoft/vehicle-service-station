﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class JobCardServiceOP
    {
        public Pahansoft.CommonObjects.DataTransferObject Save(SolutionObjects.JobCardService JobCardService)
        {
            if (JobCardService.ID == null)
                return new DataAccessObjects.JobCardServiceDO().Create(JobCardService);
            else
                return new DataAccessObjects.JobCardServiceDO().Edit(JobCardService);
        }

        public Pahansoft.CommonObjects.DataTransferObject Delete(SolutionObjects.JobCardService JobCardService)
        {
            if (JobCardService.ID != null)
                return new DataAccessObjects.JobCardServiceDO().Delete(JobCardService);
            else
            {
                JobCardService.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = JobCardService;
                return _dts;
            }
        }

        public SolutionObjects.JobCardService Get(int id)
        {
            return new DataAccessObjects.JobCardServiceDO().Find(id);
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCardService> GetAll()
        {
            return new DataAccessObjects.JobCardServiceDO().GetAll();
        }

        public System.Collections.Generic.IList<SolutionObjects.JobCardService> GetAllFor(SolutionObjects.JobCard JobCard)
        {
            return new DataAccessObjects.JobCardServiceDO().GetAllFor(JobCard);
        }
    }
}
