﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class RackOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveRack(Inventory.SolutionObjects.Rack rack)
        {
            if (rack.ID == null)
                return new Inventory.DataAccessObjects.RackDO().Create(rack);
            else
                return new Inventory.DataAccessObjects.RackDO().Edit(rack);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteRack(Inventory.SolutionObjects.Rack rack)
        {
            if (rack.ID != null)
                return new Inventory.DataAccessObjects.RackDO().Delete(rack);
            else
            {
                rack.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = rack;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Rack GetRack(int id)
        {
            return new Inventory.DataAccessObjects.RackDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> GetAllRacks()
        {
            return new Inventory.DataAccessObjects.RackDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Rack> SearchRacks(string description, SolutionObjects.Store store, SolutionObjects.Location location)
        {
            return new Inventory.DataAccessObjects.RackDO().Search(description, store, location);
        }
    }
}
