﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class LocationOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveLocation(Inventory.SolutionObjects.Location location)
        {
            if (location.ID == null)
                return new Inventory.DataAccessObjects.LocationDO().Create(location);
            else
                return new Inventory.DataAccessObjects.LocationDO().Edit(location);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteLocation(Inventory.SolutionObjects.Location location)
        {
            if (location.ID != null)
                return new Inventory.DataAccessObjects.LocationDO().Delete(location);
            else
            {
                location.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = location;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Location GetLocation(int id)
        {
            return new Inventory.DataAccessObjects.LocationDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Location> GetAllLocations()
        {
            return new Inventory.DataAccessObjects.LocationDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Location> SearchLocations(string description, SolutionObjects.Store store)
        {
            return new Inventory.DataAccessObjects.LocationDO().Search(description, store);
        }
    }
}
