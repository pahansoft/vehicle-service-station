﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class PurchaseOrderDetail : SolutionObjectBase
    {
        public PurchaseOrderDetail() { }
        public PurchaseOrderDetail(int? id) { this.ID = id; }

        public PurchaseOrder PurchaseOrder { get; set; }
        public ProductItem ProductItem { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? ReceivedQty { get; set; }
        public decimal? LineTotal { get; set; }
    }
}
