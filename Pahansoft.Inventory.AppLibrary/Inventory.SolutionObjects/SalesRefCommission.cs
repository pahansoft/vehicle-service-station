﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class SalesRefCommission : SolutionObjectBase
    {
        public SalesRefCommission() { }
        public SalesRefCommission(int? id) { this.ID = id; }

        public SolutionObjects.SalesRef SalesRef { get; set; }
        public DateTime? DateCommision { get; set; }
        public SolutionObjects.SalesIssue SalesIssue { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? CommissionAmount { get; set; }
    }
}
