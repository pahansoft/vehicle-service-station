﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class SalesIssue : SolutionObjectBase
    {
        public SalesIssue() { }
        public SalesIssue(int? id) { this.ID = id; }

        public string IssueNumber { get; set; }
        public string CardNumber { get; set; }
        public DateTime? DateIssue { get; set; }
        public SolutionObjects.Customer Customer { get; set; }
        public SolutionObjects.SalesRef SalesRef { get; set; }
        public decimal? CommissionRate { get; set; }
        public decimal? CommissionAmount { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal? TotalTax { get; set; }
        public decimal? TotalGross { get; set; }
        public decimal? TotalNet { get; set; }
        public string Remarks { get; set; }
        public decimal? InitialPayment { get; set; }
        public bool IsCredit { get; set; }

        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.SalesIssueItem> SalesIssueItemList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.CustomerPayment> CustomerPaymentList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.SalesRefCommission> SalesRefCommissionList { get; set; }
        public Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock> ItemStockList { get; set; }
    }
}
