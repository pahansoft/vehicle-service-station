﻿<%@ Page Title="" Language="C#" MasterPageFile="~/shrd/repo.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="repo_stock_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../../inc_fils/js/main_menu.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>

     

    <script type="text/javascript">
        function print(store,location,rack) {
            var WinPrint = window.open("repo.aspx?Store=" + store + "&Location=" + location + "&Rack=" + rack,'left=5,top=5,width=1005,height=700,toolbar=0,scrollbars=1,status=0');
            WinPrint.focus();
        }
    </script>
    <asp:Panel ID="PanelAllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">STOCK REPORT -
                                            <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" 
                                                Font-Underline="False" onclick="LinkButtonSearchOperation_Click1"
                                                >Search</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="PanelSearchDetails" Visible="true" Width="100%" runat="server">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td class="pageDataArea">
                                    <fieldset>
                                        <legend style="font-weight: bold">Search Details</legend>
                                        <div style="padding: 10px;">
                                            <table border="0" style="width: 100%">
                                                <tr>
                                                    <td style="height: 28px">Store</td>
                                                    <td style="height: 28px">
                                                        <asp:DropDownList ID="DropDownListStore" runat="server" Width="150px" 
                                                            Visible="true" AutoPostBack="True" 
                                                            onselectedindexchanged="DropDownListStore_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 28px">Location</td>
                                                    <td style="height: 28px">
                                                        <asp:DropDownList ID="DropDownListLocation" runat="server" Width="150px" 
                                                            Visible="true" AutoPostBack="True" onselectedindexchanged="DropDownListLocation_SelectedIndexChanged"
                                                         >
                                                        </asp:DropDownList>
                                                    </td></tr>
                                                    <tr>
                                                    <td style="height: 28px">Rack</td>
                                                    <td style="height: 28px">
                                                        <asp:DropDownList ID="DropDownListRack" runat="server" Width="150px" Visible="true"
                                                         >
                                                        </asp:DropDownList>
                                                    </td>
                                                    </tr> 
                                                </tr>
                                                    <tr>
                                                        <td style="width: 15%">&nbsp;</td>
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                <tr>
                                                    <td></td>
                                                    <td class="pageButtonArea">
                                                        <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Print" OnClick="ButtonPrint_Click"
                                                            Width="70px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>  
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Pleasecontact
                            your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
     <script>
         console.log($('#ctl00_ContentPlaceHolder1_HiddenField_DefaultDate').val());
         $(document).ready(function () {

             jQuery('#ctl00_ContentPlaceHolder1_TextBoxDateFrom').datetimepicker({
                 timepicker: false,
                 format: 'd/m/Y'
             });
             jQuery('#ctl00_ContentPlaceHolder1_TextBoxDateTo').datetimepicker({
                 timepicker: false,
                 format: 'd/m/Y'
             });

         });

    </script>
</asp:Content>

