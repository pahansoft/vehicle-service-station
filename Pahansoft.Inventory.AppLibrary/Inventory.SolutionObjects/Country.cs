﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class Country : SolutionObjectBase
    {
        public Country() { }
        public Country(int? id) { this.ID = id; }

        public string Description { get; set; }
    }
}
