﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
 CodeFile="Default.aspx.cs" Inherits="tran_lodg_page_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
  <tr>
   <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px;
    padding-top: 15px" align="right">
     <img src="../../images/lodingPage.jpg" />
   </td>
   <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold;
    color: #000099;">We Help you to </span><span style="position: absolute; top: 230px;
     left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Re-shape your
     business to enjoy more freedom </span>
  </tr>
 </table>
</asp:Content>