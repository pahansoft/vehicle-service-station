﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class ManufacturerOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveManufacturer(Inventory.SolutionObjects.Manufacturer manufacturer)
        {
            if (manufacturer.ID == null)
                return new Inventory.DataAccessObjects.ManufacturerDO().Create(manufacturer);
            else
                return new Inventory.DataAccessObjects.ManufacturerDO().Edit(manufacturer);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteManufacturer(Inventory.SolutionObjects.Manufacturer manufacturer)
        {
            if (manufacturer.ID != null)
                return new Inventory.DataAccessObjects.ManufacturerDO().Delete(manufacturer);
            else
            {
                manufacturer.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = manufacturer;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Manufacturer GetManufacturer(int id)
        {
            return new Inventory.DataAccessObjects.ManufacturerDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> GetAllManufacturers()
        {
            return new Inventory.DataAccessObjects.ManufacturerDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> SearchManufacturers(string description, SolutionObjects.Country country)
        {
            return new Inventory.DataAccessObjects.ManufacturerDO().Search(description, country);
        }
    }
}
