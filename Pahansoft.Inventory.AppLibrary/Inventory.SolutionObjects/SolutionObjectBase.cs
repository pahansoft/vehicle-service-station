﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public abstract class SolutionObjectBase
    {
        private int? _id;
        public int? ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _user;
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private object _timeStamp;
        public object TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }

        private bool _archived;
        public bool Archived
        {
            get { return _archived; }
            set { _archived = value; }
        }
    }
}
