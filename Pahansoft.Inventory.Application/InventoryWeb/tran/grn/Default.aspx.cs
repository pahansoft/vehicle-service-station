﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class tran_grn_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.GRN _grn;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxGRNNumber.Text = string.Empty;
        TextBoxDateGRN.Text = DateTime.Today.ToShortDateString();
        //TextBoxPONumber.Text = string.Empty;
        //TextBoxDatePO.Text = string.Empty;
        DropDownListSupplier.SelectedIndex = 0;
        TextBoxTotalGross.Text = string.Empty;
        TextBoxTotalTax.Text = string.Empty;
        TextBoxTotalDiscount.Text = string.Empty;
        TextBoxTotalNet.Text = string.Empty;
        TextBoxRemarks.Text = string.Empty;

        GridViewItemAdd.DataSource = null;
        GridViewItemAdd.DataBind();
        ClearItem();
    }

    private void ClearItem()
    {
        DropDownListProductItem.SelectedIndex = 0;
        TextBoxLotNumber.Text = string.Empty;
        DropDownListStore.SelectedIndex = 0;
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);
        TextBoxQuantity.Text = string.Empty;
        TextBoxUnitPrice.Text = string.Empty;
        TextBoxSellingPrice.Text = string.Empty;
        TextBoxDateManufactured.Text = string.Empty;
        TextBoxDiscount.Text = string.Empty;
        TextBoxTax.Text = string.Empty;
        TextBoxGrossAmount.Text = string.Empty;
        TextBoxNetAmount.Text = string.Empty;
    }

    private void ClearSearch()
    {
        TextBoxSrchGRNNumber.Text = string.Empty;
        TextBoxSrchDateGRN.Text = string.Empty;
        //TextBoxSrchPONumber.Text = string.Empty;
        DropDownListSrchSupplier.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListSupplier);
        Util.ClearDropdown(DropDownListSrchSupplier);
        foreach (SolutionObjects.Supplier supplier in new OperationObjects.SupplierOP().GetAllSuppliers())
        {
            DropDownListSupplier.Items.Add(new ListItem(supplier.Name, supplier.ID.Value.ToString()));
            DropDownListSrchSupplier.Items.Add(new ListItem(supplier.Name, supplier.ID.Value.ToString()));
        }

        Util.ClearDropdown(DropDownListProductItem);
        foreach (SolutionObjects.ProductItem productItem in new OperationObjects.ProductItemOP().GetAllProductItems())
            DropDownListProductItem.Items.Add(new ListItem(productItem.Code + "-" + productItem.Description, productItem.ID.Value.ToString()));

        Util.ClearDropdown(DropDownListStore);
        foreach (SolutionObjects.Store store in new OperationObjects.StoreOP().GetAllStores())
            DropDownListStore.Items.Add(new ListItem(store.Description, store.ID.Value.ToString()));

    }

    private void Search()
    {
        DateTime? _dateGRN = Util.ParseDate(TextBoxSrchDateGRN.Text.Trim());
        SolutionObjects.Supplier _supplier = null;
        if (DropDownListSrchSupplier.SelectedIndex != 0)
            _supplier = new SolutionObjects.Supplier(int.Parse(DropDownListSrchSupplier.SelectedValue));

        //GridViewSearchList.DataSource = new OperationObjects.GRNOP().SearchGRNs(TextBoxSrchGRNNumber.Text.Trim(), _dateGRN, TextBoxSrchPONumber.Text.Trim(), _supplier);
        GridViewSearchList.DataBind();
    }

    private void DisplayTotals()
    {
        _grn = ViewState[_target] as SolutionObjects.GRN;

        Decimal _totalDiscount = 0;
        Decimal _totalTax = 0;
        Decimal _totalGross = 0;
        Decimal _totalNet = 0;

        foreach (SolutionObjects.GRNDetail gRNDetail in _grn.GRNDetailList)
        {
            _totalDiscount += gRNDetail.Discount != null ? gRNDetail.Discount.Value : 0;
            _totalTax += gRNDetail.Tax != null ? gRNDetail.Tax.Value : 0;
            _totalGross += gRNDetail.GrossAmount != null ? gRNDetail.GrossAmount.Value : 0;
            _totalNet += gRNDetail.NetAmount != null ? gRNDetail.NetAmount.Value : 0;
        }

        TextBoxTotalDiscount.Text = _totalDiscount != 0 ? _totalDiscount.ToString("F") : string.Empty;
        TextBoxTotalTax.Text = _totalTax != 0 ? _totalTax.ToString("F") : string.Empty;
        TextBoxTotalGross.Text = _totalGross != 0 ? _totalGross.ToString("F") : string.Empty;
        TextBoxTotalNet.Text = _totalNet != 0 ? _totalNet.ToString("F") : string.Empty;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            _grn = new SolutionObjects.GRN();
            _grn.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
            ViewState[_target] = _grn;
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        _grn = new SolutionObjects.GRN();
        _grn.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
        ViewState[_target] = _grn;

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        PanelView.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void DropDownListStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListLocation);
        Util.ClearDropdown(DropDownListRack);

        if (DropDownListStore.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Location location in new OperationObjects.LocationOP().SearchLocations(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue))))
                DropDownListLocation.Items.Add(new ListItem(location.Description, location.ID.Value.ToString()));
        }
    }
    protected void DropDownListLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Util.ClearDropdown(DropDownListRack);

        if (DropDownListLocation.SelectedIndex != 0)
        {
            foreach (SolutionObjects.Rack rack in new OperationObjects.RackOP().SearchRacks(string.Empty, new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue)), new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue))))
                DropDownListRack.Items.Add(new ListItem(rack.Description, rack.ID.Value.ToString()));
        }
    }
    protected void GridViewItemAdd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        _grn = ViewState[_target] as SolutionObjects.GRN;
        _grn.GRNDetailList.RemoveAt(int.Parse(e.CommandArgument.ToString()));

        ViewState[_target] = _grn;
        GridViewItemAdd.DataSource = _grn.GRNDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
    }
    protected void GridViewItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.GRNDetail _gRNDetail = e.Row.DataItem as SolutionObjects.GRNDetail;
            if (_gRNDetail.ProductItem != null)
                _gRNDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItem(_gRNDetail.ProductItem.ID.Value);
            if (_gRNDetail.Store != null)
                _gRNDetail.Store = new OperationObjects.StoreOP().GetStore(_gRNDetail.Store.ID.Value);
            if (_gRNDetail.Location != null)
                _gRNDetail.Location = new OperationObjects.LocationOP().GetLocation(_gRNDetail.Location.ID.Value);
            if (_gRNDetail.Rack != null)
                _gRNDetail.Rack = new OperationObjects.RackOP().GetRack(_gRNDetail.Rack.ID.Value);

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_gRNDetail.Store != null ? _gRNDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_gRNDetail.Location != null ? _gRNDetail.Location.Description : "-").Append("<br>");
            _sbLocation.Append(_gRNDetail.Rack != null ? _gRNDetail.Rack.Description : "-");

            StringBuilder _sbQuantity = new StringBuilder();
            _sbQuantity.Append(_gRNDetail.Quantity != null ? _gRNDetail.Quantity.Value.ToString() : "-");

            StringBuilder _sbPrice = new StringBuilder();
            _sbPrice.Append("UNIT: ").Append(_gRNDetail.UnitPrice != null ? _gRNDetail.UnitPrice.Value.ToString("F") : "-").Append("<br>");
            _sbPrice.Append("SELL: ").Append(_gRNDetail.SellingPrice != null ? _gRNDetail.SellingPrice.Value.ToString("F") : "-");

            StringBuilder _sbDate = new StringBuilder();
            _sbDate.Append("MFD: ").Append(_gRNDetail.DateManufactured != null ? _gRNDetail.DateManufactured.Value.ToShortDateString() : "-");

            StringBuilder _sbAmount = new StringBuilder();
            _sbAmount.Append("TAX: ").Append(_gRNDetail.Tax != null ? _gRNDetail.Tax.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("DIS: ").Append(_gRNDetail.Discount != null ? _gRNDetail.Discount.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("GRS: ").Append(_gRNDetail.GrossAmount != null ? _gRNDetail.GrossAmount.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("NET: ").Append(_gRNDetail.NetAmount != null ? _gRNDetail.NetAmount.Value.ToString("F") : "-");

            Util.EncodeString(e.Row.Cells[1], _gRNDetail.ProductItem != null ? _gRNDetail.ProductItem.Code + "-" + _gRNDetail.ProductItem.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[2], _gRNDetail.LotNumber, 20);
            e.Row.Cells[3].Text = _sbLocation.ToString();
            e.Row.Cells[4].Text = _sbQuantity.ToString();
            e.Row.Cells[5].Text = _sbPrice.ToString();
            e.Row.Cells[6].Text = _sbDate.ToString();
            e.Row.Cells[7].Text = _sbAmount.ToString();
        }
    }
    protected void ButtonItemAdd_Click(object sender, EventArgs e)
    {
        _grn = ViewState[_target] as SolutionObjects.GRN;

        SolutionObjects.GRNDetail _gRNDetail = new SolutionObjects.GRNDetail();
        _gRNDetail.GRN = _grn;
        if (DropDownListProductItem.SelectedIndex != 0)
            _gRNDetail.ProductItem = new SolutionObjects.ProductItem(int.Parse(DropDownListProductItem.SelectedValue));
        _gRNDetail.StockType = SolutionObjects.StockTypes.Trading;
        _gRNDetail.LotNumber = TextBoxLotNumber.Text.Trim();
        if (DropDownListStore.SelectedIndex != 0)
            _gRNDetail.Store = new SolutionObjects.Store(int.Parse(DropDownListStore.SelectedValue));
        if (DropDownListLocation.SelectedIndex != 0)
            _gRNDetail.Location = new SolutionObjects.Location(int.Parse(DropDownListLocation.SelectedValue));
        if (DropDownListRack.SelectedIndex != 0)
            _gRNDetail.Rack = new SolutionObjects.Rack(int.Parse(DropDownListRack.SelectedValue));
        _gRNDetail.Quantity = Util.ParseInt(TextBoxQuantity.Text.Trim());
        _gRNDetail.UnitPrice = Util.ParseDecimal(TextBoxUnitPrice.Text.Trim());
        _gRNDetail.SellingPrice = Util.ParseDecimal(TextBoxSellingPrice.Text.Trim());
        _gRNDetail.DateManufactured = Util.ParseDate(TextBoxDateManufactured.Text.Trim());
        _gRNDetail.Discount = Util.ParseDecimal(TextBoxDiscount.Text.Trim());
        _gRNDetail.Tax = Util.ParseDecimal(TextBoxTax.Text.Trim());
        _gRNDetail.GrossAmount = Util.ParseDecimal(TextBoxGrossAmount.Text.Trim());
        _gRNDetail.NetAmount = Util.ParseDecimal(TextBoxNetAmount.Text.Trim());
        _gRNDetail.User = User.Identity.Name;
        _grn.GRNDetailList.Add(_gRNDetail);

        ViewState[_target] = _grn;
        GridViewItemAdd.DataSource = _grn.GRNDetailList;
        GridViewItemAdd.DataBind();
        DisplayTotals();
        ClearItem();
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
        _grn = ViewState[_target] as SolutionObjects.GRN;

        if (_grn.ID == null)
            _grn.GRNNumber = Util.GenereteSequenceNumber("GRN", DateTime.Today.Year, DateTime.Today.Month);
        _grn.DateGRN = Util.ParseDate(TextBoxDateGRN.Text.Trim());
        //_grn.PONumber = TextBoxPONumber.Text.Trim();
        //_grn.DatePO = Util.ParseDate(TextBoxDatePO.Text.Trim());
        if (DropDownListSupplier.SelectedIndex != 0)
            _grn.Supplier = new SolutionObjects.Supplier(int.Parse(DropDownListSupplier.SelectedValue));
        else
            _grn.Supplier = null;
        _grn.TotalTax = Util.ParseDecimal(TextBoxTotalTax.Text.Trim());
        _grn.TotalDiscount = Util.ParseDecimal(TextBoxTotalDiscount.Text.Trim());
        _grn.TotalGrossAmount = Util.ParseDecimal(TextBoxTotalGross.Text.Trim());
        _grn.TotalNetAmount = Util.ParseDecimal(TextBoxTotalNet.Text.Trim());
        _grn.Remarks = TextBoxRemarks.Text.Trim();
        _grn.User = User.Identity.Name;

        try
        {
            bool _isNew = false;
            if (_grn.ID == null)
                _isNew = true;

            CommonObjects.DataTransferObject _dtc = new OperationObjects.GRNOP().SaveGRN(_grn);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                if (_isNew)
                    LabelSucsessMessage.Text = "Record saved successfully<br>GRN Number : " + _grn.GRNNumber;
                else
                    LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                _grn = new SolutionObjects.GRN();
                _grn.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
                ViewState[_target] = _grn;
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _grn = ViewState[_target] as SolutionObjects.GRN;

        if (_grn.ID != null)
        {
            _grn = new OperationObjects.GRNOP().GetGRN(_grn.ID.Value);
            ViewState[_target] = _grn;

            TextBoxGRNNumber.Text = _grn.GRNNumber;
            TextBoxDateGRN.Text = _grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty;
            //TextBoxPONumber.Text = _grn.PONumber;
            //TextBoxDatePO.Text = _grn.DatePO != null ? _grn.DatePO.Value.ToShortDateString() : string.Empty;
            if (_grn.Supplier != null)
                DropDownListSupplier.SelectedValue = _grn.Supplier.ID.Value.ToString();
            TextBoxTotalTax.Text = _grn.TotalTax != null ? _grn.TotalTax.Value.ToString("F") : string.Empty;
            TextBoxTotalDiscount.Text = _grn.TotalDiscount != null ? _grn.TotalDiscount.Value.ToString("F") : string.Empty;
            TextBoxTotalGross.Text = _grn.TotalGrossAmount != null ? _grn.TotalGrossAmount.Value.ToString("F") : string.Empty;
            TextBoxTotalNet.Text = _grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty;
            TextBoxRemarks.Text = _grn.Remarks;

            GridViewItemAdd.DataSource = _grn.GRNDetailList;
            GridViewItemAdd.DataBind();
        }
        else
        {
            _grn = new SolutionObjects.GRN();
            _grn.GRNDetailList = new CommonObjects.PropertyList<SolutionObjects.GRNDetail>();
            ViewState[_target] = _grn;
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _grn = new OperationObjects.GRNOP().GetGRN(_targetid);

            ClearAll();
            ViewState[_target] = _grn;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxGRNNumber.Text = _grn.GRNNumber;
            TextBoxDateGRN.Text = _grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty;
            //TextBoxPONumber.Text = _grn.PONumber;
            //TextBoxDatePO.Text = _grn.DatePO != null ? _grn.DatePO.Value.ToShortDateString() : string.Empty;
            if (_grn.Supplier != null)
                DropDownListSupplier.SelectedValue = _grn.Supplier.ID.Value.ToString();
            TextBoxTotalTax.Text = _grn.TotalTax != null ? _grn.TotalTax.Value.ToString("F") : string.Empty;
            TextBoxTotalDiscount.Text = _grn.TotalDiscount != null ? _grn.TotalDiscount.Value.ToString("F") : string.Empty;
            TextBoxTotalGross.Text = _grn.TotalGrossAmount != null ? _grn.TotalGrossAmount.Value.ToString("F") : string.Empty;
            TextBoxTotalNet.Text = _grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty;
            TextBoxRemarks.Text = _grn.Remarks;

            GridViewItemAdd.DataSource = _grn.GRNDetailList;
            GridViewItemAdd.DataBind();
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _grn = new OperationObjects.GRNOP().GetGRN(_targetid);
            _grn.User = User.Identity.Name;

            int _listCount = _grn.GRNDetailList.Count - 1;
            while (_listCount >= 0)
            {
                _grn.GRNDetailList.RemoveAt(_listCount);
                _listCount--;
            }

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.GRNOP().DeleteGRN(_grn);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }

        if (e.CommandName.Equals("ViewRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _grn = new OperationObjects.GRNOP().GetGRN(_targetid);

            ViewState[_target] = _grn;

            PanelAddNew.Visible = false;
            PanelSearchDetails.Visible = false;
            PanelView.Visible = true;
            LabelPageOperation.Text = "View";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            LabelViewGRNNumber.Text = Util.FormatEmptyString(_grn.GRNNumber);
            LabelViewDateGRN.Text = Util.FormatEmptyString(_grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty);
            //LabelViewPONumber.Text = Util.FormatEmptyString(_grn.PONumber);
            //LabelViewDatePO.Text = Util.FormatEmptyString(_grn.DatePO != null ? _grn.DatePO.Value.ToShortDateString() : string.Empty);
            if (_grn.Supplier != null)
                LabelViewSupplier.Text = new OperationObjects.SupplierOP().GetSupplier(_grn.Supplier.ID.Value).Name;
            else
                LabelViewSupplier.Text = "-";
            LabelViewTotalTax.Text = Util.FormatEmptyString(_grn.TotalTax != null ? _grn.TotalTax.Value.ToString("F") : string.Empty);
            LabelViewTotalDiscount.Text = Util.FormatEmptyString(_grn.TotalDiscount != null ? _grn.TotalDiscount.Value.ToString("F") : string.Empty);
            LabelViewTotalGross.Text = Util.FormatEmptyString(_grn.TotalGrossAmount != null ? _grn.TotalGrossAmount.Value.ToString("F") : string.Empty);
            LabelViewTotalNet.Text = Util.FormatEmptyString(_grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty);

            GridViewItemView.DataSource = _grn.GRNDetailList;
            GridViewItemView.DataBind();
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _grn = e.Row.DataItem as SolutionObjects.GRN;
            if (_grn.Supplier != null)
                _grn.Supplier = new OperationObjects.SupplierOP().GetSupplier(_grn.Supplier.ID.Value);

            Util.EncodeString(e.Row.Cells[3], _grn.GRNNumber, 20);
            Util.EncodeString(e.Row.Cells[4], _grn.DateGRN != null ? _grn.DateGRN.Value.ToShortDateString() : string.Empty, 20);
            //Util.EncodeString(e.Row.Cells[5], _grn.PONumber, 20);
            Util.EncodeString(e.Row.Cells[6], _grn.Supplier != null ? _grn.Supplier.Name : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[7], _grn.TotalNetAmount != null ? _grn.TotalNetAmount.Value.ToString("F") : string.Empty, 20);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _grn.GRNNumber + "');");
                }
            }
        }
    }

    protected void GridViewItemView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SolutionObjects.GRNDetail _gRNDetail = e.Row.DataItem as SolutionObjects.GRNDetail;
            if (_gRNDetail.ProductItem != null)
                _gRNDetail.ProductItem = new OperationObjects.ProductItemOP().GetProductItem(_gRNDetail.ProductItem.ID.Value);
            if (_gRNDetail.Store != null)
                _gRNDetail.Store = new OperationObjects.StoreOP().GetStore(_gRNDetail.Store.ID.Value);
            if (_gRNDetail.Location != null)
                _gRNDetail.Location = new OperationObjects.LocationOP().GetLocation(_gRNDetail.Location.ID.Value);
            if (_gRNDetail.Rack != null)
                _gRNDetail.Rack = new OperationObjects.RackOP().GetRack(_gRNDetail.Rack.ID.Value);

            StringBuilder _sbLocation = new StringBuilder();
            _sbLocation.Append(_gRNDetail.Store != null ? _gRNDetail.Store.Description : "-").Append("<br>");
            _sbLocation.Append(_gRNDetail.Location != null ? _gRNDetail.Location.Description : "-").Append("<br>");
            _sbLocation.Append(_gRNDetail.Rack != null ? _gRNDetail.Rack.Description : "-");

            StringBuilder _sbQuantity = new StringBuilder();
            _sbQuantity.Append(_gRNDetail.Quantity != null ? _gRNDetail.Quantity.Value.ToString() : "-");

            StringBuilder _sbPrice = new StringBuilder();
            _sbPrice.Append("UNIT: ").Append(_gRNDetail.UnitPrice != null ? _gRNDetail.UnitPrice.Value.ToString("F") : "-").Append("<br>");
            _sbPrice.Append("SELL: ").Append(_gRNDetail.SellingPrice != null ? _gRNDetail.SellingPrice.Value.ToString("F") : "-");

            StringBuilder _sbDate = new StringBuilder();
            _sbDate.Append("MFD: ").Append(_gRNDetail.DateManufactured != null ? _gRNDetail.DateManufactured.Value.ToShortDateString() : "-");

            StringBuilder _sbAmount = new StringBuilder();
            _sbAmount.Append("TAX: ").Append(_gRNDetail.Tax != null ? _gRNDetail.Tax.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("DIS: ").Append(_gRNDetail.Discount != null ? _gRNDetail.Discount.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("GRS: ").Append(_gRNDetail.GrossAmount != null ? _gRNDetail.GrossAmount.Value.ToString("F") : "-").Append("<br>");
            _sbAmount.Append("NET: ").Append(_gRNDetail.NetAmount != null ? _gRNDetail.NetAmount.Value.ToString("F") : "-");

            Util.EncodeString(e.Row.Cells[0], _gRNDetail.ProductItem != null ? _gRNDetail.ProductItem.Code + "-" + _gRNDetail.ProductItem.Description : string.Empty, 50);
            Util.EncodeString(e.Row.Cells[1], _gRNDetail.LotNumber, 20);
            e.Row.Cells[2].Text = _sbLocation.ToString();
            e.Row.Cells[3].Text = _sbQuantity.ToString();
            e.Row.Cells[4].Text = _sbPrice.ToString();
            e.Row.Cells[5].Text = _sbDate.ToString();
            e.Row.Cells[6].Text = _sbAmount.ToString();
        }
    }

    protected void LinkButtonCalculate_Click(object sender, EventArgs e)
    {
        decimal _unitPrice = 0;
        decimal _quantity = 0;
        decimal _discount = 0;
        decimal _tax = 0;
        decimal _gross = 0;
        decimal _net = 0;

        _unitPrice = Util.ParseDecimal(TextBoxUnitPrice.Text.Trim()) != null ? Util.ParseDecimal(TextBoxUnitPrice.Text.Trim()).Value : 0;
        _quantity = Util.ParseInt(TextBoxQuantity.Text.Trim()) != null ? Util.ParseInt(TextBoxQuantity.Text.Trim()).Value : 0;
        _gross = _unitPrice * _quantity;

        if (TextBoxDiscount.Text.Trim() != string.Empty)
            _discount = Util.ParseDecimal(TextBoxDiscount.Text.Trim()) != null ? Util.ParseDecimal(TextBoxDiscount.Text.Trim()).Value : 0;

        if (TextBoxTax.Text.Trim() != string.Empty)
            _tax = Util.ParseDecimal(TextBoxTax.Text.Trim()) != null ? Util.ParseDecimal(TextBoxTax.Text.Trim()).Value : 0;

        _net = _gross - _discount + _tax;

        TextBoxGrossAmount.Text = _gross.ToString("F");
        TextBoxNetAmount.Text = _net.ToString("F");
    }
}