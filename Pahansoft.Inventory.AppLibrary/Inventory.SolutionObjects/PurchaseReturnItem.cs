﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.SolutionObjects
{
    [Serializable]
    public class PurchaseReturnItem : SolutionObjectBase
    {
        public PurchaseReturnItem() { }
        public PurchaseReturnItem(int? id) { this.ID = id; }

        public SolutionObjects.PurchaseReturn PurchaseReturn { get; set; }
        public SolutionObjects.GRNDetail GRNDetail { get; set; }
        public decimal? ReturnedQuantity { get; set; }
        public decimal? ReturnedPrice { get; set; }
    }
}
