﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion

public partial class mast_sett_brnd_name_Default : System.Web.UI.Page
{
    #region - Private Variables -

    public const string _target = "Target";
    SolutionObjects.BrandName _brandName;

    #endregion

    #region - Private Methods -

    private void ClearAll()
    {
        TextBoxDescription.Text = string.Empty;
        DropDownListManufacturer.SelectedIndex = 0;
    }

    private void FillDropDownList()
    {
        Util.ClearDropdown(DropDownListManufacturer);
        Util.ClearDropdown(DropDownListSrchManufacturer);
        foreach (SolutionObjects.Manufacturer manufacturer in new OperationObjects.ManufacturerOP().GetAllManufacturers())
        {
            DropDownListManufacturer.Items.Add(new ListItem(manufacturer.Description, manufacturer.ID.Value.ToString()));
            DropDownListSrchManufacturer.Items.Add(new ListItem(manufacturer.Description, manufacturer.ID.Value.ToString()));
        }

    }

    private void ClearSearch()
    {
        TextBoxSrchDescription.Text = string.Empty;
        DropDownListSrchManufacturer.SelectedIndex = 0;
    }

    private void Search()
    {
        SolutionObjects.Manufacturer _manufacturer = null;
        if (DropDownListSrchManufacturer.SelectedIndex != 0)
            _manufacturer = new SolutionObjects.Manufacturer(int.Parse(DropDownListSrchManufacturer.SelectedValue));

        GridViewSearchList.DataSource = new OperationObjects.BrandNameOP().SearchBrandNames(TextBoxSrchDescription.Text.Trim(), _manufacturer);
        GridViewSearchList.DataBind();
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!Session["UserType"].ToString().Equals("Admin"))
        //{
        //    PanelllowedPage.Visible = false;
        //    PanelNotAllowedPage.Visible = true;
        //}
        if (!Page.IsPostBack)
        {
            ClearAll();
            FillDropDownList();
            ViewState[_target] = new SolutionObjects.BrandName();
            LabelPageOperation.Text = "Add New";

            GridViewSearchList.DataSource = null;
            GridViewSearchList.DataBind();
        }
        PanelInformationMessage.Visible = false;
    }
    protected void LinkButtonSearchOperation_Click(object sender, EventArgs e)
    {
        ClearSearch();
        GridViewSearchList.DataSource = null;
        GridViewSearchList.DataBind();

        PanelAddNew.Visible = false;
        PanelSearchDetails.Visible = true;
        LabelPageOperation.Text = "Search";
        LinkButtonAddNewOperation.Visible = true;
        LinkButtonSearchOperation.Visible = false;
    }
    protected void LinkButtonAddNewOperation_Click(object sender, EventArgs e)
    {
        ClearAll();
        ViewState[_target] = new SolutionObjects.BrandName();

        PanelAddNew.Visible = true;
        PanelSearchDetails.Visible = false;
        LabelPageOperation.Text = "Add New";
        LinkButtonAddNewOperation.Visible = false;
        LinkButtonSearchOperation.Visible = true;
    }
    protected void ButtonCloseMessage_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e)
    {
        PanelInformationMessage.Visible = false;

        _brandName = ViewState[_target] as SolutionObjects.BrandName;
        _brandName.Description = TextBoxDescription.Text.Trim();
        if (DropDownListManufacturer.SelectedIndex != 0)
            _brandName.Manufacturer = new SolutionObjects.Manufacturer(int.Parse(DropDownListManufacturer.SelectedValue));
        else
            _brandName.Manufacturer = null;

        _brandName.User = User.Identity.Name;

        try
        {
            CommonObjects.DataTransferObject _dtc = new OperationObjects.BrandNameOP().SaveBrandName(_brandName);
            if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = true;
                PanelErrorMessage.Visible = false;
                LabelSucsessMessage.Text = "Record saved successfully";

                ClearAll();
                ViewState[_target] = new SolutionObjects.BrandName();
                LabelPageOperation.Text = "Add New";
            }
            else
            {
                PanelInformationMessage.Visible = true;
                PanelSucsessMessage.Visible = false;
                PanelErrorMessage.Visible = true;
                if (_dtc.Message.Contains("UNIQUE KEY"))
                    LabelErrorMessage.Text = "Description cannot be duplicated";
                else
                    LabelErrorMessage.Text = _dtc.Message;
            }
        }
        catch (Exception ex)
        {
            PanelInformationMessage.Visible = true;
            PanelSucsessMessage.Visible = false;
            PanelErrorMessage.Visible = true;
            if (ex.Message.Contains("UNIQUE KEY"))
                LabelErrorMessage.Text = "Description cannot be duplicated";
            else
                LabelErrorMessage.Text = ex.Message;
        }
    }
    protected void ButtonReset_Click(object sender, EventArgs e)
    {
        ClearAll();
        _brandName = ViewState[_target] as SolutionObjects.BrandName;

        if (_brandName.ID != null)
        {
            _brandName = new OperationObjects.BrandNameOP().GetBrandName(_brandName.ID.Value);
            ViewState[_target] = _brandName;

            TextBoxDescription.Text = _brandName.Description;
            if (_brandName.Manufacturer != null)
                DropDownListManufacturer.SelectedValue = _brandName.Manufacturer.ID.Value.ToString();
        }
        else
        {
            ViewState[_target] = new SolutionObjects.BrandName();
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        Search();
    }
    protected void GridViewSearchList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewSearchList.PageIndex = e.NewPageIndex;
        Search();
    }
    protected void GridViewSearchList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _brandName = new OperationObjects.BrandNameOP().GetBrandName(_targetid);

            ClearAll();
            ViewState[_target] = _brandName;

            PanelAddNew.Visible = true;
            PanelSearchDetails.Visible = false;
            LabelPageOperation.Text = "Edit";
            LinkButtonAddNewOperation.Visible = false;
            LinkButtonSearchOperation.Visible = true;

            TextBoxDescription.Text = _brandName.Description;
            if (_brandName.Manufacturer != null)
                DropDownListManufacturer.SelectedValue = _brandName.Manufacturer.ID.Value.ToString();
        }

        if (e.CommandName.Equals("DeleteRecord"))
        {
            int _targetid = Convert.ToInt32(GridViewSearchList.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["ID"].ToString());
            _brandName = new OperationObjects.BrandNameOP().GetBrandName(_targetid);
            _brandName.User = User.Identity.Name;

            try
            {
                CommonObjects.DataTransferObject _dtc = new OperationObjects.BrandNameOP().DeleteBrandName(_brandName);
                if (_dtc.Status == CommonObjects.TransactionStatus.Completed)
                    Search();
                else
                    return;
            }
            catch (Exception)
            {
                return;
            }
        }
    }
    protected void GridViewSearchList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            _brandName = e.Row.DataItem as SolutionObjects.BrandName;
            if (_brandName.Manufacturer != null)
                _brandName.Manufacturer = new OperationObjects.ManufacturerOP().GetManufacturer(_brandName.Manufacturer.ID.Value);
            Util.EncodeString(e.Row.Cells[2], _brandName.Description, 50);
            Util.EncodeString(e.Row.Cells[3], _brandName.Manufacturer != null ? _brandName.Manufacturer.Description : string.Empty, 50);

            foreach (Control control in e.Row.Cells[1].Controls)
            {
                if (control != null)
                {
                    ((LinkButton)control).Attributes.Add("onclick", "javascript:return " +
                    "confirm('Are you sure you want to delete this record : " + _brandName.Description + "');");
                }
            }
        }
    }
}