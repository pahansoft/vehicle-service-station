﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class GRNOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveGRN(Inventory.SolutionObjects.GRN grn)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (grn.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.GRNDO().Create(grn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList)
                    {
                        grnDetail.IsNewRec = true;
                        innerDTO = new DataAccessObjects.GRNDetailDO().Create(grnDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    //CreateItemStock(grn);

                    //// Add, Edit ItemStock List
                    //foreach (SolutionObjects.ItemStock itemStock in grn.ItemStockList)
                    //{
                    //    innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                    //    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    //        throw new Exception(innerDTO.Message);
                    //}
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.GRNDO().Edit(grn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList)
                    {
                        if (grnDetail.ID == null)
                        {
                            grnDetail.IsNewRec = true;
                            innerDTO = new DataAccessObjects.GRNDetailDO().Create(grnDetail);
                        }
                        else
                            innerDTO = new DataAccessObjects.GRNDetailDO().Edit(grnDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.GRNDetailDO().Delete(grnDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    //CreateItemStock(grn);

                    //// Add, Edit ItemStock List
                    //foreach (SolutionObjects.ItemStock itemStock in grn.ItemStockList)
                    //{
                    //    innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                    //    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    //        throw new Exception(innerDTO.Message);
                    //}
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject ApproveGRN(Inventory.SolutionObjects.GRN grn)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                if (grn.ID == null)
                {
                    dto = new Inventory.DataAccessObjects.GRNDO().Create(grn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add new GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList)
                    {
                        grnDetail.IsNewRec = true;
                        innerDTO = new DataAccessObjects.GRNDetailDO().Create(grnDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(grn);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in grn.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }
                else
                {
                    dto = new Inventory.DataAccessObjects.GRNDO().Edit(grn);
                    if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(dto.Message);

                    // Add, Edit GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList)
                    {
                        if (grnDetail.ID == null)
                        {
                            grnDetail.IsNewRec = true;
                            innerDTO = new DataAccessObjects.GRNDetailDO().Create(grnDetail);
                        }
                        else
                            innerDTO = new DataAccessObjects.GRNDetailDO().Edit(grnDetail);

                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    // Delete GRNDetail List
                    foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList.RemovedList)
                    {
                        innerDTO = new DataAccessObjects.GRNDetailDO().Delete(grnDetail);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }

                    CreateItemStock(grn);

                    // Add, Edit ItemStock List
                    foreach (SolutionObjects.ItemStock itemStock in grn.ItemStockList)
                    {
                        innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                        if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                            throw new Exception(innerDTO.Message);
                    }
                }

                scope.Complete();
            }
            return dto;
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteGRN(Inventory.SolutionObjects.GRN grn)
        {
            Pahansoft.CommonObjects.DataTransferObject dto = new Pahansoft.CommonObjects.DataTransferObject();
            Pahansoft.CommonObjects.DataTransferObject innerDTO = new Pahansoft.CommonObjects.DataTransferObject();

            System.Transactions.TransactionOptions options = new System.Transactions.TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            /// Obtain a new scope of transaction boundry.
            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, options, System.Transactions.EnterpriseServicesInteropOption.Automatic))
            {
                dto = new Inventory.DataAccessObjects.GRNDO().Delete(grn);
                if (dto.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                    throw new Exception(dto.Message);

                // Delete GRNDetail List
                foreach (SolutionObjects.GRNDetail grnDetail in grn.GRNDetailList.RemovedList)
                {
                    innerDTO = new DataAccessObjects.GRNDetailDO().Delete(grnDetail);
                    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                        throw new Exception(innerDTO.Message);
                }

                //CreateItemStock(grn);

                //// Add, Edit ItemStock List
                //foreach (SolutionObjects.ItemStock itemStock in grn.ItemStockList)
                //{
                //    innerDTO = new ItemStockOP().SaveItemStock(itemStock);
                //    if (innerDTO.Status == Pahansoft.CommonObjects.TransactionStatus.Failed)
                //        throw new Exception(innerDTO.Message);
                //}

                scope.Complete();
            }
            return dto;
        }

        public Inventory.SolutionObjects.GRN GetGRN(int id)
        {
            return new Inventory.DataAccessObjects.GRNDO().Find(id);
        }

        public Inventory.SolutionObjects.GRN GetGRNProxy(int id)
        {
            return new Inventory.DataAccessObjects.GRNDO().FindProxy(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> GetAllGRNs()
        {
            return new Inventory.DataAccessObjects.GRNDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRN> SearchGRNs(string gRNNumber, DateTime? dateGRN, SolutionObjects.Supplier supplier, bool? approveOnly)
        {
            return new Inventory.DataAccessObjects.GRNDO().Search(gRNNumber, dateGRN, supplier, approveOnly);
        }

        private SolutionObjects.GRN CreateItemStock(SolutionObjects.GRN _grn)
        {
            _grn.ItemStockList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStock>();

            SolutionObjects.ItemStock _itemStock = null;
            SolutionObjects.ItemStockTransaction _itemStockTransaction;
            bool _hasFound;
            foreach (SolutionObjects.GRNDetail gRNDetail in _grn.GRNDetailList)
            {
                //if (!gRNDetail.IsNewRec)
                //    continue;

                _hasFound = false;

                foreach (SolutionObjects.ItemStock grnItemStock in _grn.ItemStockList)
                {
                    if (grnItemStock.ProductItem.ID == gRNDetail.ProductItem.ID
                        //&& grnItemStock.LotNumber == gRNDetail.LotNumber
                        && grnItemStock.Store.ID == gRNDetail.Store.ID
                        && grnItemStock.Location.ID == gRNDetail.Location.ID
                        && grnItemStock.Rack.ID == gRNDetail.Rack.ID
                        && grnItemStock.UnitPrice == gRNDetail.UnitPrice
                        && grnItemStock.SellingPrice == gRNDetail.SellingPrice)
                    {
                        _itemStock = grnItemStock;
                        _itemStock.CurrentQuantity += gRNDetail.Quantity;
                        _hasFound = true;
                        break;
                    }
                }

                if (!_hasFound)
                {
                    IList<SolutionObjects.ItemStock> _existItemStockList = new OperationObjects.ItemStockOP().Search(gRNDetail.ProductItem, gRNDetail.Store, gRNDetail.Location, gRNDetail.Rack, SolutionObjects.StockTypes.Trading, null, gRNDetail.UnitPrice, gRNDetail.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _itemStock.CurrentQuantity += gRNDetail.Quantity;
                        _hasFound = true;
                    }
                }

                if (!_hasFound)
                {
                    _itemStock = new SolutionObjects.ItemStock();
                    _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();
                    _itemStock.CurrentQuantity = gRNDetail.Quantity;
                }

                _itemStock.ProductItem = gRNDetail.ProductItem;
                _itemStock.StockTypes = SolutionObjects.StockTypes.Trading;
                _itemStock.LotNumber = gRNDetail.LotNumber;
                _itemStock.Store = gRNDetail.Store;
                _itemStock.Location = gRNDetail.Location;
                _itemStock.Rack = gRNDetail.Rack;
                _itemStock.UnitPrice = gRNDetail.UnitPrice;
                _itemStock.SellingPrice = gRNDetail.SellingPrice;
                _itemStock.DateManufactured = gRNDetail.DateManufactured;
                _itemStock.DateExpiry = gRNDetail.DateExpiry;
                _itemStock.User = _grn.User;
                _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                _itemStockTransaction.ItemStock = _itemStock;
                _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.GRN;
                _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                _itemStockTransaction.TransactionID = _grn.ID;
                _itemStockTransaction.TransactionDetailID = gRNDetail.ID;
                _itemStockTransaction.ReferenceNumber = _grn.GRNNumber;
                _itemStockTransaction.Store = _itemStock.Store;
                _itemStockTransaction.Location = _itemStock.Location;
                _itemStockTransaction.Rack = _itemStock.Rack;
                _itemStockTransaction.Quantity = gRNDetail.Quantity;
                _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                _itemStockTransaction.User = _grn.User;
                _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                _grn.ItemStockList.Add(_itemStock);
            }

            foreach (SolutionObjects.GRNDetail gRNDetail in _grn.GRNDetailList.RemovedList)
            {
                if (gRNDetail.ID != null)
                {
                    IList<SolutionObjects.ItemStock> _existItemStockList = new OperationObjects.ItemStockOP().Search(gRNDetail.ProductItem, gRNDetail.Store, gRNDetail.Location, gRNDetail.Rack, SolutionObjects.StockTypes.Trading, gRNDetail.LotNumber, gRNDetail.UnitPrice, gRNDetail.SellingPrice);
                    if (_existItemStockList.Count > 0)
                    {
                        _itemStock = _existItemStockList[0];
                        _itemStock.CurrentQuantity -= gRNDetail.Quantity;
                        _itemStock.ProductItem = gRNDetail.ProductItem;
                        _itemStock.StockTypes = SolutionObjects.StockTypes.Trading;
                        _itemStock.LotNumber = gRNDetail.LotNumber;
                        _itemStock.Store = gRNDetail.Store;
                        _itemStock.Location = gRNDetail.Location;
                        _itemStock.Rack = gRNDetail.Rack;
                        _itemStock.UnitPrice = gRNDetail.UnitPrice;
                        _itemStock.SellingPrice = gRNDetail.SellingPrice;
                        _itemStock.DateManufactured = gRNDetail.DateManufactured;
                        _itemStock.DateExpiry = gRNDetail.DateExpiry;
                        _itemStock.User = _grn.User;
                        _itemStock.ItemStockTransactionList = new Pahansoft.CommonObjects.PropertyList<SolutionObjects.ItemStockTransaction>();

                        _itemStockTransaction = new SolutionObjects.ItemStockTransaction();
                        _itemStockTransaction.ItemStock = _itemStock;
                        _itemStockTransaction.ProductItem = _itemStock.ProductItem;
                        _itemStockTransaction.TransactionType = SolutionObjects.TransactionTypes.GRN;
                        _itemStockTransaction.StockType = SolutionObjects.StockTypes.Trading;
                        _itemStockTransaction.TransactionID = _grn.ID;
                        _itemStockTransaction.TransactionDetailID = gRNDetail.ID;
                        _itemStockTransaction.ReferenceNumber = _grn.GRNNumber;
                        _itemStockTransaction.Store = _itemStock.Store;
                        _itemStockTransaction.Location = _itemStock.Location;
                        _itemStockTransaction.Rack = _itemStock.Rack;
                        _itemStockTransaction.Quantity = gRNDetail.Quantity;
                        _itemStockTransaction.UnitPrice = _itemStock.UnitPrice;
                        _itemStockTransaction.SellingPrice = _itemStock.SellingPrice;
                        _itemStockTransaction.Remarks = "Delete GRN Detail";
                        _itemStockTransaction.User = _grn.User;
                        _itemStock.ItemStockTransactionList.Add(_itemStockTransaction);

                        _grn.ItemStockList.Add(_itemStock);
                    }
                }
            }

            return _grn;
        }

    }
}
