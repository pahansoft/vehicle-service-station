﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class StoreOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveStore(Inventory.SolutionObjects.Store store)
        {
            if (store.ID == null)
                return new Inventory.DataAccessObjects.StoreDO().Create(store);
            else
                return new Inventory.DataAccessObjects.StoreDO().Edit(store);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteStore(Inventory.SolutionObjects.Store store)
        {
            if (store.ID != null)
                return new Inventory.DataAccessObjects.StoreDO().Delete(store);
            else
            {
                store.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = store;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.Store GetStore(int id)
        {
            return new Inventory.DataAccessObjects.StoreDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Store> GetAllStores()
        {
            return new Inventory.DataAccessObjects.StoreDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Store> SearchStores(string description)
        {
            return new Inventory.DataAccessObjects.StoreDO().Search(description);
        }
    }
}
