﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inventory.OperationObjects
{
    public class GRNDetailOP
    {
        public Pahansoft.CommonObjects.DataTransferObject SaveGRNDetail(Inventory.SolutionObjects.GRNDetail grnDetail)
        {
            if (grnDetail.ID == null)
                return new Inventory.DataAccessObjects.GRNDetailDO().Create(grnDetail);
            else
                return new Inventory.DataAccessObjects.GRNDetailDO().Edit(grnDetail);
        }

        public Pahansoft.CommonObjects.DataTransferObject DeleteGRNDetail(Inventory.SolutionObjects.GRNDetail grnDetail)
        {
            if (grnDetail.ID != null)
                return new Inventory.DataAccessObjects.GRNDetailDO().Delete(grnDetail);
            else
            {
                grnDetail.Archived = true;
                Pahansoft.CommonObjects.DataTransferObject _dts = new Pahansoft.CommonObjects.DataTransferObject();
                _dts.Status = Pahansoft.CommonObjects.TransactionStatus.Completed;
                _dts.TransferObject = grnDetail;
                return _dts;
            }
        }

        public Inventory.SolutionObjects.GRNDetail GetGRNDetail(int id)
        {
            return new Inventory.DataAccessObjects.GRNDetailDO().Find(id);
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> GetAllGRNDetails()
        {
            return new Inventory.DataAccessObjects.GRNDetailDO().GetAll();
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.GRNDetail> GetAllFor(SolutionObjects.GRN grn)
        {
            return new Inventory.DataAccessObjects.GRNDetailDO().GetAllFor(grn);
        }
    }
}
