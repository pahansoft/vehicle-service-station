﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;

#endregion


public partial class repo_stock_repo_repo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int? ProductType = null;
        string code = null;
        string description = null;
        string partNumber = null;
        bool? brandNew = null;

        if (Request.Params["ItemType"].ToString() != string.Empty)
            ProductType = int.Parse(Request.Params["ItemType"].ToString());
        if (Request.Params["code"].ToString() != string.Empty)
            code = Request.Params["code"].ToString();
        if (Request.Params["description"].ToString() != string.Empty)
            description = Request.Params["description"].ToString();
        if (Request.Params["partNumber"].ToString() != string.Empty)
            partNumber = Request.Params["partNumber"].ToString();
        if (Request.Params["brandNew"].ToString() != string.Empty)
            brandNew = bool.Parse( Request.Params["brandNew"].ToString());
        Generate(ProductType, code, description, partNumber, brandNew);
    }

    private int GetPageCount(int count, int linesPerPage)
    {
        int _pageCount = 0;
        if (count > linesPerPage)
        {
            if (count % linesPerPage == 0)
                _pageCount = count / linesPerPage;
            else
                _pageCount = count / linesPerPage + 1;
        }
        else
            _pageCount = 1;

        return _pageCount;
    }
    private void Generate(int? ProductType,string code,string description,string partNumber,bool? BrandNewItemOnly)
    {
        var list = new OperationObjects.ProductItemOP().SearchProductItemsForReport(code,description,partNumber, ProductType, BrandNewItemOnly);
        if (list.Count == 0)
            return;

        int _linesPerPage = 1000;
        int _pageCount = GetPageCount(list.Count, _linesPerPage);
        int _innerCount = 0;
        int _listIndex = 0;
        StringBuilder _sb = new StringBuilder();
        StringBuilder inneSb = new StringBuilder();

        for (int i = 0; i < _pageCount; i++)
        {
            if (_listIndex == list.Count)
                break;
            _sb.Append("<div style=\"page-break-after: always;\">");
            _sb.Append("<table align=\"center\" style=\"width: 90%;  font-family:Courier New;\">");

            #region - Page Header -

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"text-align: center; font-size:large; font-weight:bold;\"> SERVICE STATION ITEMS REPORT</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"text-align: center; font-size:small; font-weight:bold;\">&nbsp;</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #endregion
            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">");
            _sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 100%;\" cellpadding=\"3\">");

            _sb.Append("<tr style=\"text-align: center; font-size:small; font-weight:bold; height:30px; background-color: #F5F5F5;\">");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Item Code</td>");
            _sb.Append("<td style=\"width : 60%; text-align: center;\">Description </td>");
            _sb.Append("<td style=\"width : 15%; text-align: center;\">Part Number </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Reoder Level </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Reoder Quantity </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Unit Price </td>");
            _sb.Append("<td style=\"width : 10%; text-align: center;\">Sales Price </td>");


            _innerCount = 0;
            for (int j = _listIndex; j < list.Count; j++)
            {
                _sb.Append("<tr style=\"font-size:small;\">");
               _sb.Append("<td>" + list[_listIndex].Code + "</td>");

               _sb.Append("<td>" + list[_listIndex].Description + list[_listIndex].OtherDescription + "</td>");
               _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].ReferenceNumber + "</td>");
               _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].ROL + "</td>");
               _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].ROQuantity + "</td>");
               _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].CostPrice.ToString("F") + "</td>");
               _sb.Append("<td style=\" text-align: right;\">" + list[_listIndex].SellingPrice.ToString("F") + "</td>");

                

                if (_innerCount == _linesPerPage - 1)
                {
                    _listIndex += 1;
                    break;
                }
                else
                {
                    if (_listIndex == list.Count - 1)
                        break;
                    else
                    {
                        _listIndex += 1;
                        _innerCount += 1;

                    }
                }
            }

            _sb.Append("</table>");
            _sb.Append("</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">&nbsp;</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #region - Footer -

            _sb.Append("<tr>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("<td style=\"width: 96%;\">");
            _sb.Append("<table style=\"width: 100%;\">");

            _sb.Append("<tr style=\"font-size:small;\">");
            _sb.Append("<td style=\"width : 50%\">" + ("Page [" + (i + 1) + "] of [" + _pageCount + "]") + "</td>");
            _sb.Append("<td style=\"width : 50%; text-align: right;\">" + (DateTime.Today.ToShortDateString() + "&nbsp;&nbsp;" + DateTime.Now.ToShortTimeString()) + "</td>");
            _sb.Append("</tr>");

            _sb.Append("</table>");
            _sb.Append("</td>");
            _sb.Append("<td style=\"width: 2%\"></td>");
            _sb.Append("</tr>");

            #endregion

            _sb.Append("</table>");
            _sb.Append("</div>");
        }
        LabelReportBody1.Text = _sb.ToString();

    }

}