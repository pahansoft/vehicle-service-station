﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Inventory.DataAccessObjects
{
    public class JobCardServiceDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.JobCardService JobCardService)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_CreateJobCardService";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardService.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.JobCard != null ? JobCardService.JobCard.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ServiceID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.Service != null ? JobCardService.Service.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountPercentage", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.DiscountPercentage);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.DiscountAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TaxAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.TaxAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.NetAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardService;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCardService.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.JobCardService JobCardService)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_EditJobCardService";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCardService.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardService.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.JobCard != null ? JobCardService.JobCard.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ServiceID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.Service != null ? JobCardService.Service.ID : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@GrossAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.GrossAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountPercentage", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.DiscountPercentage);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@DiscountAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.DiscountAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TaxAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.TaxAmount);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@NetAmount", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, JobCardService.NetAmount);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardService;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.JobCardService JobCardService)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_DeleteJobCardService";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCardService.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, JobCardService.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, JobCardService.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = JobCardService;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                JobCardService.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.JobCardService Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetJobCardService";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.JobCardService JobCardService = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int IDOrdinal = dr.GetOrdinal("ID");
                        int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                        int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                        int ServiceIDOrdinal = dr.GetOrdinal("ServiceID");
                        int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                        int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                        int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                        int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                        int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                        JobCardService = new Inventory.SolutionObjects.JobCardService(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardService.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardService.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ServiceIDOrdinal)) JobCardService.Service = new SolutionObjects.Service(dr.GetInt32(ServiceIDOrdinal));
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardService.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardService.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardService.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardService.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardService.NetAmount = dr.GetDecimal(NetAmountOrdinal);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardService;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardService> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllJobCardServices";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardService> JobCardServiceList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCardService>();
            Inventory.SolutionObjects.JobCardService JobCardService;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int ServiceIDOrdinal = dr.GetOrdinal("ServiceID");
                    int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                    int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                    int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                    int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                    while (dr.Read())
                    {
                        JobCardService = new Inventory.SolutionObjects.JobCardService(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardService.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardService.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ServiceIDOrdinal)) JobCardService.Service = new SolutionObjects.Service(dr.GetInt32(ServiceIDOrdinal));
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardService.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardService.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardService.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardService.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardService.NetAmount = dr.GetDecimal(NetAmountOrdinal);

                        JobCardServiceList.Add(JobCardService);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardServiceList;
        }

        public System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardService> GetAllFor(SolutionObjects.JobCard JobCard)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "T_SP_GetAllJobCardServicesForJobCard";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@JobCardID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, JobCard.ID);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.JobCardService> JobCardServiceList = new System.Collections.Generic.List<Inventory.SolutionObjects.JobCardService>();
            Inventory.SolutionObjects.JobCardService JobCardService;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int IDOrdinal = dr.GetOrdinal("ID");
                    int TimeStampOrdinal = dr.GetOrdinal("TimeStamp");

                    int JobCardIDOrdinal = dr.GetOrdinal("JobCardID");
                    int ServiceIDOrdinal = dr.GetOrdinal("ServiceID");
                    int GrossAmountOrdinal = dr.GetOrdinal("GrossAmount");
                    int DiscountPercentageOrdinal = dr.GetOrdinal("DiscountPercentage");
                    int DiscountAmountOrdinal = dr.GetOrdinal("DiscountAmount");
                    int TaxAmountOrdinal = dr.GetOrdinal("TaxAmount");
                    int NetAmountOrdinal = dr.GetOrdinal("NetAmount");

                    while (dr.Read())
                    {
                        JobCardService = new Inventory.SolutionObjects.JobCardService(dr.GetInt32(IDOrdinal));
                        if (!dr.IsDBNull(TimeStampOrdinal)) JobCardService.TimeStamp = dr.GetValue(TimeStampOrdinal);

                        if (!dr.IsDBNull(JobCardIDOrdinal)) JobCardService.JobCard = new SolutionObjects.JobCard(dr.GetInt32(JobCardIDOrdinal));
                        if (!dr.IsDBNull(ServiceIDOrdinal)) JobCardService.Service = new SolutionObjects.Service(dr.GetInt32(ServiceIDOrdinal));
                        if (!dr.IsDBNull(GrossAmountOrdinal)) JobCardService.GrossAmount = dr.GetDecimal(GrossAmountOrdinal);
                        if (!dr.IsDBNull(DiscountPercentageOrdinal)) JobCardService.DiscountPercentage = dr.GetDecimal(DiscountPercentageOrdinal);
                        if (!dr.IsDBNull(DiscountAmountOrdinal)) JobCardService.DiscountAmount = dr.GetDecimal(DiscountAmountOrdinal);
                        if (!dr.IsDBNull(TaxAmountOrdinal)) JobCardService.TaxAmount = dr.GetDecimal(TaxAmountOrdinal);
                        if (!dr.IsDBNull(NetAmountOrdinal)) JobCardService.NetAmount = dr.GetDecimal(NetAmountOrdinal);

                        JobCardServiceList.Add(JobCardService);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return JobCardServiceList;
        }

        #endregion
    }
}
