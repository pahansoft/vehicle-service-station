﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Inventory.DataAccessObjects
{
    public class ManufacturerDO
    {
        #region --- Create ---

        public Pahansoft.CommonObjects.DataTransferObject Create(Inventory.SolutionObjects.Manufacturer manufacturer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_CreateManufacturer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Output, command, null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, manufacturer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, manufacturer.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CountryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, manufacturer.Country != null ? manufacturer.Country.ID : null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = manufacturer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                manufacturer.ID = (int?)(command.Parameters["@ID"] as System.Data.IDataParameter).Value;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Edit ---

        public Pahansoft.CommonObjects.DataTransferObject Edit(Inventory.SolutionObjects.Manufacturer manufacturer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_EditManufacturer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, manufacturer.ID.Value);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, manufacturer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, manufacturer.User);

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, manufacturer.Description);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CountryID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, manufacturer.Country != null ? manufacturer.Country.ID : null);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = manufacturer;
            Pahansoft.CommonObjects.DataHandler.ExecuteStatement(command, dataTransferObject);

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Delete ---

        public Pahansoft.CommonObjects.DataTransferObject Delete(Inventory.SolutionObjects.Manufacturer manufacturer)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_DeleteManufacturer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, manufacturer.ID);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@TimeStamp", System.Data.SqlDbType.Timestamp, System.Data.ParameterDirection.Input, command, manufacturer.TimeStamp);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@User", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, manufacturer.User);

            Pahansoft.CommonObjects.DataTransferObject dataTransferObject = new Pahansoft.CommonObjects.DataTransferObject();
            dataTransferObject.TransferObject = manufacturer;
            Pahansoft.CommonObjects.DataHandler.DeleteStatement(command, dataTransferObject);

            if (dataTransferObject.Status == Pahansoft.CommonObjects.TransactionStatus.Completed)
                manufacturer.Archived = true;

            command.Dispose();
            command = null;

            return dataTransferObject;
        }

        #endregion

        #region --- Find ---

        public Inventory.SolutionObjects.Manufacturer Find(int id)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetManufacturer";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@ID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, command, id);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            Inventory.SolutionObjects.Manufacturer manufacturer = null;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    if (dr.Read())
                    {
                        int idOrdinal = dr.GetOrdinal("ID");
                        int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                        int descriptionOrdinal = dr.GetOrdinal("Description");
                        int countryIDOrdinal = dr.GetOrdinal("CountryID");

                        manufacturer = new Inventory.SolutionObjects.Manufacturer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) manufacturer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) manufacturer.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) manufacturer.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return manufacturer;
        }

        #endregion

        #region --- Get All ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> GetAll()
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_GetAllManufacturers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> manufacturerList = new System.Collections.Generic.List<Inventory.SolutionObjects.Manufacturer>();
            Inventory.SolutionObjects.Manufacturer manufacturer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int countryIDOrdinal = dr.GetOrdinal("CountryID");

                    while (dr.Read())
                    {
                        manufacturer = new Inventory.SolutionObjects.Manufacturer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) manufacturer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) manufacturer.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) manufacturer.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));

                        manufacturerList.Add(manufacturer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return manufacturerList;
        }

        #endregion

        #region --- Search ---

        public System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> Search(string description, SolutionObjects.Country country)
        {
            System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
            command.CommandText = "M_SP_FindAllManufacturers";
            command.CommandType = CommandType.StoredProcedure;

            Pahansoft.CommonObjects.DataHandler.AddSPParams("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, command, description != string.Empty ? description : null);
            Pahansoft.CommonObjects.DataHandler.AddSPParams("@CountryID", System.Data.SqlDbType.Decimal, System.Data.ParameterDirection.Input, command, country != null ? country.ID : null);

            Pahansoft.CommonObjects.DataHandler.OpenCommandConnection(command);
            System.Collections.Generic.IList<Inventory.SolutionObjects.Manufacturer> manufacturerList = new System.Collections.Generic.List<Inventory.SolutionObjects.Manufacturer>();
            Inventory.SolutionObjects.Manufacturer manufacturer;

            if (command.Connection.State == ConnectionState.Open)
            {
                System.Data.IDataReader dr = command.ExecuteReader();

                if (dr.RecordsAffected != 0)
                {
                    int idOrdinal = dr.GetOrdinal("ID");
                    int timestampOrdinal = dr.GetOrdinal("TimeStamp");

                    int descriptionOrdinal = dr.GetOrdinal("Description");
                    int countryIDOrdinal = dr.GetOrdinal("CountryID");

                    while (dr.Read())
                    {
                        manufacturer = new Inventory.SolutionObjects.Manufacturer(dr.GetInt32(idOrdinal));
                        if (!dr.IsDBNull(timestampOrdinal)) manufacturer.TimeStamp = dr.GetValue(timestampOrdinal);

                        if (!dr.IsDBNull(descriptionOrdinal)) manufacturer.Description = dr.GetString(descriptionOrdinal);
                        if (!dr.IsDBNull(countryIDOrdinal)) manufacturer.Country = new SolutionObjects.Country(dr.GetInt32(countryIDOrdinal));

                        manufacturerList.Add(manufacturer);
                    }
                }
            }

            Pahansoft.CommonObjects.DataHandler.CloseCommandConnection(command);
            return manufacturerList;
        }

        #endregion
    }
}
