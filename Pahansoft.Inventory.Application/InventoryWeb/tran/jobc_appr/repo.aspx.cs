﻿#region - Imports -

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using OperationObjects = Inventory.OperationObjects;
using SolutionObjects = Inventory.SolutionObjects;
using CommonObjects = Pahansoft.CommonObjects;
using System.IO;
using System.Configuration;
using System.Globalization;

#endregion

public partial class tran_jobc_appr_repo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int? JobCardID = Util.ParseInt(Request.Params["JobCardID"].ToString());
        GenerateText(JobCardID.Value);
    }

    private void GenerateText(int JobCardID)
    {
        var JobCard = new OperationObjects.JobCardOP().Get(JobCardID);
        JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);
        JobCard.Supervisor = new OperationObjects.SupervisorOP().GetSupervisor(JobCard.Supervisor.ID.Value);
        var Payments = new OperationObjects.CustomerPaymentOP().Search(JobCard.Customer.ID, SolutionObjects.DebitCreditTypes.Cr, JobCard.ID);

        string session = Session.SessionID + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
        string path = Server.MapPath(ConfigurationManager.AppSettings["DocumentPath"] + session + "Invoice.txt");
        FileInfo fe = new FileInfo(path);
        int diff = 0;
        StreamWriter text = fe.CreateText();

        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");
        //text.WriteLine(" ");
        //text.WriteLine(" ");
        //text.WriteLine(" ");
  
         var sbJobNo = new StringBuilder();
        sbJobNo.Append(JobCard.JobNumber);
        diff = 20 - JobCard.JobNumber.Length;
        for (int d = 0; d < diff; d++)
            sbJobNo.Append(" ");
      
        var sbCustomer = new StringBuilder();
        if (JobCard.Customer.Name.Length > 40)
            sbCustomer.Append(JobCard.Customer.Name.Substring(0, 40) + sbJobNo.ToString());
        else
        {
            diff = 40 - JobCard.Customer.Name.Length;
            sbCustomer.Append(JobCard.Customer.Name);
            for (int d = 0; d < diff; d++)
                sbCustomer.Append(" ");
        }
        text.WriteLine(("            ") + sbCustomer.ToString() +("                     ")+ sbJobNo.ToString());

        var sbAddr1 = new StringBuilder();
        if (JobCard.Customer.AddressLine1.Length > 40)
            sbAddr1.Append(JobCard.Customer.AddressLine1.Substring(0, 40));
        else
        {
            diff = 40 - JobCard.Customer.AddressLine1.Length;
            sbAddr1.Append(JobCard.Customer.AddressLine1);
            for (int d = 0; d < diff; d++)
                sbAddr1.Append(" ");
        }
        text.WriteLine(("            ") + sbAddr1.ToString());

        var sbAddr2 = new StringBuilder();
        if (JobCard.Customer.AddressLine2.Length > 40)
            sbAddr2.Append(JobCard.Customer.AddressLine2.Substring(0, 40));
        else
        {
            diff = 40 - JobCard.Customer.AddressLine2.Length;
            sbAddr2.Append(JobCard.Customer.AddressLine2);
            for (int d = 0; d < diff; d++)
                sbAddr2.Append(" ");
        }
        text.WriteLine(("            ") + sbAddr2.ToString() + "                     " + JobCard.DateApproved.Value.Day + "-" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(JobCard.DateApproved.Value.Month) + "-" + JobCard.DateApproved.Value.Year);

        var sbAddr3 = new StringBuilder();
        if (JobCard.Customer.AddressLine3.Length > 40)
            sbAddr3.Append(JobCard.Customer.AddressLine3.Substring(0, 40));
        else
        {
            diff = 40 - JobCard.Customer.AddressLine3.Length;
            sbAddr3.Append(JobCard.Customer.AddressLine3);
            for (int d = 0; d < diff; d++)
                sbAddr3.Append(" ");
        }
        

        text.WriteLine(("            ") + sbAddr3.ToString());
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(("            ") + JobCard.VehicleNumber);
        text.WriteLine(" ");
        text.WriteLine(" ");
        text.WriteLine(" ");

        int printedLine = 0;

        foreach (var item in JobCard.ItemList)
        {
            item.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(item.ItemStock.ID.Value);
            item.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(item.ItemStock.ProductItem.ID.Value);

            var sbCode = new StringBuilder();
            if (item.ItemStock.ProductItem.Code.Length > 6)
                sbCode.Append(item.ItemStock.ProductItem.Code.Substring(0, 6));
            else
                sbCode.Append(item.ItemStock.ProductItem.Code);
            diff = 8 - item.ItemStock.ProductItem.Code.Length;
            for (int d = 0; d < diff; d++)
                sbCode.Append(" ");
            var sbDescription = new StringBuilder();
            if (item.ItemStock.ProductItem.Description.Length > 48)
                sbDescription.Append(item.ItemStock.ProductItem.Description.Substring(0, 48));
            else
            {
                diff = 48 - item.ItemStock.ProductItem.Description.Length;
                sbDescription.Append(item.ItemStock.ProductItem.Description);
                for (int d = 0; d < diff; d++)
                    sbDescription.Append(" ");
            }

            var sbQty = new StringBuilder();
            diff = 4 - item.Quantity.Value.ToString("F").Length;
            for (int d = 0; d < diff; d++)
                sbQty.Append(" ");
            sbQty.Append(item.Quantity.Value.ToString("0.##"));

            var sbUnitPrice = new StringBuilder();
            diff = 12 - item.UnitPrice.Value.ToString("F").Length;
            for (int d = 0; d < diff; d++)
                sbUnitPrice.Append(" ");
            sbUnitPrice.Append(item.UnitPrice.Value.ToString("F"));
      
            var sbAmount = new StringBuilder();
            diff = 10 - item.NetAmount.Value.ToString("F").Length;
            for (int d = 0; d < diff; d++)
                sbAmount.Append(" ");
            sbAmount.Append(item.NetAmount.Value.ToString("F"));

  
            if (printedLine > 28)
            {
                for (int i = 0; i < 29; i++)
                {
                    text.WriteLine("");
                }
                text.WriteLine("" + sbCode.ToString() + sbDescription.ToString() + ("           ") + sbQty.ToString() + (" ") + sbUnitPrice.ToString() + ("  ") + sbAmount.ToString());
                printedLine = 1;
            }
            else
            {
                text.WriteLine("" + sbCode.ToString() + sbDescription.ToString() + ("           ") + sbQty.ToString() + (" ") + sbUnitPrice.ToString() + ("  ") + sbAmount.ToString());
                printedLine++;
            }
        }

        foreach (var item in JobCard.ServiceList)
        {
            item.Service = new OperationObjects.ServiceOP().GetService(item.Service.ID.Value);

            var sbCode = new StringBuilder();
            sbCode.Append("SERV");
            diff = 8 - 4;
            for (int d = 0; d < diff; d++)
                sbCode.Append(" ");

            var sbDescription = new StringBuilder();
            if (item.Service.Description.Length > 48)
                sbDescription.Append(item.Service.Description.Substring(0, 48));
            else
            {
                diff = 48 - item.Service.Description.Length;
                sbDescription.Append(item.Service.Description);
                for (int d = 0; d < diff; d++)
                    sbDescription.Append(" ");
            }

            var sbQty = new StringBuilder();
            diff = 4 -3;
            for (int d = 0; d < diff; d++)
                sbQty.Append(" ");
            sbQty.Append("1");

            var sbUnitPrice = new StringBuilder();
            diff = 12 - item.GrossAmount.Value.ToString("F").Length;
            for (int d = 0; d < diff; d++)
                sbUnitPrice.Append(" ");
            sbUnitPrice.Append(item.GrossAmount.Value.ToString("F"));

            //var sbDiscountP = new StringBuilder();
            //diff = 10 - (item.DiscountPercentage != null ? item.DiscountPercentage.Value.ToString("F").Length : 0);
            //for (int d = 0; d < diff; d++)
            //    sbDiscountP.Append(" ");
            //sbDiscountP.Append(item.DiscountPercentage != null ? item.DiscountPercentage.Value.ToString("F") : string.Empty);

            var sbAmount = new StringBuilder();
            diff = 10 - item.NetAmount.Value.ToString("F").Length;
            for (int d = 0; d < diff; d++)
                sbAmount.Append(" ");
            sbAmount.Append(item.NetAmount.Value.ToString("F"));

            if (printedLine > 28)
            {
                for (int i = 0; i < 29; i++)
                {
                    text.WriteLine("");
                }
                text.WriteLine(sbCode.ToString() + sbDescription.ToString() + ("          ") + sbQty.ToString() + (" ") + sbUnitPrice.ToString() + ("  ") + sbAmount.ToString());
                printedLine = 1;
            }
            else
            {
                text.WriteLine(sbCode.ToString() + sbDescription.ToString() + ("          ") + sbQty.ToString() + (" ") + sbUnitPrice.ToString() + ("  ") + sbAmount.ToString());
                printedLine++;
            }
            
        }
        
            diff = 37 - printedLine;
            for (int d = 0; d < diff; d++)
                text.WriteLine(" ");
        
       
        var sbTotal = new StringBuilder();
        diff = 74;
        for (int d = 0; d < diff; d++)
            sbTotal.Append(" ");
        diff = 20 - JobCard.TotalNet.Value.ToString("F").Length;
        for (int d = 0; d < diff; d++)
            sbTotal.Append(" ");
        sbTotal.Append(JobCard.TotalNet.Value.ToString("F"));
        text.WriteLine(sbTotal.ToString());
        //text.WriteLine(" ");
        //text.WriteLine(" ");

        var sbTax = new StringBuilder();
        diff = 66;
        for (int d = 0; d < diff; d++)
            sbTax.Append(" ");
        diff = 20 - JobCard.TotalTax.Value.ToString("F").Length;
        for (int d = 0; d < diff; d++)
            sbTax.Append(" ");
        sbTax.Append(JobCard.TotalTax.Value.ToString("F"));
        //text.WriteLine(sbTax.ToString());
        //text.WriteLine(" ");
        //text.WriteLine(" ");
        var sbUser = new StringBuilder();
        sbUser.Append(User.Identity.Name);
        diff = 20 - User.Identity.Name.Length;
        for (int d = 0; d < diff; d++)
            sbUser.Append(" ");
        text.WriteLine(("  ")+sbUser.ToString());

        text.Close();
        Response.ContentType = "text/plain";
        Response.AppendHeader("content-disposition", "attachment; filename= Invoice.txt");
        Response.Charset = "";
        Response.TransmitFile(path);
        Response.End();
    }

    private void Generate(int JobCardID)
    {
        var JobCard = new OperationObjects.JobCardOP().Get(JobCardID);
        JobCard.Customer = new OperationObjects.CustomerOP().GetCustomer(JobCard.Customer.ID.Value);
        JobCard.Supervisor = new OperationObjects.SupervisorOP().GetSupervisor(JobCard.Supervisor.ID.Value);
        var Payments = new OperationObjects.CustomerPaymentOP().Search(JobCard.Customer.ID, SolutionObjects.DebitCreditTypes.Cr, JobCard.ID);

        var sb = new StringBuilder();

        sb.Append("<div style=\"page-break-after: always;\">");
        sb.Append("<table style=\"width: 100%; font-family:Courier New;\">");

        #region - Page Header -

        sb.Append("<tr>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td colspan=\"4\" style=\"text-align: center; font-size:medium; font-weight:bold;\">IMANTHA INTERNATIONALS (PVT) LTD - JOB INVOICE</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr style=\"text-align: left; font-size:small;\">");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td style=\"width: 10%; font-weight:bold;\">CUSTOMER</td>");
        sb.Append("<td style=\"width: 55%; \">" + JobCard.Customer.Name + "</td>");
        sb.Append("<td style=\"width: 10%; font-weight:bold;\">JOB NUMBER</td>");
        sb.Append("<td style=\"width: 25%; \">" + JobCard.JobNumber + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr style=\"text-align: left; font-size:small;\">");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td style=\"font-weight:bold;\">ADDRESS</td>");
        sb.Append("<td>" + JobCard.Address + "</td>");
        sb.Append("<td style=\"font-weight:bold;\">DATE</td>");
        sb.Append("<td>" + JobCard.DateApproved.Value.ToShortDateString() + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr style=\"text-align: left; font-size:small;\">");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td style=\"font-weight:bold;\">SUPERVISOR</td>");
        sb.Append("<td>" + JobCard.Supervisor.Name + "</td>");
        sb.Append("<td style=\"font-weight:bold;\">VEHICLE NO</td>");
        sb.Append("<td>" + JobCard.VehicleNumber + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr style=\"text-align: left; font-size:small;\">");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td style=\"font-weight:bold;\">NET AMOUNT</td>");
        sb.Append("<td colspan=\"3\">" + JobCard.TotalNet.Value.ToString() + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr style=\"text-align: left; font-size:small;\">");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td style=\"font-weight:bold;\">PAID AMOUNT</td>");
        sb.Append("<td colspan=\"3\">" + Payments.Sum(t => t.Amount).Value.ToString() + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        #endregion

        sb.Append("<tr>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td colspan=\"4\" style=\"text-align: center; font-size:small; font-weight:bold;\">&nbsp;</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        if (JobCard.ItemList.Count > 0)
        {
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 2%\"></td>");
            sb.Append("<td colspan=\"4\">");
            sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 60%;\" cellpadding=\"3\">");

            sb.Append("<tr style=\"text-align: left; font-size:small; font-weight:bold;\">");
            sb.Append("<td style=\"width: 60%;\">ITEM</td>");
            sb.Append("<td style=\"width: 20%; text-align: right;\">QUANTITY</td>");
            sb.Append("<td style=\"width: 20%; text-align: right;\">AMOUNT</td>");
            sb.Append("</tr>");

            foreach (var item in JobCard.ItemList)
            {
                item.ItemStock = new OperationObjects.ItemStockOP().GetItemStockProxy(item.ItemStock.ID.Value);
                item.ItemStock.ProductItem = new OperationObjects.ProductItemOP().GetProductItemProxy(item.ItemStock.ProductItem.ID.Value);

                sb.Append("<tr style=\"text-align: left; font-size:small;\">");
                sb.Append("<td>" + item.ItemStock.ProductItem.Description + "</td>");
                sb.Append("<td style=\"text-align: right;\">" + item.Quantity.Value.ToString() + "</td>");
                sb.Append("<td style=\"text-align: right;\">" + item.NetAmount.Value.ToString() + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("<td style=\"width: 2%\"></td>");
            sb.Append("</tr>");
        }

        sb.Append("<tr>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td colspan=\"4\" style=\"text-align: center; font-size:small; font-weight:bold;\">&nbsp;</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        if (JobCard.ServiceList.Count > 0)
        {
            sb.Append("<tr>");
            sb.Append("<td style=\"width: 2%\"></td>");
            sb.Append("<td colspan=\"4\">");
            sb.Append("<table border=\"1\" style=\"border-collapse: collapse; width: 60%;\" cellpadding=\"3\">");

            sb.Append("<tr style=\"text-align: left; font-size:small; font-weight:bold;\">");
            sb.Append("<td style=\"width: 80%;\">SERVICE</td>");
            sb.Append("<td style=\"width: 20%; text-align: right;\">AMOUNT</td>");
            sb.Append("</tr>");

            foreach (var item in JobCard.ServiceList)
            {
                item.Service = new OperationObjects.ServiceOP().GetService(item.Service.ID.Value);

                sb.Append("<tr style=\"text-align: left; font-size:small;\">");
                sb.Append("<td>" + item.Service.Description + "</td>");
                sb.Append("<td style=\"text-align: right;\">" + item.NetAmount.Value.ToString() + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("<td style=\"width: 2%\"></td>");
            sb.Append("</tr>");
        }

        sb.Append("<tr>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td colspan=\"4\" style=\"text-align: center; font-size:small; font-weight:bold;\">&nbsp;</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("<td colspan=\"4\" style=\"width: 55%; text-align: left; font-size:small; font-weight:bold;\">THIS IS A COMPUTER PRINTED DOCUMENT ON " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "</td>");
        sb.Append("<td style=\"width: 2%\"></td>");
        sb.Append("</tr>");

        sb.Append("</table>");
        sb.Append("</div>");

        LabelReportBody.Text = sb.ToString();
    }
}