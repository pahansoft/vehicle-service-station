﻿<%@ Page Language="C#" MasterPageFile="~/shrd/tran.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="tran_purc_order_Default" Title="Inventory Control System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="../../inc_fils/js/jquery.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery.maskedinput-1.3.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-1.10.0.min.js"></script>
    <script type="text/javascript" src="../../inc_fils/js/jquery-ui.min.js"></script>

    <link href="../../inc_fils/css/jquery-ui.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
            SearchSupplier("ctl00_ContentPlaceHolder1_TextBoxSupplier");
            SearchSupplier("ctl00_ContentPlaceHolder1_TextBoxSrchSupplier");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            SearchProductItem("ctl00_ContentPlaceHolder1_TextBoxProductItem");
        });

        function SearchProductItem(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetProductItems",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchSupplier(control) {
            $("#" + control).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/GetSuppliers",
                        data: "{'Text':'" + document.getElementById(control).value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function lineTotal() {
            var txtValue1 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxUnitPrice').value;
            var txtValue2 = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxQuantity').value;

            var result = 0;
            if (txtValue1 && txtValue2) {
                if (parseFloat(txtValue1) != 0 && parseFloat(txtValue2) != 0)
                    result = parseFloat(txtValue1) * parseFloat(txtValue2);
            }

            if (result) {
                document.getElementById('ctl00_ContentPlaceHolder1_TextBoxLineTotal').value = result;
            }
        }

        function print(orderID) {
            var WinPrint = window.open("repo.aspx?OrderID=" + orderID, '', 'left=5,top=5,width=1005,height=700,toolbar=0,scrollbars=1,status=0');
            WinPrint.focus();
        }

    </script>

    <asp:Panel ID="PanelllowedPage" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td class="pageHedder">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 17px">Purchase Order -
           <asp:Label ID="LabelPageOperation" runat="server"></asp:Label>
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td align="right" style="height: 17px">
                                            <asp:LinkButton ID="LinkButtonSearchOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonSearchOperation_Click">Search</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButtonAddNewOperation" runat="server" Font-Underline="False"
                                                OnClick="LinkButtonAddNewOperation_Click" Visible="False">Add New</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="PanelInformationMessage" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <table border="1" style="border-collapse: collapse; width: 100%; background-color: whitesmoke; border-right: gainsboro 1px solid; border-top: gainsboro 1px solid; border-left: gainsboro 1px solid; border-bottom: gainsboro 1px solid;"
                                                    cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height: 50px">
                                                            <asp:Panel ID="PanelErrorMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px; width: 35px">
                                                                            <img src="../../images/warning.png" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="LabelErrorMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelSucsessMessage" Width="100%" runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="padding-left: 20px;">
                                                                            <asp:Label ID="LabelSucsessMessage" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" style="padding-right: 20px">
                                                                            <asp:Button ID="ButtonCloseMessage" CssClass="button" runat="server" Text="Close"
                                                                                Style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                                                BackColor="Transparent" CausesValidation="False" Font-Bold="True"
                                                                                Font-Underline="False" Width="50px" OnClick="ButtonCloseMessage_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelAddNew" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">PO Number <span style="color: Red;">*</span>
                                                                </td>
                                                                <td style="width: 35%">
                                                                    <asp:TextBox ID="TextBoxPONumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px" ReadOnly="true" Text="- AUTO GENERATED -"></asp:TextBox>
                                                                </td>
                                                                <td>PO Date
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxPODate" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                        ControlToValidate="TextBoxPODate" Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxPODate"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="submit"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supplier <span style="color: Red;">*</span>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="TextBoxSupplier" runat="server" CssClass="textbox" MaxLength="400" Width="400px"></asp:TextBox>
                                                                    &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxSupplier"
                                                                        Display="Dynamic" ErrorMessage="Required" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <table style="width: 100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:GridView ID="GridViewItemAdd" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                                    OnRowDataBound="GridViewItemAdd_RowDataBound" OnRowCommand="GridViewItemAdd_RowCommand"
                                                                                    DataKeyNames="ID" AutoGenerateColumns="False">
                                                                                    <PagerSettings Mode="NumericFirstLast" />
                                                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                                                    <Columns>
                                                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                                                                        </asp:ButtonField>
                                                                                        <asp:BoundField HeaderText="Product Item">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Unit Price">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Quantity">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField HeaderText="Line Total">
                                                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                                Wrap="False" />
                                                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                    <PagerStyle CssClass="grid-pager" />
                                                                                    <EmptyDataTemplate>
                                                                                        <div id="norec" class="no-record-msg">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="padding-left: 15px">No Items(s) Found!
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </EmptyDataTemplate>
                                                                                    <HeaderStyle CssClass="grid-header" />
                                                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%">&nbsp;
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Product Item <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxProductItem" runat="server" CssClass="textbox" MaxLength="400" Width="400px"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxProductItem"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Unit Price <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxUnitPrice" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="lineTotal();"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="TextBoxUnitPrice"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="99999999999" MinimumValue="0"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Quantity <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxQuantity" runat="server" CssClass="textbox" MaxLength="20"
                                                                                    Width="100px" onkeyup="lineTotal();"></asp:TextBox>
                                                                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Required" ValidationGroup="ItemAdd"></asp:RequiredFieldValidator>
                                                                                <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="TextBoxQuantity"
                                                                                    Display="Dynamic" ErrorMessage="Invalid" MaximumValue="1000000" MinimumValue="1"
                                                                                    Type="Double" ValidationGroup="ItemAdd"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Line Total <span style="color: red">*</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="TextBoxLineTotal" runat="server" CssClass="textbox"
                                                                                    MaxLength="20" Width="100px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="LabelItemError" runat="server" Style="font-weight: 700; color: #FF0000"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td class="pageButtonArea">
                                                                                <asp:Button ID="ButtonItemAdd" runat="server" CssClass="button" OnClick="ButtonItemAdd_Click"
                                                                                    Text="Add" ValidationGroup="ItemAdd" Width="70px" />
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <div style="padding: 10px;">
                                                        <asp:UpdatePanel ID="UpdatePanelSummary" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <table border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 15%;">Total Amount 
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxTotalAmount" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Width="100px" ReadOnly="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="vertical-align: text-top; padding-top: 3px">Remarks
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="TextBoxRemarks" runat="server" CssClass="textbox" MaxLength="20"
                                                                                Rows="4" TextMode="MultiLine" Width="450px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td class="pageButtonArea">
                                                            <asp:Button ID="ButtonSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="submit"
                                                                OnClick="ButtonSubmit_Click" Width="70px" />
                                                            &nbsp;<asp:Button ID="ButtonReset" CssClass="button" runat="server" Text="Reset" CausesValidation="False"
                                                                OnClick="ButtonReset_Click" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelSearchDetails" Visible="false" Width="100%" runat="server">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">Search Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td style="width: 15%">PO Number
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchPONumber" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="150px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>PO Date 
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchPODate" runat="server" CssClass="textbox" MaxLength="20"
                                                                        Width="100px"></asp:TextBox>
                                                                    &nbsp;[dd/mm/yyyy]<asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="TextBoxSrchPODate"
                                                                        Display="Dynamic" ErrorMessage="Invalid" MaximumValue="31/12/3000" MinimumValue="01/01/1900"
                                                                        Type="Date" ValidationGroup="search"></asp:RangeValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Supplier
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxSrchSupplier" runat="server" CssClass="textbox" MaxLength="400" Width="400px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="pageButtonArea">
                                                                    <asp:Button ID="ButtonSearch" CssClass="button" runat="server" Text="Search" OnClick="ButtonSearch_Click"
                                                                        Width="70px" ValidationGroup="search" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <asp:GridView ID="GridViewSearchList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="grid-stcMain" DataKeyNames="ID" OnRowCommand="GridViewSearchList_RowCommand"
                                                    OnRowDataBound="GridViewSearchList_RowDataBound" Width="100%" AllowPaging="True"
                                                    OnPageIndexChanging="GridViewSearchList_PageIndexChanging">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <RowStyle CssClass="grid-row-normal-stc" />
                                                    <Columns>
                                                        <asp:ButtonField CommandName="EditRecord" Text="Edit">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="DeleteRecord" Text="Delete">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:ButtonField CommandName="ViewRecord" Text="View">
                                                            <ItemStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                Width="20px" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField HeaderText="PO Number">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="PO Date">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Supplier">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Total Amount">
                                                            <HeaderStyle CssClass="display-cell" HorizontalAlign="Left" VerticalAlign="Middle"
                                                                Wrap="False" />
                                                            <ItemStyle CssClass="display-cell" VerticalAlign="Middle" Wrap="False" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="grid-pager" />
                                                    <EmptyDataTemplate>
                                                        <div id="norec" class="no-record-msg">
                                                            <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding-left: 15px">No Record(s) Found!
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="grid-header" />
                                                    <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelView" Width="100%" runat="server" Visible="false">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td class="pageDataArea">
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Details</legend>
                                                    <div style="padding: 10px;">
                                                        <table border="0" style="width: 100%">
                                                            <tr class="view-back">
                                                                <td style="width: 20%;">PO Number
                                                                </td>
                                                                <td style="width: 1%; text-align: center;">:
                                                                </td>
                                                                <td style="width: 79%;">
                                                                    <asp:Label ID="LabelViewPONumber" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>PO Date
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewPODate" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Supplier
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewSupplier" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Amount
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewTotalAmount" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr class="view-back">
                                                                <td>Remarks
                                                                </td>
                                                                <td style="text-align: center;">:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelViewRemarks" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                                <br />
                                                <fieldset>
                                                    <legend style="font-weight: bold">PO Item(s)</legend>
                                                    <div style="padding: 10px;">
                                                        <table style="width: 100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="GridViewItemView" runat="server" Width="100%" CssClass="grid-stcMain"
                                                                            OnRowDataBound="GridViewItemView_RowDataBound" AutoGenerateColumns="False">
                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                            <RowStyle CssClass="grid-row-normal-stc" />
                                                                            <Columns>
                                                                                <asp:BoundField HeaderText="Product Item">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Unit Price">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Quantity">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField HeaderText="Line Total">
                                                                                    <HeaderStyle CssClass="display-cell" HorizontalAlign="Center" VerticalAlign="Middle"
                                                                                        Wrap="False" />
                                                                                    <ItemStyle CssClass="display-cell" VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <PagerStyle CssClass="grid-pager" />
                                                                            <EmptyDataTemplate>
                                                                                <div id="norec" class="no-record-msg">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" height="36" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding-left: 15px">No Items(s) Found!
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </EmptyDataTemplate>
                                                                            <HeaderStyle CssClass="grid-header" />
                                                                            <AlternatingRowStyle CssClass="grid-row-alt-stc" />
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotAllowedPage" Visible="false" runat="server" Width="100%">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td class="pageBackground" style="padding-right: 15px; padding-left: 15px; padding-bottom: 15px; padding-top: 15px"
                    align="right">
                    <img src="../../../images/AccessDenied.jpg" />
                </td>
                <caption>
                    <span style="position: absolute; top: 185px; left: 250px; font-size: 30px; font-weight: bold; color: #b01002;">You are not allowed to view this page. </span><span style="position: absolute; top: 230px; left: 250px; font-size: 24px; font-weight: bold; color: #636363;">Please
       contact your system administrator. </span>
                </caption>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript">
        $("#ctl00_ContentPlaceHolder1_TextBoxPODate").mask("99/99/9999");
        $("#ctl00_ContentPlaceHolder1_TextBoxSrchPODate").mask("99/99/9999");
    </script>
</asp:Content>
